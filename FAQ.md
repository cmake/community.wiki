

<!-- toc -->

- [General information and availability](#general-information-and-availability)
  * [What is CMake?](#what-is-cmake)
  * [What is its license?](#what-is-its-license)
  * [What is the current release?](#what-is-the-current-release)
  * [I found a Bug\! What should I do?](#i-found-a-bug-what-should-i-do)
  * [I want a new feature in CMake. What should I do?](#i-want-a-new-feature-in-cmake-what-should-i-do)
  * [What is the most recent version covered by the Mastering CMake book?](#what-is-the-most-recent-version-covered-by-the-mastering-cmake-book)
  * [Where can I find searchable CMake Mailing Archives?](#where-can-i-find-searchable-cmake-mailing-archives)
- [Running CMake](#running-cmake)
  * [Is there an option to produce more 'verbose' compiling?](#is-there-an-option-to-produce-more-verbose-compiling)
  * [Is there a way to skip checking of dependent libraries when compiling?](#is-there-a-way-to-skip-checking-of-dependent-libraries-when-compiling)
  * [I set a cmake variable in my environment, but it didn't change anything. Why?](#i-set-a-cmake-variable-in-my-environment-but-it-didnt-change-anything-why)
  * [How do I use a different compiler?](#how-do-i-use-a-different-compiler)
    + [Method 1: use environment variables](#method-1-use-environment-variables)
    + [Method 2: use cmake -D](#method-2-use-cmake-d)
    + [Method 3 (avoid): use set()](#method-3-avoid-use-set)
  * [I change CMAKE_C_COMPILER in the GUI but it changes back on the next configure step. Why?](#i-change-cmake_c_compiler-in-the-gui-but-it-changes-back-on-the-next-configure-step-why)
  * [In CCMake, typing full paths is tedious. Is there a better way?](#in-ccmake-typing-full-paths-is-tedious-is-there-a-better-way)
- [Out-of-source build trees](#out-of-source-build-trees)
  * [What is an "out-of-source" build?](#what-is-an-out-of-source-build)
  * [I run an out-of-source build but CMake generates in-source anyway. Why?](#i-run-an-out-of-source-build-but-cmake-generates-in-source-anyway-why)
  * [Why does CMake use full paths, or can I copy my build tree?](#why-does-cmake-use-full-paths-or-can-i-copy-my-build-tree)
  * [CMake does not generate a "make distclean" target. Why?](#cmake-does-not-generate-a-make-distclean-target-why)
  * [Running "make clean" does not remove custom command outputs. Why?](#running-make-clean-does-not-remove-custom-command-outputs-why)
- [Writing CMakeLists.txt](#writing-cmakeliststxt)
  * [How to have backward and forward compatibility?](#how-to-have-backward-and-forward-compatibility)
  * [How do I get the current source or binary directory?](#how-do-i-get-the-current-source-or-binary-directory)
  * [Why are my CMake variables not updated in the GUI after a SET command?](#why-are-my-cmake-variables-not-updated-in-the-gui-after-a-set-command)
  * [How can I change the default build mode and see it reflected in the GUI?](#how-can-i-change-the-default-build-mode-and-see-it-reflected-in-the-gui)
  * [How do I generate an executable, then use the executable to generate a file?](#how-do-i-generate-an-executable-then-use-the-executable-to-generate-a-file)
  * [How can I generate a source file during the build?](#how-can-i-generate-a-source-file-during-the-build)
  * [How can I add a dependency to a source file which is generated in a subdirectory?](#how-can-i-add-a-dependency-to-a-source-file-which-is-generated-in-a-subdirectory)
  * [How can I generate a file used in more than one target in the same directory?](#how-can-i-generate-a-file-used-in-more-than-one-target-in-the-same-directory)
  * [I use exec_program but the result is not set in subdirectories. Why?](#i-use-exec_program-but-the-result-is-not-set-in-subdirectories-why)
  * [How can I get or set environment variables?](#how-can-i-get-or-set-environment-variables)
  * [Why do I have unwanted semicolons ; in my compiler flags?](#why-do-i-have-unwanted-semicolons-in-my-compiler-flags)
  * [How can I get quoting and escapes to work properly?](#how-can-i-get-quoting-and-escapes-to-work-properly)
  * [Isn't the "Expression" in the "else(Expression)" confusing?](#isnt-the-expression-in-the-else-expression-confusing)
  * [Which regular expressions are supported by CMake?](#which-regular-expressions-are-supported-by-cmake)
  * [How to convert a semicolon separated list to a whitespace separated string?](#how-to-convert-a-semicolon-separated-list-to-a-whitespace-separated-string)
  * [How can I build multiple modes without switching ?](#how-can-i-build-multiple-modes-without-switching-)
  * [How can I specify my own configurations (for generators that allow it) ?](#how-can-i-specify-my-own-configurations-for-generators-that-allow-it-)
  * [How can I extend the build modes with a custom made one ?](#how-can-i-extend-the-build-modes-with-a-custom-made-one-)
  * [Why does `foreach` skip empty values?](#why-does-foreach-skip-empty-values)
  * [Does CMake support precompiled headers?](#does-cmake-support-precompiled-headers)
- [Writing FindXXX.cmake files](#writing-findxxxcmake-files)
  * [What are the rules to write a FindXXX.cmake file?](#what-are-the-rules-to-write-a-findxxxcmake-file)
  * [Why does find_library look in system directories before its PATHS option?](#why-does-find_library-look-in-system-directories-before-its-paths-option)
- [Finding and using external packages](#finding-and-using-external-packages)
  * [How do I use CMake to generate SWIG wrapper libraries?](#how-do-i-use-cmake-to-generate-swig-wrapper-libraries)
  * [How do I use CMake to build LaTeX documents?](#how-do-i-use-cmake-to-build-latex-documents)
  * [How do I get LaTeX references to be correct?](#how-do-i-get-latex-references-to-be-correct)
  * [How can I set TEXINPUTS for a LaTeX compilation?](#how-can-i-set-texinputs-for-a-latex-compilation)
- [Library questions](#library-questions)
  * [Can I build both shared and static libraries with one ADD_LIBRARY command?](#can-i-build-both-shared-and-static-libraries-with-one-add_library-command)
  * [Does that mean I have to build all my library objects twice, once for shared and once for static?\! I don't like that\!](#does-that-mean-i-have-to-build-all-my-library-objects-twice-once-for-shared-and-once-for-static-i-dont-like-that)
  * [How do I make my shared and static libraries have the same root name, but different suffixes?](#how-do-i-make-my-shared-and-static-libraries-have-the-same-root-name-but-different-suffixes)
  * [How do I rename a library after it has already been built?](#how-do-i-rename-a-library-after-it-has-already-been-built)
  * [Does CMake support "convenience" libraries?](#does-cmake-support-convenience-libraries)
  * [Why are libraries linked to my shared library included when something links to it?](#why-are-libraries-linked-to-my-shared-library-included-when-something-links-to-it)
  * [CMake dependency scanner](#cmake-dependency-scanner)
- [Installation questions](#installation-questions)
  * [Does CMake's "make install" support DESTDIR?](#does-cmakes-make-install-support-destdir)
  * [Can I do "make uninstall" with CMake?](#can-i-do-make-uninstall-with-cmake)
- [Distribution questions](#distribution-questions)
  * [Where is "make dist"?](#where-is-make-dist)
  * [What is the best way to distribute source code or binaries for a cmake-based project?](#what-is-the-best-way-to-distribute-source-code-or-binaries-for-a-cmake-based-project)
- [Platform-specific questions](#platform-specific-questions)
  * [How do I build universal binaries on Mac OS X?](#how-do-i-build-universal-binaries-on-mac-os-x)
  * [How can I apply resources on Mac OS X automatically?](#how-can-i-apply-resources-on-mac-os-x-automatically)
  * [Why does find_library not find .DLL libraries under WIN32?](#why-does-find_library-not-find-dll-libraries-under-win32)
  * [Why am I getting a linker error to _mainCRTStartup under WIN32?](#why-am-i-getting-a-linker-error-to-_maincrtstartup-under-win32)
  * [Why do I get this error: nafxcwd.lib(appcore.obj) : error LNK2001: unresolved external symbol ___argv](#why-do-i-get-this-error-nafxcwdlibappcoreobj-error-lnk2001-unresolved-external-symbol-___argv)
  * [How to use MFC with CMake](#how-to-use-mfc-with-cmake)
  * [How To Put Files in Folders in Visual Studio Projects](#how-to-put-files-in-folders-in-visual-studio-projects)
  * [How to create Visual Studio 6 Projects that contain only a single build type](#how-to-create-visual-studio-6-projects-that-contain-only-a-single-build-type)
  * [Can CMake set the Debugging/Working Directory property in Visual Studio projects?](#can-cmake-set-the-debuggingworking-directory-property-in-visual-studio-projects)
  * [Why does CMakeSetup with the message "LINK : fatal error LNK1104: cannot open file 'user32.lib'" while configuring a project?](#why-does-cmakesetup-with-the-message-link-fatal-error-lnk1104-cannot-open-file-user32lib-while-configuring-a-project)
  * [How can I avoid the error "Arg list too long" when running make?](#how-can-i-avoid-the-error-arg-list-too-long-when-running-make)
  * [How can I find out platforms definitions, search paths, etc. from gcc ?](#how-can-i-find-out-platforms-definitions-search-paths-etc-from-gcc-)
  * [How can I get a windows registry key ?](#how-can-i-get-a-windows-registry-key-)
  * [How can I build my MSVC application with a static runtime?](#how-can-i-build-my-msvc-application-with-a-static-runtime)
    + [Manual Replace](#manual-replace)
    + [Make Override Files](#make-override-files)
    + [Override CMAKE_MSVC_RUNTIME_LIBRARY](#override-cmake_msvc_runtime_library)
  * [Why do generated Xcode projects have a CMake PostBuild Rules phase?](#why-do-generated-xcode-projects-have-a-cmake-postbuild-rules-phase)
  * [Why does CMake not find my Xcode compiler on OS X with the Unix Makefiles generator?](#why-does-cmake-not-find-my-xcode-compiler-on-os-x-with-the-unix-makefiles-generator)
- [Other Questions](#other-questions)
  * [Why does CMake generate recursive Makefiles?](#why-does-cmake-generate-recursive-makefiles)
  * [Why can't I make multiple targets on the command line in parallel?](#why-cant-i-make-multiple-targets-on-the-command-line-in-parallel)

<!-- tocstop -->

## General information and availability

### What is CMake?

CMake is a cross-platform, open-source make system. CMake is used to
control the software compilation process using simple
platform-independent and compiler-independent configuration files. CMake
generates native makefiles and workspaces that can be used in the
compiler environment of your choice. CMake is quite sophisticated: it is
possible to support complex environments requiring system configuration,
preprocessor generation, code generation, and template instantiation.
Please go to <https://cmake.org/overview> to learn more about CMake.

### What is its license?

CMake is distributed under the OSI-approved BSD 3-clause License.
See the [licensing page](https://cmake.org/licensing/) for details.

The snippets on this wiki are provided under the same license.

### What is the current release?

The latest release of CMake is always available at:
<https://cmake.org/download>

From there, you can fetch CMake binaries for Windows or several Unix
variants, or you can download the source code of CMake.

You can also access nightly development through Git; see
<https://cmake.org/download> for more information. You may also
browse the [git repository online](https://gitlab.kitware.com/cmake/cmake).

### I found a Bug\! What should I do?

If you have a patch to contribute, please read
[CONTRIBUTING.rst](https://gitlab.kitware.com/cmake/cmake/-/blob/master/CONTRIBUTING.rst)
at the top of the CMake source tree.

Otherwise, please report the bug in our bug tracker:
<https://gitlab.kitware.com/cmake/cmake/-/issues>

Please make sure to look at the old bugs not to include duplicates,
include detailed instructions of the bug and how to reproduce it.

### I want a new feature in CMake. What should I do?

Report a feature request in our Bug tracker <https://gitlab.kitware.com/cmake/cmake/-/issues>

Please make sure to look at the old feature requests not to include
duplicates, include detailed instructions of the feature and proposed
implementation.

### What is the most recent version covered by the Mastering CMake book?

A new edition of the [Mastering
CMake](http://www.kitware.com/products/cmakebook.html) book has been
released which documents CMake 2.6.

The following features have been added since printing the book:

  - New `install` command (`cmake --help-command install`)
  - New `list` command (`cmake --help-command list`)
  - Updated `find_path`, `find_program`, and `find_file` commands to be
    more powerful (`cmake --help-command find_path`)
  - RPATH and Mac OS X install_name support (`cmake --help-command set_target_properties`)
  - CPack Beta (not finished or documented)
  - `execute_process` was added and replaces `exec_program`
  - Other changes have been bug fixes and internal CMake restructuring

### Where can I find searchable CMake Mailing Archives?

The original mailman-based mailing lists are now longer used.
They have been superseded by the [CMake Forums](https://discourse.cmake.org/), which provide their own search functionality.

There exist at least the following searchable archives of the original mailing lists:

*   ~~[cmake on Gmane](http://dir.gmane.org/gmane.comp.programming.tools.cmake.user)~~
*   <http://www.mail-archive.com/cmake@cmake.org/>
*   <http://marc.info/?l=cmake>
*   Use google *site* keyword in order to search directly in the CMake browsable ML:

        site:cmake.org/pipermail/cmake <search terms>

## Running CMake

### Is there an option to produce more 'verbose' compiling?

On Makefile generators, you can set the Makefile variable `VERBOSE` to 1.
For example on UNIX:
```shell
make VERBOSE=1
```

You can also set `CMAKE_VERBOSE_MAKEFILE` to ON.

On Windows (nmake) you can override `CMAKE_VERBOSE_MAKEFILE` by using
```
nmake /S
```

On Unix make you can mostly override verbose mode by using
```shell
make VERBOSE=""
```

If you are on Windows using Borland or NMake Makefiles, you will see
lines like:
```
cl @c:\DOCUME~1\ANDY~1.KIT\LOCALS~1\Temp\nma03504
```

The reason for this is that Borland and Microsoft Visual Studio make
programs have limitation on the length of command strings. They overcome
this limitation by writing arguments to the file and then pass file to
the program.

If you actually want to see what the command looks like, set
`CMAKE_START_TEMP_FILE` and `CMAKE_END_TEMP_FILE` to `""` -- be warned,
however, you cannot set these as variables on the CMake command line
with `-D`. Instead, see the very bottom of the file
`Modules/Platform/Windows.cmake` and uncomment the lines that set these
variables to the empty string.

### Is there a way to skip checking of dependent libraries when compiling?

**Using the Makefile Generator**

When using the Makefile generator under \*nix you can append "/fast" to
your target name. For example:
```shell
make target_name/fast
```

Under Windows use a backslash instead:
```
make target_name\fast
```

Be aware that this can cause link errors if the targets that were
skipped are not actually built. Use it only if you know what you're
doing!

**Using Visual Studio \>= 7.1**

If you have Visual Studio .NET 7.1 or greater you can use the native
option to right click on a project and choose to build just that project.

**Using Visual Studio \<= 7.0**

CMake doesn't try to compile all dependent libraries when you compile a
library but it will do so for binary targets. You can't avoid this
however you can take advantage of CTRL+F7 to manually compile a source
file for the affected target and then relink the target by right
clicking on it and choosing Link. You'll have to ensure that all
dependent libraries are made up-to-date however or suffer through
Visual's slow check.

### I set a cmake variable in my environment, but it didn't change anything. Why?

CMake build settings are stored in the CMake cache corresponding to a
project's build tree. They are called CMake "cache entries" and have no
relation to your command shell's environment variables. Use a CMake GUI
(CMakeSetup on Windows or ccmake on UNIX) or the wizard mode (cmake -i)
to edit cache entries. Initial values may also be specified for a build
by using the -D command line argument to cmake when it is first run to
produce a new build tree.

### How do I use a different compiler?

#### Method 1: use environment variables

For C and C++, set the `CC` and `CXX` environment variables. This method
is not guaranteed to work for all generators. (Specifically, if you are
trying to set Xcode's `GCC_VERSION`, this method confuses Xcode.)

For example:
```shell
CC=gcc-4.2 CXX=/usr/bin/g++-4.2 cmake -G "Your Generator" path/to/your/source
```

#### Method 2: use cmake -D

Set the appropriate `CMAKE_FOO_COMPILER` variable(s) to a valid compiler
name or full path on the command-line using `cmake -D`.

For example:
```shell
cmake -G "Your Generator" -D CMAKE_C_COMPILER=gcc-4.2 -D CMAKE_CXX_COMPILER=g++-4.2 path/to/your/source
```

#### Method 3 (avoid): use set()

Set the appropriate `CMAKE_FOO_COMPILER` variable(s) to a valid compiler
name or full path in a list file using `set()`. This must be done
*before* any language is set(ie before any `project()` or
`enable_language()` command).

For example:
```cmake
set(CMAKE_C_COMPILER "gcc-4.2")
set(CMAKE_CXX_COMPILER "/usr/bin/g++-4.2")

project("YourProjectName")
```

### I change CMAKE_C_COMPILER in the GUI but it changes back on the next configure step. Why?

Once a build tree is created with a given compiler it cannot be changed.
There are a variety of implementation reasons for this policy.

### In CCMake, typing full paths is tedious. Is there a better way?

Since CMake 1.6, you can use tab completion in the path entries in
CCMake. All you do is type first couple of characters and press <TAB>
key. CCMake will examine the current typed path and try to expand it to
some existing path. If that is possible, it will do it. If not, it will
not do anything.

For example:
```
/usr/loc<TAB>
```
will expand to
```
/usr/local/
```

## Out-of-source build trees

### What is an "out-of-source" build?

When your build generates files, they have to go somewhere. An in-source
build puts them in your source tree. An out-of-source build puts them in
a completely separate directory, so that your source tree is unchanged.

In the first example, an in-place build is performed, i.e., the binaries
are placed in the same directory as the source code.

```shell
cd Hello
ccmake .
make
```

In the second example, an out-of-place build is performed, i.e., the
source code, libraries, and executables are produced in a directory
separate from the source code directory(ies).

```shell
mkdir HelloBuild
cd HelloBuild
ccmake ../Hello
make
```

Out-of-source builds are recommended, as you can build multiple variants
in separate directories, e.g., HelloBuildDebug, HelloBuildRelease.

Note: Before performing an out-of-source build, ensure that all CMake
generated in-source build information is removed from the source
directory, e.g., CMakeFiles directory,
CMakeCache.txt.

### I run an out-of-source build but CMake generates in-source anyway. Why?

This means that there is a CMakeCache.txt file in the source tree,
possibly as part of an existing in-source build. If CMake is given the
path to a directory with a CMakeCache.txt file, it assumes the directory
is a build tree. Therefore if one runs `cmake ../mysrc` to build
out-of-source but there is a mysrc/CMakeCache.txt file then cmake will
treat mysrc as the build tree.

This is a side-effect of the feature that allows `cmake .` to be used to
regenerate a build tree. The behavior will not be changed because mixing
in-source and out-of-source builds is not safe anyway (configured
headers may be found in the wrong place).

### Why does CMake use full paths, or can I copy my build tree?

CMake uses full paths because:

1.  configured header files may have full paths in them, and moving
    those files without re-configuring would cause upredictable
    behavior.
2.  because cmake supports out of source builds, if custom commands used
    relative paths to the source tree, they would not work when they are
    run in the build tree because the current directory would be
    incorrect.
3.  on Unix systems rpaths might be built into executables so they can
    find shared libraries at run time. If the build tree is moved old
    executables may use the old shared libraries, and not the new ones.

Can the build tree be copied or moved?

The short answer is NO. The reason is because full paths are used in
CMake, see above. The main problem is that cmake would need to detect
when the binary tree has been moved and rerun. Often when people want to
move a binary tree it is so that they can distribute it to other users
who may not have cmake in which case this would not work even if cmake
would detect the move.

The workaround is to create a new build tree without copying or moving
the old one.

### CMake does not generate a "make distclean" target. Why?

Some build trees created with GNU autotools have a `make distclean`
target that cleans the build and also removes Makefiles and other parts
of the generated build system. CMake does not generate a `make distclean`
target because CMakeLists.txt files can run scripts and
arbitrary commands; CMake has no way of tracking exactly which files are
generated as part of running CMake. Providing a distclean target would
give users the false impression that it would work as expected. (CMake
does generate a `make clean` target to remove files generated by the
compiler and linker.)

A `make distclean` target is only necessary if the user performs an
in-source build. CMake supports in-source builds, but we strongly
encourage users to adopt the notion of an out-of-source build. Using a
build tree that is separate from the source tree will prevent CMake from
generating any files in the source tree. Because CMake does not change
the source tree, there is no need for a distclean target. One can start
a fresh build by deleting the build tree or creating a separate build
tree.

(If a CMakeLists.txt uses `add_custom_command` to generate source files
in the source tree, not the build tree, then in CMake 2.2 or higher
`make clean` will remove them. See next question.)

### Running "make clean" does not remove custom command outputs. Why?

In CMake 2.2 and higher custom command outputs should be removed by make
clean. Make sure you are using at least this version. Prior to CMake 2.2
custom command outputs were not automatically added to the list of files
to clean. In CMake 2.0 the developer can specify a list of files to be
deleted. This can be done using `set_directory_properties` setting
property `ADDITIONAL_MAKE_CLEAN_FILES` to the list of files.

We however strongly recommend using an "out-of-source" build which never
writes any files to the source tree. Using a separate source and build
tree greatly reduces the need for `make clean` and `make distclean`
targets to clean away files that differ between builds.

## Writing CMakeLists.txt

### How to have backward and forward compatibility?

As of CMake 2.6 we employ a "Policy" mechanism to provide backwards
compatibility. The basic requirement for projects is to include one line
at the top of the highest CMakeLists.txt file:
```cmake
cmake_minimum_required(VERSION 2.6) # or other version
```
This tells versions of CMake older than that specified that they are too
old to build the project. They will report this information to the user.
It also tells versions of CMake newer than that specified that the
project may not be aware of policies introduced in later versions, which
enables additional compatibility. For futher documentation, see

  - [cmake-policies(7) manual](https://cmake.org/cmake/help/latest/manual/cmake-policies.7.html)
  - [cmake_policy()
    command](https://cmake.org/cmake/help/latest/command/cmake_policy.html)
  - [CMake Policy Mechanism](doc/cmake/Policies "wikilink")

### How do I get the current source or binary directory?

The variable `CMAKE_CURRENT_SOURCE_DIR` contains the absolute path to
your current source directory, while `CMAKE_CURRENT_BINARY_DIR` points
to the equivalent binary directory.

### Why are my CMake variables not updated in the GUI after a SET command?

The cache variables listed in the GUI when you press "Configure" are
used to initialize the values seen by the code in CMakeLists.txt files.

Changes made by the code are used during the configure step and seen by
the generators but are not stored back into the cache. For example:
```cmake
set(BUILD_SHARED_LIBS ON)
```
will turn on building of shared libraries for the directory containing
the command and all subdirectories, but the change will not appear in
the GUI.

You can use the CACHE and FORCE options on the `set` command to change
variables in a way that will be reflected in the GUI. Run
```shell
cmake --help-command set
```
to see full instructions for the command.

### How can I change the default build mode and see it reflected in the GUI?

Adapt the following commands in your CMakeLists.txt (this example sets
the Release With Debug Information mode):
```cmake
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
      FORCE)
endif()
```

### How do I generate an executable, then use the executable to generate a file?

Create the generator executable by just adding a target:
```cmake
add_executable(generate generate.c)
```

The rest of the process is simpler in CMake 2.6 and above than in
previous versions.

Use `add_custom_command` to specify a custom build rule for the file.
(In this example we assume `generate` accepts the input and output files
as arguments.)
```cmake
add_custom_command(
  OUTPUT someoutput.txt
  COMMAND generate ${CMAKE_CURRENT_SOURCE_DIR}/someinput.txt ${CMAKE_CURRENT_BINARY_DIR}/someoutput.txt
  DEPENDS generate ${CMAKE_CURRENT_SOURCE_DIR}/someinput.txt
)
```

This tells CMake how to build the file but does not actually add a rule
to the build system. Another target must require it. One may create a
custom target explicitly for this rule:
```cmake
add_custom_target(driver ALL DEPENDS someoutput.txt)
```
or the file may be added as part of some other target:
```cmake
add_executable(product product.c someoutput.txt)
```

In CMake 2.4 and below the `generate` target may
not be specified directly in the `COMMAND` option of
`add_custom_command` (but it can still be used in the `DEPENDS` option
as of CMake 2.4). Instead use `get_target_property` to obtain the
location of the generated executable. Additionally, the output must
always be specified by full path.
```cmake
get_target_property(GENERATE_EXE generate LOCATION)
add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/someoutput.txt
  COMMAND ${GENERATE_EXE} ${CMAKE_CURRENT_SOURCE_DIR}/someinput.txt ${CMAKE_CURRENT_BINARY_DIR}/someoutput.txt
  DEPENDS generate
)
```

### How can I generate a source file during the build?

The `add_custom_command` command lets you generate a source file that
you can then include in another target. For example:
```cmake
add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/foo.c
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/bar.c ${CMAKE_CURRENT_BINARY_DIR}/foo.c
  DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/bar.c
)
add_executable(foo foo.c)
```

This will create an executable by copying "bar.c" to "foo.c" and then
compiling "foo.c" to produce "foo". CMake allows you to put generated source
files in the current source or binary directory, so we were careful to
output foo.c to the current binary directory. When we add foo.c to foo,
CMake will look in either directory for it. Even if foo.c does not yet
exist, CMake is smart enough to notice that a custom command creates it.
(For the file named as the OUTPUT, CMake has its GENERATED source file
property set to true.)

You can also use `add_custom_command` when the [generator command is
another executable in the same
project](FAQ#how-do-i-generate-an-executable-then-use-the-executable-to-generate-a-file "wikilink").

Sometimes, the program doing the generation may generate multiple output
files that each need to be part of the build. CMake 2.4 or higher
supports having multiple files listed in the OUTPUT section. For
example, suppose you had a program that read input.txt and generated
three files output1.cpp, output2.h, and output3.cpp, and that those
three files needed to be compiled into an executable program. The cmake
list file for that would look like this:
```cmake
project(FOO)
# make sure cmake addes the binary directory for the project to the include path
include_directories(${FOO_BINARY_DIR})
# add the executable that will do the generation
add_executable(my_generator my_generator.cxx)
get_target_property(MY_GENERATOR_EXE my_generator LOCATION)
# add the custom command that will generate all three files
add_custom_command(
  OUTPUT ${FOO_BINARY_DIR}/output1.cpp ${FOO_BINARY_DIR}/output2.h ${FOO_BINARY_DIR}/output3.cpp
  COMMAND ${MY_GENERATOR_EXE} ${FOO_BINARY_DIR} ${FOO_SOURCE_DIR}/input.txt
  DEPENDS my_generator
  MAIN_DEPENDENCY ${FOO_SOURCE_DIR}/input.txt
)
# now create an executable using the generated files
add_executable(generated
               ${FOO_BINARY_DIR}/output1.cpp
               ${FOO_BINARY_DIR}/output2.h
               ${FOO_BINARY_DIR}/output3.cpp)
```

CMake 2.4 allows you to generate a header file. Because generated
headers often cause unnecessary rebuilds, you should try to avoid them;
consider using the `configure_file` command to prepare the header at
CMake time. If you must generate a header file, use code like this:
```cmake
add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/foo.h
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/bar.h ${CMAKE_CURRENT_BINARY_DIR}/foo.h
  DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/bar.h
)
add_executable(foo foo.c ${CMAKE_CURRENT_BINARY_DIR}/foo.h)
```

This is like the first example above, except that it generates a header
instead of a C file. The header might not exist when the build system
scans dependencies for "foo.c", so there is no way for CMake to know that
this target requires "foo.h" unless we can tell it that "foo.h" may exist in
the future. We give CMake this knowledge by listing the generated header
file in the set of source files for the target. (This requires CMake
2.4. Previous versions of CMake required use of the `OBJECT_DEPENDS`
source file property.)

### How can I add a dependency to a source file which is generated in a subdirectory?

Rules created with `add_custom_command` as
[above](FAQ#how-can-i-generate-a-source-file-during-the-build "wikilink")
have scope only in the directory in which they are specified. If the
generated file is needed in another directory, a target-level dependency
needs to be added. Create a target in the subdirectory with the custom
rule in order to drive it:

```cmake
# subdir/CMakeLists.txt
add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/foo.c
  COMMAND ${CMAKE_COMMAND} copy ${CMAKE_CURRENT_SOURCE_DIR}/bar.c ${CMAKE_CURRENT_BINARY_DIR}/foo.c
  DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/bar.c
)
add_custom_target(generate_foo DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/foo.c)
```
Now other targets can depend on the target from the subdirectory:
```cmake
# CMakeLists.txt
add_subdirectory(subdir)
# Create the executable.
add_executable(generated ${CMAKE_CURRENT_BINARY_DIR}/subdir/foo.c)
# Tell CMake the source won't be available until build time.
set_source_files_properties(${CMAKE_CURRENT_BINARY_DIR}/subdir/foo.c PROPERTIES GENERATED 1)
# Make sure the source is generated before the executable builds.
add_dependencies(generated generate_foo)
```

### How can I generate a file used in more than one target in the same directory?

CMake will generate a copy of a custom command in the build rules for
every target that lists the custom command output as a source file.
There is no other place to put it. If a single custom command output is
listed in more than one target and the targets have no dependency
relationship they will race to run the custom command during a parallel
build and possibly clobber each other. For example:
```cmake
add_custom_command(OUTPUT Foo.c COMMAND ... DEPENDS ...)
add_library(FooStatic STATIC Foo.c)
add_library(FooShared SHARED Foo.c)
```

There are at least two solutions to this problem. First, one may use
add_dependencies to serialize the targets involved:
```cmake
add_dependencies(FooShared FooStatic)
```

Second, one may add a custom target dedicated to generating files for
the other targets:
```cmake
add_custom_target(Foo DEPENDS Foo.c)
add_dependencies(FooStatic Foo)
add_dependencies(FooShared Foo)
```

The latter solution ensures that generated files are up to date before
any of the independent targets build so none of them will run the custom
command and there will be no race.

### I use exec_program but the result is not set in subdirectories. Why?

An unfortunate holdover from ancient CMake versions is that certain
commands are "inherited" into subdirectories and others are not.
`exec_program` is not inherited. What this means is that when the
listfile code from a parent directory executes in a subdirectory the
`exec_program` command is left out. Therefore the code executes
differently. This problem was fixed in CMake 2.2, but for older versions
you will have to cache the result:
```cmake
exec_program(my-program OUTPUT_VARIABLE MY_OUTPUT)
set(MY_OUTPUT "${MY_OUTPUT}" CACHE INTERNAL "")
```

This will store the result in a global location so it will be available
in the subdirectory. Be sure to choose a descriptive name for `MY_OUTPUT`
to avoid conflict in the global setting.

### How can I get or set environment variables?

CMake names environment variables using an ENV prefix and surrounding
the names in curly braces. Here is an example:
```cmake
message("$ENV{PATH}")
```

Reading variables will work in any version of CMake. Writing to them
works in CMake 2.2 and higher using the following syntax:
```cmake
set(ENV{HELLO} "World")
```

Note that there is currently no way to tell apart an empty environment
variable value from a variable that is not set at all.

One should avoid using environment variables for controlling the flow of
CMake code (such as in `if` commands). The build system generated by CMake
may re-run CMake automatically when CMakeLists.txt files change. The
environment in which this is executed is controlled by the build system
and may not match that in which CMake was originally run. If you want to
control build settings on the CMake command line, you need to use cache
variables set with the -D option. The settings will be saved in
CMakeCache.txt so that they don't have to be repeated every time CMake
is run on the same build tree.

Also, environment variables `set` in the CMakeLists.txt *only* take effect
for cmake itself (*configure-time*), so you cannot use this method to
set an environment variable that a custom command might need
(*build-time*). Barring environment variable support by various CMake
commands (e.g. add_custom_command(), currently not supported yet), an
acceptable workaround may be to invoke shell scripts instead which wrap
the commands to be executed.

### Why do I have unwanted semicolons ; in my compiler flags?

CMake has a list data type. A list is stored as a string of
semicolon-separated list elements. Whitespace separated arguments to a
`set` statement are interpreted as list elements. For instance,

```cmake
set(var a b c d e)
```

will give `var` a value of `a;b;c;d;e` and this list can be used
when invoking other CMake commands. However, if you then pass `${var}` directly
in a context that is expecting a command-line string fragment:

```cmake
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${var}")
```

you are passing `a;b;c;d;e` which is not what you want.

See the [cmake-language(7)](https://cmake.org/cmake/help/latest/manual/cmake-language.7.html) manual for details on `;`-separated lists and how they are evaluated in unquoted and quoted strings.

### How can I get quoting and escapes to work properly?

If you want to escape a character in CMake, you use `\`, like in C
code. For example, if you wanted to have a quote embedded in a string
you would do this: `\"`. However, each level of CMake that processes
your code will need one level of escaping to work. So, if you configure
a file that is read by cmake or cpack and you want to do the same thing,
you would do `\\\"`. You would still need to escape the `"` for the
first cmake that processes the string. However, this time, you would
want to also escape a `\` as well. This would leave the next level of
processing with `\"`. Also, for custom commands that may get passed to
a shell, it maybe required to do escaping for that shell.

### Isn't the "Expression" in the "else(Expression)" confusing?

Traditional CMakeLists.txt files prior to 2.6.0, require the following
syntax. In the `if` syntax, the `else` section requires the same
(Expression) as the `if` section. This sometimes can make the script kind
of hard to follow, take the short example below:

```cmake
if(WIN32)
  ...do something...
else(WIN32)
  ...do something else...
endif(WIN32)
```
You might think that the `else` section, here containing "...do something
else...", is for the WIN32 portion of the script. That is not so\! It is
actually handling the NOT WIN32 section.

As of CMake 2.6.0 the `else()` and `endif()` constructs can be empty. The
same is true for closing constructs on `endmacro()`, `endfunction()`, and
`endforeach()`. If you require 2.4.x compatibility, CMake 2.4.3 or greater
recognizes the `CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS` option (which is
superfluous in 2.6.0)

```cmake
cmake_minimum_required(VERSION 2.4.3)
set(CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS true)

if(WIN32)
  ...do something...
elseif(APPLE)
  ...do something else...
else()
  ...do something else...
endif()
```

### Which regular expressions are supported by CMake?

(for further details, please refer to the original CMake implementation
source at
[Source/kwsys/RegularExpression.hxx.in](https://gitlab.kitware.com/utils/kwsys/-/blob/6e17188724dd933df14d03e39fdfc73ac621bc0c/RegularExpression.hxx.in#L152-292))

When using `MATCHES` or `MATCHALL` in an `if` command, or using any of the
`string(REGEX ...)` commands, CMake expects regular expressions, not globs
(wild cards). CMake uses the same regular expression engine above all
platforms. Here are the meanings of the metacharacters:

- `^` Matches at beginning of a line
- `$` Matches at end of a line
- `.` Matches any single character
- `[`...`]` Matches any character(s) inside the brackets
- `[^`...`]` Matches any character(s) not inside the brackets
- `-` Matches any character in range on either side of a dash
- `|` Matches a pattern on either side of the `|`
- `*` Matches preceding pattern zero or more times
- `+` Matches preceding pattern one or more times
- `?` Matches preceding pattern zero or once only
- `(`...`)` Saves a matched expression and uses it in a later match

Example: `\[-\]\[L\](\[^ ;\])+` matches all strings beginning with `-L`
and ending with a space or a semicolon, the usual linkdirs under Linux.

Here is how to catch a part of a string. The variable test is filled
with some content, and then we want to catch the "me":
```cmake
set(test "hello world ! catch: me if you can")
string(REGEX REPLACE ".*catch: ([^ ]+).*" "\\1" result "${test}" )
message(STATUS "result= ${result}")
```
This is slightly tricky. The part inside the brackets is available in
`\\1` . CMake will copy the variable test to the variable result, but
then it will replace everything that the regular expression matches with
`\\1`. This means the first regular expression has to match the whole
string and the part we want to catch has to be put in parens.

    -- result= me

For those of you who know Perl, the equivalent Perl code could be:
```perl
$test = "hello world ! catch: me if you can";
$result = $test;
$result =~ s/.*catch: ([^ ]+).*/$1/;
print "-- result= $result\n";
```

There are other ways to do this in Perl, but this is how we do it in
CMake because `\1` does not become a variable like `$1` does in Perl, so
there is no `set(result ${\1})` in CMake.

Not sure whether CMake regex implementation supports modifiers for case
insensitive matching, thus if this is needed it's perhaps best to do
matches on a result of `string(TOLOWER ...)`.

### How to convert a semicolon separated list to a whitespace separated string?

```cmake
set(foo
  abc.c
  abc.b
  abc.a
)

foreach(arg ${foo})
  set(bar "${bar} ${arg}")
endforeach(arg ${foo})

message("foo: ${foo}")
message("bar: ${bar}")
```

### How can I build multiple modes without switching ?

To build multiple modes (e.g. Debug and Release) in one shot without
constantly running `cmake -DCMAKE_BUILD_TYPE=Debug` and
`cmake -DCMAKE_BUILD_TYPE=Release` in source tree create a directory for
builds, e.g.:

    Project-directory/
      /Build

Inside you can place as many target directories for out-of-source build
modes as you want, e.g.:

    Project-directory/
      /Build
        /Debug
        /Release

In each of these directories issue a command (assuming that you have
CMakeLists.txt directly in Project-directory)
```shell
cmake -DCMAKE_BUILD_TYPE=type_of_build ../../
```
to create a cmake cache configured for requested build type.

Now you can make each build just by entering appropriate directory and
executing a make command.

### How can I specify my own configurations (for generators that allow it) ?

For generators that allow it (like Visual Studio), CMake generates four
configurations by default: Debug, Release, MinSizeRel and
RelWithDebInfo. Many people just need Debug and Release, or need other
configurations. To modify this, you need to change the variable
`CMAKE_CONFIGURATION_TYPES` in the cache:

```cmake
if(CMAKE_CONFIGURATION_TYPES)
  set(CMAKE_CONFIGURATION_TYPES Debug Release DebugMX31 ReleaseMX31)
  set(CMAKE_CONFIGURATION_TYPES "${CMAKE_CONFIGURATION_TYPES}" CACHE STRING
    "Reset the configurations to what we need"
    FORCE)
endif()
```

Note that the first line checks whether the generator supports multiple
configurations.

If you just want to add your own configurations while keeping the
default ones, you can use list operations:
```cmake
if(CMAKE_CONFIGURATION_TYPES)
  list(APPEND CMAKE_CONFIGURATION_TYPES SuperDuper)
  list(REMOVE_DUPLICATES CMAKE_CONFIGURATION_TYPES)
  set(CMAKE_CONFIGURATION_TYPES "${CMAKE_CONFIGURATION_TYPES}" CACHE STRING
    "Add the configurations that we need"
    FORCE)
endif()
```

### How can I extend the build modes with a custom made one ?

The following code snippet (taken from a CMakeLists.txt) adds a
"Maintainer" mode:

```cmake
set(CMAKE_CXX_FLAGS_MAINTAINER "-Wall -Wabi" CACHE STRING
 "Flags used by the C++ compiler during maintainer builds."
  FORCE)
set(CMAKE_C_FLAGS_MAINTAINER "-Wall -pedantic" CACHE STRING
  "Flags used by the C compiler during maintainer builds."
  FORCE)
set(CMAKE_EXE_LINKER_FLAGS_MAINTAINER
  "-Wl,--warn-unresolved-symbols,--warn-once" CACHE STRING
  "Flags used for linking binaries during maintainer builds."
  FORCE)
set(CMAKE_SHARED_LINKER_FLAGS_MAINTAINER
  "-Wl,--warn-unresolved-symbols,--warn-once" CACHE STRING
  "Flags used by the shared libraries linker during maintainer builds."
  FORCE)
mark_as_advanced(
  CMAKE_CXX_FLAGS_MAINTAINER
  CMAKE_C_FLAGS_MAINTAINER
  CMAKE_EXE_LINKER_FLAGS_MAINTAINER
  CMAKE_SHARED_LINKER_FLAGS_MAINTAINER)
# Update the documentation string of CMAKE_BUILD_TYPE for GUIs
set(CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}" CACHE STRING
  "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel Maintainer."
  FORCE)
```

Notes: The flags used in this example are specific to GCC. Change them
as needed for your project. Additionally the `set(CMAKE_BUILD_TYPE)`
command will override a `CMAKE_BUILD_TYPE` previously set in the
CMakeLists.txt. A simple `set(CMAKE_BUILD_TYPE)` does silently
overwrite, the change is only visible in the GUI if "CACHE" is set when
overriding.

### Why does `foreach` skip empty values?

The code in question is of the form

```cmake
set(var "a;b;;c;d") # list 'a', 'b', '', 'c', 'd'
foreach(v ${var})
  # v=a, v=b, v=c, v=d, one at a time
endforeach()
```
and the loop variable `v` never attains the empty-string value `''`. This
is because the `${var}` syntax is an unquoted argument so the CMake
language expands the list and removes the empty value. The foreach
command does not even see the empty value. One can verify this because
the code

```cmake
foreach(v a b "" c d)
  ...
endforeach()
```
will see the empty value.

### Does CMake support precompiled headers?

Yes and no. Every platform does precompiled headers a bit differently,
and there is currently no first-class interface provided by CMake and
implemented on every platform. However, CMake does provide enough
primitives for projects to use precompiled headers on specific
platforms. Our issue tracker has a [feature
request](https://gitlab.kitware.com/cmake/cmake/issues/1260) with
attachements providing user-contributed helper macros for some
platforms.

## Writing FindXXX.cmake files

### What are the rules to write a FindXXX.cmake file?

See the [cmake-developer(7) manual](https://cmake.org/cmake/help/latest/manual/cmake-developer.7.html#find-modules) "Find Modules" section.

### Why does find_library look in system directories before its PATHS option?

The code in question is often of the form
```cmake
find_library(FOO_LIBRARY NAMES foo PATHS /opt/foo/lib)
```

CMake will find `/usr/lib/libfoo.so` instead of
`/opt/foo/lib/libfoo.so` if both exist. The reason is that
`/opt/foo/lib` is a *hard-coded guess* of the location. The
documentation of
[`find_library`](https://cmake.org/cmake/help/latest/command/find_library.html)
specifies the search order. User, project, and system configuration
variables are always more local than hard-coded guesses and should
override them, so the PATHS option is used last.

Some find-modules compute probable locations based on other information
*available from the system* such as a project-specific environment
variable. The HINTS option (CMake 2.6 and higher) takes precedence over
system directories specifically for this case:
```cmake
file(TO_CMAKE_PATH "$ENV{FOO_LIB_DIR}" FOO_LIB_DIR)
find_library(FOO_LIBRARY NAMES foo HINTS ${FOO_LIB_DIR})
```

CMake will find `$ENV{FOO_LIB_DIR}/libfoo.so` before
`/usr/lib/libfoo.so`.

## Finding and using external packages

### How do I use CMake to generate SWIG wrapper libraries?

CMake version 2 includes a module that supports the generation of SWIG
wrapper libraries. The SWIG package defines the following macros:
`swig_add_module` and `swig_link_libraries`.

```cmake
# This example shows how to use python
# Currently these languages have been tested:
#  perl tcl ruby php4 pike

find_package(SWIG REQUIRED)
include(${SWIG_USE_FILE})

find_package(PythonLibs)
include_directories(${PYTHON_INCLUDE_PATH})

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_SWIG_FLAGS "")

set_source_files_properties(example.i PROPERTIES CPLUSPLUS ON)
set_source_files_properties(example.i PROPERTIES SWIG_FLAGS "-includeall")
swig_add_module(example python
  example.i example.cxx)
swig_link_libraries(example ${PYTHON_LIBRARIES})

# This example shows how to use tcl
project(TCL_WRAP)
set(MODULE_NAME project)
set(INTERFACE_FILES project.i)
set(SRC_FILES Vertex.h Vertex.cxx Shapes.h Shapes.cxx)

find_package(SWIG REQUIRED)
include(${SWIG_USE_FILE})

# Look for TCL
include_directories(${TCL_INCLUDE_PATH})

find_library(TCL_LIBRARY NAMES tcl tcl84 tcl83 tcl82 tcl80
             PATHS /usr/lib /usr/local/lib)
if(TCL_LIBRARY)
  target_add_library(${MODULE_NAME} TCL_LIBRARY)
endif()

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_SWIG_FLAGS "-c++")

set_source_files_properties(${INTERFACE_FILES} PROPERTIES CPLUSPLUS ON)
set_source_files_properties(${INTERFACE_FILES} PROPERTIES CMAKE_SWIG_FLAGS "-includeall")
swig_add_module(${MODULE_NAME} tcl ${INTERFACE_FILES} ${SRC_FILES})
swig_link_libraries(${MODULE_NAME} ${TCL_LIBRARIES})
```

If you get errors indicating that C and C++ include files cannot be
found, like,

    Error: Unable to find 'string.h'
    Error: Unable to find 'time.h'
    Error: Unable to find 'string'
    Error: Unable to find 'functional'
    Error: Unable to find 'utility'
    Error: Unable to find 'limits.h'
    Error: Unable to find 'fstream'
    Error: Unable to find 'sys/times.h'
    Error: Unable to find 'unistd.h'
    Error: Unable to find 'malloc.h'
       ...

try setting the `-includeall` property on fewer source files:

```cmake
# Try doing this on fewer files
set_source_files_properties(example.i PROPERTIES SWIG_FLAGS "-includeall")
```

In particular, you may need `-includeall` only on the top-level `.i`
files.

### How do I use CMake to build LaTeX documents?

Use the following approach. Note that you have to set `LATEX_COMPILE` to
LaTeX executable, `DVIPDF_COMPILE` to dvi to pdf converter. Also, the
LaTeX source is TDocument.tex and the result is called TDocument.pdf.
Note that this uses commands in CMake version 1.8 or later.

```cmake
project(Document)
if(LATEX_COMPILE)
  add_custom_command(
    OUTPUT    ${Document_BINARY_DIR}/TDocument.dvi
    DEPENDS   ${Document_BINARY_DIR}/TDocument.tex
    COMMAND   ${LATEX_COMPILE}
    ARGS      ${Document_SOURCE_DIR}/TDocument.tex
    )
endif()

if(DVIPDF_COMPILE)
  add_custom_command(
    OUTPUT    ${Document_BINARY_DIR}/TDocument.pdf
    DEPENDS   ${Document_BINARY_DIR}/TDocument.dvi
    COMMAND   ${DVIPDF_COMPILE}
    ARGS      ${Document_SOURCE_DIR}/TDocument.dvi
    )
endif()

add_custom_target(LaTeXDocument ALL echo
  DEPENDS ${Document_BINARY_DIR}/TDocument.pdf
  )
```
The following uses commands in CMake version 2.0 and later

```cmake
project(Document)
#
# Find LaTeX
#
find_package(LATEX)

if(LATEX_COMPILER)
  add_custom_command(
    OUTPUT    ${Document_BINARY_DIR}/TDocument.dvi
    COMMAND   ${LATEX_COMPILER}
    ARGS      ${Document_SOURCE_DIR}/TDocument.tex
    DEPENDS   ${Document_SOURCE_DIR}/TDocument.tex
    COMMENT   "Tex2dvi"
  )

if(DVIPS_CONVERTER)
  add_custom_command(
    OUTPUT    ${Document_BINARY_DIR}/TDocument.ps
    COMMAND   ${DVIPS_CONVERTER}
    ARGS      ${Document_BINARY_DIR}/TDocument.dvi
              -o ${Document_BINARY_DIR}/TDocument.ps
    DEPENDS   ${Document_BINARY_DIR}/TDocument.dvi
    COMMENT   "dvi2ps"
 )

if(PS2PDF_CONVERTER)
  add_custom_command(
    OUTPUT    ${Document_BINARY_DIR}/TDocument.pdf
    COMMAND   ${PS2PDF_CONVERTER}
    ARGS      ${Document_BINARY_DIR}/TDocument.ps
    DEPENDS   ${Document_BINARY_DIR}/TDocument.ps
    COMMENT   "ps2pdf"
  )

add_custom_target(LaTeXDocument ALL echo
  DEPENDS   ${Document_BINARY_DIR}/TDocument.pdf
)
endif()  # PS2PDF_CONVERTER
endif()  # DVIPS_CONVERTER
endif()  # LATEX_COMPILER
```

### How do I get LaTeX references to be correct?

When your latex document contains references (e.g. `\ref{...}` command)
you get to run two passes of latex. In the most general case, i.e. when
additionally your document uses a bibtex bibliography, you shall need
three passes of latex (and one pass of bibtex):

1.  latex (first pass: for bibtex to have an .aux file)
2.  bibtex (for generating the .bbl file)
3.  latex (second pass)
4.  latex (third pass)

The following code snippet illustrates how you can "pervert" the bibtex
and latex generated auxilary files (.aux, .log, .dvi, .bbl...) to create
an "artificial" set of CMake dependencies. The side-effect of those
dependencies should hopefully be the above described sequence of calls
to latex and bibtex
```cmake
add_custom_command(
  OUTPUT    ${CMAKE_CURRENT_BINARY_DIR}/UsersManual.aux
  DEPENDS   ${CMAKE_CURRENT_SOURCE_DIR}/UsersManual.tex
  COMMAND   ${LATEX_COMPILER}
  ARGS      -interaction=batchmode ${CMAKE_CURRENT_BINARY_DIR}/UsersManual
  COMMENT   "Latex (first pass)"
  )

add_custom_command(
  OUTPUT    ${CMAKE_CURRENT_BINARY_DIR}/UsersManual.bbl
  DEPENDS   ${CMAKE_CURRENT_BINARY_DIR}/UsersManual.aux
  COMMAND   ${BIBTEX_COMPILER}
  ARGS      -terse ${CMAKE_CURRENT_BINARY_DIR}/UsersManual
  COMMENT   "Bibtex"
  )

add_custom_command(
  OUTPUT    ${CMAKE_CURRENT_BINARY_DIR}/UsersManual.dvi
  DEPENDS   ${CMAKE_CURRENT_BINARY_DIR}/UsersManual.bbl
  COMMAND   ${LATEX_COMPILER}
  ARGS      -interaction=batchmode ${CMAKE_CURRENT_BINARY_DIR}/UsersManual
  COMMENT   "Latex (second pass)"
  )

add_custom_command(
  OUTPUT    ${CMAKE_CURRENT_BINARY_DIR}/UsersManual.log
  DEPENDS   ${CMAKE_CURRENT_BINARY_DIR}/UsersManual.bbl
            ${CMAKE_CURRENT_BINARY_DIR}/UsersManual.dvi
  COMMAND   ${LATEX_COMPILER}
  ARGS      -interaction=batchmode ${CMAKE_CURRENT_BINARY_DIR}/UsersManual
  COMMENT   "Latex (third pass)"
  )

# Eventually trigger the whole process
add_custom_target(LaTeXDocument ALL echo
   DEPENDS   ${CMAKE_CURRENT_BINARY_DIR}/UsersManual.log
   )
```

### How can I set TEXINPUTS for a LaTeX compilation?

First note that most often you can avoid using TEXINPUTS by copying all
the necessary files (.tex source file and included graphic files e.g.
.eps files) from your `PROJECT_SOURCE_DIR` hirarchy to your
`PROJECT_BINARY_DIR` subdir \[refer to `configure_file` with the COPYONLY
flag set for copying files\]. Since by default latex uses the current
working directory as value for TEXINPUTS you should be all set. As
expected, this trick is quick AND dirty since your concerned
`PROJECT_BINARY_DIR` subdir now contains files that are NOT generated by
CMake (in the sense that those files are not the result of a system
command but were merely duplicated)...

If you consider it is cleaner or easier to define a TEXINPUTS
environment variable \[the latex command probably misses a -I flag\] you
can find an example in the InsightDocuments cvs archive (refer to the
section "cvs access" near the bottom of Kitware's ITK download page) or
use google with keywords `ITK_TEXINPUTS CONFIGURE_FILE`. Look at
InsightDocuments/CourseWare/Training/Vis2003/Latex/CMakeLists.txt and
search for e.g. "LaTeXWrapper.sh.in".

Roughly the mechanism goes:

  - `set(ITK_TEXINPUTS ...)` with the desired TEXINPUTS
  - `configure_file`
    "InsightDocuments/CourseWare/Training/Vis2003/LaTeXWrapper.sh.in"
    which generates an sh shell script setting the shell variable
    TEXINPUTS prior to running the latex command
  - use `add_custom_command` to invoke this shell script

This very example is Win32 portable (except that LaTeXWrapper.bat.in
generates a .bat shell script)

## Library questions

### Can I build both shared and static libraries with one ADD_LIBRARY command?

No. Each library you build must have a unique target name, i.e. the
`libname` field of the `add_library` command. That way, CMake can track
dependencies separately for each library. Libraries can have the same
`OUTPUT_NAME`, see the `set_target_properties` command, but this is not
the default.

### Does that mean I have to build all my library objects twice, once for shared and once for static?\! I don't like that\!

In practice, most libraries have different defines and compiler flags
for the shared vs. static cases. So you would have to build all your
library objects twice anyways. However, if you happen to have
*exactly* the same defines and compiler flags for the shared vs.
static cases, then use [object libraries](https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#object-libraries).

<details><summary>Original Answer</summary>

...if you're using Linux and a GCC-style linker, you could do the
following. Note that for this to work correctly on linux, the zzSTATIC
source files should have been compiled with "-fPIC" to ensure they will
work in a shared library.
```cmake
if(CMAKE_SYSTEM_NAME STREQUAL "Linux" AND CMAKE_COMPILER_IS_GNUCC)
  add_library(zzSTATIC STATIC -fPIC)
  add_library(zzDYNAMIC SHARED)
  target_link_libraries(zzDYNAMIC -Wl,-whole-archive zzSTATIC -Wl,-no-whole-archive)
endif()
```

...if you want a cross-platform approach that works on all compilers,
not just GCC or Linux, you could extract the locations of previously
built object files and insert them directly into the libraries that need
them. This is documented in [CMake Feature Request \#5155: standard way
to locate object files](https://cmake.org/Bug/view.php?id=5155).
Unfortunately this approach relies on CMake's internal implementation,
and that implementation could change in the future, breaking your
code.

</details>

### How do I make my shared and static libraries have the same root name, but different suffixes?

Set the `OUTPUT_NAME` of your shared and static libraries to the same
thing.

```cmake
add_library(foo SHARED ${foo_sources})
add_library(foo-static STATIC ${foo_sources})
# The library target "foo" already has a default OUTPUT_NAME of "foo", so we don't need to change it.
# The library target "foo-static" has a default OUTPUT_NAME of "foo-static", so change it.
set_target_properties(foo-static PROPERTIES OUTPUT_NAME "foo")
# Now the library target "foo-static" will be named "foo.lib" with MS tools.
# This conflicts with the "foo.lib" import library corresponding to "foo.dll",
# so we add a "lib" prefix (which is default on other platforms anyway):
set_target_properties(foo-static PROPERTIES PREFIX "lib")
```

One more detail is needed with CMake 2.6.x and lower (but not CMake 2.8
or higher). If you are building your shared and static libraries in the
same directory, you will also need the following to keep your shared and
static libraries from clobbering each other during the build.

```cmake
# Help CMake 2.6.x and lower (not necessary for 2.8 and above, but doesn't hurt):
set_target_properties(foo PROPERTIES CLEAN_DIRECT_OUTPUT 1)
set_target_properties(foo-static PROPERTIES CLEAN_DIRECT_OUTPUT 1)
```

### How do I rename a library after it has already been built?

You don't rename it. It's been built\! Its name is whatever
CMakeLists.txt says it's supposed to be.

Perhaps you want to copy the library to a different name. But, are you
sure that's what you want to do? You could just change the name in your
`add_library` command or change its OUTPUT_NAME property using
`set_target_property()`. If you really really want to copy the library
to a different name, try:

```cmake
get_target_property(LIB_NAME Foo LOCATION)
get_target_property(Bar_prefix Foo PREFIX)
get_target_property(Bar_suffix Foo SUFFIX)
set(NEW_LIB_NAME ${Bar_prefix}Bar${Bar_suffix})

add_custom_command(
  TARGET Foo
  POST_BUILD
  COMMAND ${CMAKE_COMMAND} -E copy ${LIB_NAME} ${NEW_LIB_NAME}
)
```
On Windows you may also want to copy the .dll import lib, using the same
approach as above, but with `IMPORT_PREFIX` and `IMPORT_SUFFIX`. *Problem:
`LOCATION` only refers to 1 file, the .dll. What is a simple way to get
the location of the import lib? Could provide a complicated way, but
that's annoying.*

### Does CMake support "convenience" libraries?

No. CMake does not currently support convenience libraries. A
"convenience" library, as GNU libtool calls it, is an archive of objects
to be mixed into other libraries. Other libraries "link" to the
convenience library, but the convenience library does not export any
symbols; GNU libtool never installs the convenience library; no programs
ever link to the convenience library.

This does **not** mean that a project using convenience libraries cannot
be converted to CMake. Instead the source files may be listed in each
target that needs them. They will be built for each target separately
using all the preprocessor definitions and flags configured for that
target.

### Why are libraries linked to my shared library included when something links to it?

This question arises when one has a library B which links to some
library A. When a third target, say C, links to B, CMake will
automatically include C to A also. When the libraries are static, then
this is always necessary. When the libraries are shared, this is the
default behavior provided by CMake with the simple
`target_link_libraries signature`. CMake 3.0 and above provide `PUBLIC`,
`PRIVATE`, and `INTERFACE` options to target_link_libraries to specify
whether linking should be used for just the implementation and/or made
part of the interface (transitive dependencies):
```cmake
target_link_libraries(myLibrary PRIVATE myImplDependency)
```

CMake 2.8.7 and above provide `LINK_PUBLIC` and `LINK_PRIVATE` options
for the same purpose (these were superseded by PUBLIC/PRIVATE/INTERFACE
in 3.0 for consistency with options other commands like
`target_compile_definitions`).

### CMake dependency scanner

CMake does not preprocess source files while scanning dependencies. Code
like
```
#if 0
# include "bla.h"
#endif
```
will result in a dependency on "bla.h". This sometimes leads to source
files recompiling unnecessarily but will not break the build.

## Installation questions

### Does CMake's "make install" support DESTDIR?

**Yes**, especially when the build-system generator uses CMake's builtin
support for installing files: Simply define the DESTDIR environment
variable during installation and CMake will treat that value as the root
of the file system for all installation paths; naturally, the DESTDIR
path must be absolute.

For example, if the Makefile generator is used, then all of the
following are example usages of DESTDIR (perhaps assuming the bash shell
for the last 2):

1. `make install DESTDIR="/some/absolute/path"`
2. `make DESTDIR="/some/absolute/path" install`
3. `DESTDIR="/some/absolute/path" make install`
4. `export DESTDIR="/some/absolute/path`<br/>
   `make install`

### Can I do "make uninstall" with CMake?

By default, CMake does not provide the "make uninstall" target, so you
cannot do this. We do not want "make uninstall" to remove useful files
from the system.

If you want an "uninstall" target in your project, then nobody prevents
you from providing one. You need to delete the files listed in
install_manifest.txt file. Here is how to do it. First create file
cmake_uninstall.cmake.in in the top-level directory of the project:

```cmake
if(NOT EXISTS "@CMAKE_BINARY_DIR@/install_manifest.txt")
  message(FATAL_ERROR "Cannot find install manifest: @CMAKE_BINARY_DIR@/install_manifest.txt")
endif()

file(READ "@CMAKE_BINARY_DIR@/install_manifest.txt" files)
string(REGEX REPLACE "\n" ";" files "${files}")
foreach(file ${files})
  message(STATUS "Uninstalling $ENV{DESTDIR}${file}")
  if(IS_SYMLINK "$ENV{DESTDIR}${file}" OR EXISTS "$ENV{DESTDIR}${file}")
    execute_process(
      COMMAND "@CMAKE_COMMAND@" -E remove "$ENV{DESTDIR}${file}"
      OUTPUT_VARIABLE rm_out
      RESULT_VARIABLE rm_retval
      )
    if(NOT "${rm_retval}" STREQUAL 0)
      message(FATAL_ERROR "Problem when removing $ENV{DESTDIR}${file}")
    endif()
  else(IS_SYMLINK "$ENV{DESTDIR}${file}" OR EXISTS "$ENV{DESTDIR}${file}")
    message(STATUS "File $ENV{DESTDIR}${file} does not exist.")
  endif()
endforeach()
```

Then in the top-level CMakeLists.txt add the following logic:
```cmake
# uninstall target
if(NOT TARGET uninstall)
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
    IMMEDIATE @ONLY)

  add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
endif()
```
Now you will have an "uninstall" target at the top-level directory of
your build tree.

Instead of creating an "uninstall" target, Unix users could enter this
command in the shell:
```shell
xargs rm < install_manifest.txt
```

## Distribution questions

### Where is "make dist"?

CMake doesn't create a `make dist` target.

### What is the best way to distribute source code or binaries for a cmake-based project?

For creating source or binary packages there is now
[CPack](Home#cpack "wikilink") coming with CMake, see the
[documentation](Home#cpack "wikilink").

Of course you can also use any other ways to create packages.

## Platform-specific questions

### How do I build universal binaries on Mac OS X?

Before running CMake with an empty build tree, set the
`CMAKE_OSX_ARCHITECTURES` environment variable. It should be set to
contain a `;` separated list of architectures that you want in the binary.
For example, for 32-bit PowerPC and Intel you would do this:

    CMAKE_OSX_ARCHITECTURES=ppc;i386

If you wish to build both as 32 and 64 bit, do this:

    CMAKE_OSX_ARCHITECTURES=ppc;i386;ppc64;x86_64

You can also set the same named CMake cache variable on an existing
binary tree. This works with both makefiles and the Xcode generator.

In addition, you can also set the `CMAKE_OSX_SYSROOT` variable to point
to the sysroot (aka Mac OS SDK) to be used. CMake will attempt to pick
one on your system, but it can be changed in the cache or via an
environment variable before running CMake. The 10.4u SDK or later must
be used to create a Universal Binary.

Universal Binaries are essentially cross compilation and so you should
avoid using `try_run`, especially for things like testing endianess or
variable size because the result will only be correct for one
architecture.

Lastly, note that CTest is only able to test one architecture. See bug
6157.

### How can I apply resources on Mac OS X automatically?

Using `add_custom_command`. For example, let's say you are creating
executable MyExecutable, which needs the resources file Carbon.r. All
you do is add a custom rule which is executed after the executable is
linked:
```cmake
add_executable(MyExecutable ${MyExecutable_SRCS})
get_target_property(MyExecutable_PATH MyExecutable LOCATION)

if(APPLE)
  find_program(APPLE_RESOURCE Rez /Developer/Tools)
  if(APPLE_RESOURCE)
    add_custom_command(TARGET MyExecutable POST_BUILD
                       COMMAND ${APPLE_RESOURCE} Carbon.r -o ${MyExecutable_PATH})
  endif()
endif()
```

This will execute:

    /Developer/Tools/Rez Carbon.r -o /binary/path/MyExecutable

after MyExecutable is linked.

'Rez' may be located elsewhere on disk, depending on the version of Mac
OS X and Xcode. You can use 'which Rez' in Terminal to find it's full
path.

### Why does find_library not find .DLL libraries under WIN32?

For those who come from a Unix background to MS Windows:

You never link directly to the .dll, you have to link against the import
library .lib for the .dll.

Linking against dynamic libraries (.dll under Windows) is quite
different from linking against ELF shared objects (.so) under platforms
like Linux or NetBSD. In Windows, there are two types of library, a
static library and an import library (both confusingly use the .lib
extension, however). In Windows, when you build an import library
(A.lib) you will get a corresponding (A.dll) that you only need at
runtime. At compile time you will need the import library.

Conclusion: There is no need to find a .dll for linking. You only need
to find the .lib import library.

Some more details can be found here:
[2](http://xenophilia.org/winvunix.html).

### Why am I getting a linker error to _mainCRTStartup under WIN32?

Your program is a GUI application using WinMain (/subsystem:windows) and
not a console application using main. You have to use the WIN32 option
with the `add_executable` command.
```cmake
add_executable(exename WIN32 source1 source2 ... sourceN)
```

The second argument to `add_executable` can be `WIN32`. This indicates that
the executable, when compiled on Windows, is a Windows app (using
WinMain) and not a console app (using main). Please note that on Unix
platforms, CMake ignores the WIN32 and the compiler will use "main" in
any case.

### Why do I get this error: nafxcwd.lib(appcore.obj) : error LNK2001: unresolved external symbol ___argv

This is because the application is using both the static and dll
versions of the MFC library. To fix the problem, you can do the
following:

```cmake
set(CMAKE_MFC_FLAG 2)      # force the IDE to use static MFC
add_definitions(-D_AFXDLL) # make sure if afx.h is included the dll MFC is used
```

### How to use MFC with CMake

To use MFC, the `CMAKE_MFC_FLAG` variable must be set as follows:

0. Use Standard Windows Libraries
1. Use MFC in a Static Library
2. Use MFC in a Shared DLL

This can be set in a CMakeLists.txt file and will enable MFC in the
application. It should be set to 1 or 2. This is used in visual studio 6
and 7 project files. The CMakeSetup dialog uses MFC and the
CMakeLists.txt looks like this:
```cmake
add_definitions(-D_AFXDLL)
set(CMAKE_MFC_FLAG 2)
add_executable(CMakeSetup WIN32 ${SRCS})
```

Note that visual studio 9 project files do not appear to work with
CMAKE_MFC_FLAG 1; this may be related to
[bug 7056](https://cmake.org/Bug/view.php?id=7056).

In order to use MFC with UNICODE, you must also [specify the entry point
wWinMainCRTStartup](http://msdn.microsoft.com/en-us/library/dybsewaf.aspx).
For example:
```cmake
set(CMAKE_MFC_FLAG 2)
set_target_properties(MyApp PROPERTIES
  COMPILE_DEFINITIONS _AFXDLL,_UNICODE,UNICODE,_BIND_TO_CURRENT_CRT_VERSION,_BIND_TO_CURRENT_MFC_VERSION
  LINK_FLAGS "/ENTRY:\"wWinMainCRTStartup\"")
```

See [this
article](http://stackoverflow.com/questions/59635/app-does-not-run-with-vs-2008-sp1-dlls-previous-version-works-with-rtm-versions)
as to why `_BIND_TO_CURRENT_CRT_VERSION` and `_BIND_TO_CURRENT_MFC_VERSION`
are necessary for Visual Studio 2008 SP1.

### How To Put Files in Folders in Visual Studio Projects

The Visual Studio IDE supports putting files into folders. CMake can be
used to put files in folders with the `source_group` command.

```cmake
source_group(name [REGULAR_EXPRESSION regex] [FILES src1 src2 ...])
```
Defines a group into which sources will be placed in project files. This
is mainly used to setup file tabs in Visual Studio. Any file whose name
is listed or matches the regular expression will be placed in this group
provided the source file is being passed to `add_executable` or
`add_library`. For example:
```cmake
source_group(FooFiles FILES foo.cxx)
source_group(BarFiles FILES bar.cxx)
add_library(foo foo.cxx bar.cxx)
```

In the event a file matches multiple groups, the LAST group that
explicitly lists the file will be favored, if any. If no group
explicitly lists the file, the LAST group whose regular expression
matches the file will be favored. For backwards compatibility this
command is also supports the format `source_group(name regex)`.

As a convenience to developers CMake automatically adds standard header
files to a "Header Files" folder and standard source files to a "Source
Files" folder for Visual Studio Projects. This can be overridden via the
`source_group` method documented above.

### How to create Visual Studio 6 Projects that contain only a single build type

For Visual Studio.NET (version 7.0 and above) it is possible to set the
`CMAKE_CONFIGURATION_TYPES` variable to the build type(s)
(Debug/Release/...) that you want. This does not work for Visual Studio
6. There is however a way to achieve this. To create your own set of
configurations:

1.  Create a directory in which you copy the files \*.dsptemplate and
    CMakeVisualStudio6Configurations.cmake from CMake's Templates
    directory.
2.  Edit the .cmake file and change the `set(CMAKE_CONFIGURATION_TYPES ...)`
    line to set the build types that you want in your set.
3.  Edit the \*Header.dsptemplate files to contain only the
    configuration types you want in your set.
4.  In your CMakeLists.txt file, set the `MSPROJECT_TEMPLATE_DIRECTORY`
    to the directory that you created.

That's it. Run CMake and your new configuration files will be created.

Note: Editing the \*Header.dsptemplates files should be done very
carefully. Here are some guidelines:

- You *must* remove the targets that you do not want in your set at the
  bottom of the file (e.g. `\# Name "OUTPUT_LIBNAME - Win32 MinSizeRel"`)
- You can remove the `\!IF "$(CFG)" == ...` until `\!ELSEIF "$(CFG)" == ...`
  or `\!ELSEIF "$(CFG)" == ...` until `\!ENDIF` lines for the
  configurations you do not want. Make sure that the resulting code still
  starts with `\!IF ...` and ends with `\!ENDIF` with any number of
  `\!ELSEIF` sections in between. If you create templates for a single
  configuration (aka makefile), it is possible to remove everything
  starting from `\!IF` until and including `\!ENDIF` and leave only the
  contents of the relevant section intact.
- Do not edit the lines starting with `\!MESSAGE` as the changes
  may - and probably will - corrupt your resulting DSP files.
  The only thing I was able to change
  without corrupting the DSP is to remove the irrevant configurations from
  the "Possible choices for configuration are:" list.

If you have only a single configuration in your set, you may want to get
rid of the intermediate dir that MsDev creates. You can do that by
setting:

1. `PROP BASE Output_Dir ""`
2. `PROP BASE Intermediate_Dir ""`
3. `PROP Intermediate_Dir ""`
4. `PROP Output_Dir "LIBRARY_OUTPUT_PATH"`

or

1. `PROP Output_Dir "EXECUTABLE_OUTPUT_PATH"`

Additionally you should then also edit the `\# ADD LINK32` line in the
DLLHeader.dsptemplate file. Change for example
`/out:"LIBRARY_OUTPUT_PATHDebug/OUTPUT_LIBNAMEDEBUG_POSTFIX.dll"`
into `/out:"LIBRARY_OUTPUT_PATHOUTPUT_LIBNAMEDEBUG_POSTFIX.dll"`
(Note that the configuration name and also the slash are removed).

It is even possible to rename the pre-defined configurations of CMake in
this way. Let's say you prefer 'PreProduction' over 'RelWithDebInfo'.
You can change the name in the \*.dsptemplate files, but you should also
change it in the CMakeVisualStudio6Configurations.cmake file. Be
careful, however. Only entries relevant to the configuration name should
be changed. Do not change the /debug options and the entries that
contain the build type in capital characters. Internally in CMake the
build type will still remain 'RelWithDebInfo', so also the
CMAKE_BUILD_TYPE should be set to the old value. You can only change
the way it is named in MSDev.

Note: Apparently MsDev as command-line build tool only performs a
partial check on the build type. It will match all configuration types
that CONTAIN the build type in their name. (e.g. if you have renamed
RelWithDebInfo to DebugRelease, Debug will build Debug and DebugRelease,
Release will build Release and DebugRelease. This may be exactly what
you want, but be warned.)

### Can CMake set the Debugging/Working Directory property in Visual Studio projects?

Not directly. The value of this property is not stored in the project
files. It is stored in extra files created by the IDE when a solution is
loaded (VS .NET 2003 uses a hidden .suo file next to the .sln solution
file). The format of these files is not known to CMake and cannot be
generated. In some versions of VS the files are binary and not human
readable.

However, for Visual Studio versions at least 2005 and newer, Ryan
Pavlik maintains CMake modules that can create
these files: [main
script](https://github.com/rpavlik/cmake-modules/blob/master/CreateLaunchers.cmake),
also requires [this
directory](https://github.com/rpavlik/cmake-modules/tree/master/launcher-templates).

### Why does CMakeSetup with the message "LINK : fatal error LNK1104: cannot open file 'user32.lib'" while configuring a project?

The path to the SDK libs (user32.lib) must be added by the IDE when the
project generator "Visual Studio 8 2005" is used, because cmake uses
VCExpress.exe and on the fly generated project files to check for
compiling (VCExpress.exe reads some config files for the compiler/linker
options)

So add the sdk lib path (...\\Microsoft Platform SDK\\Lib) at
Tools-\>Options-\>Projects and Solutions-\>VC++ Directories-\>Library
files

See also:

  - <http://msdn.microsoft.com/vstudio/express/visualc/usingpsdk/>

### How can I avoid the error "Arg list too long" when running make?

This error is sometimes encountered when building a static library with
many object files using Unix make command. It typically looks something
like this:

    gmake[2]: execvp: /bin/sh: Arg list too long

When make tries to run the archiver program to build the static library
the shell it uses complains that the argument list is too long. In some
shells this can be fixed by setting an environment variable such as
`ARG_MAX` to extend the length of the command line it will allow.

The error can also happen when linking shared libraries, and can be
solved by upping the sysconf parameter `MAX_ARG`.

On AIX this can be done with the command:
```shell
chdev -l sys0 -a ncargs='30'
```
Obviously an alternative approach might be to contemplate reducing the
object count by splitting off some suitable group of objects into their
separate static library.

### How can I find out platforms definitions, search paths, etc. from gcc ?

The following is really the best if not only way to get information
about predefined macros with a GNU compiler:
```shell
touch empty.c
gcc -v -dD -E empty.c
```

This will give you all you might want to know about the preprocessor,
the builtin include search dirs and all predefined definitions, so you
can check whether it's `__LINUX` or `_LINUX_` or `_APPLE_` or `__APPLE`
etc. The empty file and all these parameters are really required. You
probably want to redirect the output (both stdout and stderr) to a file.
If you want the information for C++, use a C++ file suffix for the empty
file.

This is how you can get the builtin library search paths:
```shell
gcc --print-search-dirs
```

### How can I get a windows registry key ?

The only thing to know is that you can't use just the `set` command in
place of `get_` command. CMake read the value from the registry only when
you "get" it from the cache. For instance:
```cmake
get_filename_component(SDK_ROOT_PATH  "[HKEY_LOCAL_MACHINE\\SOFTWARE\\PACKAGE;Install_Dir]" ABSOLUTE CACHE)
```

If a key name (ex: Install_Dir in this case) was not specified , the
Default key value will be get. Now you could use the `SDK_ROOT_PATH` to
add include and lib path to your project:
```cmake
include_directories(
  ${SDK_ROOT_PATH}/include
)

link_directories(
  ${SDK_ROOT_PATH}/lib
)
```

You can also read a registry key in the PATHS section of a
`find_library`, `find_path`, `find_program`, or `find_file` command:
```cmake
find_file(BOOT_DOT_INI boot.ini PATHS [HKEY_CURRENT_USER\\Environment;HOMEDRIVE])
```

For other examples have a look in the CMake Modules folder :

- FindJava.cmake
- FindPythonLibs.cmake
- ..

### How can I build my MSVC application with a static runtime?

Here are three options you could employ to compile your MSVC application
with `/MT` instead of `/MD`.

#### Manual Replace

You can rely on the user to manually modify `CMAKE_C_FLAGS_DEBUG`,
`CMAKE_C_FLAGS_RELEASE`, `CMAKE_CXX_FLAGS_DEBUG`,
`CMAKE_CXX_FLAGS_RELEASE`, etc. within the cache editor. After an
initial configure of your software they would have to replace `/MD`
entries with `/MT`.

#### Make Override Files

If you intend to build the entire source tree using `/MT` and you don't
need this ever to be configurable via the CMake GUI, the best approach
is to create an override file which initializes the starting cache
values for the compile flags.

First create a c_flag_overrides.cmake & cxx_flag_overrides.cmake
file which contain something like this... (or whatever flags you wish to
use per compiler).
```cmake
# c_flag_overrides.cmake
if(MSVC)
  set(CMAKE_C_FLAGS_DEBUG_INIT "/D_DEBUG /MTd /Zi /Ob0 /Od /RTC1")
  set(CMAKE_C_FLAGS_MINSIZEREL_INIT     "/MT /O1 /Ob1 /D NDEBUG")
  set(CMAKE_C_FLAGS_RELEASE_INIT        "/MT /O2 /Ob2 /D NDEBUG")
  set(CMAKE_C_FLAGS_RELWITHDEBINFO_INIT "/MT /Zi /O2 /Ob1 /D NDEBUG")
endif()

# cxx_flag_overrides.cmake
if(MSVC)
  set(CMAKE_CXX_FLAGS_DEBUG_INIT "/D_DEBUG /MTd /Zi /Ob0 /Od /RTC1")
  set(CMAKE_CXX_FLAGS_MINSIZEREL_INIT     "/MT /O1 /Ob1 /D NDEBUG")
  set(CMAKE_CXX_FLAGS_RELEASE_INIT        "/MT /O2 /Ob2 /D NDEBUG")
  set(CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT "/MT /Zi /O2 /Ob1 /D NDEBUG")
endif()
```
**NOTE:** These files are only evaluated on the first run of CMake so
they can't be dependent on a CMake option() meant to be toggled from the
GUI, for example. They could be dependent on a command line `-D` option or
an environment variable if desired.

Then enable them by setting the following variables prior to the
`project()` command.

```cmake
set(CMAKE_USER_MAKE_RULES_OVERRIDE
  ${CMAKE_CURRENT_SOURCE_DIR}/c_flag_overrides.cmake)
set(CMAKE_USER_MAKE_RULES_OVERRIDE_CXX
  ${CMAKE_CURRENT_SOURCE_DIR}/cxx_flag_overrides.cmake)
project(Bar)
```

#### Override CMAKE_MSVC_RUNTIME_LIBRARY

Alternatively, if you need dynamic control of `/MT` via some configure
option you could use set the `CMAKE_MSVC_RUNTIME_LIBRARY` variable.
Note: `CMAKE_MSVC_RUNTIME_LIBRARY` is a directory level option, however.

```cmake
set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
```

You could also change the `MSVC_RUNTIME_LIBRARY` property of your target.

### Why do generated Xcode projects have a CMake PostBuild Rules phase?

CMake needs precise control over target link command lines that Xcode
runs at build time. Xcode provides a "Link Binary With Libraries" build
phase in which one may list files or products from other targets to
include when linking. However, it does not allow arbitrary flags such as
`-lfoo` commonly passed by projects to CMake's `target_link_libraries`
command. It also does not allow *full* paths to library files and
instead splits `/path/to/libfoo.a` into `-L/path/to` and `-lfoo` which does
not guarantee the proper library will be found.

CMake works around this limitation by putting the entire link line in
the `OTHER_LDFLAGS` build setting where it has full control.
Unfortunately that build setting is just a string, so the resulting
build system has no dependency on the files named in the link line.
Furthermore, Xcode does not have any build setting to list arbitrary
extra dependencies of the link step. If any of a target's dependencies
are newer than the target's binary Xcode does not know it needs to run
the link rule again to bring the target up to date.

In order to work around the dependency problem, CMake includes in each
target a post-build phase that removes the link outputs of all *other*
targets that *depend* on the current target. This is necessary because
all those *other* targets lack dependencies on the current target in the
generated build system. Instead when a target finishes building it knows
that any other targets that depend on it will be out of date. The
post-build phase removes the other targets so that when it is their turn
to build Xcode will re-link them.

The post-build phase could be avoided if Xcode were to simply provide a
setting to specify link dependencies on arbitrary files. This feature
would be useful independent of CMake as it would allow users to add
dependencies on files they mention in
`OTHER_LDFLAGS`.

### Why does CMake not find my Xcode compiler on OS X with the Unix Makefiles generator?

Xcode 4.3 and later do not install the command-line development tools by
default. One must also install the "Command Line Tools for Xcode"
package. As of Xcode 4.4 one may install it from the Xcode menu:

    Xcode -> Preferences -> Downloads -> Components -> Command Line Tools

As of Xcode 4.5 the command-line tools are also available as an
independent download that does not require the full Xcode.

It is also possible to set up a shell environment to run "cmake" and
"make" without installing the Command Line Tools package.

CMake 2.8.10 and greater will recognize when the command-line tools are
not installed, find the MacOSX SDK, and add the "-isysroot" flag. One
may use the environment:
```shell
export PATH="$(dirname $(xcrun --find make)):$PATH"
export CC="$(xcrun --find cc)"
export CXX="$(xcrun --find c++)"
```

The above environment will work with CMake 2.8.8 and 2.8.9 if one also
specifies architectures explicitly (earlier CMake versions are not aware
of Xcode 4.3+ SDKs):
```shell
export CMAKE_OSX_ARCHITECTURES=x86_64
```

Alternatively one may add `-isysroot` flags via the environment:
```shell
SDK="$(xcodebuild -sdk macosx -version Path)" &&
export CFLAGS="-isysroot $SDK" &&
export CXXFLAGS="-isysroot $SDK" &&
unset SDK
```

## Other Questions

### Why does CMake generate recursive Makefiles?

This question is often asked with reference to this paper:

- Miller, Peter A., [Recursive Make Considered
  Harmful](http://miller.emu.id.au/pmiller/books/rmch/), AUUGN Journal
  of AUUG Inc., 19(1), pp. 14-25, 1998

The summary of our response may be worded "recursive make considered
necessary". CMake *must* generate makefiles that invoke other makefiles
in order to implement automatic implicit dependency scanning in
combination with generated source/header files. Since CMake works with
primitive UNIX make tools we may not use GNU make extensions to load new
make rules into an already-running make process.

CMake does not actually generate truly recursive makefiles that follow
the directory structure. It generates a fixed 3-level makefile structure
in which each level has a defined purpose:

1.  **Makefile**: Command-line interface entry points. Maps "make"
    invocations into calls to the level 2 makefile:
      - `make -f CMakeFiles/Makefile2 ...`
2.  **CMakeFiles/Makefile2**: Inter-target dependencies. Evaluates
    targets in dependency order invoking for each target the depends and
    build steps in level 3:
      - `make -f CMakeFiles/$(target).dir/build.make .../depend`
      - `make -f CMakeFiles/$(target).dir/build.make .../build`
3.  **CMakeFiles/<target>.dir/build.make**: File-level dependencies and
    rules:
      - `depend`: Evaluates custom commands (to produce generate source
        files) and then scans sources for implicit dependencies.
      - `build`: Loads dependency scanning results from previous step.
        Compiles and links.

As a flat-file, minimalistically-designed command-line build alternative
to make, using the newly-added (2012) Ninja generator might be
worthwhile.

### Why can't I make multiple targets on the command line in parallel?

The Makefiles generated by CMake do not support explicit specification
of multiple targets on the command line as
```shell
make target1 target2 -j
```

because it was not a design goal at the time the generators were
written. Instead use
```shell
make target1 -j && make target2 -j
```
or use `add_custom_target` and `add_dependencies` to create a single
target depending on both targets and then make that.

See the multi-level makefile layout described in the answer to the
[above
question](#why-does-cmake-generate-recursive-makefiles "wikilink").
It's the first level's invocation of the second level that needs to be
re-worked and perhaps combined in order to fix this limitation. However,
the first level is what allows one to run "make" from any subdirectory
to build the targets there along with dependencies so there must be a
separate Makefile in each directory. Supporting both at the same time is
not a trivial change from the current design.

See issue tracker entry
[14312](https://gitlab.kitware.com/cmake/cmake/-/issues/14312). Any solution must work with ancient UNIX make and cannot depend on GNU make features.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_FAQ) in another wiki.