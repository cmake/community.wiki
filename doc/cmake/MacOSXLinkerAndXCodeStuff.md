# Building multiple configurations with Xcode

If you do release and debug builds with Xcode for frameworks, Xcode will
create completely separate frameworks for each configuration (e.g.
Release and Debug). This is how CMake supports different configurations
in Xcode.

There is an additional mechanism how this can be done. In the
configuration dialog "Build variants" can be created, the default
variant is "normal". When adding an additional name to this list, the
library will be built in an additional variant and the name will be
appended to the library name. E.g. if you add "profile" to the list,
additionally to the library "foolib" now also "foolib_profile" will be
created within the same framework. To get different settings/compiler
flags/etc. for this version, Xcode variables have to be defined for this
build variant. They have the same name as the normal Xcode variables,
but have again the name appended:

    BUILD_VARIANTS ="normal debug"
    OTHER_CFLAGS_debug=" -DMY_DEBUG=1 -DDEBUG=1 -gfull -O0 -fno-inline"
    OTHER_CFLAGS_normal=" -DMY_DEBUG=0 -DNODEBUG=1 -gfull -O3"

When building something which links to that framework, the normal name
is used when building. When that application starts, by default it will
load the normal variant. If the developer wants to use the "profile"
variant, he has to set the DYLD_IMAGE_SUFFIX environment variable
accordingly. So if DYLD_IMAGE_SUFFIX is set to "_profile", the
application will get "foolib_profile". The same mechanism is used in
the original OSX frameworks, there are "debug" variants of the
frameworks available. This means if DYLD_IMAGE_SUFFIX is set to
"_debug", an application will get the debug versions of the OS X libs
and the debug version of "foolib" (if it exists). If the developer wants
only debug versions of his own libs then he has to use a different
variant name.

# Versions in frameworks

Frameworks have version directories, usually version "A" with the
symbolic link "Current" pointing to it. When an application is linked to
a framework using "-framework Foo", then the link "Current" is used but
the link destination is recorded in the application, so that when the
application starts it doesn't matter to which version "Current" points.

If there are two versions, "A" and "B" in a framework, and "Current"
points to B, but a developer wants to build his application against
version "A", the dylib_file option has to be used for
    gcc:

    gcc -o Prog3 -dylib_file Foo.framework/Versions/B/Foo:Foo.framework/Versions/A/Foo -framework Foo

Alternatively the library inside the framework could be used directly:

    gcc -o Prog3 Foo.framework/Versions/A/Foo

# dylib_compatibility_version and dylib_current_version

dylib_current_version is the current version of a dyld library
(equivalent to the full versioned name of a so library).
dylib_compatibility_version is the version to which the current
version is compatible. I.e. the current version 1.2.3 could be
compatible down to version 1.0.0 (or any other version before it, e.g.
0.5.3). This is somewhat similar to the SOVERSION of an so library,
where a different SO version means a really different version of the
library, but with the two dyld versions this can be used much more fine
grained. So if an executable is linked against a dyld library with
compatibility version 1.0.0, but when executing the linker finds only a
current version 0.9.0 the app will not start.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:MacOSXLinkerAndXCodeStuff) in another wiki.
