CMake is a mature tool with many features, both big and small. Many go
unnoticed or are taken for granted. Help us create a comprehensive list
of features that make CMake really cool. Please list your addition under
the appropriate package: CMake, CTest, or CPack.

## CMake Features

  - Color output for make
  - Progress output for make
  - Incremental linking support with vs 8,9 and manifests
  - Supports out-of-tree builds
  - Auto-rerun of cmake if any cmake input files change (works with vs
    8, 9 using ide macros)
  - Auto depend information for C++, C, and Fortran
      - [Graphviz](http://graphviz.org/) output for visualizing
        dependency trees
  - Full support for library versions
  - Full cross platform install system
  - Generate project files for major IDEs: Visual Studio, Xcode,
    Eclipse, KDevelop
      - not tied to make, other portable generators like ant possible
  - Ability to add custom rules and targets
  - Compute link depend information, and chaining of dependent libraries
  - Works with parallel make and is fast, can build very large projects
    like KDE on build farms
  - make help, make foo.s, make foo.E, VERBOSE=1 make PROJECT/fast
  - Advanced RPATH handling, full support for all kinds of static/shared
    libs and plugins, no more cryptic foo.la libtool "libraries"
  - support for chrpath, i.e. changing the RPATH without need to
    actually link again
  - Works on many host operating systems (a full list would be good)
  - Supports many toolchains: GNU, MS, Borland, Sun, also e.g sdcc
  - Beta cross compiling, to Linux, Windows, eCos, supercomputers, no
    OS, from 8bit uCs to 64bit CPUs
  - Full dependencies: build a target in some directory, and everything
    this target depends on will be up to date
  - Extensive test suite and nightly builds/test on many platforms
  - modular design (e.g. the Find modules, language, toolchain and OS
    support files) \* \> easily extendable
  - just one tool instead of automake+autoconf+libtool+m4+shell+make
  - Good scripting language that supports:
      - control structures (conditional, iterative)
      - regular expressions, eliminating need for grep+awk+sed+perl
      - macros (similar to functions, with counted or vararg parameters)
      - portable commands for file and directory manipulation
  - Extensive auxiliary cmake modules for finding and simplifying use of
    popular libraries (boost, sdl, fltk, etc.) and utilities (swig,
    etc).
  - Comes with a GUI layer for easy edition of input variables, both
    Curses and QT based options.
  - Command line support
  - it's a native tool, windows devs don't have to deal with POSIX
    shells, OSX devs can continue to use XCode
  - .tar.gz archiving available on all platforms: no need to chase down
    tar/gzip for Windows
  - can create OSX library frameworks
  - can create OSX application bundles
  - scales well for really, really big projects like KDE

## CTest Features

  - Run all or sub-sets of tests for a project
  - Submit testing results to Dart 1,2 and CDash
  - Run tests that build and run --build-and-test command
  - support coverage with gcc, and bullseye coverage tools
  - support memory checking with valgrind, purify, and bounds checker

## CPack Features

  - Simple Declarative creation of packages/installers using CMake+CPack
  - Create Source or Binary packages
  - A wealth of supported formats:
      - Create professional windows installers with
        [NSIS](http://nsis.sourceforge.net/Main_Page)
      - Create tar.gz tar.Z on any platform
      - Create self extracting tar.gz .sh files
      - Create rpm
      - Create Debian .deb files
      - Create Cygwin setup packages

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/Really_Cool_CMake_Features) in another wiki.
