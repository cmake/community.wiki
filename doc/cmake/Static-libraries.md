Writing a FindXXX.cmake module that works with shared libraries is
relatively easy. You need only find the *interface* libraries and the
system will take care of resolving dependencies when the executable is
run. With static libraries, the linker must be provided with the entire
dependency graph. Determining the dependency graph is project-specific,
but common methods are wrapper compilers, Makefile systems, and
pkg-config. All of these provide a link line which includes all
dependencies. However, CMake does not accept link lines directly. A hack
is for the CMake module to parse the link line itself. To be truly
portable, the semantics of the underlying linker must be preserved.
Normally this means that each library pathh is added to a stack and each
library is searched in this stack and then in system directories. Every
module distributed with CMake that attempts to parse link lines gets
this wrong as of 2.6.3rc1 when [multiple
versions](doc/cmake/Multiple-versions "wikilink") of libraries are present.
Most FindXXX modules make no attempt to resolve dependencies, hence
require manual intervention when shared libraries are not available.

The
[ResolveCompilerPaths](http://github.com/jedbrown/cmake-modules/tree/master/ResolveCompilerPaths.cmake)
module provides macros RESOLVE_LIBRARIES and RESOLVE_INCLUDES which do
better, but still are not perfect. Example FindXXX modules which uses
these macros are
[FindPETSc.cmake](http://github.com/jedbrown/cmake-modules/tree/master/FindPETSc.cmake)
and
[FindiMesh.cmake](http://github.com/jedbrown/cmake-modules/tree/master/FindiMesh.cmake).

There is no way to ask FIND_LIBRARY to only find static libraries. See
[this](http://www.cmake.org/pipermail/cmake/2008-November/025090.html)
feature request.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:Static_libraries) in another wiki.
