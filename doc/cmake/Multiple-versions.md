When multiple ABI-incompatible versions of libraries are installed, it
is likely that existing FindXXX modules will not be able to find a
version in a nonstandard location, or will find a combination of
libraries that produces a broken executable. Some will work correctly if
XXX_DIR is set the first time CMake is run, but if the wrong version is
found on the first pass (perhaps because the user didn't know they had
to set this variable in advance), the only way (short of manually
editing a possibly huge number of advanced cache entries) to switch
versions is to delete the cache and try again.

A suggestion is for all packages to make all the advanced XXX_INCLUDES,
XXX_LIBRARIES, ... cache entries subordinate to an XXX_DIR cache entry
(which takes precedence over an XXX_DIR environment variable. When
XXX_DIR is set, it should take precedence over system search paths
(i.e. must use HINTS because PATHS is searched [too
late](http://public.kitware.com/Bug/view.php?id=8035)). If XXX_DIR is
changed, the advanced entries should be recomputed. CMake currently
makes this awkward, but the
[FindPackageMultipass](http://github.com/jedbrown/cmake-modules/tree/master/FindPackageMultipass.cmake)
module provides some help. For example usage, see
[FindPETSc.cmake](http://github.com/jedbrown/cmake-modules/tree/master/FindPETSc.cmake)
or
[FindiMesh.cmake](http://github.com/jedbrown/cmake-modules/tree/master/FindiMesh.cmake).
Since almost every package would be better with this behavior, maybe
something similar could be incorporated in CMake or at least a nicer
interface could be designed.

Handling multiple versions correctly with wrapper compilers, Makefile
includes, and pkg-config requires preserving the semantics of the
linker. See
[ResolveCompilerPaths](http://github.com/jedbrown/cmake-modules/tree/master/ResolveCompilerPaths.cmake)
and the discussion of [static
libraries](doc/cmake/Static-libraries "wikilink").

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:Multiple_versions) in another wiki.
