## Standard CMake Modules

Previous part: [A -
FindK\*](doc/cmake/version_compatibility_matrix/StandardCMakeModulesA "wikilink")

| CMake Option                   | 2.8.12   | 2.8.11   | 2.8.10   | 2.8.9    | 2.8.8     | 2.8.7           | 2.8.6    | 2.8.5    | 2.8.4    | 2.8.3    | 2.8.2    | 2.8.1    | 2.8.0     | 2.6-patch 4 | 2.6-patch 3     | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| ------------------------------ | -------- | -------- | -------- | -------- | --------- | --------------- | -------- | -------- | -------- | -------- | -------- | -------- | --------- | ----------- | --------------- | ----------- | ----------- | ----------- |
| FindLAPACK                     | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X \[1\]     |
| FindLATEX                      | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindLibArchive                 | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |
| FindLibLZMA                    | X        | X        | X        | X        | \-        | \-              | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |
| FindLibXml2                    | X        | X        | X        | X        | X         | X \[2\]         | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X \[3\]     | X           |
| FindLibXslt                    | X        | X        | X        | X        | X         | X \[4\]         | X \[5\]  | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindLua50                      | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X \[6\]     | X \[7\]     | X           |
| FindLua51                      | X        | X        | X        | X        | X         | X \[8\]         | X        | X        | X        | X        | X        | X        | X         | X           | X               | X \[9\]     | X \[10\]    | X           |
| FindMFC                        | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindMPEG                       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindMPEG2                      | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindMPI                        | X        | X        | X        | X        | X         | X               | X        | X \[11\] | X \[12\] | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindMatlab                     | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindMotif                      | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindOpenAL                     | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindOpenGL                     | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindOpenMP                     | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | \-          | \-          | \-          |
| FindOpenSSL                    | X        | X        | X        | X        | X         | X \[13\]        | X        | X        | X \[14\] | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindOpenSceneGraph             | X        | X        | X        | X \[15\] | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | \-          | \-          | \-          |
| FindOpenThreads                | X        | X        | X        | X \[16\] | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindPHP4                       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindPNG                        | X        | X        | X        | X        | X         | X \[17\] \[18\] | X \[19\] | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindPackageHandleStandardArgs  | X        | X        | X \[20\] | X        | X         | X \[21\]        | X        | X        | X        | X        | X \[22\] | X        | X         | X           | X               | X           | X           | X           |
| FindPackageMessage             | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindPerl                       | X        | X        | X        | X        | X         | X \[23\]        | X        | X        | X        | X        | X \[24\] | X        | X         | X           | X               | X           | X           | X           |
| FindPerlLibs                   | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X \[25\]    | X               | X           | X           | X           |
| FindPhysFS                     | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindPike                       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindPkgConfig                  | X        | X        | X        | X        | X         | X \[26\]        | X        | X        | X        | X        | X        | X \[27\] | X         | X           | X               | X           | X           | X           |
| FindPostgreSQL                 | X        | X        | X        | X        | X         | X \[28\]        | X        | X        | X        | X        | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |
| FindProducer                   | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindProtobuf                   | X        | X        | X        | X        | X         | X \[29\]        | X        | X        | X \[30\] | X        | X        | X        | X         | \-          | \-              | \-          | \-          | \-          |
| FindPythonInterp               | X        | X        | X        | X        | X         | X               | X        | X \[31\] | X        | X \[32\] | X        | X        | X         | X           | X               | X           | X           | X           |
| FindPythonLibs                 | X        | X        | X        | X        | X \[33\]  | X \[34\]        | X        | X        | X        | X \[35\] | X        | X        | X         | X \[36\]    | X               | X           | X           | X           |
| FindQt                         | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindQt3                        | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindQt4                        | X \[37\] | X \[38\] | X \[39\] | X \[40\] | X \[41\]  | X               | X        | X        | X        | X        | X \[42\] | X        | X \[43\]  | X \[44\]    | X \[45\] \[46\] | X \[47\]    | X \[48\]    | X           |
| FindQuickTime                  | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindRTI                        | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | \-          | \-          | \-          |
| FindRuby                       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X \[49\] | X        | X \[50\]  | X \[51\]    | X               | X           | X           | X           |
| FindSDL                        | X        | X        | X        | X \[52\] | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindSDL_image                 | X        | X        | X        | X \[53\] | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindSDL_mixer                 | X        | X        | X        | X \[54\] | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindSDL_net                   | X        | X        | X        | X \[55\] | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindSDL_sound                 | X        | X        | X        | X \[56\] | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindSDL_ttf                   | X        | X        | X        | X \[57\] | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindSWIG                       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X \[58\] | X        | X         | X           | X               | X           | X           | X           |
| FindSelfPackers                | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindSquish                     | X        | X \[59\] | X \[60\] | X        | X         | X               | X        | X        | X        | X        | X \[61\] | X        | X         | X           | X               | \-          | \-          | \-          |
| FindSubversion                 | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X \[62\] | X        | X         | X           | X               | X           | X           | X           |
| FindTCL                        | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindTIFF                       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindTclStub                    | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X \[63\]    | X           | X           |
| FindTclsh                      | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindThreads                    | X        | X        | X        | X        | X         | X               | X        | X        | X        | X \[64\] | X \[65\] | X \[66\] | X         | X           | X               | X           | X           | X           |
| FindUnixCommands               | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindVTK                        | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindWget                       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindWish                       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindX11                        | X        | X        | X        | X        | X         | X \[67\]        | X        | X \[68\] | X        | X        | X        | X \[69\] | X \[70\]  | X           | X               | X           | X           | X           |
| FindXMLRPC                     | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindZLIB                       | X        | X        | X        | X        | X         | X               | X \[71\] | X        | X        | X \[72\] | X \[73\] | X \[74\] | X         | X           | X               | X           | X           | X           |
| Findosg                        | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgAnimation               | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | \-          | \-          | \-          |
| FindosgDB                      | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgFX                      | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgGA                      | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgIntrospection           | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgManipulator             | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgParticle                | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgPresentation            | X        | X        | X        | X        | X         | \-              | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |
| FindosgProducer                | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgQt                      | X        | X        | X        | X        | X         | \-              | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |
| FindosgShadow                  | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgSim                     | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgTerrain                 | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgText                    | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgUtil                    | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgViewer                  | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FindosgVolume                  | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | \-          | \-          | \-          |
| FindosgWidget                  | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | \-          | \-          | \-          |
| Findosg_functions             | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | \-          | \-          | \-          |
| FindwxWidgets                  | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X \[75\] | X \[76\] | X         | X \[77\]    | X               | X           | X           | X           |
| FindwxWindows                  | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| FortranCInterface              | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X \[78\]  | X \[79\]    | X               | \-          | \-          | \-          |
| GNUInstallDirs                 | X        | X        | X        | X        | X         | X               | X        | X        | \-       | \-       | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |
| GenerateExportHeader           | X        | X        | X        | X        | X         | X               | X        | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |
| GetPrerequisites               | X        | X        | X \[80\] | X        | X         | X               | X \[81\] | X        | X        | X        | X        | X        | X         | X \[82\]    | X               | X           | X \[83\]    | X           |
| ITKCompatibility               | \-       | \-       | \-       | \-       | \-        | \-              | \-       | \-       | \-       | \-       | \-       | \-       | \- \[84\] | X           | X               | X           | X           | X           |
| InstallRequiredSystemLibraries | X        | X        | X        | X        | X         | X               | X        | X        | X        | X \[85\] | X        | X        | X         | X           | X               | X           | X           | X           |
| MacroAddFileDependencies       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X \[86\]    | X               | X           | X           | X           |
| ProcessorCount                 | X        | X        | X        | X        | X         | X               | X        | X        | \-       | \-       | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |
| Qt4ConfigDependentSettings     | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | \-        | \-          | \-              | \-          | \-          | \-          |
| Qt4Macros                      | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | \-        | \-          | \-              | \-          | \-          | \-          |
| SelectLibraryConfigurations    | X        | X \[87\] | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | \-          | \-              | \-          | \-          | \-          |
| SquishTestScript               | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | \-          | \-          | \-          |
| TestBigEndian                  | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| TestCXXAcceptsFlag             | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| TestForANSIForScope            | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| TestForANSIStreamHeaders       | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| TestForSSTREAM                 | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| TestForSTDNamespace            | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| UseEcos                        | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| UseJava                        | X        | X \[88\] | X        | X        | X         | X               | X        | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |
| UsePkgConfig                   | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| UseQt4                         | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| UseSWIG                        | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| Use_wxWindows                 | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| UsewxWidgets                   | X        | X        | X        | X        | X         | X               | X        | X        | X        | X        | X        | X        | X         | X           | X               | X           | X           | X           |
| WriteBasicConfigVersionFile    | \-       | \-       | \-       | \-       | \- \[89\] | X               | X        | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-              | \-          | \-          | \-          |

Previous part: [A -
FindK\*](doc/cmake/version_compatibility_matrix/StandardCMakeModulesA "wikilink")

### Footnotes

<references/>

1.  missing: LAPACK95_FOUND, BLA_STATIC, BLA_VENDOR, BLA_F95
2.  missing: LIBXML2_VERSION_STRING
3.  missing: LIBXML2_XMLLINT_EXECUTABLE
4.  missing: LIBXSLT_VERSION_STRING
5.  missing: variables LIBXSLT_EXSLT_LIBRARIES,
    LIBXSLT_XSLTPROC_EXECUTABLE
6.  deprecated: LUA_FOUND
7.  missing: LUA50_FOUND
8.  missing: LUA_VERSION_STRING
9.  deprecated: LUA_FOUND
10. missing: LUA51_FOUND
11. deprecated: MPI_FOUND, MPI_COMPILE_FLAGS, MPI_LINK_FLAGS,
    MPI_COMPILER, MPI_INCLUDE_PATH, MPI_LIBRARIES, MPI_LIBRARY,
    MPI_EXTRA_LIBRARY
12. missing: MPI_<lang>_FOUND, MPI_<lang>_COMPILER,
    MPI_<lang>_COMPILE_FLAGS, MPI_<lang>_INCLUDE_PATH,
    MPI_<lang>_LINK_FLAGS, MPI_<lang>_LIBRARIES
13. missing: $path component in OPENSSL_VERSION
14. missing: OPENSSL_VERSION
15. missing: OSG_DIR as (not environment-)variable
16. missing: OSG_DIR, OPENTHREADS_DIR as (not environment-)variable
17. deprecated: variable PNG_INCLUDE_DIR
18. missing: PNG_VERSION_STRING
19. missing: variable PNG_INCLUDE_DIRS
20. missing: option FOUND_VAR
21. missing: option HANDLE_COMPONENTS
22. missing: complex mode(version support, config-mode, fail-message)
23. missing: PERL_VERSION_STRING
24. missing: find_package version support
25. missing: PERLLIBS_FOUND, PERL_LIBRARY, PERL_SITESEARCH,
    PERL_SITELIB, PERL_VENDORARCH, PERL_VENDORLIB, PERL_ARCHLIB,
    PERL_PRIVLIB, PERL_EXTRA_C_FLAGS
26. missing: PKG_CONFIG_VERSION_STRING, PKG_CONFIG_FOUND
27. missing: option QUIET
28. missing: PostgreSQL_VERSION_STRING
29. missing: PROTOBUF_IMPORT_DIRS
30. missing: PROTOBUF_SRC_ROOT_FOLDER, PROTOBUF_PROTOC_LIBRARIES,
    PROTOBUF_LITE_LIBRARIES, PROTOBUF_LIBRARY_DEBUG,
    PROTOBUF_PROTOC_LIBRARY_DEBUG, PROTOBUF_LITE_LIBRARY,
    PROTOBUF_LITE_LIBRARY_DEBUG
31. missing: PYTHON_VERSION_STRING, PYTHON_VERSION_MAJOR,
    PYTHON_VERSION_MINOR, PYTHON_VERSION_PATCH
32. missing: Python_ADDITIONAL_VERSIONS
33. deprecated: PYTHON_DEBUG_LIBRARIES
34. missing: PYTHONLIBS_VERSION_STRING
35. missing: Python_ADDITIONAL_VERSIONS
36. missing: PYTHON_INCLUDE_DIRS
37. deprecated: macro qt4_automoc (use the CMAKE_AUTOMOC feature
    instead), function qt4_use_modules (Use target_link_libraries
    with IMPORTED targets instead.)
38. deprecated: QT4_FOUND
39. missing: variable QT_INCLUDE_DIRS_NO_SYSTEM
40. missing: function QT4_USE_MODULES
41. missing: support for properties AUTOMOC, INCLUDE, CLASSNAME,
    NO_NAMESPACE
42. missing: QT_IMPORTS_DIR
43. missing: QT_USE_IMPORTED_TARGETS, QT_USE_QTMULTIMEDIA,
    QT_USE_QTDECLARATIVE, QT_QTMULTIMEDIA_\*, QT_QTDECLARATIVE_\*,
    QT_DESIGNER_EXECUTABLE, QT_LINGUIST_EXECUTABLE
44. missing: find_package version support, QT_USE_QTSCRIPTTOOLS,
    QT_QTSCRIPTTOOLS_\*, QT_MAC_USE_COCOA; QT4_ADD_DBUS_ADAPTOR:
    classname support; changed: QT4_GENERATE_DBUS_INTERFACE: OPTIONS
45. missing: find_package components support
46. deprecated: QT_QT_INCLUDE_DIR
47. missing: QT_USE_QAXCONTAINER, QT_USE_QAXSERVER,
    QT_QAXCONTAINER_\*, QT_QAXSERVER_\*, QT_BINARY_DIR,
    QT_TRANSLATIONS_DIR
48. changed: QT4_CREATE_TRANSLATION: directories, OPTIONS
49. missing: find_package version support
50. deprecated: RUBY_INCLUDE_PATH
51. missing: RUBY_INCLUDE_DIRS, RUBY_VERSION, RUBY_FOUND
52. missing: variable SDL_VERSION_STRING
53. missing: variable SDL_VERSION_STRING
54. missing: variable SDL_VERSION_STRING
55. missing: variable SDL_VERSION_STRING
56. missing: variable SDL_VERSION_STRING
57. missing: variable SDL_VERSION_STRING
58. missing: find_package version support
59. deprecated: macro SQUISH_ADD_TEST
60. missing: macro squish_v3_add_test, squish_v4_add_test
61. missing: SQUISH_VERSION_\*
62. missing: SUBVERSION_FOUND; missing: find_package version support
63. missing: TTK_STUB_LIBRARY
64. deprecated: CMAKE_THREAD_PREFER_PTHREADS
65. missing: CMAKE_THREAD_PREFER_PTHREAD
66. missing: CMAKE_THREAD_PREFER_PTHREADS
67. missing: X11_Xxf86vm_LIB, X11_Xmu_INCLUDE_PATH, X11_Xmu_LIB,
    X11_Xmu_FOUND
68. missing: X11_SM_INCLUDE_PATH, X11_SM_LIB, X11_SM_FOUND,
    X11_Xkbfile_INCLUDE_PATH, X11_Xkbfile_LIB, X11_Xkbfile_FOUND,
    X11_XSync_INCLUDE_PATH, (inX11_Xext_LIB), X11_XSync_FOUND
69. missing: Xi support
70. missing: X11_X11_INCLUDE_PATH, X11_X11_LIB
71. missing: ZLIB_ROOT
72. deprecated: ZLIB_\*_VERSION
73. missing: ZLIB_VERSION_MAJOR, ZLIB_VERSION_MINOR,
    ZLIB_VERSION_PATCH
74. missing: ZLIB_VERSION_STRING, ZLIB_\*_VERSION
75. missing: wxWidgets_CONFIG_OPTIONS
76. missing: wxWidgets_EXCLUDE_COMMON_LIBRARIES
77. missing: wxWidgets_DEFINITIONS_DEBUG
78. deprecated: test_fortran_mangling, discover_fortran_mangling,
    discover_fortran_module_mangling
79. missing: FortranCInterface_GLOBAL_FOUND,
    FortranCInterface_MODULE_FOUND, FortranCInterface_HEADER,
    FortranCInterface_VERIFY
80. missing in add_jar: support for SOURCES, INCLUDE_JARS
81. missing: exepath
82. missing: gp_resolved_file_type
83. missing: gp_item_default_embedded_path, gp_resolve_item
84. deprecated
85. various behaviour changes between 2.8.3 and 2.8.4, please refer to
    documentation
86. missing: QUIT
87. change: when only one of basename_LIBRARY_DEBUG and
    basename_LIBRARY_RELEASE is set, older versions also set the
    missing variable. Newer versions (2.8.12) set the missing variable
    to NOTFOUND
88. missing: ENTRY_POINT, VERSION, OUTPUT_NAME, OUTPUT_DIR
89. deprecated, use WRITE_BASIC_PACKAGE_VERSION_FILE in
    CMakePackageConfigHelpers instead.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/StandardCMakeModulesFindL) in another wiki.
