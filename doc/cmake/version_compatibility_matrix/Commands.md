## Commands

| CMake Option                     | 2.8.12   | 2.8.11   | 2.8.10   | 2.8.9    | 2.8.8    | 2.8.7    | 2.8.6    | 2.8.5     | 2.8.4    | 2.8.3    | 2.8.2    | 2.8.1    | 2.8.0    | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| -------------------------------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | --------- | -------- | -------- | -------- | -------- | -------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| add_compile_options            | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-       | \-       | \-       | \-       | \-       | \-          | \-          | \-          | \-          | \-          |
| add_custom_command             | X        | X \[1\]  | X \[2\]  | X \[3\]  | X        | X        | X        | X         | X        | X \[4\]  | X        | X        | X        | X           | X           | X           | X           | X           |
| add_custom_target              | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X \[5\]     | X           | X           | X           |
| add_definitions                 | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| add_dependencies                | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| add_executable                  | X        | X \[6\]  | X        | X        | X        | X \[7\]  | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| add_library                     | X        | X \[8\]  | X        | X        | X        | X \[9\]  | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X \[10\]    | X           |
| add_subdirectory                | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| add_test                        | X        | X \[11\] | X \[12\] | X \[13\] | X        | X        | X        | X         | X        | X \[14\] | X        | X        | X        | X           | \-          | X           | X           | X           |
| aux_source_directory           | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| break                            | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| build_command                   | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X \[15\] | X           | X           | X           | X           | X           |
| cmake_host_system_information | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-       | \-       | \-       | \-       | \-       | \-          | \-          | \-          | \-          | \-          |
| cmake_minimum_required         | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X \[16\] | X        | X           | X           | X           | X           | X           |
| cmake_policy                    | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X \[17\]    | X           |
| configure_file                  | X        | X        | X        | X        | X        | X        | X \[18\] | X         | X        | X        | X        | X \[19\] | X        | X           | X           | X           | X           | X           |
| create_test_sourcelist         | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| define_property                 | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| else                             | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| elseif                           | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| enable_language                 | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| enable_testing                  | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| endforeach                       | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| endfunction                      | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| endif                            | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| endmacro                         | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| endwhile                         | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| execute_process                 | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| export                           | X        | X \[20\] | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X \[21\]    | X           | X           | X           | X           |
| file                             | X        | X \[22\] | X \[23\] | X \[24\] | X        | X        | X \[25\] | X         | X \[26\] | X        | X        | X        | X        | X \[27\]    | X           | X           | X \[28\]    | X           |
| find_file                       | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X \[29\]    |
| find_library                    | X        | X        | X \[30\] | X        | X        | X        | X        | X         | X \[31\] | X        | X        | X        | X        | X           | X           | X           | X           | X \[32\]    |
| find_package                    | X        | X        | X        | X        | X        | X \[33\] | X        | X \[34\]  | X \[35\] | X        | X \[36\] | X        | X \[37\] | X \[38\]    | X \[39\]    | X           | X \[40\]    | X \[41\]    |
| find_path                       | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X \[42\]    |
| find_program                    | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X \[43\]    |
| fltk_wrap_ui                   | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| foreach                          | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X \[44\]    | X           | X           | X           | X           |
| function                         | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| get_cmake_property             | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| get_directory_property         | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| get_filename_component         | X \[45\] | X \[46\] | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X \[47\]    | X           | X           | X           |
| get_property                    | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X \[48\]    | X           | X           | X           | X           |
| get_source_file_property      | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| get_target_property            | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| get_test_property              | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| if                               | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X \[49\] | X \[50\]    | X           | X           | X \[51\]    | X           |
| include                          | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X \[52\]    | X           | X           | X           |
| include_directories             | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| include_external_msproject     | X        | X        | X        | X        | X \[53\] | X        | X        | X         | X        | X        | X        | X \[54\] | X        | X           | X           | X           | X           | X           |
| include_regular_expression     | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| install                          | X        | X \[55\] | X        | X        | X \[56\] | X        | X        | X         | X        | X        | X        | X        | X        | X \[57\]    | X           | X           | X           | X           |
| link_directories                | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| list                             | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| load_cache                      | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| load_command                    | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| macro                            | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| mark_as_advanced               | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| math                             | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| message                          | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X \[58\]    | X           | X           | X           | X           |
| option                           | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| output_required_files          | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \- \[59\] | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| project                          | X        | X        | X        | X        | X \[60\] | X \[61\] | X        | X         | X        | X        | X        | X        | X        | X           | X \[62\]    | X           | X           | X           |
| qt_wrap_cpp                    | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| qt_wrap_ui                     | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| remove_definitions              | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| return                           | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X \[63\]    | X           |
| separate_arguments              | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X \[64\]    | X           | X           | X           | X           |
| set                              | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| set_directory_properties       | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| set_property                    | X        | X        | X        | X        | X        | X        | X        | X \[65\]  | X        | X        | X        | X        | X        | X \[66\]    | X           | X           | X           | X           |
| set_source_files_properties   | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| set_target_properties          | X        | X        | X        | X        | X        | X        | X \[67\] | X \[68\]  | X        | X        | X        | X        | X        | X           | X \[69\]    | X           | X           | X           |
| set_tests_properties           | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| site_name                       | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| source_group                    | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| string                           | X        | X \[70\] | X \[71\] | X        | X        | X        | X \[72\] | X         | X \[73\] | X        | X        | X        | X        | X \[74\]    | X           | X           | X           | X           |
| target_compile_definitions     | X        | X \[75\] | \-       | \-       | \-       | \-       | \-       | \-        | \-       | \-       | \-       | \-       | \-       | \-          | \-          | \-          | \-          | \-          |
| target_compile_options         | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-       | \-       | \-       | \-       | \-       | \-          | \-          | \-          | \-          | \-          |
| target_include_directories     | X        | X \[76\] | \-       | \-       | \-       | \-       | \-       | \-        | \-       | \-       | \-       | \-       | \-       | \-          | \-          | \-          | \-          | \-          |
| target_link_libraries          | X        | X \[77\] | X \[78\] | X        | X        | X        | X \[79\] | X         | X        | X        | X        | X        | X        | X           | X           | X           | X \[80\]    | X           |
| try_compile                     | X        | X \[81\] | X \[82\] | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| try_run                         | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| unset                            | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | \-          | \-          | \-          |
| variable_watch                  | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |
| while                            | X        | X        | X        | X        | X        | X        | X        | X         | X        | X        | X        | X        | X        | X           | X           | X           | X           | X           |

### Footnotes

<references/>

1.  missing: generator expressions `JOIN`, `C_COMPILER_ID`,
    `CXX_COMPILER_ID`, `VERSION_GREATER`, `VERSION_EQUAL`,
    `VERSION_LESS`, `C_COMPILER_VERSION`, `CXX_COMPILER_VERSION`
2.  missing: generator expressions `SEMICOLON`, `TARGET_NAME`,
    `INSTALL_INTERFACE`, `BUILD_INTERFACE`, `TARGET_POLICY`,
    `INSTALL_PREFIX`
3.  missing: generator expressions `0`, `1`, `AND`, `OR`, `NOT`,
    `CONFIG`, `BOOL`, `STREQUAL`, `ANGLE-R`, `COMMA`, `TARGET_PROPERTY`
4.  missing: generator expressions
5.  missing: option SOURCES
6.  missing: ALIAS signature
7.  missing: option GLOBAL
8.  missing: ALIAS signature
9.  missing: options GLOBAL, OBJECT
10. missing: add library type UNKNOWN
11. missing: generator expressions `JOIN`, `C_COMPILER_ID`,
    `CXX_COMPILER_ID`, `VERSION_GREATER`, `VERSION_EQUAL`,
    `VERSION_LESS`, `C_COMPILER_VERSION`, `CXX_COMPILER_VERSION`
12. missing: generator expressions `SEMICOLON`, `TARGET_NAME`,
    `INSTALL_INTERFACE`, `BUILD_INTERFACE`, `TARGET_POLICY`,
    `INSTALL_PREFIX`
13. missing: generator expressions `0`, `1`, `AND`, `OR`, `NOT`,
    `CONFIG`, `BOOL`, `STREQUAL`, `ANGLE-R`, `COMMA`, `TARGET_PROPERTY`
14. missing: option WORKING_DIRECTORY
15. missing: supports options CONFIGURATION, PROJECT_NAME, and TARGET
16. missing: optional .tweak to version
17. missing: support GET
18. missing: option NEWLINE_STYLE
19. missing: DOWNLOAD supports options EXPECTED_MD5 and SHOW_PROGRESS
20. missing option: EXPORT_LINK_INTERFACE_LIBRARIES
21. missing: PACKAGE
22. missing: GENERATE
23. missing: TIMESTAMP
24. missing: parameters EXPECTED_HASH, TLS_VERIFY, TLS_CAINFO
25. missing: hash functions MD5,SHA1,SHA224,SHA256,SHA384,SHA512
26. missing: DOWNLOAD supports INACTIVITY_TIMEOUT; UPLOAD
27. missing: RENAME, COPY, INSTALL
28. missing: GLOB_RECURSE supports FOLLOW_SYMLINKS
29. missing: option HINTS
30. missing: option NAMES_PER_DIR
31. missing: support for CMAKE_LIBRARY_ARCHITECTURE
32. missing: option HINTS
33. missing: options CONFIG, MODULE, OPTIONAL_COMPONENTS
34. missing: support for CMAKE_DISABLE_FIND_PACKAGE_<package>
35. missing: option NO_CMAKE_SYSTEM_PACKAGE_REGISTRY; support for
    CMAKE_LIBRARY_ARCHITECTURE
36. missing: sets variable <package>_CONSIDERED_CONFIGS
37. change: doesn't search in <package>_DIR when considering
    CMAKE_ENVIRONMENT_PATH
38. missing: option NO_CMAKE_PACKAGE_REGISTRY
39. missing: option NO_POLICY_SCOPE; change: don't continue, if
    <package>_DIR is set incorrectly; doesn't check
    PACKAGE_VERSION_UNSUITABLE; fewer search-paths
40. missing: sets variable PACKAGE_FIND_VERSION_TWEAK and
    PACKAGE_FIND_VERSION_COUNT
41. missing: option HINTS
42. missing: option HINTS
43. missing: option HINTS
44. missing: IN LISTS, IN ITEMS
45. deprecated: PATH
46. missing: DIRECTORY
47. missing: option REALPATH
48. missing: CACHE
49. missing: IS_SYMLINK
50. missing: allow constants, parenthesis
51. missing: TARGET, VERSION_LESS, VERSION_EQUAL, VERSION_GREATER
52. missing: option NO_POLICY_SCOPE
53. missing: parameters TYPE, GUID and PLATFORM
54. change: prefix created target with "INCLUDE_EXTERNAL_MSPROJECT_"
55. missing: INCLUDES DESTINATION, EXPORT_LINK_INTERFACE_LIBRARIES
56. change: newer versions add targets to a default component if no
    component is specified.
57. missing: DIRECTORY supports OPTIONAL
58. missing: WARNING, AUTHOR_WARNING
59. deprecated
60. missing: variable CMAKE_PROJECT_<projectName>_INCLUDE;
    deprecated: variable CMAKE_PROJECT_<projectName>_INCLUDE_FILE
61. missing: variable CMAKE_PROJECT_<projectName>_INCLUDE_FILE
62. missing: custom languages supported
63. missing: returns from file
64. missing: UNIX_COMMAND, WINDOWS_COMMAND
65. missing: APPEND_STRING
66. missing: CACHE
67. missing: support for VS_SCC_AUXPATH
68. missing: support for VS_GLOBAL_<variable>
69. missing: support VS_SCC_PROJECTNAME, VS_SCC_LOCALPATH,
    VS_SCC_PROVIDER
70. missing: MAKE_C_IDENTIFIER
71. missing: TIMESTAMP
72. missing: hash functions MD5,SHA1,SHA224,SHA256,SHA384,SHA512
73. missing: FIND
74. missing: RANDOM supports RANDOM_SEED
75. missing generator expressions: `JOIN`, `TARGET_NAME`,
    `INSTALL_INTERFACE`, `BUILD_INTERFACE`, `C_COMPILER_ID`,
    `CXX_COMPILER_ID`, `VERSION_GREATER`, `VERSION_LESS`,
    `VERSION_EQUAL`, `C_COMPILER_VERSION`, `CXX_COMPILER_VERSION`,
    `TARGET_FILE`, `TARGET_LINKER_FILE`, `TARGET_SONAME_FILE`,
    `TARGET_FILE_DIR`, `TARGET_LINKER_FILE_DIR`,
    `TARGET_SONAME_FILE_DIR`, `TARGET_PROPERTY`, `TARGET_POLICY`,
    `INSTALL_PREFIX`, `AND`, `OR`, `NOT`
76. missing option: SYSTEM; missing: generator expressions `JOIN`,
    `C_COMPILER_ID`, `CXX_COMPILER_ID`, `VERSION_GREATER`,
    `VERSION_EQUAL`, `VERSION_LESS`, `C_COMPILER_VERSION`,
    `CXX_COMPILER_VERSION`
77. missing: generator expressions `JOIN`, `C_COMPILER_ID`,
    `CXX_COMPILER_ID`, `VERSION_GREATER`, `VERSION_EQUAL`,
    `VERSION_LESS`, `C_COMPILER_VERSION`, `CXX_COMPILER_VERSION`
78. missing: support for properties INTERFACE_COMPILE_DEFINITONS,
    INTERFACE_INCLUDE_DIRECTORIES,
    INTERFACE_POSITION_INDEPENDENT_CODE, support for generator
    expressions
79. missing: modes LINK_PUBLIC and LINK_PRIVATE
80. missing: option LINK_INTERFACE_LIBRARIES
81. missing: SOURCES, COPY_FILE_ERROR
82. missing: parameter LINK_LIBRARIES

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Commands) in another wiki.
