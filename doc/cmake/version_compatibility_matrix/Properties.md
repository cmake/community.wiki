## Properties

  - [Properties of Global
    Scope](doc/cmake/version_compatibility_matrix/Properties-of-Global-Scope "wikilink")
  - [Properties on
    Directories](doc/cmake/version_compatibility_matrix/Properties-on-Directories "wikilink")
  - [Properties on
    Targets](doc/cmake/version_compatibility_matrix/Properties-on-Targets "wikilink")
  - [Properties on
    Tests](doc/cmake/version_compatibility_matrix/Properties-on-Tests "wikilink")
  - [Properties on Source
    Files](doc/cmake/version_compatibility_matrix/Properties-on-Source-Files "wikilink")
  - [Properties on Cache
    Entries](doc/cmake/version_compatibility_matrix/Properties-on-Cache-Entries "wikilink")

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Properties) in another wiki.
