## Variables that Provide Information

| CMake Option                            | 2.8.12 | 2.8.11 | 2.8.10   | 2.8.9 | 2.8.8 | 2.8.7 | 2.8.6   | 2.8.5 | 2.8.4 | 2.8.3 | 2.8.2 | 2.8.1 | 2.8.0    | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| --------------------------------------- | ------ | ------ | -------- | ----- | ----- | ----- | ------- | ----- | ----- | ----- | ----- | ----- | -------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| CMAKE_AR                               | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_ARGC                             | X      | X      | X        | X     | X     | X     | X       | X     | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| CMAKE_ARGV0                            | X      | X      | X        | X     | X     | X     | X       | X     | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| CMAKE_BINARY_DIR                      | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_BUILD_TOOL                      | X      | X      | X        | X     | X     | X     | X \[1\] | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CACHEFILE_DIR                   | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CACHE_MAJOR_VERSION            | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CACHE_MINOR_VERSION            | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CACHE_PATCH_VERSION            | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| CMAKE_CACHE_RELEASE_VERSION          | \-     | \-     | \-       | \-    | \-    | \-    | \-      | \-    | \-    | \-    | \-    | \-    | \- \[2\] | X           | X           | X           | X           | X           |
| CMAKE_CFG_INTDIR                      | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_COMMAND                          | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CROSSCOMPILING                   | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CTEST_COMMAND                   | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CURRENT_BINARY_DIR             | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CURRENT_LIST_DIR               | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| CMAKE_CURRENT_LIST_FILE              | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CURRENT_LIST_LINE              | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_CURRENT_SOURCE_DIR             | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_DL_LIBS                         | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_EDIT_COMMAND                    | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_EXECUTABLE_SUFFIX               | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_EXTRA_GENERATOR                 | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | \-       | \-          | \-          | \-          | \-          | \-          |
| CMAKE_EXTRA_SHARED_LIBRARY_SUFFIXES | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| CMAKE_GENERATOR                        | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_GENERATOR_TOOLSET               | X      | X      | \-       | \-    | \-    | \-    | \-      | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| CMAKE_HOME_DIRECTORY                  | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_IMPORT_LIBRARY_PREFIX          | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_IMPORT_LIBRARY_SUFFIX          | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_LINK_LIBRARY_SUFFIX            | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_MAJOR_VERSION                   | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_MAKE_PROGRAM                    | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_MINIMUM_REQUIRED_VERSION       | X      | \-     | \-       | \-    | \-    | \-    | \-      | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| CMAKE_MINOR_VERSION                   | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_PARENT_LIST_FILE               | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_PATCH_VERSION                   | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | \-          | \-          | \-          |
| CMAKE_PROJECT_NAME                    | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_RANLIB                           | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_ROOT                             | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_SCRIPT_MODE_FILE               | X      | X      | X        | X     | X     | X     | X       | X     | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| CMAKE_SHARED_LIBRARY_PREFIX          | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_SHARED_LIBRARY_SUFFIX          | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_SHARED_MODULE_PREFIX           | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_SHARED_MODULE_SUFFIX           | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_SIZEOF_VOID_P                  | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_SKIP_RPATH                      | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_SOURCE_DIR                      | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_STANDARD_LIBRARIES              | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_STATIC_LIBRARY_PREFIX          | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_STATIC_LIBRARY_SUFFIX          | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_TWEAK_VERSION                   | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| CMAKE_USING_VC_FREE_TOOLS           | \-     | \-     | \- \[3\] | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_VERBOSE_MAKEFILE                | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CMAKE_VERSION                          | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | \-          | \-          | \-          |
| CMAKE_VS_PLATFORM_TOOLSET            | X      | X      | X        | \-    | \-    | \-    | \-      | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| CMAKE_XCODE_PLATFORM_TOOLSET         | X      | X      | \-       | \-    | \-    | \-    | \-      | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| PROJECT_BINARY_DIR                    | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| PROJECT_NAME                           | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| PROJECT_SOURCE_DIR                    | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| \[Project name\]_BINARY_DIR           | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| \[Project name\]_SOURCE_DIR           | X      | X      | X        | X     | X     | X     | X       | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |

### Footnotes

<references/>

1.  missing: CMAKE_BUILD_TOOL
2.  deprecated
3.  deprecated

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Variables_that_Provide_Information) in another wiki.
