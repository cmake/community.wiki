## Variables for Languages

| CMake Option                                          | 2.8.12 | 2.8.11 | 2.8.10 | 2.8.9 | 2.8.8 | 2.8.7 | 2.8.6 | 2.8.5 | 2.8.4 | 2.8.3 | 2.8.2 | 2.8.1 | 2.8.0 | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| ----------------------------------------------------- | ------ | ------ | ------ | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----------- | ----------- | ----------- | ----------- | ----------- |
| CMAKE_<LANG>_ARCHIVE_APPEND                        | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_ARCHIVE_CREATE                        | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_ARCHIVE_FINISH                        | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_COMPILER                               | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_COMPILER_ABI                          | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_COMPILER_ID                           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_COMPILER_LOADED                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| CMAKE_<LANG>_COMPILER_VERSION                      | X      | X      | X      | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_<LANG>_COMPILE_OBJECT                        | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_FLAGS                                  | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_<LANG>_CREATE_SHARED_LIBRARY                | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_CREATE_SHARED_MODULE                 | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_CREATE_STATIC_LIBRARY                | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_FLAGS_DEBUG                           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_FLAGS_MINSIZEREL                      | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_FLAGS_RELEASE                         | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_FLAGS_RELWITHDEBINFO                  | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_IGNORE_EXTENSIONS                     | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_IMPLICIT_INCLUDE_DIRECTORIES         | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | \-          | \-          | \-          | \-          |
| CMAKE_<LANG>_IMPLICIT_LINK_DIRECTORIES            | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| CMAKE_<LANG>_IMPLICIT_LINK_FRAMEWORK_DIRECTORIES | X      | X      | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_<LANG>_IMPLICIT_LINK_LIBRARIES              | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| CMAKE_<LANG>_LIBRARY_ARCHITECTURE                  | X      | X      | X      | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_<LANG>_LINKER_PREFERENCE                     | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X \[1\]     | X           | X           | X           | X           |
| CMAKE_<LANG>_LINKER_PREFERENCE_PROPAGATES         | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| CMAKE_<LANG>_LINK_EXECUTABLE                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_OUTPUT_EXTENSION                      | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_PLATFORM_ID                           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_SIZEOF_DATA_PTR                      | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_<LANG>_SOURCE_FILE_EXTENSIONS               | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_COMPILER_IS_GNU<LANG>                        | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_Fortran_MODDIR_DEFAULT                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_Fortran_MODDIR_FLAG                          | X      | X      | X      | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_Fortran_MODOUT_FLAG                          | X      | X      | X      | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_INTERNAL_PLATFORM_ABI                        | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_USER_MAKE_RULES_OVERRIDE_<LANG>            | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |

### Footnotes

<references/>

1.  changed semantics

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Variables_for_Languages) in another wiki.
