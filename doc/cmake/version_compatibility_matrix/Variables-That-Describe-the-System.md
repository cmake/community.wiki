## Variables That Describe the System

| CMake Option                        | 2.8.12 | 2.8.11 | 2.8.10 | 2.8.9 | 2.8.8 | 2.8.7 | 2.8.6 | 2.8.5 | 2.8.4 | 2.8.3 | 2.8.2 | 2.8.1 | 2.8.0 | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| ----------------------------------- | ------ | ------ | ------ | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----------- | ----------- | ----------- | ----------- | ----------- |
| APPLE                               | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| BORLAND                             | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_CL_64                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_COMPILER_2005               | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_HOST_APPLE                  | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_HOST_SYSTEM                 | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_HOST_SYSTEM_NAME           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_HOST_SYSTEM_PROCESSOR      | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_HOST_SYSTEM_VERSION        | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_HOST_UNIX                   | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_HOST_WIN32                  | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_LIBRARY_ARCHITECTURE        | X      | X      | X      | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_LIBRARY_ARCHITECTURE_REGEX | X      | X      | X      | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_OBJECT_PATH_MAX            | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | \-          | \-          |
| CMAKE_SYSTEM                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_SYSTEM_NAME                 | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_SYSTEM_PROCESSOR            | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_SYSTEM_VERSION              | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CYGWIN                              | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| ENV                                 | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| MSVC                                | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| MSVC10                              | X      | X      | X      | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| MSVC11                              | X      | X      | X      | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| MSVC12                              | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| MSVC60                              | X      | X      | X      | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| MSVC70                              | X      | X      | X      | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| MSVC71                              | X      | X      | X      | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| MSVC80                              | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| MSVC90                              | X      | X      | X      | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| MSVC_IDE                           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| MSVC_VERSION                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| UNIX                                | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| WIN32                               | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| XCODE_VERSION                      | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |

### Footnotes

<references/>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Variables_That_Describe_the_System) in another wiki.
