## Standard CMake Modules

Next part: [FindL\* -
Z](doc/cmake/version_compatibility_matrix/StandardCMakeModulesFindL "wikilink")

| CMake Option                     | 2.8.12   | 2.8.11   | 2.8.10   | 2.8.9    | 2.8.8    | 2.8.7    | 2.8.6    | 2.8.5    | 2.8.4    | 2.8.3    | 2.8.2    | 2.8.1    | 2.8.0     | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| -------------------------------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | --------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| AddFileDependencies              | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X \[1\]  | X        | X         | X           | X           | X           | X           | X           |
| BundleUtilities                  | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | \-          | \-          |
| CMakeAddFortranSubdirectory      | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CMakeASM-ATTInformation          | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \- \[2\]  | X           | X           | \-          | \-          | \-          |
| CMakeASM_MASMInformation        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \- \[3\]  | X           | X           | \-          | \-          | \-          |
| CMakeBackwardCompatibilityCXX    | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CMakeDependentOption             | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CMakeDetermineASM-ATTCompiler    | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \- \[4\]  | X           | X           | X           | X           | X           |
| CMakeDetermineASMCompiler        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \- \[5\]  | X           | X           | X           | X           | X           |
| CMakeDetermineASM_MSASMCompiler | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \- \[6\]  | X           | X           | \-          | \-          | \-          |
| CMakeDetermineVSServicePack      | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | \-          | \-          | \-          | \-          | \-          |
| CMakeExpandImportedTargets       | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CMakeExportBuildSettings         | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \- \[7\]  | X           | X           | X           | X           | X           |
| CMakeFindFrameworks              | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CMakeFindPackageMode             | X        | X        | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CMakeForceCompiler               | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X \[8\]   | X           | X           | X           | X           | X           |
| CMakeGraphVizOptions             | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CMakePackageConfigHelpers        | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CMakeParseArguments              | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CMakePrintHelpers                | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CMakeImportBuildSettings         | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \- \[9\]  | X           | X           | X           | X           | X           |
| CMakeJavaInformation             | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \- \[10\] | X           | X           | X           | X           | X           |
| CMakePrintSystemInformation      | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CMakePushCheckState              | X        | X \[11\] | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CMakeVerifyManifest              | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | \-          | \-          | \-          | \-          | \-          |
| CPack                            | X        | X        | X        | X        | X \[12\] | X \[13\] | X        | X        | X        | X \[14\] | X        | X        | X \[15\]  | X \[16\]    | X           | X           | X           | X           |
| CPackBundle                      | X        | X        | X        | X        | X \[17\] | X \[18\] | X        | X        | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CPackComponent                   | X        | X        | X        | X        | X        | X \[19\] | X        | X        | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CPackCygwin                      | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CPackDMG                         | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CPackDeb                         | X        | X        | X        | X        | X \[20\] | X        | X        | X        | X        | X        | X \[21\] | X        | X         | \-          | \-          | \-          | \-          | \-          |
| CPackNSIS                        | X        | X        | X        | X        | X \[22\] | X \[23\] | X        | X        | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CPackPackageMaker                | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CPackRPM                         | X        | X \[24\] | X        | X        | X        | X \[25\] | X \[26\] | X \[27\] | X        | X        | X \[28\] | X \[29\] | X \[30\]  | X           | X           | X           | X           | X           |
| CPackWIX                         | X        | X \[31\] | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CTest                            | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CTestScriptMode                  | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | \-          | \-          | \-          | \-          | \-          |
| CTestUseLaunchers                | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CheckCCompilerFlag               | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckCSourceCompiles             | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X \[32\]    | X           | X           | X           | X           |
| CheckCSourceRuns                 | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckCXXCompilerFlag             | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckCXXSourceCompiles           | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X \[33\]    | X           | X           | X           | X           |
| CheckCXXSourceRuns               | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckCXXSymbolExists             | X        | X        | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CheckFortranFunctionExists       | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckFunctionExists              | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckIncludeFile                 | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckIncludeFileCXX              | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckIncludeFiles                | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckLanguage                    | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CheckLibraryExists               | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckPrototypeDefinition         | X        | X        | X        | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| CheckStructHasMember             | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckSymbolExists                | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| CheckTypeSize                    | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X \[34\]  | X           | X           | X           | X           | X           |
| CheckVariableExists              | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| Dart                             | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| DeployQt4                        | X        | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| Documentation                    | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| ExternalData                     | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| ExternalProject                  | X        | X        | X \[35\] | X \[36\] | X        | X        | X        | X        | X \[37\] | X \[38\] | X \[39\] | X \[40\] | X \[41\]  | \-          | \-          | \-          | \-          | \-          |
| FeatureSummary                   | X        | X        | X        | X        | X        | X        | X \[42\] | X \[43\] | X \[44\] | X \[45\] | X \[46\] | X        | X         | X           | X           | X           | X           | X           |
| FindALSA                         | X        | X        | X        | X        | X        | X \[47\] | X        | X        | X        | X        | X        | X        | X         | \-          | \-          | \-          | \-          | \-          |
| FindASPELL                       | X        | X        | X        | X        | X        | X        | X        | X \[48\] | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindAVIFile                      | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindArmadillo                    | X        | X        | X        | X        | X        | X        | X        | X        | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| FindBISON                        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X \[49\] | X        | X         | \-          | \-          | \-          | \-          | \-          |
| FindBLAS                         | X        | X        | X        | X        | X        | X        | X        | X \[50\] | X        | X        | X        | X        | X         | X           | X           | X           | X           | X \[51\]    |
| FindBZip2                        | X        | X        | X        | X        | X        | X \[52\] | X        | X        | X        | X        | X \[53\] | X        | X         | X           | X           | X           | X           | X           |
| FindBoost                        | X        | X        | X        | X        | X        | X        | X        | X \[54\] | X        | X        | X \[55\] | X        | X         | X \[56\]    | X \[57\]    | X \[58\]    | X           | X \[59\]    |
| FindBullet                       | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | \-          | \-          | \-          | \-          | \-          |
| FindCABLE                        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindCUDA                         | X        | X        | X \[60\] | X \[61\] | X        | X \[62\] | X        | X        | X        | X        | X        | X        | X         | \-          | \-          | \-          | \-          | \-          |
| FindCURL                         | X        | X        | X        | X        | X        | X \[63\] | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindCVS                          | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindCoin3D                       | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | \-          | \-          | \-          |
| FindCups                         | X        | X        | X        | X        | X        | X \[64\] | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindCurses                       | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindCxxTest                      | X        | X        | X        | X        | X        | X        | X        | X        | X        | X \[65\] | X \[66\] | X        | X         | X           | X           | \-          | \-          | \-          |
| FindCygwin                       | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindDCMTK                        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindDart                         | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindDevIL                        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | \-          | \-          | \-          |
| FindDoxygen                      | X        | X        | X        | X        | X        | X \[67\] | X        | X        | X        | X        | X        | X        | X         | X           | X \[68\]    | X \[69\]    | X           | X           |
| FindEXPAT                        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindFLEX                         | X        | X        | X        | X        | X        | X        | X        | X \[70\] | X        | X        | X \[71\] | X        | X         | \-          | \-          | \-          | \-          | \-          |
| FindFLTK                         | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X \[72\]    | X           | X           |
| FindFLTK2                        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindFreetype                     | X        | X        | X        | X        | X        | X \[73\] | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindGCCXML                       | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindGDAL                         | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindGIF                          | X        | X        | X        | X        | X        | X        | X        | X \[74\] | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindGLEW                         | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| FindGLUT                         | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindGTK                          | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindGTK2                         | X \[75\] | X \[76\] | X        | X        | X        | X        | X        | X        | X        | X        | X \[77\] | X        | X         | \-          | \-          | \-          | \-          | \-          |
| FindGTest                        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | \-          | \-          | \-          | \-          | \-          |
| FindGettext                      | X        | X        | X        | X        | X        | X \[78\] | X        | X \[79\] | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindGit                          | X        | X        | X        | X        | X        | X \[80\] | X        | X        | X        | X        | X        | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| FindGnuTLS                       | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | \-          | \-          | \-          | \-          | \-          |
| FindGnuplot                      | X        | X        | X        | X        | X        | X \[81\] | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindHDF5                         | X        | X        | X        | X        | X        | X        | X        | X \[82\] | X \[83\] | X        | X        | X \[84\] | X \[85\]  | \-          | \-          | \-          | \-          | \-          |
| FindHSPELL                       | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X \[86\] | X         | X           | X           | X           | X           | X           |
| FindHTMLHelp                     | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindHg                           | X        | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| FindITK                          | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindIcotool                      | X        | X        | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-       | \-        | \-          | \-          | \-          | \-          | \-          |
| FindImageMagick                  | X        | X        | X        | X        | X        | X \[87\] | X        | X        | X        | X        | X        | X        | X         | X           | X           | X \[88\]    | X \[89\]    | X \[90\]    |
| FindJNI                          | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X \[91\]  | X           | X           | X           | X           | X           |
| FindJPEG                         | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindJasper                       | X        | X        | X        | X        | X        | X \[92\] | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindJava                         | X        | X        | X        | X        | X        | X        | X        | X \[93\] | X        | X        | X \[94\] | X \[95\] | X \[96\]  | X           | X           | X           | X \[97\]    | X \[98\]    |
| FindKDE3                         | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |
| FindKDE4                         | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X        | X         | X           | X           | X           | X           | X           |

Next part: [FindL\* -
Z](doc/cmake/version_compatibility_matrix/StandardCMakeModulesFindL "wikilink")

### Footnotes

<references/>

1.  missing: copy_resolved_framework_into_bundle
2.  deprecated
3.  deprecated
4.  deprecated
5.  deprecated
6.  deprecated
7.  deprecated
8.  missing: CMAKE_FORCE_Fortran_COMPILER
9.  deprecated
10. deprecated
11. missing: option RESET for cmake_push_check_state()
12. missing: CPACK_PACKAGE_DIRECTORY
13. missing: CPACK_PACKAGE_ICON, CPACK_OUTPUT_CONFIG_FILE,
    CPACK_PACKAGE_INSTALL_REGISTRY_KEY,
    CPACK_CREATE_DESKTOP_LINKS
14. missing: CPACK_NSIS_INSTALL_ROOT,
    CPACK_NSIS_EXECUTABLES_DIRECTORY,
    CPACK_NSIS_MUI_FINISHPAGE_RUN
15. missing: CPACK_DMG_VOLUME_NAME, CPACK_DMG_FORMAT,
    CPACK_DMG_DS_STORE, CPACK_DMG_BACKGROUND_IMAGE,
    CPACK_COMMAND_HDIUTIL, CPACK_COMMAND_SETFILE,
    CPACK_COMMAND_REZ, CPACK_BUNDLE_NAME, CPACK_BUNDLE_PLIST,
    CPACK_BUNDLE_ICON, CPACK_BUNDLE_STARTUP_SCRIPT
16. missing: CPACK_NSIS_PACKAGE_NAME
17. deprecated: CPACK_BUNDLE_STARTUP_SCRIPT
18. missing: CPACK_BUNDLE_STARTUP_COMMAND
19. missing: CPACK_COMPONENTS_ALL,
    CPACK_<GENNAME>_COMPONENT_INSTALL, CPACK_COMPONENTS_GROUPING,
    CPACK_COMPONENT_<compName>_DISPLAY_NAME,
    CPACK_COMPONENT_<compName>_DESCRIPTION,
    CPACK_COMPONENT_<compName>_GROUP,
    CPACK_COMPONENT_<compName>_DEPENDS,
    CPACK_COMPONENT_<compName>_REQUIRED
20. missing: CPACK_DEBIAN_PACKAGE_RECOMMENDS,
    CPACK_DEBIAN_PACKAGE_SUGGESTS,
    CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA
21. missing: CPACK_DEBIAN_PACKAGE_HOMEPAGE,
    CPACK_DEBIAN_PACKAGE_SHLIBDEPS, CPACK_DEBIAN_PACKAGE_DEBUG,
    CPACK_DEBIAN_PACKAGE_PREDEPENDS,
    CPACK_DEBIAN_PACKAGE_ENHANCES, CPACK_DEBIAN_PACKAGE_BREAKS,
    CPACK_DEBIAN_PACKAGE_CONFLICTS, CPACK_DEBIAN_PACKAGE_PROVIDES,
    CPACK_DEBIAN_PACKAGE_REPLACES
22. missing: CPACK_NSIS_EXTRA_PREINSTALL_COMMANDS,
    CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL
23. missing: CPACK_NSIS_INSTALLER_MUI_ICON_CODE,
    CPACK_NSIS_MENU_LINKS
24. missing: CPACK_RPM_<componentName>_USER_BINARY_SPECFILE,
    CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST,
    CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION
25. deprecated: CPACK_RPM_SPEC_INSTALL_POST
26. missing: CPACK_RPM_USER_FILELIST,
    CPACK_RPM_<COMPONENT>_USER_FILELIST
27. missing: CPACK_RPM_COMPONENT_INSTALL,
    CPACK_RPM_<COMPONENT>_PRE_INSTALL_SCRIPT_FILE,
    CPACK_RPM_<COMPONENT>_PRE_UNINSTALL_SCRIPT_FILE
28. missing: CPACK_RPM_PACKAGE_URL, CPACK_RPM_PACKAGE_SUGGESTS,
    CPACK_RPM_PACKAGE_OBSOLETES, CPACK_RPM_PACKAGE_RELOCATABLE,
    CPACK_RPM_CHANGELOG_FILE
29. missing: CPACK_RPM_COMPRESSION_TYPE
30. missing: CPACK_RPM_PACKAGES_PROVIDES,
    CPACK_RPM_USER_BINARY_SPECFILE,
    CPACK_RPM_GENERATE_USER_BINARY_SPECFILE_TEMPLATE,
    CPACK_RPM_PRE_INSTALL_SCRIPT_FILE,
    CPACK_RPM_PRE_UNINSTALL_SCRIPT_FILE,
    CPACK_RPM_POST_INSTALL_SCRIPT_FILE,
    CPACK_RPM_POST_UNINSTALL_SCRIPT_FILE
31. missing: CPACK_WIX_PROGRAM_MENU_FOLDER, CPACK_WIX_CULTURES,
    CPACK_WIX_TEMPLATE
32. missing: FAIL_REGEX
33. missing: FAIL_REGEX
34. missing: CMAKE_EXTRA_INCLUDE_FILES
35. missing in ExternalProject_Add: CMAKE_GENERATOR_TOOLSET
36. missing in ExternalProject_Add: DOWNLOAD_NAME, HG_REPOSITORY,
    HG_TAG, URL_HASH, TLS_VERIFY, TLS_CAINFO
37. missing in ExternalProject_Add: SVN_TRUST_CERT
38. missing in ExternalProject_Add: CMAKE_CACHE_ARGS
39. missing ExternalProject_Add_StepTargets; missing in
    ExternalProject_Add: LOG_\*, STEP_TARGETS; missing in
    ExternalProject_Add_Step: LOG
40. missing in ExternalProject_Add: SVN_USERNAME, SVN_PASSWORD,
    GIT_REPOSITORY, GIT_TAG, URL_MD5
41. missing in ExternalProject_Add: TIMEOUT
42. deprecated: set_package_info
43. missing: set_package_properties, INCLUDE_QUIET_PACKAGES,
    FATAL_ON_MISSING_REQUIRED_PACKAGES, OPTIONAL_PACKAGES_FOUND,
    OPTIONAL_PACKAGES_NOT_FOUND, RECOMMENDED_PACKAGES_FOUND,
    RECOMMENDED_PACKAGES_NOT_FOUND, REQUIRED_PACKAGES_FOUND,
    REQUIRED_PACKAGES_NOT_FOUND, RUNTIME_PACKAGES_FOUND,
    RUNTIME_PACKAGES_NOT_FOUND) (deprecated: set_feature_info,
    print_enabled_features, print_disabled_features
44. deprecated: set_feature_info, print_enabled_features,
    print_disabled_features
45. deprecated: set_feature_info, print_enabled_features,
    print_disabled_features
46. missing: feature_summary, set_package_info, add_feature_info
47. missing: ALSA_VERSION_STRING
48. missing: ASPELL_EXECUTABLE
49. missing: find_package version support
50. missing: support for vendors "Goto" and "ACML_GPU"
51. missing: BLA_STATIC, BLA_VENDOR, BLA_F95
52. missing: BZIP2_VERSION_STRING
53. deprecated: BZIP2_DEFINITIONS
54. missing: Boost_NO_BOOST_CMAKE
55. missing: Boost_NO_SYSTEM_PATHS, Boost_USE_STATIC_RUNTIME,
    Boost_USE_DEBUG_PYTHON, Boost_USE_STLPORT,
    Boost_USE_STLPORT_DEPRECATED_NATIVE_IOSTREAMS,
    Boost_THREADAPI, Boost_REALPATH
56. missing: Boost_DETAILED_FAILURE_MSG
57. deprecated: Boost_USE_MULTITHREAD
58. missing: Boost_USE_MULTITHREADED, Boost_DEBUG, Boost_COMPILER,
    Boost_${COMPONENT}_\*
59. missing: BOOSTROOT
60. missing: variables CUDA_SEPARABLE_COMPILATION,
    CUDA_cupti_LIBRARY; macros
    CUDA_COMPUTE_SEPARABLE_COMPILATION_OBJECT_FILE_NAME,
    CUDA_LINK_SEPARABLE_COMPILATION_OBJECTS
61. missing: variable CUDA_HOST_COMPILER
62. missing: CUDA_curand_LIBRARY, CUDA_cusparse_LIBRARY,
    CUDA_npp_LIBRARY, CUDA_nvcuvenc_LIBRARY, CUDA_nvcuvid_LIBRARY
63. missing: CURL_VERSION_STRING
64. missing: CUPS_VERSION_STRING
65. deprecated: CXXTEST_USE_PYTHON, CXXTEST_INCLUDE_DIR
66. missing: CXXTEST_TESTGEN_ARGS, CXXTEST_TESTGEN_EXECUTABLE,
    CXXTEST_TESTGEN_INTERPRETER, CXXTEST_INCLUDE_DIRS
67. missing: DOXYGEN_VERSION
68. deprecated: DOXYGEN, DOT
69. missing: DOXYGEN_SKIP_DOT, DOXYGEN_DOT_FOUND
70. missing: FLEX_INCLUDE_DIRS
71. missing: find_package version support
72. missing: FLTK_SKIP_OPENGL, FLTK_SKIP_FORMS, FLTK_SKIP_IMAGES,
    FLTK_SKIP_FLUID
73. missing: FREETYPE_VERSION_STRING
74. missing: GIF_VERSION (i.e. version support in find_package)
75. deprecated: GTK2_SKIP_MARK_AS_ADVANCED
76. missing: GTK2_DEFINITIONS
77. missing: GTK2_ADDITIONAL_SUFFIXES
78. missing: GETTEXT_VERSION_STRING:
79. missing: GETTEXT_PROCESS_POT, GETTEXT_PROCESS_PO_FILES
80. missing: GIT_VERSION_STRING
81. missing: GNUPLOT_VERSION_STRING
82. missing: HDF5_Fortran_HL_LIBRARIES; support for component
    "Fortran_HL"
83. missing: HDF5_Fortran_LIBRARIES, HDF5_HL_LIBRARIES,
    HDF5_Fortran_COMPILER_EXECUTABLE
84. deprecated: HDF5_INCLUDE_DIR
85. missing: HDF5_INCLUDE_DIRS
86. missing: HSPELL_VERSION_STRING, HSPELL_\*_VERSION
87. missing: ImageMagick_VERSION_STRING
88. deprecated component: Magick
89. missing component: MagickCore
90. missing: find_package components support
91. missing: JNI_FOUND
92. missing: JASPER_VERSION_STRING
93. missing: Java_JAVAH_EXECUTABLE, Java_JAVADOC_EXECUTABLE
94. missing: find_package version support
95. deprecated: JAVA_RUNTIME, JAVA_COMPILE, JAVA_ARCHIVE
96. missing: Java_VERSION\*, Java_JAVA_EXECUTABLE,
    Java_JAVAC_EXECUTABLE, Java_JAR_EXECUTABLE
97. deprecated: JAVA_AWT_LIB_PATH, JAVA_JVM_LIB_PATH
98. missing: JNI_INCLUDE_DIRS, JNI_LIBRARIES, JAVA_AWT_LIBRARY,
    JAVA_JVM_LIBRARY

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/StandardCMakeModulesA) in another wiki.
