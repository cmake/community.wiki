## Properties on Tests

| CMake Option              | 2.8.12 | 2.8.11 | 2.8.10 | 2.8.9 | 2.8.8 | 2.8.7 | 2.8.6 | 2.8.5 | 2.8.4 | 2.8.3 | 2.8.2 | 2.8.1 | 2.8.0 | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| ------------------------- | ------ | ------ | ------ | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----------- | ----------- | ----------- | ----------- | ----------- |
| ATTACHED_FILES           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-    | \-          | \-          | \-          | \-          | \-          |
| ATTACHED_FILES_ON_FAIL | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-    | \-          | \-          | \-          | \-          | \-          |
| COST                      | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-    | \-          | \-          | \-          | \-          | \-          |
| DEPENDS                   | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-    | \-          | \-          | \-          | \-          | \-          |
| ENVIRONMENT               | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| FAIL_REGULAR_EXPRESSION | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| LABELS                    | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| MEASUREMENT               | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| PASS_REGULAR_EXPRESSION | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| PROCESSORS                | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-    | \-          | \-          | \-          | \-          | \-          |
| REQUIRED_FILES           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-    | \-          | \-          | \-          | \-          | \-          |
| RESOURCE_LOCK            | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| RUN_SERIAL               | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-    | \-          | \-          | \-          | \-          | \-          |
| TIMEOUT                   | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| WILL_FAIL                | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| WORKING_DIRECTORY        | X      | X      | X      | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |

### Footnotes

<references/>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Properties_on_Tests) in another wiki.
