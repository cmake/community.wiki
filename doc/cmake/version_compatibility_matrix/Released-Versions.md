You are now on the CMake community wiki.
Many pages are now **DEPRECATED** and sometimes even considered bad
practice. Those are only kept as reference for previous CMake versions.
The documentation for the latest version is available here:
[CMake Documentation](https://cmake.org/documentation/).

-----

A compatibility matrix of cmake features you can find on the page
[CMake_Version_Compatibility_Matrix](doc/cmake/Version-Compatibility-Matrix "wikilink")
.

Below are the documentations for all previous releases of
CMake.

| Release | Date        | CMake                   | CCMake    | CTest                    | CPack      |
| ------- | ----------- | ----------------------- | --------- | ------------------------ | ---------- |
| 2.8.12  | 8 Oct 2013  | \[\[CMake_2.8.12_Docs | cmake\]\] | \[\[CCMake_2.8.12_Docs | ccmake\]\] |
| 2.8.11  | 17 May 2013 | \[\[CMake_2.8.11_Docs | cmake\]\] | \[\[CCMake_2.8.11_Docs | ccmake\]\] |
| 2.8.10  | 31 Oct 2012 | \[\[CMake_2.8.10_Docs | cmake\]\] | \[\[CCMake_2.8.10_Docs | ccmake\]\] |
| 2.8.9   | 09 Aug 2012 | \[\[CMake_2.8.9_Docs  | cmake\]\] | \[\[CCMake_2.8.9_Docs  | ccmake\]\] |
| 2.8.8   | 18 Apr 2012 | \[\[CMake_2.8.8_Docs  | cmake\]\] | \[\[CCMake_2.8.8_Docs  | ccmake\]\] |
| 2.8.7   | 30 Dec 2011 | \[\[CMake_2.8.7_Docs  | cmake\]\] | \[\[CCMake_2.8.7_Docs  | ccmake\]\] |
| 2.8.6   | 04 Oct 2011 | \[\[CMake_2.8.6_Docs  | cmake\]\] | \[\[CCMake_2.8.6_Docs  | ccmake\]\] |
| 2.8.5   | 08 Jul 2011 | \[\[CMake_2.8.5_Docs  | cmake\]\] | \[\[CCMake_2.8.5_Docs  | ccmake\]\] |
| 2.8.4   | 16 Feb 2011 | \[\[CMake_2.8.4_Docs  | cmake\]\] | \[\[CCMake_2.8.4_Docs  | ccmake\]\] |
| 2.8.3   | 03 Nov 2010 | \[\[CMake_2.8.3_Docs  | cmake\]\] | \[\[CCMake_2.8.3_Docs  | ccmake\]\] |
| 2.8.2   | 28 Jun 2010 | \[\[CMake_2.8.2_Docs  | cmake\]\] | \[\[CCMake_2.8.2_Docs  | ccmake\]\] |
| 2.8.1   | 17 Mar 2010 | \[\[CMake_2.8.1_Docs  | cmake\]\] | \[\[CCMake_2.8.1_Docs  | ccmake\]\] |
| 2.8.0   | 13 Nov 2009 | \[\[CMake_2.8.0_Docs  | cmake\]\] | \[\[CCMake_2.8.0_Docs  | ccmake\]\] |
| 2.6.4   | 01 May 2009 | \[\[CMake_2.6.4_Docs  | cmake\]\] | \[\[CCMake_2.6.4_Docs  | ccmake\]\] |
| 2.6.3   | 09 Mar 2009 | \[\[CMake_2.6.3_Docs  | cmake\]\] | \[\[CCMake_2.6.3_Docs  | ccmake\]\] |
| 2.6.2   | 25 Sep 2008 | \[\[CMake_2.6.2_Docs  | cmake\]\] | \[\[CCMake_2.6.2_Docs  | ccmake\]\] |
| 2.6.1   | 04 Aug 2008 | \[\[CMake_2.6.1_Docs  | cmake\]\] | \[\[CCMake_2.6.1_Docs  | ccmake\]\] |
| 2.6.0   | 07 May 2008 | \[\[CMake_2.6.0_Docs  | cmake\]\] | \[\[CCMake_2.6.0_Docs  | ccmake\]\] |
| 2.4.8   | 23 Jan 2008 | \[\[CMake_2.4.8_Docs  | cmake\]\] | \[\[CCMake_2.4.8_Docs  | ccmake\]\] |
| 2.4.7   | 18 Jul 2007 | \[\[CMake_2.4.7_Docs  | cmake\]\] | \[\[CCMake_2.4.7_Docs  | ccmake\]\] |
| 2.4.6   | 11 Jan 2007 | \[\[CMake_2.4.6_Docs  | cmake\]\] | \[\[CCMake_2.4.6_Docs  | ccmake\]\] |
| 2.4.5   | 04 Dec 2006 | \[\[CMake_2.4.5_Docs  | cmake\]\] | \[\[CCMake_2.4.5_Docs  | ccmake\]\] |
| 2.4.4   | 21 Nov 2006 | \[\[CMake_2.4.4_Docs  | cmake\]\] | \[\[CCMake_2.4.4_Docs  | ccmake\]\] |
| 2.4.3   | 20 Sep 2006 | \[\[CMake_2.4.3_Docs  | cmake\]\] | \[\[CCMake_2.4.3_Docs  | ccmake\]\] |
| 2.2.3   | 05 Dec 2005 | \[\[CMake_2.2.3_Docs  | cmake\]\] | \[\[CCMake_2.2.3_Docs  | ccmake\]\] |
| 2.0.6   | 14 Apr 2005 | \[\[CMake_2.0.6_Docs  | cmake\]\] | \[\[CCMake_2.0.6_Docs  | ccmake\]\] |
| 1.8.3   | 07 Jan 2004 | \[\[CMake_1.8.3_Docs  | cmake\]\] | \[\[CCMake_1.8.3_Docs  | ccmake\]\] |

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Released_Versions) in another wiki.
