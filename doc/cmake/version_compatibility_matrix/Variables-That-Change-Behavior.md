## Variables That Change Behavior

| CMake Option                                     | 2.8.12 | 2.8.11  | 2.8.10 | 2.8.9 | 2.8.8 | 2.8.7 | 2.8.6 | 2.8.5 | 2.8.4 | 2.8.3 | 2.8.2 | 2.8.1 | 2.8.0 | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| ------------------------------------------------ | ------ | ------- | ------ | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----------- | ----------- | ----------- | ----------- | ----------- |
| BUILD_SHARED_LIBS                              | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_ABSOLUTE_DESTINATION_FILES              | X      | X       | X      | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_AUTOMOC_RELAXED_MODE                    | X      | X       | X      | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_BACKWARDS_COMPATIBILITY                  | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_BUILD_TYPE                               | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_COLOR_MAKEFILE                           | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | \-          | \-          | \-          |
| CMAKE_CONFIGURATION_TYPES                      | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_DEBUG_TARGET_PROPERTIES                 | X      | X       | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_DISABLE_FIND_PACKAGE_<PackageName>     | X      | X       | X      | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_ERROR_DEPRECATED                         | X      | \-      | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION | X      | X       | X      | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_FIND_LIBRARY_PREFIXES                   | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_FIND_LIBRARY_SUFFIXES                   | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_FIND_PACKAGE_WARN_NO_MODULE           | X      | X       | X      | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_IGNORE_PATH                              | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_INCLUDE_PATH                             | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | \-          |
| CMAKE_INSTALL_DEFAULT_COMPONENT_NAME         | X      | X       | X      | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_INSTALL_PREFIX                           | X      | X \[1\] | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_LIBRARY_PATH                             | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | \-          |
| CMAKE_MFC_FLAG                                 | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_MODULE_PATH                              | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_NOT_USING_CONFIG_FLAGS                 | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_POLICY_DEFAULT_CMP<NNNN>                | X      | X       | X      | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_PREFIX_PATH                              | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | \-          |
| CMAKE_PROGRAM_PATH                             | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | \-          |
| CMAKE_SKIP_INSTALL_ALL_DEPENDENCY            | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| CMAKE_SYSTEM_IGNORE_PATH                      | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_SYSTEM_INCLUDE_PATH                     | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | \-          |
| CMAKE_SYSTEM_LIBRARY_PATH                     | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | \-          |
| CMAKE_SYSTEM_PREFIX_PATH                      | X      | X \[2\] | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | \-          |
| CMAKE_SYSTEM_PROGRAM_PATH                     | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | \-          |
| CMAKE_USER_MAKE_RULES_OVERRIDE               | X      | X       | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_WARN_DEPRECATED                          | X      | \-      | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION  | X      | X       | X      | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |

### Footnotes

<references/>

1.  change: newer versions (2.8.12) also add CMAKE_INSTALL_PREFIX to
    CMAKE_SYSTEM_PREFIX_PATH
2.  change: newer versions (2.8.12) also add CMAKE_INSTALL_PREFIX to
    CMAKE_SYSTEM_PREFIX_PATH

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Variables_That_Change_Behavior) in another wiki.
