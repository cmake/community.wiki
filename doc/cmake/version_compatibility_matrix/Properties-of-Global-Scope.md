## Properties of Global Scope

| CMake Option                             | 2.8.12 | 2.8.11 | 2.8.10 | 2.8.9 | 2.8.8 | 2.8.7 | 2.8.6 | 2.8.5 | 2.8.4 | 2.8.3 | 2.8.2 | 2.8.1 | 2.8.0 | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| ---------------------------------------- | ------ | ------ | ------ | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----------- | ----------- | ----------- | ----------- | ----------- |
| ALLOW_DUPLICATE_CUSTOM_TARGETS        | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| AUTOMOC_TARGETS_FOLDER                 | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| DEBUG_CONFIGURATIONS                    | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | \-          | \-          | \-          |
| DISABLED_FEATURES                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| ENABLED_FEATURES                        | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| ENABLED_LANGUAGES                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | \-          |
| FIND_LIBRARY_USE_LIB64_PATHS         | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| FIND_LIBRARY_USE_OPENBSD_VERSIONING  | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| GLOBAL_DEPENDS_DEBUG_MODE             | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| GLOBAL_DEPENDS_NO_CYCLES              | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| IN_TRY_COMPILE                         | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| PACKAGES_FOUND                          | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| PACKAGES_NOT_FOUND                     | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| PREDEFINED_TARGETS_FOLDER              | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| REPORT_UNDEFINED_PROPERTIES            | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| RULE_LAUNCH_COMPILE                    | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| RULE_LAUNCH_CUSTOM                     | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| RULE_LAUNCH_LINK                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| RULE_MESSAGES                           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| TARGET_ARCHIVES_MAY_BE_SHARED_LIBS  | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| TARGET_SUPPORTS_SHARED_LIBS           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| USE_FOLDERS                             | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| __CMAKE_DELETE_CACHE_CHANGE_VARS_ | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |

### Footnotes

<references/>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Properties_of_Global_Scope) in another wiki.
