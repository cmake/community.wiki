## Variables

  - [Variables That Change
    Behavior](doc/cmake/version_compatibility_matrix/Variables-That-Change-Behavior "wikilink")
  - [Variables That Describe The
    System](doc/cmake/version_compatibility_matrix/Variables-That-Describe-the-System "wikilink")
  - [Variables for
    Languages](doc/cmake/version_compatibility_matrix/Variables-for-Languages "wikilink")
  - [Variables That Control The
    Build](doc/cmake/version_compatibility_matrix/Variables-that-Control-the-Build "wikilink")
  - [Variables That Provide
    Information](doc/cmake/version_compatibility_matrix/Variables-that-Provide-Information "wikilink")

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Variables) in another wiki.
