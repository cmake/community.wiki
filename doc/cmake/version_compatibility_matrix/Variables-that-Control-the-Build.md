## Variables that Control the Build

| CMake Option                                | 2.8.12 | 2.8.11 | 2.8.10 | 2.8.9 | 2.8.8 | 2.8.7 | 2.8.6 | 2.8.5 | 2.8.4 | 2.8.3 | 2.8.2 | 2.8.1 | 2.8.0 | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| ------------------------------------------- | ------ | ------ | ------ | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----------- | ----------- | ----------- | ----------- | ----------- |
| CMAKE_<CONFIG>_POSTFIX                    | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | \-          | \-          | \-          |
| CMAKE_<CONFIG>_VISIBILITY_PRESET         | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_ARCHIVE_OUTPUT_DIRECTORY           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_AUTOMOC                              | X      | X      | X      | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_AUTOMOC_MOC_OPTIONS                | X      | X      | X      | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_BUILD_WITH_INSTALL_RPATH          | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_DEBUG_POSTFIX                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_EXE_LINKER_FLAGS                   | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_EXE_LINKER_FLAGS_<CONFIG>         | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_Fortran_FORMAT                      | X      | X      | X      | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_Fortran_MODULE_DIRECTORY           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_GNUtoMS                              | X      | X      | X      | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_INCLUDE_CURRENT_DIR                | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| CMAKE_INCLUDE_CURRENT_DIR_IN_INTERFACE | X      | X      | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_INSTALL_NAME_DIR                   | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_INSTALL_RPATH                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_INSTALL_RPATH_USE_LINK_PATH      | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_LIBRARY_OUTPUT_DIRECTORY           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_LIBRARY_PATH_FLAG                  | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_LINK_DEF_FILE_FLAG                | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_LINK_DEPENDS_NO_SHARED            | X      | X      | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_LINK_INTERFACE_LIBRARIES           | X      | X      | X      | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_LINK_LIBRARY_FILE_FLAG            | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_LINK_LIBRARY_FLAG                  | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_MACOSX_BUNDLE                       | X      | X      | X      | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_MODULE_LINKER_FLAGS                | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_MODULE_LINKER_FLAGS_<CONFIG>      | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_NO_BUILTIN_CHRPATH                 | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | \-          | \-          | \-          | \-          | \-          |
| CMAKE_PDB_OUTPUT_DIRECTORY               | X      | X      | X      | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_POSITION_INDEPENDENT_FLAGS         | X      | X      | X      | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_RUNTIME_OUTPUT_DIRECTORY           | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_SHARED_LINKER_FLAGS                | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_SHARED_LINKER_FLAGS_<CONFIG>      | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_SKIP_BUILD_RPATH                   | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_SKIP_INSTALL_RPATH                 | X      | X      | X      | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_STATIC_LINKER_FLAGS                | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_STATIC_LINKER_FLAGS_<CONFIG>      | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_TRY_COMPILE_CONFIGURATION          | X      | X      | X      | X     | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_USE_RELATIVE_PATHS                 | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| CMAKE_VISIBILITY_INLINES_HIDDEN          | X      | \-     | \-     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| CMAKE_WIN32_EXECUTABLE                    | X      | X      | X      | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-          | \-          | \-          | \-          | \-          |
| EXECUTABLE_OUTPUT_PATH                    | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |
| LIBRARY_OUTPUT_PATH                       | X      | X      | X      | X     | X     | X     | X     | X     | X     | X     | X     | X     | X     | X           | X           | X           | X           | X           |

### Footnotes

<references/>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Variables_that_Control_the_Build) in another wiki.
