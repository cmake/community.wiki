## Properties on Targets

| CMake Option                                      | 2.8.12    | 2.8.11   | 2.8.10  | 2.8.9    | 2.8.8 | 2.8.7 | 2.8.6 | 2.8.5 | 2.8.4 | 2.8.3 | 2.8.2 | 2.8.1 | 2.8.0    | 2.6-patch 4 | 2.6-patch 3 | 2.6-patch 2 | 2.6-patch 1 | 2.6-patch 0 |
| ------------------------------------------------- | --------- | -------- | ------- | -------- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | -------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| <CONFIG>_OUTPUT_NAME                            | \-        | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \- \[1\] | X           | X           | X           | X           | X           |
| <CONFIG>_POSTFIX                                 | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | \-          | \-          | \-          |
| <LANG>_VISIBILITY_PRESET                        | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| ALIASED_TARGET                                   | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| ARCHIVE_OUTPUT_DIRECTORY                        | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| ARCHIVE_OUTPUT_DIRECTORY_<CONFIG>              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | \-       | \-          | \-          | \-          | \-          | \-          |
| ARCHIVE_OUTPUT_NAME                             | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| ARCHIVE_OUTPUT_NAME_<CONFIG>                   | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| AUTOMOC                                           | X         | X        | X \[2\] | X        | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| AUTOMOC_MOC_OPTIONS                             | X         | X        | X       | X        | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| BUILD_WITH_INSTALL_RPATH                       | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| CLEAN_DIRECT_OUTPUT                             | \-        | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \- \[3\] | X           | X           | X           | X           | X           |
| COMPILE_DEFINITIONS                              | X         | X \[4\]  | X       | X \[5\]  | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| COMPILE_DEFINITIONS_<CONFIG>                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| COMPILE_FLAGS                                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| COMPILE_OPTIONS                                  | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| DEBUG_POSTFIX                                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| DEFINE_SYMBOL                                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| ENABLE_EXPORTS                                   | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| EXCLUDE_FROM_ALL                                | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| EXCLUDE_FROM_DEFAULT_BUILD                     | X         | X        | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| EXCLUDE_FROM_DEFAULT_BUILD_<CONFIG>           | X         | X        | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| EXPORT_NAME                                      | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| EchoString                                        | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| FOLDER                                            | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| FRAMEWORK                                         | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| Fortran_FORMAT                                   | X         | X        | X       | X        | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| Fortran_MODULE_DIRECTORY                        | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| GENERATOR_FILE_NAME                             | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| GNUtoMS                                           | X         | X        | X       | X        | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| HAS_CXX                                          | \-        | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \- \[6\] | X           | X           | X           | X           | X           |
| IMPLICIT_DEPENDS_INCLUDE_TRANSFORM             | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | \-          |
| IMPORTED                                          | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_CONFIGURATIONS                          | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_IMPLIB                                  | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_IMPLIB_<CONFIG>                        | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_LINK_DEPENDENT_LIBRARIES              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_LINK_DEPENDENT_LIBRARIES_<CONFIG>    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_LINK_INTERFACE_LANGUAGES              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| IMPORTED_LINK_INTERFACE_LANGUAGES_<CONFIG>    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| IMPORTED_LINK_INTERFACE_LIBRARIES              | \- \[7\]  | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_LINK_INTERFACE_LIBRARIES_<CONFIG>    | \- \[8\]  | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_LINK_INTERFACE_MULTIPLICITY           | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| IMPORTED_LINK_INTERFACE_MULTIPLICITY_<CONFIG> | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| IMPORTED_LOCATION                                | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_LOCATION_<CONFIG>                      | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_NO_SONAME                              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| IMPORTED_NO_SONAME_<CONFIG>                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| IMPORTED_SONAME                                  | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORTED_SONAME_<CONFIG>                        | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORT_PREFIX                                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| IMPORT_SUFFIX                                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| INCLUDE_DIRECTORIES                              | X         | X \[9\]  | X       | X \[10\] | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| INSTALL_NAME_DIR                                | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| INSTALL_RPATH                                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| INSTALL_RPATH_USE_LINK_PATH                   | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| INTERFACE_COMPILE_DEFINITIONS                   | X         | X \[11\] | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| INTERFACE_COMPILE_OPTIONS                       | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| INTERFACE_INCLUDE_DIRECTORIES                   | X         | X \[12\] | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| INTERFACE_LINK_LIBRARIES                        | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| INTERFACE_POSITION_INDEPENDENT_CODE            | X         | X        | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| INTERFACE_SYSTEM_INCLUDE_DIRECTORIES           | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| INTERPROCEDURAL_OPTIMIZATION                     | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| INTERPROCEDURAL_OPTIMIZATION_<CONFIG>           | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| LABELS                                            | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| LIBRARY_OUTPUT_DIRECTORY                        | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| LIBRARY_OUTPUT_DIRECTORY_<CONFIG>              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | \-       | \-          | \-          | \-          | \-          | \-          |
| LIBRARY_OUTPUT_NAME                             | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| LIBRARY_OUTPUT_NAME_<CONFIG>                   | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| LINKER_LANGUAGE                                  | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X \[13\]    | X           | X           | X           | X           |
| LINK_DEPENDS                                     | X         | X        | X       | X        | X     | X     | X     | X     | X     | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| LINK_DEPENDS_NO_SHARED                         | X         | X        | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| LINK_FLAGS                                       | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| LINK_FLAGS_<CONFIG>                             | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| LINK_INTERFACE_LIBRARIES                        | \- \[14\] | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| LINK_INTERFACE_LIBRARIES_<CONFIG>              | \- \[15\] | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| LINK_INTERFACE_MULTIPLICITY                     | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| LINK_INTERFACE_MULTIPLICITY_<CONFIG>           | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| LINK_LIBRARIES                                   | X         | X \[16\] | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| LINK_SEARCH_END_STATIC                         | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| LINK_SEARCH_START_STATIC                       | X         | X        | X       | X        | X     | X     | X     | X     | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| LOCATION                                          | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X \[17\]    | X           | X           |
| LOCATION_<CONFIG>                                | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| MACOSX_BUNDLE                                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| MACOSX_BUNDLE_INFO_PLIST                       | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | \-          |
| MACOSX_FRAMEWORK_INFO_PLIST                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | \-          | \-          |
| MACOSX_RPATH                                     | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| MAP_IMPORTED_CONFIG_<CONFIG>                   | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| NAME                                              | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| NO_SONAME                                        | X         | X        | X       | X        | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| OSX_ARCHITECTURES                                | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | \-       | \-          | \-          | \-          | \-          | \-          |
| OSX_ARCHITECTURES_<CONFIG>                      | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | \-       | \-          | \-          | \-          | \-          | \-          |
| OUTPUT_NAME                                      | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| OUTPUT_NAME_<CONFIG>                            | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| PDB_NAME                                         | X         | X        | X       | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| PDB_NAME_<CONFIG>                               | X         | X        | X       | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| PDB_OUTPUT_DIRECTORY                            | X         | X        | X       | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| PDB_OUTPUT_DIRECTORY_<CONFIG>                  | X         | X        | X       | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| POSITION_INDEPENDENT_CODE                       | X         | X        | X       | X        | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| POST_INSTALL_SCRIPT                             | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| PREFIX                                            | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| PRE_INSTALL_SCRIPT                              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| PRIVATE_HEADER                                   | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| PROJECT_LABEL                                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | \-          | \-          | \-          |
| PUBLIC_HEADER                                    | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| RESOURCE                                          | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| RULE_LAUNCH_COMPILE                             | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| RULE_LAUNCH_CUSTOM                              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| RULE_LAUNCH_LINK                                | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| RUNTIME_OUTPUT_DIRECTORY                        | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| RUNTIME_OUTPUT_DIRECTORY_<CONFIG>              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | \-       | \-          | \-          | \-          | \-          | \-          |
| RUNTIME_OUTPUT_NAME                             | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| RUNTIME_OUTPUT_NAME_<CONFIG>                   | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | \-          | \-          | \-          | \-          | \-          |
| SKIP_BUILD_RPATH                                | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| SOURCES                                           | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| SOVERSION                                         | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| STATIC_LIBRARY_FLAGS                            | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| STATIC_LIBRARY_FLAGS_<CONFIG>                  | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| SUFFIX                                            | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| TYPE                                              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | \-          | \-          | \-          |
| VERSION                                           | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| VISIBILITY_INLINES_HIDDEN                       | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| VS_DOTNET_REFERENCES                            | X         | X        | X       | X        | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| VS_DOTNET_TARGET_FRAMEWORK_VERSION            | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| VS_GLOBAL_<variable>                            | X         | X        | X       | X        | X     | X     | X     | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| VS_GLOBAL_KEYWORD                               | X         | X        | X       | X        | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| VS_GLOBAL_PROJECT_TYPES                        | X         | X        | X       | X        | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| VS_GLOBAL_ROOTNAMESPACE                         | X         | \-       | \-      | \-       | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| VS_KEYWORD                                       | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | \-          | \-          | \-          |
| VS_SCC_AUXPATH                                  | X         | X        | X       | X        | X     | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| VS_SCC_LOCALPATH                                | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | \-          | \-          | \-          |
| VS_SCC_PROJECTNAME                              | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | \-          | \-          | \-          |
| VS_SCC_PROVIDER                                 | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | \-          | \-          | \-          |
| VS_WINRT_EXTENSIONS                             | X         | X        | X       | X        | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| VS_WINRT_REFERENCES                             | X         | X        | X       | X        | X     | \-    | \-    | \-    | \-    | \-    | \-    | \-    | \-       | \-          | \-          | \-          | \-          | \-          |
| WIN32_EXECUTABLE                                 | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |
| XCODE_ATTRIBUTE_<an-attribute>                  | X         | X        | X       | X        | X     | X     | X     | X     | X     | X     | X     | X     | X        | X           | X           | X           | X           | X           |

### Footnotes

<references/>

1.  deprecated: use OUTPUT_NAME_<CONFIG>
2.  missing: support for Qt5
3.  removed?
4.  missing: generator expressions `JOIN`, `C_COMPILER_ID`,
    `CXX_COMPILER_ID`, `VERSION_GREATER`, `VERSION_LESS`,
    `VERSION_EQUAL`, `C_COMPILER_VERSION`, `CXX_COMPILER_VERSION`
5.  missing: generator expressions
6.  deprecated: set LINKER_LANGUAGE instead
7.  deprecated: use INTERFACE_LINK_LIBRARIES instead
8.  deprecated: use INTERFACE_LINK_LIBRARIES instead
9.  missing: generator expressions `JOIN`, `C_COMPILER_ID`,
    `CXX_COMPILER_ID`, `VERSION_GREATER`, `VERSION_LESS`,
    `VERSION_EQUAL`, `C_COMPILER_VERSION`, `CXX_COMPILER_VERSION`
10. missing: generator expressions
11. missing: generator expressions `JOIN`, `C_COMPILER_ID`,
    `CXX_COMPILER_ID`, `VERSION_GREATER`, `VERSION_LESS`,
    `VERSION_EQUAL`, `C_COMPILER_VERSION`, `CXX_COMPILER_VERSION`
12. missing: generator expressions `JOIN`, `C_COMPILER_ID`,
    `CXX_COMPILER_ID`, `VERSION_GREATER`, `VERSION_LESS`,
    `VERSION_EQUAL`, `C_COMPILER_VERSION`, `CXX_COMPILER_VERSION`
13. default value changed
14. deprecated: use INTERFACE_LINK_LIBRARIES instead
15. deprecated: use INTERFACE_LINK_LIBRARIES instead
16. missing: generator expressions `JOIN`, `C_COMPILER_ID`,
    `CXX_COMPILER_ID`, `VERSION_GREATER`, `VERSION_LESS`,
    `VERSION_EQUAL`, `C_COMPILER_VERSION`, `CXX_COMPILER_VERSION`
17. missing: semantic for imported targets

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix/Properties_on_Targets) in another wiki.
