See also: [CMake Platform Dependent
Issues](doc/cmake/Platform-Dependent-Issues "wikilink")

## Eclipse CDT support

Starting with version 2.6.0 CMake supports generating projects for CDT
4.0 or newer. **[Here](doc/editors/Eclipse-CDT4-Generator "wikilink")** you can find
information how to use this.

If you are using an older version of CDT or CMake without the Eclipse
generator, you can configure Eclipse also manually to work with CMake
based projects. This is described
**[here](doc/editors/Eclipse-UNIX-Tutorial "wikilink")**.

[Here](doc/Editors "wikilink") you find information about
plugins which offer syntax highlighting and more for CMake in Eclipse.

## KDevelop3 Generator

(cmake . -G KDevelop3)

![Cmake-kdevelop3](/uploads/bc3df34a826abb4ccda251e5205a3222/Cmake-kdevelop3.png)

The KDevelop project generator is available in CMake since version
2.2.0. It works with KDevelop 3.1.1 and newer. Out-of-source builds are
fully supported. As with all other project generators, adding files to
the project has to be done by editing the CMakeLists.txt.

### Internals

Beside creating all the same files as UNIX Makefile generator, it
creates in the toplevel binary directory files:

  - Project.kdevelop
  - Project.kdevelop.filelist

The first one is XML describing the project, while the second one is a
list of relative paths to the source files. The source files are grouped
based on globbing expression in the XML file.

## Code::Blocks Generator

(cmake . -G "CodeBlocks - Unix Makefiles")
![Cmake-codeblocks](/uploads/1547a03886228669c759d8dd6e69ca7d/Cmake-codeblocks.png)

Starting with version 2.6.0 CMake also comes with a project file
generator for the [CodeBlocks IDE](http://www.codeblocks.org/). This
support is still in beta and needs feedback in order to mature.

Known issues:

  - "Build file" doesn't work
  - Sometimes cancelling the build doesn't work

## Makefile generators

The following targets are available:

  - all :

<!-- end list -->

  - preinstall :

<!-- end list -->

  - install :

<!-- end list -->

  - install/fast :

<!-- end list -->

  - rebuild_cache : just runs the cmake configure and generate steps,
    but don't actually start building

<!-- end list -->

  - edit_cache : run the user interface for working with
    cmake/modifying the cache contents. This is either cmake-gui or
    ccmake.

<!-- end list -->

  - <target> :

<!-- end list -->

  - <target>/fast :

<!-- end list -->

  - file.o :

<!-- end list -->

  - file.i :

<!-- end list -->

  - file.s :

The following variants of the generator are available:

  - "Unix Makefiles" : This generator generates Makefiles for make under
    Unix and also for **cygwin** (i.e. with Unix paths)

<!-- end list -->

  - "MinGW Makefiles" : Windows only, generates Makefiles for GNU make
    from the [MinGW project](http://www.mingw.org), the makefiles work
    also with other "native" GNU make versions for Windows. You can get
    this e.g. from <http://unxutils.sourceforge.net>, the download
    location is
    <http://sourceforge.net/project/showfiles.php?group_id=9328&package_id=9393>
    . From that zip file only /usr/wbin/make.exe is needed. The GNU make
    from www.steve.org.uk is version 3.75, this version has a bug, it
    does not work. The generated Makefiles use DOS/Windows paths and use
    cmd.exe as shell. GNU make supports parallel builds and can also be
    used with cl.exe as compiler. This can be done by setting the
    environment variables CC and CXX to cl.exe before running CMake.

<!-- end list -->

  - "MSYS Makefiles" : Windows only, generates Makefiles to be used
    under [MSYS](http://www.mingw.org/msys.shtml). It uses Unix-like
    paths and sh.exe from MSYS as shell.

<!-- end list -->

  - "NMake Makefiles": Windows only, generates Makefiles for MS nmake.
    The makefiles use DOS/Windows paths and care for nmake specific
    issues. The environment has to be setup correctly, e.g. by running
    vcvars32.bat. This can be done by running from the "Visual Studio
    Command prompt".

<!-- end list -->

  - "NMake Makefiles JOM" : Like "NMake Makefiles", but generates
    Makefiles for jom instead of nmake. jom is like nmake, but supports
    the -j flag. jom.zip should point to the newest release. [Get jom
    through ftp](ftp://ftp.qt.nokia.com/jom/) - 0.9.4 has been confirmed
    to work. Make sure the containing folder is in the PATH.

<!-- end list -->

  - "Borland Makefiles": Windows only, generates Makefiles for Borland
    make. The makefiles use DOS/Windows paths and care for Borland make
    specific issues.

<!-- end list -->

  - "Watcom WMake": Windows only, generates Makefiles for Borland make.
    The makefiles use DOS/Windows paths and care for Watcom wmake
    specific
issues.

## How to generate NMake Makefiles using Visual C++ Express Edition (free Visual Studio 8)

<b>These instructions are largely obsolete with the release of [Visual
Studio 2008 Express
Edition](http://www.microsoft.com/express/download/), which bundles the
win32 SDK with the Visual C++ component.</b>

### Goal

To compile your project on the command line using the one-year free
Visual C++ 2005 Express Edition, you can generate NMake Makefiles with
CMake, and then use nmake.

### Rationale

To use the free compiler, 'Visual C++ Express Edition' and 'Microsoft
Platform Software development Kit (SDK)' have to be downloaded and
installed separately. The batch file for the 'Visual Studio 2005 Command
Prompt' must be update to include the path, the include directory, and
the libraries for Microsoft Platform SDK.

### Instructions

Download VC Express \*and\* Platform SDK from
<http://www.microsoft.com>. Install them in C:\\Program Files. The
license expires one year after your start using it.

Create a batch file called 'VS8_Command_Prompt.bat' in your home
directory (for example in C:\\cygwin\\home\\my_username\\Bin). Edit
this file and type:

    <nowiki>
     @echo off

     if not exist "C:\Program Files\Microsoft Visual Studio 8\VC\vcvarsall.bat" goto missing
     echo call "C:\Program Files\Microsoft Visual Studio 8\VC\vcvarsall.bat"
     call "C:\Program Files\Microsoft Visual Studio 8\VC\vcvarsall.bat"

     @set PATH=C:\Program Files\Microsoft Platform SDK for Windows Server 2003 R2\Bin;%PATH%
     @set INCLUDE=C:\Program Files\Microsoft Platform SDK for Windows Server 2003 R2\Include;%INCLUDE%
     @set LIB=C:\Program Files\Microsoft Platform SDK for Windows Server 2003 R2\Lib;%LIB%
     goto :eof

     :missing
     echo Missing file
     echo "C:\Program Files\Microsoft Visual Studio 8\VC\vcvarsall.bat"
     goto :eof
    </nowiki>

Create a Windows shortcut called "VS8_Command_Prompt" in your Desktop.
Right click and go to Properties. Edit the shortcut tab (replace x86
with amd64, x64, ia64, x86_amd64, or x86_ia64 depending on your
processor
    type):

    Target: %comspec% /k ""C:\cygwin\home\Sylvain\Bin\VS8_Command_Prompt.bat"" x86
    Start in: C:\cygwin\home\my_username\Bin

Double-click on your VS8_Command_Prompt shortcut. Go to your build
directory.

    cd my_build_directory

Use CMakeSetup.exe (replace 2.2 with your CMake version number)

    \Program Files\CMake 2.2\bin\CMakeSetup.exe <my_source_directory>

Click 'Configure' in the CMake Window

Select 'Build for: NMake Makefiles'

Set the CMake variables, click 'Configure', and then 'Ok'.

Check you have a Makefile in your build directory. In the
VS8_Command_Prompt, and type:

    nmake

Your project should compile now.

## Symbian Carbide.c++ Generator

We are working on adding CMake support for generating Symbian
Carbide.c++ projects. **[Here](doc/cmake/dev/Symbian-Carbide-Generator "wikilink")**
you can find more information about this work.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Generator_Specific_Information) in another wiki.
