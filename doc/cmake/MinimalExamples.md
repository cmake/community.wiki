CMake has lots of good
[documentation](http://www.cmake.org/documentation/), but when learning
a new feature (or just starting out), there is no substitute for trying
it out in isolation. That way, if anything goes wrong, you have fewer
lines of CMakeList.txt to look through for problems.

For instance, here is the simplest possible CMakeLists.txt for a "Hello,
World" project:

    add_executable(hello hello.c)

And here's the simplest possible one for a shared library:

    add_library(hellolib SHARED hellolib.c)

That's right - CMakeLists.txt can be a single line. And if that's all
you need, that's all you should use\!

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/MinimalExamples) in another wiki.
