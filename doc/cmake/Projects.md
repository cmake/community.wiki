# Desktop suites and development platforms

  - [KDE4](http://www.kde.org) - the next version of the powerful open
    source desktop, application suite and development platform will be
    built using CMake, which together with Qt4, will make it possible to
    run KDE4 not only on Linux/UNIX, but also on Mac OS X and Windows.

<!-- end list -->

  - [Compiz](http://www.compiz.org) - An OpenGL compositing window
    manager, using CMake to facilitate having a unified build system for
    its plugin based architecture and autogenerated options code.

# Libraries

  - [PolarSSL](http://polarssl.org/) - A cryptography and SSL library.

<!-- end list -->

  - [eXtensible Data Model and Format
    (XDMF)](http://www.arl.hpc.mil/ice/)

<!-- end list -->

  - [MySQL](http://www.mysql.com/) - The world's most popular open
    source database

<!-- end list -->

  - [Grassroots DiCoM (GDCM)](http://gdcm.sf.net)

<!-- end list -->

  - [Vision-something-Libraries (VXL)](http://vxl.sourceforge.net/)

<!-- end list -->

  - [Vispack - C++ library developed for processing volumes images and
    surfaces](http://www.cs.utah.edu/~whitaker/vispack)

<!-- end list -->

  - [Teem - libraries for representing, processing, and visualizing
    scientific raster data](http://teem.sourceforge.net)

<!-- end list -->

  - [BIAS - The Basic Image AlgorithmS C++
    Library](http://www.mip.informatik.uni-kiel.de/~wwwadmin/Software/Doc/BIAS/html/intro.html)

<!-- end list -->

  - [GoLib - general c++
    library](http://www.cvgpr.uni-mannheim.de/gosch/software/golib/doc/html)

<!-- end list -->

  - [cmkSQL - an abstract SQL
    Library](http://cmk.navorski.com/index.php?wiki=CmkSql)

<!-- end list -->

  - [AtomicHF - Package to solve Hartree-Fock equations for a spherical
    system using Numerov
    algorithm](http://www.mcc.uiuc.edu/qmc/AtomicHF/)

<!-- end list -->

  - [XVT - A software development environment for easily building
    cross-platform GUI applications in C or C++.](http://www.xvt.com/)

<!-- end list -->

  - [The Half-Life 2 SDK in
    CMake](http://www.vinoisnotouzo.com/hl2sdk-cmake/)

<!-- end list -->

  - [The aften Open Source A/52 encoder](http://aften.sourceforge.net/)

<!-- end list -->

  - [PhysicsFS file i/o library](http://icculus.org/physfs/)

<!-- end list -->

  - [The Zorba XQuery Processor](http://www.zorba-xquery.org)

<!-- end list -->

  - [The Digest Software Project](http://digest.sourceforge.net)
    implements C and C++ libraries, and a PAM module, for digest
    authentication as specified by RFC 2069

<!-- end list -->

  - [libsudo](http://libsudo.sourceforge.net) allows a C/C++ application
    to execute a process as a different user (think of it as
    "system(process, user)")

<!-- end list -->

  - [Live Terrain Format](http://www.rapidterrain.com)
    High-performance synthetic natural environment engine and database
    format for conducting attenuated line of sight queries correlated to
    the real world.

<!-- end list -->

  - [OpenBabel](http://www.openbabel.org) Alternate build system in 2.2
    releases, planned to be main build system in 3.x. Chemical toolbox
    designed to speak the many languages of chemical data.

<!-- end list -->

  - [Freecell Solver](http://fc-solve.berlios.de/) - an ANSI C library
    (and some standalone command-line programs) for automatically
    solving boards of various variants of [Card
    Solitaire](https://en.wikipedia.org/wiki/Solitaire).

<!-- end list -->

  - [OpenImageIO](http://wiki.openimageio.org/wiki/Main_Page) is a
    library for reading and writing images, and a bunch of related
    classes, utilities, and applications.

<!-- end list -->

  - [Boost C++ Libraries](http://www.boost.org) Boost provides free
    peer-reviewed portable C++ source libraries, that are intended to be
    widely useful, and usable across a broad spectrum of applications.

<!-- end list -->

  - [YARP: Yet Another Robot Platform](http://eris.liralab.it/yarp/) is
    an open source middleware for robotic programming.

<!-- end list -->

  - [PoDoFo:](http://podofo.sourceforge.net/index.html) A free, portable
    and easy to use PDF parsing and creation library.

<!-- end list -->

  - [Point Cloud Library (PCL):](http://pointclouds.org/) A standalone,
    large scale, open project for 2D/3D image and point cloud
    processing.

<!-- end list -->

  - [OpenTURNS:](http://openturns.org/) Libre, cross-platform,
    C++/Python reliability library.

<!-- end list -->

  - [HDF:](http://www.hdfgroup.org/) The HDF4 and HDF5-based libraries
    and tools for organizing, storing, discovering, accessing, sharing,
    and preserving data in the face of enormous data growth in size and
    complexity.

# Toolkits

  - [PolyMCU - open-source framework for developing micro-controller
    firmware (ARM Cortex-M, IoT, GCC,
    LLVM)](https://github.com/labapart/polymcu)

<!-- end list -->

  - [Visualization Toolkit VTK](http://www.vtk.org)

<!-- end list -->

  - [Insight Segmentation and Registration Toolkit
    ITK](http://www.itk.org)

<!-- end list -->

  - [DICOM ToolKit (DCMTK)](http://dicom.offis.de/dcmtk.php.en)

<!-- end list -->

  - [Medical Imaging
    ToolKit](http://www3.ict.csiro.au/ict/content/display/0,,a16254_b16408_d72676,00.html)

<!-- end list -->

  - [MITK - The Medical Imaging Interaction
    Toolkit](http://www.mitk.org)

<!-- end list -->

  - [AA+ - A class framework for Computational
    Astronomy](http://www.naughter.com/aa.html)

<!-- end list -->

  - [Fltk - cross-platform C++ GUI toolkit for UNIX®/Linux® (X11),
    Microsoft® Windows®, and MacOS® X](http://fltk.org)

<!-- end list -->

  - [FlInventor - 3D toolkit](http://fl-inventor.sourceforge.net)

<!-- end list -->

  - [ORCA - open-source framework for developing component-based robotic
    systems](http://orca-robotics.sourceforge.net/getting.html)

<!-- end list -->

  - [KWWidgets - A free, cross-platform and open-license
    scientific-visualization GUI Toolkit.](http://www.kwwidgets.org)

<!-- end list -->

  - [IGSTK - Image Guided Surgery Toolkit](http://www.igstk.org)

<!-- end list -->

  - [COOLFluiD - CFD Environment](http://coolfluidsrv.vki.ac.be)

<!-- end list -->

  - [Webtoolkit (AKA Wt)](http://webtoolkit.eu/) is C++ library and
    application server for web applications which mimics the Qt API
    (it's like Qt but it spits HTML + CSS + JavaScript)

<!-- end list -->

  - [Ellogon - A natural language engineering
    platform](http://www.ellogon.org/)

<!-- end list -->

  - [CERTI](https://savannah.nongnu.org/projects/certi) an Open Source
    HLA RTI.

<!-- end list -->

  - [TSP](https://savannah.nongnu.org/projects/tsp) the Transport Sample
    Protocol

<!-- end list -->

  - [Equalizer Parallel Rendering
    Framework](http://www.equalizergraphic.com)

<!-- end list -->

  - [PixelLight open-source, cross-platform 3D application
    framework](http://www.pixellight.org)

# Tools

  - [GCC-XML - Dumps C++ Interface to XML](http://www.gccxml.org)

<!-- end list -->

  - [IPv6Suite - open source OMNeT++ model suite for accurate simulation
    of IPv6 protocols and
    networks](http://ctieware.eng.monash.edu.au/twiki/bin/view/Simulation/IPv6Suite)

<!-- end list -->

  - [MOOS - Mission Orientated Operating
    Suite](http://www.robots.ox.ac.uk/~pnewman/TheMOOS)

<!-- end list -->

  - [csync - a client only bidirectional file
    synchronizer](http://www.csync.org/)

<!-- end list -->

  - [pam_csync - a pam module for roaming home
    directories](http://www.csync.org/)

<!-- end list -->

  - [cdrkit - portable command-line CD/DVD recorder
    software](http://cdrkit.org/)

# Languages

  - [Goto++ - a goto
    language](http://gpp.niacland.net/telecharger.html.en)

<!-- end list -->

  - [Pawn - An embedded scripting language formerly called
    Small](http://www.compuphase.com/pawn/pawn.htm)

# Applications

  - [Aqsis - a high-quality 3D render engine that implements the
    RenderMan interface](http://www.aqsis.org)

<!-- end list -->

  - [ASPEED Software](http://www.aspeed.com)
    ASPEED's products include the ACCELLERANT SDK for parallelizing
    applications for grids or clusters. ASPEED provides APIs for easily
    improving application performance, with bindings in FORTRAN, C, C++,
    Java and C\#. ACCELLERANT also provides an Application Manager for
    quickly parallelizing batch jobs from the command line; and Workload
    Balancer for simple resource management. ACCELLERANT supports
    Windows and Linux, as well as numerous "grid vendor" products.

<!-- end list -->

  -

      -
        *"ASPEED's SDK supports a wide range of platforms and languages,
        and CMake fit the bill perfectly for our build and release
        cycle. It works for Visual Studio IDE development, and it works
        from the command line under either Windows (nmake) or Linux. It
        works for FORTRAN as well as C++. It's an enormous time-saver,
        allowing us to quickly develop applications for multiple
        platforms. This in turn has allowed us to build, test and
        release software more frequently, giving us a market
        advantage."*
        \-Mike Dalessio, Head of Development, ASPEED Software

<!-- end list -->

  - [Avidemux - a free video editor designed for simple cutting,
    filtering and encoding
    tasks.](http://avidemux.berlios.de/index.html)

<!-- end list -->

  - [Blender - Blender is the free open source 3D content creation
    suite, available for all major operating systems under the GNU
    General Public License.](http://www.blender.org/)

<!-- end list -->

  - [Boson - an OpenGL real-time strategy game for
    UNIX/Linux](http://boson.eu.org/)

<!-- end list -->

  - [CadColon](http://www.i-medlab.com) is a Computer Aided Detection
    (CAD) system designed to support radiologist's diagnosis of suspect
    polyps in the colon and rectum, using high and low dose CT.

*"I started to develop on a project on Linux OS in C++ language on 3
January 2005, and I had never written from scratch any configure.in
files, nor used autoconf tools seriously before. So since one of my task
was to create the building process for the whole project, I had 2
choices: learn and use autoconf, or search in Internet for an
alternative. The one day research ended up in CMake.org, which is an
easy but very powerful tool, which allowed me to achieve all I wanted to
do (debug/release/profile compilations, compilation based on the
developer name, easily maintainable and customizable compilation of many
shared/static libraries and applications), and which has a very fast
learning curve, exactly what a projet need to achieve its aim in short
time."*

\- Luca Cappa

  - [Cuneiform for Linux - Cuneiform is an multi-language OCR system
    originally developed and open sourced by Cognitive
    Technologies.](https://launchpad.net/cuneiform-linux)

<!-- end list -->

  - [CycabTK - An opensource mobile robot
    simulator](http://cycabtk.gforge.inria.fr/wiki/doku.php)

<!-- end list -->

  - [EMAN - Software for Single Particle Analysis and Electron
    Micrograph
    Analysis](http://ncmi.bcm.tmc.edu/homes/stevel/EMAN/doc/download.html)

<!-- end list -->

  - [Fing - A cross-platform command line tool for Network and Service
    discovery](http://www.over-look.org)

<!-- end list -->

  - [hugin, well known panorama stitching gui and
    more](http://hugin.sf.net)

<!-- end list -->

  - [ImmersaView](http://www.evl.uic.edu/cavern/agave/immersaview/index.html)

<!-- end list -->

  - [Inkscape - an Open Source vector graphics editor, with capabilities
    similar to Illustrator, CorelDraw, or Xara X, using the W3C standard
    Scalable Vector Graphics (SVG) file format](http://www.inkscape.org)

<!-- end list -->

  - [InSciTE program
    editor](http://www.5star-shareware.com/Windows/WebDev/HTML/inscite.html)

<!-- end list -->

  - [K-3D - free-as-in-freedom 3D graphics for professional
    artists](http://www.k-3d.org)

<!-- end list -->

  - [Hiawatha webserver](http://www.hiawatha-webserver.org/) - A
    webserver with the focus on being very secure and easy to use.

<!-- end list -->

  - [KVIrc - The K-Visual IRC Client](http://kvirc.net)

<!-- end list -->

  - [LyX - a document processor](http://www.lyx.org)

<!-- end list -->

  - [Mendeley Desktop - a Qt4 document-management and collaboration tool
    for academic researchers.](http://www.mendeley.com)

<!-- end list -->

  - [MojoSetup - a cross-platform software
    installer](http://icculus.org/mojosetup/)

<!-- end list -->

  - [OpenCog - An artificial intelligence
    framework](http://opencog.org/)

<!-- end list -->

  - [OpenWengo - an open source VoIP telephony
    application](http://www.openwengo.org/)

<!-- end list -->

  - [ParaView Parallel Visualization
    Application](http://www.paraview.org)

<!-- end list -->

  - [Pdf Presenter
    Console](http://westhoffswelt.de/projects/pdf_presenter_console.html)
    The Pdf Presenter Console (PPC) is a GTK based presentation viewer
    application which uses Keynote like multi-monitor output to provide
    meta information to the speaker during the presentation. It is able
    to show a normal presentation window on one screen, while showing a
    more sophisticated overview on the other one providing information
    like a picture of the next slide, as well as the left over time till
    the end of the presentation. The input files processed by PPC are
    PDF documents, which can be created using nearly any of today's
    presentation software.

<!-- end list -->

  - [QMCPACK Quantum Monte Carlo Package for
    HPC](http://cms.mcc.uiuc.edu/qmcpack)

<!-- end list -->

  - [QTM](http://qtm.blogistan.co.uk/) - A desktop blogging client based
    on Qt 4.

<!-- end list -->

  - [RUGUD (Rapid Unified Generation of Urban
    Databases)](http://www.rapidterrain.com)
    Plugin-based distributed terrain database production framework
    developed for the U.S. Department of Defense, aimed at scalable
    production of high-resolution visual and SAF terrain databases.

<!-- end list -->

  - [Rosegarden - a MIDI and audio sequencer and musical notation
    editor](http://www.rosegardenmusic.com/)

<!-- end list -->

  - [SCIRun](http://software.sci.utah.edu/)
    A visual programming environment for modeling, simulation, and
    visualization, incorporating thirdparty packages such as Teem,
    Matlab, and the Insight Toolkit. SCIRun also includes Seg3D, a
    standalone executable for the segmentation of volumetric image data.

<!-- end list -->

  - [Scribus](http://www.scribus.net/) - a powerful Open Source desktop
    publishing application, developed primarily developed for Linux, now
    also available for Mac OS X and Windows\]

<!-- end list -->

  - [Second Life](http://secondlife.com/) - Second Life® is a 3-D
    virtual world created by its Residents. Since opening to the public
    in 2003, it has grown explosively and today is inhabited by millions
    of Residents from around the globe.

<!-- end list -->

  - [Slicer - Medical Visualization and Processing Environment for
    Research](http://www.slicer.org)

<!-- end list -->

  - [Toby - a LOGO-inspired TurtleGraphics
    environment](http://icculus.org/toby/)

<!-- end list -->

  - [VolView Interactive System for Volume
    Visualization](http://www.kitware.com/products/volview.html)

<!-- end list -->

  - [Wireshark](http://www.wireshark.org) Wireshark is the world's
    foremost network protocol analyzer, and is the de facto (and often
    de jure) standard across many industries and educational
    institutions.

<!-- end list -->

  - [Multivariate Data Visualization Tool -
    XmdvTool](http://davis.wpi.edu/~xmdv)

<!-- end list -->

  - [XTrkCAD - a CAD program for designing model
    railroads](http://www.xtrkcad.org)

<!-- end list -->

  - [QuteCom - a multiplattform and multiprotocol VoIP
    softphone](http://www.qutecom.org/)

<!-- end list -->

  - [GPSDrive - a GPS navigation system](http://www.gpsdrive.de)

<!-- end list -->

  - [Avogadro](http://avogadro.openmolecules.net) - advanced molecular
    editor designed for cross-platform use in computational chemistry,
    molecular modeling, bioinformatics, materials science, and related
    areas. It offers flexible rendering and a powerful plugin
    architecture.

<!-- end list -->

  - [Sentinella - A KDE app that asociates the system activity to
    actions](http://sentinella.sourceforge.net/)

<!-- end list -->

  - [The RobotCub project](http://www.robotcub.org) realized an open
    platform for research in embodied cognition. For our software we use
    cmake ([RobotCub online manual](http://eris.liralab.it)).

<!-- end list -->

  - [WeeChat - Fast, light and extensible chat
    client](http://www.weechat.org/)

<!-- end list -->

  - [Taskwarrior / Taskserver - Multiplatform task management with
    syncing](http://taskwarrior.org/)

# Controls

  - [wxArt2d](http://wxart2d.sourceforge.net/)

# Other

  - [PLplot](http://plplot.sourceforge.net) is a mix of a core
    scientific plotting library written in C, multiple computer language
    interfaces to that library (some of them generated using
    [SWIG](http://www.swig.org/)), a set of 20+ test examples written
    for each computer language interface, multiple plotting device
    driver plug-ins that are dynamically loaded by our core library, and
    a complete docbook-based documentation build. This build complexity
    is handled with ease by CMake on Linux (with good ctest results for
    the examples written in each computer language that we interface).
    We are also beginning to get encouraging build results on the Mac OS
    X and windows platforms.

<!-- end list -->

  - [OpenGC - The Open Source Glass Cockpit
    Project](http://www.opengc.org/index.html)

<!-- end list -->

  - [GPUSEG - Real-Time, GPU-Based Foreground-Background
    Segmentation](http://oscar.vision.ee.ethz.ch/gpuseg)

<!-- end list -->

  - [OpenMOIV - is an object-oriented, 3D multi-platform toolkit that
    helps you develop molecular visualization
    applications](http://www.tecn.upf.es/openMOIV/)

<!-- end list -->

  - [Avida - Digital Life
    Platform](http://devolab.cse.msu.edu/software/avida)

<!-- end list -->

  - [Storm](http://storm.bmi.ohio-state.edu/documentation.php)

<!-- end list -->

  - [Octaviz - Viz for Octave](http://www.octave.org)

<!-- end list -->

  - [Annot3D - 3D annotation
    system](http://www.sci.utah.edu/research/annot3d.html)

<!-- end list -->

  - [MIND - DICOM query/move
    tool](http://caddlab.rad.unc.edu/software/MIND)

<!-- end list -->

  - [ohmms - object-oriented high-performance solutions for multi-scale
    materials
    simulations](http://www.mcc.uiuc.edu/ohmms/)

<!-- end list -->

  - [HornRegistration](http://www.atracsys.com/_opensource/HornRegistrationDoc/html/)

<!-- end list -->

  - [CMake User Review](http://software.ericsink.com/20040129.html)

<!-- end list -->

  - [Yzis - a brand new editor inspired by vim](http://www.yzis.org/)

<!-- end list -->

  - [MiKTeX - TeX implementation for the Windows operating
    system](http://miktex.org/)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/Projects) in another wiki.
