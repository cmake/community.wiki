## Build rules

Build rules are defined in CMakeCInformation.cmake and
CMakeCXXInformation.cmake. Essentially, they're a template for the
command CMake uses to invoke the compiler or linker to do different
tasks.

Rules for C++ sources:

* `CMAKE_CXX_CREATE_SHARED_LIBRARY`
* `CMAKE_CXX_CREATE_SHARED_MODULE`
* `CMAKE_CXX_CREATE_STATIC_LIBRARY`
* `CMAKE_CXX_COMPILE_OBJECT`
* `CMAKE_CXX_LINK_EXECUTABLE`

and the equivalents for C sources:

* `CMAKE_C_CREATE_SHARED_LIBRARY`
* `CMAKE_C_CREATE_SHARED_MODULE`
* `CMAKE_C_CREATE_STATIC_LIBRARY`
* `CMAKE_C_COMPILE_OBJECT`
* `CMAKE_C_LINK_EXECUTABLE`

You can override the variables manually, e.g. replacing some flags in
the linker command, but you can't change the value of the variables in
sharp braces. Usually you don't have to change these rules, only in rare
cases. You should only do this if you know what you are doing and there
is no other way.

## Expansion Rules

There are a couple dozen different less-then-greater-then tags that you
can put in build rules. From examining the source code the following
style names exist:

* `ASSEMBLY_SOURCE`
* `FLAGS`
* `LANGUAGE_COMPILE_FLAGS`
* `LINK_FLAGS`
* `LINK_LIBRARIES`
* `OBJECT`
* `OBJECTS`
* `OBJECTS_QUOTED`
* `OBJECT_DIR`
* `PREPROCESSED_SOURCE`
* `SOURCE`
* `TARGET`
* `TARGET_BASE`
* `TARGET_IMPLIB`
* `TARGET_INSTALLNAME_DIR`
* `TARGET_PDB`
* `TARGET_QUOTED`
* `TARGET_SONAME`
* `TARGET_VERSION_MAJOR`
* `TARGET_VERSION_MINOR`

Please note you can set these properties globally using SET or LIST, or
for a single target using SET_TARGET_PROPERTIES.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Build_Rules) in another wiki.
