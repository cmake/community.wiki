## CMake, CPack, CTest builtin documentation

All CMake tools command line do have some builtin documentation support.
The tools (cmake, cpack, ctest) supports a variable subset of:

    ctool --help-command[-list]
    ctool --help-module[-list]
    ctool --help-variable[-list]
    ctool --help-policy[-list]
    ctool --help-property[-list]
    ctool --help-full
    ctool --help-html

for the detail see:

    cmake --help
    cpack --help
    ctest --help

This builtin documentation comes from 2 different places:

1.  static variables in C++ sources
2.  text surrounded by markup in CMake script files

## Documentation in C++ sources

This is the primary source of builtin documentation.

## Documentation markup in CMake script files

This source is available since CMake 2.8.8 and is currently only used by
CPack. The idea behind CMake script markup is to be able to document
CMake scripts. Documenting a script means being able to put in
structured documentation using some comments.

Before 2.8.8, this was available through:

    cmake --help-module

which was basically extracting the first block of comments stripping out
the comment character ('\#').

From 2.8.8 and up we may be able to do more by using a basic markup
language which may be used to document:

  - variable
  - command
  - section/module

The markup may then be automatically extracted by ctool (cmake, cpack,
ctest) in order to feed their

    --help-variable[s|-list]
    --help-command[s|-list]

command lines options.

A comment in a CMake script is a line which begin by a '\#' character. A
markup line is a special line which begin with a '`##`' (double '\#')
followed by a markup word. The list of currently supported markup words
is:

  - section
  - module
  - variable
  - macro
  - end

A documentation block must always begins with a non-end markup line and
finishes with a `##end` markup line.

### Variable

### Command

### Section

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_builtin_documentation_handling) in another wiki.
