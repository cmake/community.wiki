# CMake Articles

  - [The CMake Build Manager - Cross platform and open
    source](http://www.ddj.com/documents/s=7758/ddj0301d/), William
    Hoffman and Ken Martin, Dr. Dobbs, January 2003

<!-- end list -->

  - [Cross-Platform Software Development Using
    CMake](http://www.linuxjournal.com/article/6700), Andy Cedilnik,
    LinuxJournal, October 2003

<!-- end list -->

  - [KDE switches to CMake](http://lwn.net/Articles/188693/), Alexander
    Neundorf, LinuxWeeklyNews, June 2006

<!-- end list -->

  - [CMake: The Cross Platform Build
    System](http://clubjuggler.livejournal.com/138364.html), Tanner
    Lovelace, Linux Magazine, July 2006

<!-- end list -->

  - [Building eCos applications with
    CMake](http://www.linuxdevices.com/articles/AT6762290643.html),
    Alexander Neundorf, LinuxDevices, November 2006

<!-- end list -->

  - [Einführung in das Buildsystem
    CMake](http://www.linuxmagazin.de/heft_abo/ausgaben/2007/02/mal_ausspannen?category=0),
    Alexander Neundorf, LinuxMagazin, Februar 2007

<!-- end list -->

  - [Overview over
    buildsystems](http://freshmeat.net/articles/view/1715/)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:Articles) in another wiki.
