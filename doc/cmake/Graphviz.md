# Generating Dependency Graphs with CMake

By using CMake you can automatically generate dependency graphs
(dot/graphviz) of the targets in your project. This works for
dependencies within the project, as well as dependencies to external
libraries.

## Usage

To use it, run cmake in the build tree:

cmake --graphviz=test.dot .

## Customize graphviz output

The output can be customized with the following options in
CMakeGraphVizOptions.cmake (located in base source directory)

### graphviz options

  - GRAPHVIZ_GRAPH_TYPE - default: 'digraph'
  - GRAPHVIZ_GRAPH_NAME - default: 'GG'
  - GRAPHVIZ_GRAPH_HEADER - default: 'node \[\\n fontsize =
    \\"12\\"\\n\];'
  - GRAPHVIZ_NODE_PREFIX - default: 'node'

### module inclusion config

  - GRAPHVIZ_EXECUTABLES - Export executables (default: ON)
  - GRAPHVIZ_STATIC_LIBS - Export static libs (default: ON)
  - GRAPHVIZ_SHARED_LIBS - Export shared libs (default: ON)
  - GRAPHVIZ_MODULE_LIBS - Export modules (default: ON)
  - GRAPHVIZ_EXTERNAL_LIBS - Export external libs (default: ON)
  - GRAPHVIZ_IGNORE_TARGETS - Do not export targets in the given list.
    Since 2.8.5 this supports regular expressions.
  - GRAPHVIZ_IGNORE_TARGETS_REGEX - Do not export targets which match
    the given regex. Not supported anymore since 2.8.5.

## Example output

-----

Dependencies within CMake:

![CMake-graph](/uploads/105cf44c9c67e3feca1f72eff1d19cab/CMake-graph.png)

-----

Just CMake dependencies: <graphviz>digraph GG { node \[

` fontsize = "12"`

\];

`   "node58" [ label="/usr/lib/libcurses.so" shape="ellipse"];`
`   "node42" [ label="CMakeLib" shape="diamond"];`
`   "node43" [ label="CPackLib" shape="diamond"];`
`   "node44" [ label="CTestLib" shape="diamond"];`
`   "node45" [ label="DumpDocumentation" shape="house"];`
`   "node47" [ label="ccmake" shape="house"];`
`   "node41" [ label="cmForm" shape="diamond"];`
`   "node40" [ label="cmXMLRPC" shape="diamond"];`
`   "node48" [ label="cmake" shape="house"];`
`   "node37" [ label="cmcurl" shape="diamond"];`
`   "node39" [ label="cmexpat" shape="diamond"];`
`   "node32" [ label="cmsys" shape="diamond"];`
`   "node38" [ label="cmtar" shape="diamond"];`
`   "node36" [ label="cmzlib" shape="diamond"];`
`   "node49" [ label="cpack" shape="house"];`
`   "node50" [ label="ctest" shape="house"];`
`   "node57" [ label="dl" shape="ellipse"];`
`   "node34" [ label="testDynamicLoader" shape="house"];`
`   "node32" -> "node57"`
`   "node34" -> "node32"`
`   "node37" -> "node57"`
`   "node37" -> "node36"`
`   "node40" -> "node39"`
`   "node40" -> "node37"`
`   "node41" -> "node58"`
`   "node42" -> "node32"`
`   "node42" -> "node39"`
`   "node42" -> "node36"`
`   "node42" -> "node38"`
`   "node43" -> "node42"`
`   "node44" -> "node42"`
`   "node44" -> "node37"`
`   "node44" -> "node40"`
`   "node45" -> "node42"`
`   "node47" -> "node42"`
`   "node47" -> "node41"`
`   "node48" -> "node42"`
`   "node49" -> "node43"`
`   "node50" -> "node44"`

}</graphviz>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:For_CMake_Hackers) in another wiki.
