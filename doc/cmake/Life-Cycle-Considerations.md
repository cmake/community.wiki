## Life cycle considerations

When deciding which minimum version of cmake shall be required by a
particular project, it is always good to consider the operating system
versions of your potential userbase. If your users cannot use the cmake
version supplied by their linux distribution or software vendor, they
might be less willing to try out newer versions of your project.

## CMake versions available in several linux/UNIX distributions

The following table shows cmake versions available in several operating
system releases. Only releases are shown that are still in service (i.e.
are not marked as obsolete by their respective vendor). Distributions
without clearly defined release versions ("rolling releases") are not
included in this
table.

| Release Date | Distribution Version / Codename | Supported until | CMake Version available |
| ------------ | ------------------------------- | --------------- | ----------------------- |
| 2019-04-30   | Fedora 30                       | ~ 2020-05 [^1] | 3.14.5                   |
| 2018-10-30   | Fedora 29                       | ~ 2019-11 [^3] | 3.14.5                   |
| 2018-04-26   | Ubuntu 18.04 LTS                | 2023-04         | 3.10.2                  |
| 2017-06-17   | Debian 9.0 (stretch)            | n/a [^2]       | 3.7.2                   |
| 2016-04-21   | Ubuntu 16.04 LTS (Xenial Xerus) | 2021-04         | 3.5.1                   |
| 2015-06-06   | Debian 8.0 (jessie)             | n/a [^4]       | 3.0.2                   |
| 2014-11-04   | openSUSE 11.04                  | tba             | 3.0.2                   |
| 2014-10-27   | SUSE Linux Enterprise 12        | 2024-10-31      | ?                       |
| 2014-06-09   | RHEL/CentOS 7                   | 2024-06         | 2.8.11 [^5]            |
| 2012-09-28   | Slackware 14.0                  | n/a             | 2.8.8                   |
| 2010-11-10   | RHEL/CentOS 6                   | 2020-11-30      | 2.6.4 [^6]              |

CMake version supplied by various linux distributions, by date

### Sources

This information has been compiled from the following sources:

  - https://repology.org/metapackage/cmake/versions
  - <http://fr2.rpmfind.net/linux/rpm2html/search.php?query=cmake>
  - Debian [Releases](http://www.debian.org/releases/) [CMake
    Package](http://packages.debian.org/search?keywords=cmake)
  - Ubuntu
    [Releases](https://en.wikipedia.org/wiki/List_of_Ubuntu_releases#Table_of_versions)
    [CMake Package](http://packages.ubuntu.com/search?keywords=cmake)
  - Opensuse
    [Releases](https://en.wikipedia.org/wiki/Opensuse#Version_history)
    [Community updates
    ("Evergreen")](http://en.opensuse.org/openSUSE:Evergreen)
  - Fedora [Releases](http://fedoraproject.org/wiki/Releases) [CMake
    Package](https://admin.fedoraproject.org/updates/cmake)
  - Redhat [Releases](https://access.redhat.com/knowledge/articles/3078)
  - <http://support.novell.com/lifecycle/> (search term "SUSE linux
    enterprise")
  - Mandriva [Releases](http://www.mandriva.com/en/support/lifecycle/)
  - <http://linux.web.cern.ch/linux/scientific5/>

If you have information on other linux or unix distributions, please add
them to the appropriate table. Be sure to add the source of your
information to the list of source, so others can update the information
more easily.

## CMake release dates

| CMake Version | Released   |
| ------------- | ---------- |
| 3.5.0         | 2016-03-08 |
| 3.4.3         | 2016-01-25 |
| 3.4.2         | 2016-01-19 |
| 3.4.1         | 2015-12-02 |
| 3.4.0         | 2015-11-12 |
| 3.3.2         | 2015-09-17 |
| 3.3.1         | 2015-08-13 |
| 3.3.0         | 2015-07-23 |
| 3.2.2         | 2015-04-13 |
| 3.2.3         | 2015-06-01 |
| 3.2.1         | 2015-03-10 |
| 3.2.0         | 2015-03-04 |
| 3.1.3         | 2015-02-12 |
| 3.1.2         | 2015-02-05 |
| 3.1.1         | 2015-01-22 |
| 3.1.0         | 2014-12-17 |
| 3.0.2         | 2014-09-11 |
| 3.0.1         | 2014-08-04 |
| 3.0.0         | 2014-06-10 |
| 2.8.12        | 2013-11-05 |
| 2.8.11        | 2013-05-15 |
| 2.8.10        | 2012-10-31 |
| 2.8.9         | 2012-08-09 |
| 2.8.8         | 2012-04-19 |
| 2.8.7         | 2012-01-02 |
| 2.8.6         | 2011-10-04 |
| 2.8.5         | 2011-07-14 |
| 2.8.4         | 2011-02-16 |
| 2.8.3         | 2010-11-03 |
| 2.8.2         | 2010-06-28 |
| 2.8.1         | 2010-04-05 |
| 2.8.0         | 2009-11-04 |
| 2.6.4         | 2009-05-05 |
| 2.6.3         | 2009-02-23 |
| 2.6.2         | 2008-09-25 |
| 2.6.1         | 2008-08-01 |
| 2.6.0         | 2008-05-06 |
| 2.4.8         | 2008-04-01 |
| 2.4.7         | 2007-06-16 |
| 2.4.6         | 2007-01-13 |
| 2.4.5         | 2006-12-04 |
| 2.4.4         | 2006-11-20 |
| 2.4.3         | 2006-07-31 |
| 2.4.2         | 2006-07-13 |
| 2.4.1         | 2006-05-01 |
| 2.4           | 2006-05-03 |
| 2.0           | 2004-06-11 |

Release dates of CMake versions [^7] [^8]

## References
[^1]:  One month after release of Fedora 32
[^2]:  usually around 1 year after new stable is released
[^3]:  One month after release of Fedora 31
[^4]:  usually around 1 year after new stable is released
[^5]:  as seen here:
    <http://mirror.centos.org/centos/7/os/x86_64/Packages/>
[^6]:  based on CentOS entries in
    <http://fr2.rpmfind.net/linux/rpm2html/search.php?query=cmake>
[^7]:  <http://www.kitware.com/news/home/browse/CMake>
[^8]:  <http://cmake.org/gitweb?p=cmake.git;a=tags>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Life_Cycle_Considerations) in another wiki.
