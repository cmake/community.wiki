  - Download the files from here:
    <http://www.cmake.org/HTML/Download.html>

You want the Win32 installer. Simply download and run the installer.
Accept the defaults for everything except when it asks you to add CMake
to the system path. For this choice, tell it to add CMake to the system
path for all users.

## GraphicalTutorials

  - See an example of how to use CMake to build a library here: [Windows
    XP, CMake 2.4](doc/cmake/obtaining/ExampleWindowsXPCMake24 "wikilink")

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/Obtaining) in another wiki.
