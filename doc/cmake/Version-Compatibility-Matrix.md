# Compatibility across different CMake versions

***This page applies to cmake versions 2.6.0 to 2.8.12.***

Below you can find a list of features and when they were introduced to
cmake. The features are grouped by their respective section in the
documentation. Entries marked in green show available, fully
upwards-compatible features. Entries in yellow mark changes in features
that are available in higher versions of CMake, but not in this version.
Entries in red mark incompatible changes or not available features.

For size-reasons, the matrix has been split into the following subpages:

  - [CMake
    Options](doc/cmake/version_compatibility_matrix/Options "wikilink")
  - [Commands](doc/cmake/version_compatibility_matrix/Commands "wikilink")
  - [Properties](doc/cmake/version_compatibility_matrix/Properties "wikilink")
      - [Properties of Global
        Scope](doc/cmake/version_compatibility_matrix/Properties-of-Global-Scope "wikilink")
      - [Properties on
        Directories](doc/cmake/version_compatibility_matrix/Properties-on-Directories "wikilink")
      - [Properties on
        Targets](doc/cmake/version_compatibility_matrix/Properties-on-Targets "wikilink")
      - [Properties on
        Tests](doc/cmake/version_compatibility_matrix/Properties-on-Tests "wikilink")
      - [Properties on Source
        Files](doc/cmake/version_compatibility_matrix/Properties-on-Source-Files "wikilink")
      - [Properties on Cache
        Entries](doc/cmake/version_compatibility_matrix/Properties-on-Cache-Entries "wikilink")
  - Standard CMake Modules
      - [A -
        FindK\*](doc/cmake/version_compatibility_matrix/StandardCMakeModulesA "wikilink")
      - [FindL\* -
        Z](doc/cmake/version_compatibility_matrix/StandardCMakeModulesFindL "wikilink")
  - [Variables](doc/cmake/version_compatibility_matrix/Variables "wikilink")
      - [Variables That Change
        Behavior](doc/cmake/version_compatibility_matrix/Variables-That-Change-Behavior "wikilink")
      - [Variables That Describe The
        System](doc/cmake/version_compatibility_matrix/Variables-That-Describe-the-System "wikilink")
      - [Variables for
        Languages](doc/cmake/version_compatibility_matrix/Variables-for-Languages "wikilink")
      - [Variables That Control The
        Build](doc/cmake/version_compatibility_matrix/Variables-that-Control-the-Build "wikilink")
      - [Variables That Provide
        Information](doc/cmake/version_compatibility_matrix/Variables-that-Provide-Information "wikilink")

## Additional Notes

When writing CMake code that is to be compatible with previous releases
of CMake, not only look out for changes in the API, but also consider
changes in behavior between different versions of CMake. When such a
change in behavior occurs, it is often documented in the description of
CMake-[Policies](http://www.cmake.org/cmake/help/cmake-2-8-docs.html#section_Policies).

# General Information

The information on this page was compiled from the plain-text
documentation of the various CMake releases (`cmake --help-full`). The
complete documentation for all CMake releases can be viewed on the page
[CMake Released Versions](doc/cmake/version_compatibility_matrix/Released-Versions "wikilink").

Sometimes the documentation text changes between two releases, even
though the functionality remains the same. E.g. a later release of CMake
may contain clarifications or documentation for previously
un(der)documented features. Although care has been taken in trying to
correctly identify all changes, and in minimizing false positives, some
of the information on this page may still contain errors.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Version_Compatibility_Matrix) in another wiki.
