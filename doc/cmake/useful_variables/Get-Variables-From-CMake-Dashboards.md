## Instructions

Save the following section as a script file called
GetVariablesFromCMakeDashboards.cmake.

Then run the following command from a command prompt (bash):

    cmake -P GetVariablesFromCMakeDashboards.cmake > CMakeVars.xml 2>&1

After a few minutes, you should have a CMakeVars.xml reflecting today's
dashboard data\!

The section following the script section is a small subset of sample
data from the dashboards for September 12, 2006. I didn't post the full
result because it's over a Megabyte and easily reproducible using the
script...

## Script

    #
    # GetVariablesFromCMakeDashboards.cmake
    #
    # This is a CMake "-P script" to poll today's CMake dashboards for variable
    # names and values. It gathers all the CMake variables used on all the CMake
    # dashboards that have reported in today.
    #
    # Depends on:
    #  cmake, of course (version 2.4 or later, for EXECUTE_PROCESS)
    #  curl
    #
    # When a CMake dashboard is run, there is usually a SystemInformation test
    # that runs. Part of the output of that test is to list all of the names and
    # values of all of the CMake variables in use at the time the test is run.
    # This CMake script uses regular expressions to find the start and end of the
    # "AllVariables.txt" section of the SystemInformation test output and
    # prints the results out in the form of XML.
    #
    #
    # The CMake MESSAGE command prints to stderr, so you have to use a technique
    # like this to get the output of this script as an XML file:
    #
    #   cmake -P GetVariablesFromCMakeDashboards.cmake > CMakeVars.xml 2>&1
    #
    #
    # Once you have this XML file, it is a trivial matter to open it with Excel as
    # an "XML List" and interact with it to see the scope and breadth of CMake variables
    # that are defined/used on all the dashboard machines for the SystemInformation
    # test.
    #
    # Feel free to apply other XML analysis tools and techniques to this data as well.
    # I imagine that one of the things this output will be used for is the starting point
    # (the complete list of all predefined CMake variables everywhere) for documenting
    # "predefined" CMake variables.
    #
    #
    # The procedure used to obtain this information looks like this:
    #
    # (1) Pull the contents of:
    #     http://www.cmake.org/Testing/Dashboard/MostRecentResults-Nightly/TestOverviewByCount.html
    #
    # (2) Analyze the results of (1) to get today's nightly dashboard stamp so we can pull something like:
    #     http://www.cmake.org/Testing/Dashboard/20060912-0100-Nightly/TestDetail/__Source_SystemInformation.html
    #
    # (3) For each line in the table in the results of (2) where the test has status "Passed",
    #     pull the SystemInformation output for that build/site/buildstamp... and print all
    #     the variables found there as XML output.
    #
    CMAKE_MINIMUM_REQUIRED(VERSION 2.4 FATAL_ERROR)


    IF(NOT DEFINED AD_CURL_CMD)
      FIND_PROGRAM(AD_CURL_CMD "curl"
        PATHS "C:/cygwin/bin"
        )
    ENDIF(NOT DEFINED AD_CURL_CMD)
    IF(NOT AD_CURL_CMD)
      MESSAGE(FATAL_ERROR "ERROR: curl is required to run this script...")
    ENDIF(NOT AD_CURL_CMD)


    IF(NOT DEFINED AD_BASE_URL)
      SET(AD_BASE_URL "http://www.cmake.org/Testing")
    ENDIF(NOT DEFINED AD_BASE_URL)

    IF(NOT DEFINED AD_MOSTRECENT_URL)
      SET(AD_MOSTRECENT_URL "${AD_BASE_URL}/Dashboard/MostRecentResults-Nightly")
    ENDIF(NOT DEFINED AD_MOSTRECENT_URL)

    IF(NOT DEFINED AD_OVERVIEW_URL)
      SET(AD_OVERVIEW_URL "${AD_MOSTRECENT_URL}/TestOverviewByCount.html")
    ENDIF(NOT DEFINED AD_OVERVIEW_URL)


    # Construct today's nightly AD_DATESTAMP from AD_OVERVIEW_URL's redirect instruction:
    #
    EXECUTE_PROCESS(
      COMMAND "${AD_CURL_CMD}" "${AD_OVERVIEW_URL}"
      RESULT_VARIABLE AD_CURL_RV
      ERROR_VARIABLE AD_CURL_EV
      OUTPUT_VARIABLE AD_CURL_OV
      )
    IF(NOT "${AD_CURL_RV}" STREQUAL "0")
      MESSAGE(FATAL_ERROR "ERROR: curl failed... AD_OVERVIEW_URL='${AD_OVERVIEW_URL}' AD_CURL_RV='${AD_CURL_RV}' AD_CURL_EV='${AD_CURL_EV}'")
    ENDIF(NOT "${AD_CURL_RV}" STREQUAL "0")

    STRING(REGEX REPLACE "^.*META HTTP-EQUIV=.Refresh.*URL=(.*)'>.*$" "\\1" AD_RELATIVE_URL ${AD_CURL_OV})

    STRING(REGEX REPLACE "^../([^/]+)/.*$" "\\1" AD_DATESTAMP "${AD_RELATIVE_URL}")


    # Get the list of dashboards to poll for SystemInformation test results:
    #
    SET(AD_SYSINFO_LIST "${AD_BASE_URL}/Dashboard/${AD_DATESTAMP}/TestDetail/__Source_SystemInformation.html")
    EXECUTE_PROCESS(
      COMMAND "${AD_CURL_CMD}" "${AD_SYSINFO_LIST}"
      RESULT_VARIABLE AD_CURL_RV
      ERROR_VARIABLE AD_CURL_EV
      OUTPUT_VARIABLE AD_CURL_OV
      )
    IF(NOT "${AD_CURL_RV}" STREQUAL "0")
      MESSAGE(FATAL_ERROR "ERROR: curl failed... AD_SYSINFO_LIST='${AD_SYSINFO_LIST}' AD_CURL_RV='${AD_CURL_RV}' AD_CURL_EV='${AD_CURL_EV}'")
    ENDIF(NOT "${AD_CURL_RV}" STREQUAL "0")

    # Convert output into a CMake list (where each element is one line of the output):
    #
    STRING(REGEX REPLACE ";" "\\\\;" AD_CURL_OV "${AD_CURL_OV}")
    STRING(REGEX REPLACE "\n" ";" AD_CURL_OV "${AD_CURL_OV}")

    # Gather the list of dashboard builds where the SystemInformation test passed:
    #
    FOREACH(item ${AD_CURL_OV})
      IF("${item}" MATCHES ".*<td class=.pass.><a href=..*/Sites/([^/]+)/([^/]+)/([^/]+)/.*.>.*")
        SET(AD_DASHBOARDS ${AD_DASHBOARDS} ${item})
      ENDIF("${item}" MATCHES ".*<td class=.pass.><a href=..*/Sites/([^/]+)/([^/]+)/([^/]+)/.*.>.*")
    ENDFOREACH(item)


    # Output opening XML:
    #
    MESSAGE("<Dashboards>")
    #MESSAGE("<Dashboards")
    #MESSAGE("  AD_CURL_CMD='${AD_CURL_CMD}'")
    #MESSAGE("  AD_BASE_URL='${AD_BASE_URL}'")
    #MESSAGE("  AD_MOSTRECENT_URL='${AD_MOSTRECENT_URL}'")
    #MESSAGE("  AD_OVERVIEW_URL='${AD_OVERVIEW_URL}'")
    #MESSAGE("  AD_RELATIVE_URL='${AD_RELATIVE_URL}'")
    #MESSAGE("  AD_DATESTAMP='${AD_DATESTAMP}'")
    #MESSAGE(">")


    # For each dashboard, try to get the list of CMake variables
    # spewed by the SystemInformation test...
    #
    FOREACH(item ${AD_DASHBOARDS})
      STRING(REGEX REPLACE ".*<td class=.pass.><a href=..*/(Sites/.*SystemInformation.html).>.*" "\\1" AD_PARTIAL_URL "${item}")
      SET(AD_SYSINFOTEST_URL "${AD_BASE_URL}/${AD_PARTIAL_URL}")

      STRING(REGEX REPLACE ".*<td class=.pass.><a href=..*/Sites/([^/]+)/([^/]+)/([^/]+)/.*.>.*" "\\1" AD_SITE_NAME "${item}")
      STRING(REGEX REPLACE ".*<td class=.pass.><a href=..*/Sites/([^/]+)/([^/]+)/([^/]+)/.*.>.*" "\\2" AD_BUILD_NAME "${item}")
      STRING(REGEX REPLACE ".*<td class=.pass.><a href=..*/Sites/([^/]+)/([^/]+)/([^/]+)/.*.>.*" "\\3" AD_BUILD_STAMP "${item}")

      MESSAGE(" <Dashboard site='${AD_SITE_NAME}' build='${AD_BUILD_NAME}' buildstamp='${AD_BUILD_STAMP}'>")

      EXECUTE_PROCESS(
        COMMAND "${AD_CURL_CMD}" "${AD_SYSINFOTEST_URL}"
        RESULT_VARIABLE AD_CURL_RV
        ERROR_VARIABLE AD_CURL_EV
        OUTPUT_VARIABLE AD_CURL_OV
        )
      IF(NOT "${AD_CURL_RV}" STREQUAL "0")
        MESSAGE("  <ERROR message='curl failed... AD_SYSINFOTEST_URL=${AD_SYSINFOTEST_URL} AD_CURL_RV=${AD_CURL_RV} AD_CURL_EV=${AD_CURL_EV}'/>")
      ELSE(NOT "${AD_CURL_RV}" STREQUAL "0")

        # Convert output into a CMake list (where each element is one line of the output):
        #
        STRING(REGEX REPLACE ";" "\\\\;" AD_CURL_OV "${AD_CURL_OV}")
        STRING(REGEX REPLACE "\n" ";" AD_CURL_OV "${AD_CURL_OV}")

        SET(inscope_variables 0)
        FOREACH(vitem ${AD_CURL_OV})
          IF("${vitem}" MATCHES "^Contents of .*AllVariables.txt.:$")
            SET(inscope_variables 1)
          ENDIF("${vitem}" MATCHES "^Contents of .*AllVariables.txt.:$")

          IF("${inscope_variables}" STREQUAL "1")
            IF("${vitem}" MATCHES "^Avoid ctest truncation of output")
              SET(inscope_variables 0)
            ELSE("${vitem}" MATCHES "^Avoid ctest truncation of output")
              IF("${vitem}" MATCHES "^([^ ]+) \\\"(.*)\\\"$")
                STRING(REGEX REPLACE "^([^ ]+) \\\"(.*)\\\"$" "\\1" AD_VAR_NAME "${vitem}")
                STRING(REGEX REPLACE "^([^ ]+) \\\"(.*)\\\"$" "\\2" AD_VAR_VALUE "${vitem}")

                # Since we use '' as xml attribute delimiter:
                STRING(REGEX REPLACE "'" "&apos;" AD_VAR_VALUE "${AD_VAR_VALUE}")

                MESSAGE("  <Variable name='${AD_VAR_NAME}' value='${AD_VAR_VALUE}'/>")
              ENDIF("${vitem}" MATCHES "^([^ ]+) \\\"(.*)\\\"$")
            ENDIF("${vitem}" MATCHES "^Avoid ctest truncation of output")
          ENDIF("${inscope_variables}" STREQUAL "1")
        ENDFOREACH(vitem)
      ENDIF(NOT "${AD_CURL_RV}" STREQUAL "0")

      MESSAGE(" </Dashboard>")
    ENDFOREACH(item)


    # Output closing XML:
    #
    MESSAGE("</Dashboards>")

## Sample data

    <Dashboards>
     <Dashboard site='r36n11.pbm.ihost.com' build='AIX53-xlC' buildstamp='20060912-0100-Nightly'>
      <Variable name='CMAKE_AR' value='/usr/bin/ar'/>
      <Variable name='CMAKE_BACKWARDS_COMPATIBILITY' value='2.5'/>
      <Variable name='CMAKE_BASE_NAME' value='xlC'/>
      <Variable name='CMAKE_BINARY_DIR' value='/bench1/noibm34/bench-AIX5300CFB9-xlC/CMake-AIX53-xlC/Tests/SystemInformation'/>
      <Variable name='CMAKE_BUILD_TOOL' value='/usr/bin/make'/>
      <Variable name='CMAKE_BUILD_TYPE' value=''/>
      <Variable name='CMAKE_CACHEFILE_DIR' value='/bench1/noibm34/bench-AIX5300CFB9-xlC/CMake-AIX53-xlC/Tests/SystemInformation'/>
      <Variable name='CMAKE_CACHE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_CACHE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_CACHE_RELEASE_VERSION' value='development'/>
      <Variable name='CMAKE_CFG_INTDIR' value='.'/>
      <Variable name='CMAKE_COLOR_MAKEFILE' value='ON'/>
      <Variable name='CMAKE_COMMAND' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake-AIX53-xlC/bin/cmake'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCC_RUN' value='1'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCXX_RUN' value='1'/>
      <Variable name='CMAKE_CTEST_COMMAND' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake-AIX53-xlC/bin/ctest'/>
      <Variable name='CMAKE_CURRENT_BINARY_DIR' value='/bench1/noibm34/bench-AIX5300CFB9-xlC/CMake-AIX53-xlC/Tests/SystemInformation'/>
      <Variable name='CMAKE_CURRENT_SOURCE_DIR' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_CXX_COMPILER' value='/usr/vacpp/bin/xlC'/>
      <Variable name='CMAKE_CXX_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_CXX_COMPILER_ENV_VAR' value='CXX'/>
      <Variable name='CMAKE_CXX_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_CXX_COMPILE_OBJECT' value='<CMAKE_CXX_COMPILER>  <FLAGS> -o <OBJECT> -c <SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_ASSEMBLY_SOURCE' value='<CMAKE_CXX_COMPILER> <FLAGS> -S <SOURCE> -o <ASSEMBLY_SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_PREPROCESSED_SOURCE' value='<CMAKE_CXX_COMPILER> <FLAGS> -E <SOURCE> > <PREPROCESSED_SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_SHARED_LIBRARY' value='<CMAKE_CXX_COMPILER> <CMAKE_SHARED_LIBRARY_CXX_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS> <CMAKE_SHARED_LIBRARY_SONAME_CXX_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_CXX_CREATE_SHARED_MODULE' value='<CMAKE_CXX_COMPILER> <CMAKE_SHARED_LIBRARY_CXX_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS> <CMAKE_SHARED_LIBRARY_SONAME_CXX_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_CXX_CREATE_STATIC_LIBRARY' value='<CMAKE_AR> cr <TARGET> <LINK_FLAGS> <OBJECTS> ;<CMAKE_RANLIB> <TARGET> '/>
      <Variable name='CMAKE_CXX_FLAGS' value=' '/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG' value='-g'/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG_INIT' value='-g'/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL' value='-O -DNDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL_INIT' value='-O -DNDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE' value='-O -DNDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE_INIT' value='-O -DNDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO' value='-g'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT' value='-g'/>
      <Variable name='CMAKE_CXX_IGNORE_EXTENSIONS' value='inl;h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_CXX_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_LINKER_PREFERENCE' value='Prefered'/>
      <Variable name='CMAKE_CXX_LINK_EXECUTABLE' value='<CMAKE_CXX_COMPILER>  <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_CXX_OUTPUT_EXTENSION' value='.o'/>
      <Variable name='CMAKE_CXX_SOURCE_FILE_EXTENSIONS' value='C;M;c++;cc;cpp;cxx;m;mm'/>
      <Variable name='CMAKE_C_COMPILER' value='/usr/vacpp/bin/xlc'/>
      <Variable name='CMAKE_C_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_C_COMPILER_ENV_VAR' value='CC'/>
      <Variable name='CMAKE_C_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_C_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_C_COMPILE_OBJECT' value='<CMAKE_C_COMPILER> <FLAGS> -o <OBJECT>   -c <SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_ASSEMBLY_SOURCE' value='<CMAKE_C_COMPILER> <FLAGS> -S <SOURCE> -o <ASSEMBLY_SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_PREPROCESSED_SOURCE' value='<CMAKE_C_COMPILER> <FLAGS> -E <SOURCE> > <PREPROCESSED_SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_SHARED_LIBRARY' value='<CMAKE_C_COMPILER> <CMAKE_SHARED_LIBRARY_C_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS> <CMAKE_SHARED_LIBRARY_SONAME_C_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_C_CREATE_SHARED_MODULE' value='<CMAKE_C_COMPILER> <CMAKE_SHARED_LIBRARY_C_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS> <CMAKE_SHARED_LIBRARY_SONAME_C_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_C_CREATE_STATIC_LIBRARY' value='<CMAKE_AR> cr <TARGET> <LINK_FLAGS> <OBJECTS> ;<CMAKE_RANLIB> <TARGET> '/>
      <Variable name='CMAKE_C_FLAGS' value=' '/>
      <Variable name='CMAKE_C_FLAGS_DEBUG' value='-g'/>
      <Variable name='CMAKE_C_FLAGS_DEBUG_INIT' value='-g'/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL' value='-O -DNDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL_INIT' value='-O -DNDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE' value='-O -DNDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE_INIT' value='-O -DNDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO' value='-g'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO_INIT' value='-g'/>
      <Variable name='CMAKE_C_IGNORE_EXTENSIONS' value='h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_C_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_C_LINKER_PREFERENCE' value='None'/>
      <Variable name='CMAKE_C_LINK_EXECUTABLE' value='<CMAKE_C_COMPILER> <FLAGS> <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_C_OUTPUT_EXTENSION' value='.o'/>
      <Variable name='CMAKE_C_SOURCE_FILE_EXTENSIONS' value='c'/>
      <Variable name='CMAKE_DL_LIBS' value='-lld'/>
      <Variable name='CMAKE_EDIT_COMMAND' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake-AIX53-xlC/bin/ccmake'/>
      <Variable name='CMAKE_EXECUTABLE_SUFFIX' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_DEBUG' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_MINSIZEREL' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELEASE' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO' value=''/>
      <Variable name='CMAKE_FILES_DIRECTORY' value='/CMakeFiles'/>
      <Variable name='CMAKE_FIND_LIBRARY_PREFIXES' value='lib'/>
      <Variable name='CMAKE_FIND_LIBRARY_SUFFIXES' value='.so;.a'/>
      <Variable name='CMAKE_GENERATOR' value='Unix Makefiles'/>
      <Variable name='CMAKE_HOME_DIRECTORY' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_CXX' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C_SEP' value=''/>
      <Variable name='CMAKE_INIT_VALUE' value='FALSE'/>
      <Variable name='CMAKE_INSTALL_PREFIX' value='/usr/local'/>
      <Variable name='CMAKE_LIBRARY_PATH_FLAG' value='-L'/>
      <Variable name='CMAKE_LINK_LIBRARY_FLAG' value='-l'/>
      <Variable name='CMAKE_LINK_LIBRARY_SUFFIX' value=''/>
      <Variable name='CMAKE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_MAKE_PROGRAM' value='/usr/bin/make'/>
      <Variable name='CMAKE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_DEBUG' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELEASE' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO' value=''/>
      <Variable name='CMAKE_NUMBER_OF_LOCAL_GENERATORS' value='1'/>
      <Variable name='CMAKE_PARENT_LIST_FILE' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Tests/SystemInformation/CMakeLists.txt'/>
      <Variable name='CMAKE_PATCH_VERSION' value='0'/>
      <Variable name='CMAKE_PLATFORM_ROOT_BIN' value='/bench1/noibm34/bench-AIX5300CFB9-xlC/CMake-AIX53-xlC/Tests/SystemInformation/CMakeFiles'/>
      <Variable name='CMAKE_PROJECT_NAME' value='DumpInformation'/>
      <Variable name='CMAKE_RANLIB' value='/usr/bin/ranlib'/>
      <Variable name='CMAKE_ROOT' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS' value='-G -Wl,-brtl'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS' value='-G -Wl,-brtl'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CXX_FLAGS' value=' '/>
      <Variable name='CMAKE_SHARED_LIBRARY_C_FLAGS' value=' '/>
      <Variable name='CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS' value='-Wl,-brtl,-bexpall'/>
      <Variable name='CMAKE_SHARED_LIBRARY_LINK_C_FLAGS' value='-Wl,-brtl,-bexpall'/>
      <Variable name='CMAKE_SHARED_LIBRARY_PREFIX' value='lib'/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG_SEP' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_SUFFIX' value='.so'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_DEBUG' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELEASE' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO' value=''/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_CXX_FLAGS' value='-G -Wl,-brtl'/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_C_FLAGS' value='-G -Wl,-brtl'/>
      <Variable name='CMAKE_SHARED_MODULE_CXX_FLAGS' value=' '/>
      <Variable name='CMAKE_SHARED_MODULE_C_FLAGS' value=' '/>
      <Variable name='CMAKE_SHARED_MODULE_LINK_CXX_FLAGS' value='-Wl,-brtl,-bexpall'/>
      <Variable name='CMAKE_SHARED_MODULE_LINK_C_FLAGS' value='-Wl,-brtl,-bexpall'/>
      <Variable name='CMAKE_SHARED_MODULE_PREFIX' value='lib'/>
      <Variable name='CMAKE_SHARED_MODULE_SUFFIX' value='.so'/>
      <Variable name='CMAKE_SIZEOF_VOID_P' value='4'/>
      <Variable name='CMAKE_SKIP_RPATH' value='NO'/>
      <Variable name='CMAKE_SOURCE_DIR' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_STATIC_LIBRARY_PREFIX' value='lib'/>
      <Variable name='CMAKE_STATIC_LIBRARY_SUFFIX' value='.a'/>
      <Variable name='CMAKE_SYSTEM' value='AIX-3'/>
      <Variable name='CMAKE_SYSTEM_AND_CXX_COMPILER_INFO_FILE' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Modules/Platform/AIX-xlC.cmake'/>
      <Variable name='CMAKE_SYSTEM_AND_C_COMPILER_INFO_FILE' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Modules/Platform/AIX-xlc.cmake'/>
      <Variable name='CMAKE_SYSTEM_INFO_FILE' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Modules/Platform/AIX.cmake'/>
      <Variable name='CMAKE_SYSTEM_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_NAME' value='AIX'/>
      <Variable name='CMAKE_SYSTEM_PROCESSOR' value='powerpc'/>
      <Variable name='CMAKE_SYSTEM_SPECIFIC_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_VERSION' value='3'/>
      <Variable name='CMAKE_UNAME' value='/usr/bin/uname'/>
      <Variable name='CMAKE_USE_RELATIVE_PATHS' value='OFF'/>
      <Variable name='CMAKE_VERBOSE_MAKEFILE' value='FALSE'/>
      <Variable name='DumpInformation_BINARY_DIR' value='/bench1/noibm34/bench-AIX5300CFB9-xlC/CMake-AIX53-xlC/Tests/SystemInformation'/>
      <Variable name='DumpInformation_SOURCE_DIR' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Tests/SystemInformation'/>
      <Variable name='EXECUTABLE_OUTPUT_PATH' value=''/>
      <Variable name='HAVE_CMAKE_SIZEOF_VOID_P' value='TRUE'/>
      <Variable name='LIBRARY_OUTPUT_PATH' value=''/>
      <Variable name='PROJECT_BINARY_DIR' value='/bench1/noibm34/bench-AIX5300CFB9-xlC/CMake-AIX53-xlC/Tests/SystemInformation'/>
      <Variable name='PROJECT_NAME' value='DumpInformation'/>
      <Variable name='PROJECT_SOURCE_DIR' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Tests/SystemInformation'/>
      <Variable name='RUN_CONFIGURE' value='ON'/>
      <Variable name='UNIX' value='1'/>
      <Variable name='incl' value='/u/noibm34/Dashboards/MyTests-AIX5300CFB9-xlC/CMake/Tests/SystemInformation/This does not exists'/>
     </Dashboard>
     <Dashboard site='G5.Nfsnet.Org' build='Darwin8.3-gcc4' buildstamp='20060912-0100-Nightly'>
      <Variable name='APPLE' value='1'/>
      <Variable name='CMAKE_AR' value='/usr/bin/ar'/>
      <Variable name='CMAKE_BACKWARDS_COMPATIBILITY' value='2.5'/>
      <Variable name='CMAKE_BASE_NAME' value='g++'/>
      <Variable name='CMAKE_BINARY_DIR' value='/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_BUILD_TOOL' value='/usr/bin/make'/>
      <Variable name='CMAKE_BUILD_TYPE' value=''/>
      <Variable name='CMAKE_CACHEFILE_DIR' value='/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_CACHE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_CACHE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_CACHE_RELEASE_VERSION' value='development'/>
      <Variable name='CMAKE_CFG_INTDIR' value='.'/>
      <Variable name='CMAKE_COLOR_MAKEFILE' value='ON'/>
      <Variable name='CMAKE_COMMAND' value='/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/bin/cmake'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCC' value='1'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCC_RUN' value='1'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCXX' value='1'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCXX_RUN' value='1'/>
      <Variable name='CMAKE_CTEST_COMMAND' value='/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/bin/ctest'/>
      <Variable name='CMAKE_CURRENT_BINARY_DIR' value='/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_CURRENT_SOURCE_DIR' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_CXX_COMPILER' value='/usr/bin/c++'/>
      <Variable name='CMAKE_CXX_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_CXX_COMPILER_ENV_VAR' value='CXX'/>
      <Variable name='CMAKE_CXX_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_CXX_COMPILE_OBJECT' value='<CMAKE_CXX_COMPILER>  <FLAGS> -o <OBJECT> -c <SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_ASSEMBLY_SOURCE' value='<CMAKE_CXX_COMPILER> <FLAGS> -S <SOURCE> -o <ASSEMBLY_SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_PREPROCESSED_SOURCE' value='<CMAKE_CXX_COMPILER> <FLAGS> -E <SOURCE> > <PREPROCESSED_SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_SHARED_LIBRARY' value='<CMAKE_CXX_COMPILER> <LANGUAGE_COMPILE_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS> <LINK_FLAGS> -o <TARGET> -install_name <TARGET_INSTALLNAME_DIR><TARGET_SONAME> <OBJECTS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_CXX_CREATE_SHARED_LIBRARY_FORBIDDEN_FLAGS' value='-w'/>
      <Variable name='CMAKE_CXX_CREATE_SHARED_MODULE' value='<CMAKE_CXX_COMPILER> <LANGUAGE_COMPILE_FLAGS> <CMAKE_SHARED_MODULE_CREATE_CXX_FLAGS> <LINK_FLAGS> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_CXX_CREATE_STATIC_LIBRARY' value='<CMAKE_AR> cr <TARGET> <LINK_FLAGS> <OBJECTS> ;<CMAKE_RANLIB> <TARGET> '/>
      <Variable name='CMAKE_CXX_FLAGS' value=' '/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG' value='-g'/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG_INIT' value='-g'/>
      <Variable name='CMAKE_CXX_FLAGS_INIT' value=''/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL' value='-Os -DNDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL_INIT' value='-Os -DNDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE' value='-O3 -DNDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE_INIT' value='-O3 -DNDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO' value='-O2 -g'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT' value='-O2 -g'/>
      <Variable name='CMAKE_CXX_IGNORE_EXTENSIONS' value='inl;h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_CXX_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_LINKER_PREFERENCE' value='Prefered'/>
      <Variable name='CMAKE_CXX_LINK_EXECUTABLE' value='<CMAKE_CXX_COMPILER>  <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_CXX_LINK_FLAGS' value='-headerpad_max_install_names'/>
      <Variable name='CMAKE_CXX_OUTPUT_EXTENSION' value='.o'/>
      <Variable name='CMAKE_CXX_SOURCE_FILE_EXTENSIONS' value='C;M;c++;cc;cpp;cxx;m;mm'/>
      <Variable name='CMAKE_C_COMPILER' value='/usr/bin/gcc'/>
      <Variable name='CMAKE_C_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_C_COMPILER_ENV_VAR' value='CC'/>
      <Variable name='CMAKE_C_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_C_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_C_COMPILE_OBJECT' value='<CMAKE_C_COMPILER> <FLAGS> -o <OBJECT>   -c <SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_ASSEMBLY_SOURCE' value='<CMAKE_C_COMPILER> <FLAGS> -S <SOURCE> -o <ASSEMBLY_SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_PREPROCESSED_SOURCE' value='<CMAKE_C_COMPILER> <FLAGS> -E <SOURCE> > <PREPROCESSED_SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_SHARED_LIBRARY' value='<CMAKE_C_COMPILER> <LANGUAGE_COMPILE_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS> <LINK_FLAGS> -o <TARGET> -install_name <TARGET_INSTALLNAME_DIR><TARGET_SONAME> <OBJECTS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_C_CREATE_SHARED_LIBRARY_FORBIDDEN_FLAGS' value='-w'/>
      <Variable name='CMAKE_C_CREATE_SHARED_MODULE' value='<CMAKE_C_COMPILER>  <LANGUAGE_COMPILE_FLAGS> <CMAKE_SHARED_MODULE_CREATE_C_FLAGS> <LINK_FLAGS> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_C_CREATE_STATIC_LIBRARY' value='<CMAKE_AR> cr <TARGET> <LINK_FLAGS> <OBJECTS> ;<CMAKE_RANLIB> <TARGET> '/>
      <Variable name='CMAKE_C_FLAGS' value=' '/>
      <Variable name='CMAKE_C_FLAGS_DEBUG' value='-g'/>
      <Variable name='CMAKE_C_FLAGS_DEBUG_INIT' value='-g'/>
      <Variable name='CMAKE_C_FLAGS_INIT' value=''/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL' value='-Os -DNDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL_INIT' value='-Os -DNDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE' value='-O3 -DNDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE_INIT' value='-O3 -DNDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO' value='-O2 -g'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO_INIT' value='-O2 -g'/>
      <Variable name='CMAKE_C_IGNORE_EXTENSIONS' value='h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_C_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_C_LINKER_PREFERENCE' value='None'/>
      <Variable name='CMAKE_C_LINK_EXECUTABLE' value='<CMAKE_C_COMPILER> <FLAGS> <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_C_LINK_FLAGS' value='-headerpad_max_install_names'/>
      <Variable name='CMAKE_C_OUTPUT_EXTENSION' value='.o'/>
      <Variable name='CMAKE_C_SOURCE_FILE_EXTENSIONS' value='c'/>
      <Variable name='CMAKE_DL_LIBS' value=''/>
      <Variable name='CMAKE_EDIT_COMMAND' value='/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/bin/ccmake'/>
      <Variable name='CMAKE_EXECUTABLE_SUFFIX' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_DEBUG' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_MINSIZEREL' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELEASE' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO' value=''/>
      <Variable name='CMAKE_FILES_DIRECTORY' value='/CMakeFiles'/>
      <Variable name='CMAKE_FIND_APPBUNDLE' value='FIRST'/>
      <Variable name='CMAKE_FIND_FRAMEWORK' value='FIRST'/>
      <Variable name='CMAKE_FIND_LIBRARY_PREFIXES' value='lib'/>
      <Variable name='CMAKE_FIND_LIBRARY_SUFFIXES' value='.dylib;.so;.a'/>
      <Variable name='CMAKE_GENERATOR' value='Unix Makefiles'/>
      <Variable name='CMAKE_HOME_DIRECTORY' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_CXX' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C_SEP' value=''/>
      <Variable name='CMAKE_INIT_VALUE' value='FALSE'/>
      <Variable name='CMAKE_INSTALL_PREFIX' value='/usr/local'/>
      <Variable name='CMAKE_LIBRARY_PATH_FLAG' value='-L'/>
      <Variable name='CMAKE_LINK_LIBRARY_FLAG' value='-l'/>
      <Variable name='CMAKE_LINK_LIBRARY_SUFFIX' value=''/>
      <Variable name='CMAKE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_MAKE_PROGRAM' value='/usr/bin/make'/>
      <Variable name='CMAKE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_MODULE_EXISTS' value='1'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_DEBUG' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELEASE' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO' value=''/>
      <Variable name='CMAKE_MacOSX_Content_COMPILE_OBJECT' value='"/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/bin/cmake" -E copy_if_different <SOURCE> <OBJECT>'/>
      <Variable name='CMAKE_NUMBER_OF_LOCAL_GENERATORS' value='1'/>
      <Variable name='CMAKE_OSX_ARCHITECTURES' value='ppc'/>
      <Variable name='CMAKE_OSX_SYSROOT' value='/Developer/SDKs/MacOSX10.4u.sdk'/>
      <Variable name='CMAKE_PARENT_LIST_FILE' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Tests/SystemInformation/CMakeLists.txt'/>
      <Variable name='CMAKE_PATCH_VERSION' value='0'/>
      <Variable name='CMAKE_PLATFORM_HAS_INSTALLNAME' value='1'/>
      <Variable name='CMAKE_PLATFORM_IMPLICIT_INCLUDE_DIRECTORIES' value='/usr/local/include'/>
      <Variable name='CMAKE_PLATFORM_ROOT_BIN' value='/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/Tests/SystemInformation/CMakeFiles'/>
      <Variable name='CMAKE_PROJECT_NAME' value='DumpInformation'/>
      <Variable name='CMAKE_RANLIB' value='/usr/bin/ranlib'/>
      <Variable name='CMAKE_ROOT' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS' value='-dynamiclib -headerpad_max_install_names'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS' value='-dynamiclib -headerpad_max_install_names'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CXX_FLAGS' value='-fPIC'/>
      <Variable name='CMAKE_SHARED_LIBRARY_C_FLAGS' value='-fPIC'/>
      <Variable name='CMAKE_SHARED_LIBRARY_LINK_C_FLAGS' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_PREFIX' value='lib'/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG_SEP' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_SONAME_CXX_FLAG' value='-install_name'/>
      <Variable name='CMAKE_SHARED_LIBRARY_SONAME_C_FLAG' value='-install_name'/>
      <Variable name='CMAKE_SHARED_LIBRARY_SUFFIX' value='.dylib'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_DEBUG' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELEASE' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO' value=''/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_CXX_FLAGS' value='-bundle -headerpad_max_install_names'/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_C_FLAGS' value='-bundle -headerpad_max_install_names'/>
      <Variable name='CMAKE_SHARED_MODULE_PREFIX' value='lib'/>
      <Variable name='CMAKE_SHARED_MODULE_SUFFIX' value='.so'/>
      <Variable name='CMAKE_SIZEOF_VOID_P' value='4'/>
      <Variable name='CMAKE_SKIP_RPATH' value='NO'/>
      <Variable name='CMAKE_SOURCE_DIR' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_STATIC_LIBRARY_PREFIX' value='lib'/>
      <Variable name='CMAKE_STATIC_LIBRARY_SUFFIX' value='.a'/>
      <Variable name='CMAKE_SYSTEM' value='Darwin-8.7.0'/>
      <Variable name='CMAKE_SYSTEM_AND_CXX_COMPILER_INFO_FILE' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Modules/Platform/Darwin-g++.cmake'/>
      <Variable name='CMAKE_SYSTEM_AND_C_COMPILER_INFO_FILE' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Modules/Platform/Darwin-gcc.cmake'/>
      <Variable name='CMAKE_SYSTEM_APPBUNDLE_PATH' value='~/Applications;/Applications;/Developer/Applications'/>
      <Variable name='CMAKE_SYSTEM_FRAMEWORK_PATH' value='~/Library/Frameworks;/Library/Frameworks;/Network/Library/Frameworks;/System/Library/Frameworks'/>
      <Variable name='CMAKE_SYSTEM_INCLUDE_PATH' value='/usr/include;/usr/local/include;/usr/local;/opt/local/include;/usr/X11R6/include;/usr/include/X11;/usr/pkg/include;/sw/include'/>
      <Variable name='CMAKE_SYSTEM_INFO_FILE' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Modules/Platform/Darwin.cmake'/>
      <Variable name='CMAKE_SYSTEM_LIBRARY_PATH' value='/lib;/usr/lib;/usr/local/lib;/usr/lib/w32api;/usr/X11R6/lib;/opt/local/lib;/opt/csw/lib;/opt/lib;/usr/pkg/lib;/sw/lib'/>
      <Variable name='CMAKE_SYSTEM_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_NAME' value='Darwin'/>
      <Variable name='CMAKE_SYSTEM_PROCESSOR' value='powerpc'/>
      <Variable name='CMAKE_SYSTEM_PROGRAM_PATH' value='/bin;/usr/bin;/usr/local/bin;/usr/pkg/bin;/sbin'/>
      <Variable name='CMAKE_SYSTEM_SPECIFIC_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_VERSION' value='8.7.0'/>
      <Variable name='CMAKE_UNAME' value='/usr/bin/uname'/>
      <Variable name='CMAKE_USE_RELATIVE_PATHS' value='OFF'/>
      <Variable name='CMAKE_VERBOSE_MAKEFILE' value='FALSE'/>
      <Variable name='DumpInformation_BINARY_DIR' value='/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/Tests/SystemInformation'/>
      <Variable name='DumpInformation_SOURCE_DIR' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Tests/SystemInformation'/>
      <Variable name='EXECUTABLE_OUTPUT_PATH' value=''/>
      <Variable name='HAVE_CMAKE_SIZEOF_VOID_P' value='TRUE'/>
      <Variable name='LIBRARY_OUTPUT_PATH' value=''/>
      <Variable name='PROJECT_BINARY_DIR' value='/Volumes/HoldingTank/BuildFarm-Nightly/Using_CMake_Makefiles/CMake/Tests/SystemInformation'/>
      <Variable name='PROJECT_NAME' value='DumpInformation'/>
      <Variable name='PROJECT_SOURCE_DIR' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Tests/SystemInformation'/>
      <Variable name='RUN_CONFIGURE' value='ON'/>
      <Variable name='UNIX' value='1'/>
      <Variable name='_CMAKE_OSX_MACHINE' value='ppc'/>
      <Variable name='incl' value='/Volumes/HoldingTank/BuildFarm-Nightly/Sources/CMake/Tests/SystemInformation/This does not exists'/>
     </Dashboard>
     <Dashboard site='DASH1.kitware' build='Win32-nmake71' buildstamp='20060912-0100-Nightly'>
      <Variable name='CMAKE_AR' value='CMAKE_AR-NOTFOUND'/>
      <Variable name='CMAKE_BACKWARDS_COMPATIBILITY' value='2.5'/>
      <Variable name='CMAKE_BASE_NAME' value='cl'/>
      <Variable name='CMAKE_BINARY_DIR' value='C:/Dashboards/My Tests/CMakeVSNMake71/Tests/SystemInformation'/>
      <Variable name='CMAKE_BUILD_TOOL' value='nmake'/>
      <Variable name='CMAKE_BUILD_TYPE' value='Debug'/>
      <Variable name='CMAKE_BUILD_TYPE_INIT' value='Debug'/>
      <Variable name='CMAKE_CACHEFILE_DIR' value='c:/Dashboards/My Tests/CMakeVSNMake71/Tests/SystemInformation'/>
      <Variable name='CMAKE_CACHE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_CACHE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_CACHE_RELEASE_VERSION' value='development'/>
      <Variable name='CMAKE_CFG_INTDIR' value='.'/>
      <Variable name='CMAKE_CL_64' value='0'/>
      <Variable name='CMAKE_CL_NOLOGO' value='/nologo'/>
      <Variable name='CMAKE_COLOR_MAKEFILE' value='ON'/>
      <Variable name='CMAKE_COMMAND' value='C:/Dashboards/My Tests/CMakeVSNMake71/bin/cmake.exe'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCC_RUN' value='1'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCXX_RUN' value='1'/>
      <Variable name='CMAKE_COMPILER_SUPPORTS_PDBTYPE' value='0'/>
      <Variable name='CMAKE_COMPILE_RESOURCE' value='rc <FLAGS> /fo<OBJECT> <SOURCE>'/>
      <Variable name='CMAKE_CREATE_CONSOLE_EXE' value='/subsystem:console'/>
      <Variable name='CMAKE_CREATE_WIN32_EXE' value='/subsystem:windows'/>
      <Variable name='CMAKE_CTEST_COMMAND' value='C:/Dashboards/My Tests/CMakeVSNMake71/bin/ctest.exe'/>
      <Variable name='CMAKE_CURRENT_BINARY_DIR' value='C:/Dashboards/My Tests/CMakeVSNMake71/Tests/SystemInformation'/>
      <Variable name='CMAKE_CURRENT_SOURCE_DIR' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_CXX_COMPILER' value='C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/bin/cl.exe'/>
      <Variable name='CMAKE_CXX_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_CXX_COMPILER_ENV_VAR' value='CXX'/>
      <Variable name='CMAKE_CXX_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_CXX_CREATE_STATIC_LIBRARY' value='lib /nologo <LINK_FLAGS> /out:<TARGET> <OBJECTS> '/>
      <Variable name='CMAKE_CXX_FLAGS' value=' /DWIN32 /D_WINDOWS /W3 /Zm1000 /GX /GR'/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG' value='/D_DEBUG /MDd /Zi  /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG_INIT' value='/D_DEBUG /MDd /Zi  /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_CXX_FLAGS_INIT' value='/DWIN32 /D_WINDOWS /W3 /Zm1000 /GX /GR'/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL_INIT' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE_INIT' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_IGNORE_EXTENSIONS' value='inl;h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_CXX_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_LINKER_PREFERENCE' value='Prefered'/>
      <Variable name='CMAKE_CXX_OUTPUT_EXTENSION' value='.obj'/>
      <Variable name='CMAKE_CXX_SOURCE_FILE_EXTENSIONS' value='C;M;c++;cc;cpp;cxx;m;mm'/>
      <Variable name='CMAKE_CXX_STANDARD_LIBRARIES' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_CXX_STANDARD_LIBRARIES_INIT' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_C_COMPILER' value='C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/bin/cl.exe'/>
      <Variable name='CMAKE_C_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_C_COMPILER_ENV_VAR' value='CC'/>
      <Variable name='CMAKE_C_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_C_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_C_CREATE_STATIC_LIBRARY' value='lib /nologo <LINK_FLAGS> /out:<TARGET> <OBJECTS> '/>
      <Variable name='CMAKE_C_FLAGS' value=' /DWIN32 /D_WINDOWS /W3 /Zm1000'/>
      <Variable name='CMAKE_C_FLAGS_DEBUG' value='/D_DEBUG /MDd /Zi /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_C_FLAGS_DEBUG_INIT' value='/D_DEBUG /MDd /Zi /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_C_FLAGS_INIT' value='/DWIN32 /D_WINDOWS /W3 /Zm1000'/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL_INIT' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE_INIT' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO_INIT' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_IGNORE_EXTENSIONS' value='h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_C_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_C_LINKER_PREFERENCE' value='None'/>
      <Variable name='CMAKE_C_OUTPUT_EXTENSION' value='.obj'/>
      <Variable name='CMAKE_C_SOURCE_FILE_EXTENSIONS' value='c'/>
      <Variable name='CMAKE_C_STANDARD_LIBRARIES' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_C_STANDARD_LIBRARIES_INIT' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_DL_LIBS' value=''/>
      <Variable name='CMAKE_EDIT_COMMAND' value='C:/Dashboards/My Tests/CMakeVSNMake71/bin/CMakeSetup.exe'/>
      <Variable name='CMAKE_EXECUTABLE_SUFFIX' value='.exe'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS' value=' /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_DEBUG' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_DEBUG_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_INIT' value=' /STACK:10000000 /machine:I386 /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_MINSIZEREL' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_MINSIZEREL_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELEASE' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELEASE_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_FILES_DIRECTORY' value='/CMakeFiles'/>
      <Variable name='CMAKE_FIND_LIBRARY_PREFIXES' value=''/>
      <Variable name='CMAKE_FIND_LIBRARY_SUFFIXES' value='.lib'/>
      <Variable name='CMAKE_GENERATOR' value='NMake Makefiles'/>
      <Variable name='CMAKE_GENERATOR_CC' value='cl'/>
      <Variable name='CMAKE_GENERATOR_CXX' value='cl'/>
      <Variable name='CMAKE_HOME_DIRECTORY' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_IMPORT_LIBRARY_PREFIX' value=''/>
      <Variable name='CMAKE_IMPORT_LIBRARY_SUFFIX' value='.lib'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_CXX' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C_SEP' value=''/>
      <Variable name='CMAKE_INCLUDE_FLAG_RC' value='-I'/>
      <Variable name='CMAKE_INIT_VALUE' value='FALSE'/>
      <Variable name='CMAKE_INSTALL_PREFIX' value='C:/Program Files/DumpInformation'/>
      <Variable name='CMAKE_LIBRARY_PATH_FLAG' value='-LIBPATH:'/>
      <Variable name='CMAKE_LINK_DEF_FILE_FLAG' value='/DEF:'/>
      <Variable name='CMAKE_LINK_LIBRARY_FLAG' value=''/>
      <Variable name='CMAKE_LINK_LIBRARY_SUFFIX' value='.lib'/>
      <Variable name='CMAKE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_MAKE_PROGRAM' value='nmake'/>
      <Variable name='CMAKE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS' value=' /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_DEBUG' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_DEBUG_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_INIT' value=' /STACK:10000000 /machine:I386 /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELEASE' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELEASE_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_NUMBER_OF_LOCAL_GENERATORS' value='1'/>
      <Variable name='CMAKE_PARENT_LIST_FILE' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation/CMakeLists.txt'/>
      <Variable name='CMAKE_PATCH_VERSION' value='0'/>
      <Variable name='CMAKE_PLATFORM_ROOT_BIN' value='C:/Dashboards/My Tests/CMakeVSNMake71/Tests/SystemInformation/CMakeFiles'/>
      <Variable name='CMAKE_PROJECT_NAME' value='DumpInformation'/>
      <Variable name='CMAKE_RANLIB' value=':'/>
      <Variable name='CMAKE_RC_COMPILER' value='C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/bin/rc.exe'/>
      <Variable name='CMAKE_RC_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_RC_COMPILER_ENV_VAR' value='RC'/>
      <Variable name='CMAKE_RC_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_RC_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_RC_COMPILE_OBJECT' value='<CMAKE_RC_COMPILER> <FLAGS>  /fo<OBJECT> <SOURCE>'/>
      <Variable name='CMAKE_RC_FLAGS' value=' '/>
      <Variable name='CMAKE_RC_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_RC_OUTPUT_EXTENSION' value='.res'/>
      <Variable name='CMAKE_RC_SOURCE_FILE_EXTENSIONS' value='rc'/>
      <Variable name='CMAKE_ROOT' value='C:/Dashboards/My Tests/CMake'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_LIBRARY_C_FLAGS' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_LINK_C_FLAGS' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_PREFIX' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG_SEP' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_SUFFIX' value='.dll'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS' value=' /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_DEBUG' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_DEBUG_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_INIT' value=' /STACK:10000000 /machine:I386 /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELEASE' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELEASE_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_CXX_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_C_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_MODULE_PREFIX' value=''/>
      <Variable name='CMAKE_SHARED_MODULE_SUFFIX' value='.dll'/>
      <Variable name='CMAKE_SIZEOF_VOID_P' value='4'/>
      <Variable name='CMAKE_SKIP_RPATH' value='NO'/>
      <Variable name='CMAKE_SOURCE_DIR' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_STATIC_LIBRARY_PREFIX' value=''/>
      <Variable name='CMAKE_STATIC_LIBRARY_SUFFIX' value='.lib'/>
      <Variable name='CMAKE_SYSTEM' value='Windows-5.1'/>
      <Variable name='CMAKE_SYSTEM_AND_CXX_COMPILER_INFO_FILE' value='C:/Dashboards/My Tests/CMake/Modules/Platform/Windows-cl.cmake'/>
      <Variable name='CMAKE_SYSTEM_AND_C_COMPILER_INFO_FILE' value='C:/Dashboards/My Tests/CMake/Modules/Platform/Windows-cl.cmake'/>
      <Variable name='CMAKE_SYSTEM_AND_RC_COMPILER_INFO_FILE' value='C:/Dashboards/My Tests/CMake/Modules/Platform/Windows-rc.cmake'/>
      <Variable name='CMAKE_SYSTEM_INCLUDE_PATH' value='C:\Program Files;C:\Program Files'/>
      <Variable name='CMAKE_SYSTEM_INFO_FILE' value='C:/Dashboards/My Tests/CMake/Modules/Platform/Windows.cmake'/>
      <Variable name='CMAKE_SYSTEM_LIBRARY_PATH' value='C:\Program Files;C:\Program Files'/>
      <Variable name='CMAKE_SYSTEM_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_NAME' value='Windows'/>
      <Variable name='CMAKE_SYSTEM_PROCESSOR' value='x86'/>
      <Variable name='CMAKE_SYSTEM_PROGRAM_PATH' value='C:\Program Files;C:\Program Files'/>
      <Variable name='CMAKE_SYSTEM_SPECIFIC_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_VERSION' value='5.1'/>
      <Variable name='CMAKE_USE_RELATIVE_PATHS' value='OFF'/>
      <Variable name='CMAKE_USING_VC_FREE_TOOLS' value='0'/>
      <Variable name='CMAKE_VC_COMPILER_TESTS_RUN' value='1'/>
      <Variable name='CMAKE_VERBOSE_MAKEFILE' value='FALSE'/>
      <Variable name='DumpInformation_BINARY_DIR' value='C:/Dashboards/My Tests/CMakeVSNMake71/Tests/SystemInformation'/>
      <Variable name='DumpInformation_SOURCE_DIR' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='EXECUTABLE_OUTPUT_PATH' value=''/>
      <Variable name='HAVE_CMAKE_SIZEOF_VOID_P' value='TRUE'/>
      <Variable name='LIBRARY_OUTPUT_PATH' value=''/>
      <Variable name='MSVC' value='1'/>
      <Variable name='MSVC71' value='1'/>
      <Variable name='MSVC_IDE' value='0'/>
      <Variable name='MSVC_VERSION' value='1310'/>
      <Variable name='PROJECT_BINARY_DIR' value='C:/Dashboards/My Tests/CMakeVSNMake71/Tests/SystemInformation'/>
      <Variable name='PROJECT_NAME' value='DumpInformation'/>
      <Variable name='PROJECT_SOURCE_DIR' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='RUN_CONFIGURE' value='ON'/>
      <Variable name='WIN32' value='1'/>
      <Variable name='incl' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation/This does not exists'/>
     </Dashboard>
     <Dashboard site='VOGON.kitware' build='Win32-vc71IDE' buildstamp='20060912-0141-Experimental'>
      <Variable name='ALL_BUILD_GUID_CMAKE' value='7B7740E4-CB85-4719-9D1E-2A045B3AE938'/>
      <Variable name='BUILD_TYPE' value='Debug'/>
      <Variable name='CMAKE_AR' value='C:/cygwin/bin/ar.exe'/>
      <Variable name='CMAKE_BACKWARDS_COMPATIBILITY' value='2.3'/>
      <Variable name='CMAKE_BASE_NAME' value='cl'/>
      <Variable name='CMAKE_BINARY_DIR' value='C:/Hoffman/My Builds/CMakeVSNMake71DevIDE/Tests/SystemInformation'/>
      <Variable name='CMAKE_BUILD_TOOL' value='c:/PROGRA~1/MICROS~2.NET/Common7/IDE/devenv.com'/>
      <Variable name='CMAKE_BUILD_TYPE' value='Debug'/>
      <Variable name='CMAKE_BUILD_TYPE_INIT' value='Debug'/>
      <Variable name='CMAKE_CACHEFILE_DIR' value='c:/Hoffman/My Builds/CMakeVSNMake71DevIDE/Tests/SystemInformation'/>
      <Variable name='CMAKE_CACHE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_CACHE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_CACHE_RELEASE_VERSION' value='development'/>
      <Variable name='CMAKE_CFG_INTDIR' value='$(OutDir)'/>
      <Variable name='CMAKE_CL_NOLOGO' value='/nologo'/>
      <Variable name='CMAKE_COMMAND' value='C:/Hoffman/My Builds/CMakeVSNMake71DevIDE/bin/debug/cmake.exe'/>
      <Variable name='CMAKE_COMPILER_2005' value='1'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCC_RUN' value='1'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCXX_RUN' value='1'/>
      <Variable name='CMAKE_COMPILE_RESOURCE' value='rc <FLAGS> /fo<OBJECT> <SOURCE>'/>
      <Variable name='CMAKE_CONFIGURATION_TYPES' value='Debug;Release;MinSizeRel;RelWithDebInfo'/>
      <Variable name='CMAKE_CREATE_CONSOLE_EXE' value='/subsystem:console'/>
      <Variable name='CMAKE_CREATE_WIN32_EXE' value='/subsystem:windows'/>
      <Variable name='CMAKE_CTEST_COMMAND' value='C:/Hoffman/My Builds/CMakeVSNMake71DevIDE/bin/debug/ctest.exe'/>
      <Variable name='CMAKE_CURRENT_BINARY_DIR' value='C:/Hoffman/My Builds/CMakeVSNMake71DevIDE/Tests/SystemInformation'/>
      <Variable name='CMAKE_CURRENT_SOURCE_DIR' value='C:/Hoffman/My Builds/CMakeDev/Tests/SystemInformation'/>
      <Variable name='CMAKE_CXX_COMPILER' value='cl'/>
      <Variable name='CMAKE_CXX_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_CXX_COMPILER_ENV_VAR' value='CXX'/>
      <Variable name='CMAKE_CXX_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_CXX_COMPILE_OBJECT' value='<CMAKE_CXX_COMPILER>    /nologo <FLAGS>  /TP /Fo<OBJECT> /Fd<TARGET_PDB> -c <SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_ASSEMBLY_SOURCE' value='<CMAKE_CXX_COMPILER>  /nologo <FLAGS> /TP /FAs /FoNUL /Fa<ASSEMBLY_SOURCE> /c <SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_PREPROCESSED_SOURCE' value='<CMAKE_CXX_COMPILER> > <PREPROCESSED_SOURCE>  /nologo <FLAGS> /TP -E <SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_SHARED_LIBRARY' value='link /nologo  /out:<TARGET> /PDB:<TARGET_PDB> /dll  <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES> '/>
      <Variable name='CMAKE_CXX_CREATE_SHARED_MODULE' value='link /nologo  /out:<TARGET> /PDB:<TARGET_PDB> /dll  <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES> '/>
      <Variable name='CMAKE_CXX_CREATE_STATIC_LIBRARY' value='lib /nologo <LINK_FLAGS> /out:<TARGET> <OBJECTS> '/>
      <Variable name='CMAKE_CXX_FLAGS' value=' /W3 /Zm1000 /GX /GR'/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG' value='/MDd /Zi /Od /GZ'/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG_INIT' value='/D_DEBUG /MDd /Zi  /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_CXX_FLAGS_INIT' value='/DWIN32 /D_WINDOWS /W3 /Zm1000 /GX /GR'/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL' value='/MD /O1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL_INIT' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE' value='/MD /O2 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE_INIT' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO' value='/MD /Zi /O2 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_IGNORE_EXTENSIONS' value='inl;h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_CXX_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_LINKER_PREFERENCE' value='Prefered'/>
      <Variable name='CMAKE_CXX_LINK_EXECUTABLE' value='<CMAKE_CXX_COMPILER> /nologo  <FLAGS> <OBJECTS> /Fe<TARGET> /Fd<TARGET_PDB> -link <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_CXX_OUTPUT_EXTENSION' value='.obj'/>
      <Variable name='CMAKE_CXX_SOURCE_FILE_EXTENSIONS' value='C;M;c++;cc;cpp;cxx;m;mm'/>
      <Variable name='CMAKE_CXX_STACK_SIZE' value='10000000'/>
      <Variable name='CMAKE_CXX_STANDARD_LIBRARIES' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_CXX_STANDARD_LIBRARIES_INIT' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_CXX_WARNING_LEVEL' value='3'/>
      <Variable name='CMAKE_C_COMPILER' value='cl'/>
      <Variable name='CMAKE_C_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_C_COMPILER_ENV_VAR' value='CC'/>
      <Variable name='CMAKE_C_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_C_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_C_COMPILE_OBJECT' value='<CMAKE_C_COMPILER>  /nologo <FLAGS> /Fo<OBJECT> /Fd<TARGET_PDB>  -c <SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_ASSEMBLY_SOURCE' value='<CMAKE_C_COMPILER>  /nologo <FLAGS> /FAs /FoNUL /Fa<ASSEMBLY_SOURCE> /c <SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_PREPROCESSED_SOURCE' value='<CMAKE_C_COMPILER> > <PREPROCESSED_SOURCE>  /nologo <FLAGS> -E <SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_SHARED_LIBRARY' value='link /nologo  /out:<TARGET> /PDB:<TARGET_PDB> /dll  <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES> '/>
      <Variable name='CMAKE_C_CREATE_SHARED_MODULE' value='link /nologo  /out:<TARGET> /PDB:<TARGET_PDB> /dll  <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES> '/>
      <Variable name='CMAKE_C_CREATE_STATIC_LIBRARY' value='lib /nologo <LINK_FLAGS> /out:<TARGET> <OBJECTS> '/>
      <Variable name='CMAKE_C_FLAGS' value=' /W3 /Zm1000'/>
      <Variable name='CMAKE_C_FLAGS_DEBUG' value='/MDd /Zi /Od /GZ'/>
      <Variable name='CMAKE_C_FLAGS_DEBUG_INIT' value='/D_DEBUG /MDd /Zi /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_C_FLAGS_INIT' value='/DWIN32 /D_WINDOWS /W3 /Zm1000'/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL' value='/MD /O1 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL_INIT' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE' value='/MD /O2 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE_INIT' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO' value='/MD /Zi /O2 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO_INIT' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_IGNORE_EXTENSIONS' value='h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_C_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_C_LINKER_PREFERENCE' value='None'/>
      <Variable name='CMAKE_C_LINK_EXECUTABLE' value='<CMAKE_C_COMPILER> /nologo  <FLAGS> <OBJECTS> /Fe<TARGET> /Fd<TARGET_PDB> -link <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_C_OUTPUT_EXTENSION' value='.obj'/>
      <Variable name='CMAKE_C_SOURCE_FILE_EXTENSIONS' value='c'/>
      <Variable name='CMAKE_C_STANDARD_LIBRARIES' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_C_STANDARD_LIBRARIES_INIT' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_DL_LIBS' value=''/>
      <Variable name='CMAKE_EDIT_COMMAND' value='C:/Hoffman/My Builds/CMakeVSNMake71DevIDE/bin/debug/CMakeSetup.exe'/>
      <Variable name='CMAKE_EXECUTABLE_SUFFIX' value='.exe'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS' value='/STACK:10000000 /machine:I386 /INCREMENTAL:YES'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_DEBUG' value='/debug'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_DEBUG_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_INIT' value=' /STACK:10000000 /machine:I386 /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_MINSIZEREL' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_MINSIZEREL_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELEASE' value=''/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELEASE_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO' value='/debug'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_FILES_DIRECTORY' value='/CMakeFiles'/>
      <Variable name='CMAKE_FIND_LIBRARY_PREFIXES' value=''/>
      <Variable name='CMAKE_FIND_LIBRARY_SUFFIXES' value='.lib'/>
      <Variable name='CMAKE_GENERATOR' value='Visual Studio 7 .NET 2003'/>
      <Variable name='CMAKE_GENERATOR_CC' value='cl'/>
      <Variable name='CMAKE_GENERATOR_CXX' value='cl'/>
      <Variable name='CMAKE_GENERATOR_Fortran' value='ifort'/>
      <Variable name='CMAKE_GENERATOR_NO_COMPILER_ENV' value='1'/>
      <Variable name='CMAKE_GENERATOR_RC' value='rc'/>
      <Variable name='CMAKE_HOME_DIRECTORY' value='C:/Hoffman/My Builds/CMakeDev/Tests/SystemInformation'/>
      <Variable name='CMAKE_IMPORT_LIBRARY_PREFIX' value=''/>
      <Variable name='CMAKE_IMPORT_LIBRARY_SUFFIX' value='.lib'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_CXX' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C_SEP' value=''/>
      <Variable name='CMAKE_INCLUDE_FLAG_RC' value='-I'/>
      <Variable name='CMAKE_INIT_VALUE' value='FALSE'/>
      <Variable name='CMAKE_INSTALL_PREFIX' value='C:/Program Files/DumpInformation'/>
      <Variable name='CMAKE_LIBRARY_PATH_FLAG' value='-LIBPATH:'/>
      <Variable name='CMAKE_LINK_DEF_FILE_FLAG' value='/DEF:'/>
      <Variable name='CMAKE_LINK_LIBRARY_FLAG' value=''/>
      <Variable name='CMAKE_LINK_LIBRARY_SUFFIX' value='.lib'/>
      <Variable name='CMAKE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_MAKE_PROGRAM' value='c:/PROGRA~1/MICROS~2.NET/Common7/IDE/devenv.com'/>
      <Variable name='CMAKE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS' value='/STACK:10000000 /machine:I386 /INCREMENTAL:YES'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_DEBUG' value='/debug'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_DEBUG_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_INIT' value=' /STACK:10000000 /machine:I386 /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELEASE' value=''/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELEASE_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO' value='/debug'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_NO_BUILD_TYPE' value='1'/>
      <Variable name='CMAKE_NUMBER_OF_LOCAL_GENERATORS' value='1'/>
      <Variable name='CMAKE_PARENT_LIST_FILE' value='C:/Hoffman/My Builds/CMakeDev/Tests/SystemInformation/CMakeLists.txt'/>
      <Variable name='CMAKE_PATCH_VERSION' value='0'/>
      <Variable name='CMAKE_PLATFORM_ROOT_BIN' value='C:/Hoffman/My Builds/CMakeVSNMake71DevIDE/Tests/SystemInformation/CMakeFiles'/>
      <Variable name='CMAKE_PROJECT_NAME' value='DumpInformation'/>
      <Variable name='CMAKE_RANLIB' value='C:/cygwin/bin/ranlib.exe'/>
      <Variable name='CMAKE_RC_COMPILER' value='rc'/>
      <Variable name='CMAKE_RC_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_RC_COMPILER_ENV_VAR' value='RC'/>
      <Variable name='CMAKE_RC_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_RC_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_RC_COMPILE_OBJECT' value='<CMAKE_RC_COMPILER> <FLAGS>  /fo<OBJECT> <SOURCE>'/>
      <Variable name='CMAKE_RC_FLAGS' value=' '/>
      <Variable name='CMAKE_RC_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_RC_OUTPUT_EXTENSION' value='.res'/>
      <Variable name='CMAKE_RC_SOURCE_FILE_EXTENSIONS' value='rc'/>
      <Variable name='CMAKE_ROOT' value='C:/Hoffman/My Builds/CMakeDev'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_LIBRARY_C_FLAGS' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_LINK_C_FLAGS' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_PREFIX' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG_SEP' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_SUFFIX' value='.dll'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS' value='/STACK:10000000 /machine:I386 /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_DEBUG' value='/debug'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_DEBUG_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_INIT' value=' /STACK:10000000 /machine:I386 /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELEASE' value=''/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELEASE_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO' value='/debug'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_CXX_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_C_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_MODULE_PREFIX' value=''/>
      <Variable name='CMAKE_SHARED_MODULE_SUFFIX' value='.dll'/>
      <Variable name='CMAKE_SIZEOF_VOID_P' value='4'/>
      <Variable name='CMAKE_SKIP_RPATH' value='NO'/>
      <Variable name='CMAKE_SOURCE_DIR' value='C:/Hoffman/My Builds/CMakeDev/Tests/SystemInformation'/>
      <Variable name='CMAKE_STANDARD_LIBRARIES' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_STATIC_LIBRARY_PREFIX' value=''/>
      <Variable name='CMAKE_STATIC_LIBRARY_SUFFIX' value='.lib'/>
      <Variable name='CMAKE_SYSTEM' value='Windows-5.1'/>
      <Variable name='CMAKE_SYSTEM_AND_CXX_COMPILER_INFO_FILE' value='C:/Hoffman/My Builds/CMakeDev/Modules/Platform/Windows-cl.cmake'/>
      <Variable name='CMAKE_SYSTEM_AND_C_COMPILER_INFO_FILE' value='C:/Hoffman/My Builds/CMakeDev/Modules/Platform/Windows-cl.cmake'/>
      <Variable name='CMAKE_SYSTEM_AND_RC_COMPILER_INFO_FILE' value='C:/Hoffman/My Builds/CMakeDev/Modules/Platform/Windows-rc.cmake'/>
      <Variable name='CMAKE_SYSTEM_INCLUDE_PATH' value='C:\Program Files;C:\Program Files'/>
      <Variable name='CMAKE_SYSTEM_INFO_FILE' value='C:/Hoffman/My Builds/CMakeDev/Modules/Platform/Windows.cmake'/>
      <Variable name='CMAKE_SYSTEM_LIBRARY_PATH' value='C:\Program Files;C:\Program Files'/>
      <Variable name='CMAKE_SYSTEM_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_NAME' value='Windows'/>
      <Variable name='CMAKE_SYSTEM_PROCESSOR' value='x86'/>
      <Variable name='CMAKE_SYSTEM_PROGRAM_PATH' value='C:\Program Files;C:\Program Files'/>
      <Variable name='CMAKE_SYSTEM_SPECIFIC_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_VERSION' value='5.1'/>
      <Variable name='CMAKE_USE_RELATIVE_PATHS' value='OFF'/>
      <Variable name='CMAKE_VC_COMPILER_TESTS_RUN' value='1'/>
      <Variable name='CMAKE_VERBOSE_MAKEFILE' value='FALSE'/>
      <Variable name='DumpInformation_BINARY_DIR' value='C:/Hoffman/My Builds/CMakeVSNMake71DevIDE/Tests/SystemInformation'/>
      <Variable name='DumpInformation_CMAKE_PATH' value='C:/Hoffman/My Builds/CMakeVSNMake71DevIDE/Tests/SystemInformation'/>
      <Variable name='DumpInformation_GUID_CMAKE' value='EB8FB2E9-F8DA-4036-9226-6B45E4664178'/>
      <Variable name='DumpInformation_SOURCE_DIR' value='C:/Hoffman/My Builds/CMakeDev/Tests/SystemInformation'/>
      <Variable name='EDIT_CACHE_GUID_CMAKE' value='8EA1CB90-5B48-4B1E-935A-9288F736262C'/>
      <Variable name='EXECUTABLE_OUTPUT_PATH' value=''/>
      <Variable name='HAVE_CMAKE_SIZEOF_VOID_P' value='TRUE'/>
      <Variable name='INSTALL_GUID_CMAKE' value='BB5BF21A-2A34-4407-9AC0-43BC476369F3'/>
      <Variable name='LIBRARY_OUTPUT_PATH' value=''/>
      <Variable name='MSVC' value='1'/>
      <Variable name='MSVC71' value='1'/>
      <Variable name='MSVC_IDE' value='1'/>
      <Variable name='PACKAGE_GUID_CMAKE' value='62E296DF-B093-4AF8-8B5D-B6F94E957F4B'/>
      <Variable name='PROJECT_BINARY_DIR' value='C:/Hoffman/My Builds/CMakeVSNMake71DevIDE/Tests/SystemInformation'/>
      <Variable name='PROJECT_NAME' value='DumpInformation'/>
      <Variable name='PROJECT_SOURCE_DIR' value='C:/Hoffman/My Builds/CMakeDev/Tests/SystemInformation'/>
      <Variable name='REBUILD_CACHE_GUID_CMAKE' value='0EF11DE3-C3A6-4F08-BFFA-0BAF1B8C9608'/>
      <Variable name='RUN_CONFIGURE' value='ON'/>
      <Variable name='RUN_TESTS_GUID_CMAKE' value='A04713B4-9EE1-49C8-983C-6061473EFD02'/>
      <Variable name='WIN32' value='1'/>
      <Variable name='incl' value='C:/Hoffman/My Builds/CMakeDev/Tests/SystemInformation/This does not exists'/>
     </Dashboard>
     <Dashboard site='DASH3.kitware' build='Win32-vs60' buildstamp='20060912-0100-Nightly'>
      <Variable name='CMAKE_AR' value='CMAKE_AR-NOTFOUND'/>
      <Variable name='CMAKE_BACKWARDS_COMPATIBILITY' value='2.5'/>
      <Variable name='CMAKE_BASE_NAME' value='CL'/>
      <Variable name='CMAKE_BINARY_DIR' value='C:/Dashboards/My Tests/CMakeVS60/Tests/SystemInformation'/>
      <Variable name='CMAKE_BUILD_TOOL' value='c:/PROGRA~1/MICROS~2/Common/MSDev98/Bin/MSDEV.COM'/>
      <Variable name='CMAKE_BUILD_TYPE' value='Release'/>
      <Variable name='CMAKE_BUILD_TYPE_INIT' value='Debug'/>
      <Variable name='CMAKE_CACHEFILE_DIR' value='c:/Dashboards/My Tests/CMakeVS60/Tests/SystemInformation'/>
      <Variable name='CMAKE_CACHE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_CACHE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_CACHE_RELEASE_VERSION' value='development'/>
      <Variable name='CMAKE_CFG_INTDIR' value='$(IntDir)'/>
      <Variable name='CMAKE_CL_NOLOGO' value='/nologo'/>
      <Variable name='CMAKE_COMMAND' value='C:/Dashboards/My Tests/CMakeVS60/bin/Release/cmake.exe'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCC_RUN' value='1'/>
      <Variable name='CMAKE_COMPILER_IS_GNUCXX_RUN' value='1'/>
      <Variable name='CMAKE_COMPILE_RESOURCE' value='rc <FLAGS> /fo<OBJECT> <SOURCE>'/>
      <Variable name='CMAKE_CONFIGURATION_TYPES' value='Debug;Release;MinSizeRel;RelWithDebInfo'/>
      <Variable name='CMAKE_CREATE_CONSOLE_EXE' value='/subsystem:console'/>
      <Variable name='CMAKE_CREATE_WIN32_EXE' value='/subsystem:windows'/>
      <Variable name='CMAKE_CTEST_COMMAND' value='C:/Dashboards/My Tests/CMakeVS60/bin/Release/ctest.exe'/>
      <Variable name='CMAKE_CURRENT_BINARY_DIR' value='C:/Dashboards/My Tests/CMakeVS60/Tests/SystemInformation'/>
      <Variable name='CMAKE_CURRENT_SOURCE_DIR' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_CXX_COMPILER' value='c:/Program Files/Microsoft Visual Studio/VC98/Bin/CL.EXE'/>
      <Variable name='CMAKE_CXX_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_CXX_COMPILER_ENV_VAR' value='CXX'/>
      <Variable name='CMAKE_CXX_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_CXX_COMPILE_OBJECT' value='<CMAKE_CXX_COMPILER>    /nologo <FLAGS>  /TP /Fo<OBJECT> /Fd<TARGET_PDB> -c <SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_ASSEMBLY_SOURCE' value='<CMAKE_CXX_COMPILER>  /nologo <FLAGS> /TP /FAs /FoNUL /Fa<ASSEMBLY_SOURCE> /c <SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_PREPROCESSED_SOURCE' value='<CMAKE_CXX_COMPILER> > <PREPROCESSED_SOURCE>  /nologo <FLAGS> /TP -E <SOURCE>'/>
      <Variable name='CMAKE_CXX_CREATE_SHARED_LIBRARY' value='link /nologo  /out:<TARGET> /PDB:<TARGET_PDB> /dll  <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES> '/>
      <Variable name='CMAKE_CXX_CREATE_SHARED_MODULE' value='link /nologo  /out:<TARGET> /PDB:<TARGET_PDB> /dll  <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES> '/>
      <Variable name='CMAKE_CXX_CREATE_STATIC_LIBRARY' value='lib /nologo <LINK_FLAGS> /out:<TARGET> <OBJECTS> '/>
      <Variable name='CMAKE_CXX_FLAGS' value=' /DWIN32 /D_WINDOWS /W3 /Zm1000 /GX /GR'/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG' value='/D_DEBUG /MDd /Zi  /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_CXX_FLAGS_DEBUG_INIT' value='/D_DEBUG /MDd /Zi  /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_CXX_FLAGS_INIT' value='/DWIN32 /D_WINDOWS /W3 /Zm1000 /GX /GR'/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_MINSIZEREL_INIT' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELEASE_INIT' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_CXX_IGNORE_EXTENSIONS' value='inl;h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_CXX_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_CXX_LINKER_PREFERENCE' value='Prefered'/>
      <Variable name='CMAKE_CXX_LINK_EXECUTABLE' value='<CMAKE_CXX_COMPILER> /nologo  <FLAGS> <OBJECTS> /Fe<TARGET> /Fd<TARGET_PDB> -link <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_CXX_OUTPUT_EXTENSION' value='.obj'/>
      <Variable name='CMAKE_CXX_SOURCE_FILE_EXTENSIONS' value='C;M;c++;cc;cpp;cxx;m;mm'/>
      <Variable name='CMAKE_CXX_STANDARD_LIBRARIES' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_CXX_STANDARD_LIBRARIES_INIT' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_C_COMPILER' value='c:/Program Files/Microsoft Visual Studio/VC98/Bin/CL.EXE'/>
      <Variable name='CMAKE_C_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_C_COMPILER_ENV_VAR' value='CC'/>
      <Variable name='CMAKE_C_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_C_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_C_COMPILE_OBJECT' value='<CMAKE_C_COMPILER>  /nologo <FLAGS> /Fo<OBJECT> /Fd<TARGET_PDB>  -c <SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_ASSEMBLY_SOURCE' value='<CMAKE_C_COMPILER>  /nologo <FLAGS> /FAs /FoNUL /Fa<ASSEMBLY_SOURCE> /c <SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_PREPROCESSED_SOURCE' value='<CMAKE_C_COMPILER> > <PREPROCESSED_SOURCE>  /nologo <FLAGS> -E <SOURCE>'/>
      <Variable name='CMAKE_C_CREATE_SHARED_LIBRARY' value='link /nologo  /out:<TARGET> /PDB:<TARGET_PDB> /dll  <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES> '/>
      <Variable name='CMAKE_C_CREATE_SHARED_MODULE' value='link /nologo  /out:<TARGET> /PDB:<TARGET_PDB> /dll  <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES> '/>
      <Variable name='CMAKE_C_CREATE_STATIC_LIBRARY' value='lib /nologo <LINK_FLAGS> /out:<TARGET> <OBJECTS> '/>
      <Variable name='CMAKE_C_FLAGS' value=' /DWIN32 /D_WINDOWS /W3 /Zm1000'/>
      <Variable name='CMAKE_C_FLAGS_DEBUG' value='/D_DEBUG /MDd /Zi /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_C_FLAGS_DEBUG_INIT' value='/D_DEBUG /MDd /Zi /Ob0 /Od /GZ'/>
      <Variable name='CMAKE_C_FLAGS_INIT' value='/DWIN32 /D_WINDOWS /W3 /Zm1000'/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_MINSIZEREL_INIT' value='/MD /O1 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELEASE_INIT' value='/MD /O2 /Ob2 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_FLAGS_RELWITHDEBINFO_INIT' value='/MD /Zi /O2 /Ob1 /D NDEBUG'/>
      <Variable name='CMAKE_C_IGNORE_EXTENSIONS' value='h;H;o;O;obj;OBJ;def;DEF;rc;RC'/>
      <Variable name='CMAKE_C_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_C_LINKER_PREFERENCE' value='None'/>
      <Variable name='CMAKE_C_LINK_EXECUTABLE' value='<CMAKE_C_COMPILER> /nologo  <FLAGS> <OBJECTS> /Fe<TARGET> /Fd<TARGET_PDB> -link <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <LINK_LIBRARIES>'/>
      <Variable name='CMAKE_C_OUTPUT_EXTENSION' value='.obj'/>
      <Variable name='CMAKE_C_SOURCE_FILE_EXTENSIONS' value='c'/>
      <Variable name='CMAKE_C_STANDARD_LIBRARIES' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_C_STANDARD_LIBRARIES_INIT' value='kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'/>
      <Variable name='CMAKE_DL_LIBS' value=''/>
      <Variable name='CMAKE_EDIT_COMMAND' value='C:/Dashboards/My Tests/CMakeVS60/bin/Release/CMakeSetup.exe'/>
      <Variable name='CMAKE_EXECUTABLE_SUFFIX' value='.exe'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS' value=' /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_DEBUG' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_DEBUG_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_INIT' value=' /STACK:10000000 /machine:I386 /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_MINSIZEREL' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_MINSIZEREL_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELEASE' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELEASE_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_FILES_DIRECTORY' value='/CMakeFiles'/>
      <Variable name='CMAKE_FIND_LIBRARY_PREFIXES' value=''/>
      <Variable name='CMAKE_FIND_LIBRARY_SUFFIXES' value='.lib'/>
      <Variable name='CMAKE_GENERATOR' value='Visual Studio 6'/>
      <Variable name='CMAKE_GENERATOR_CC' value='cl'/>
      <Variable name='CMAKE_GENERATOR_CXX' value='cl'/>
      <Variable name='CMAKE_GENERATOR_Fortran' value='ifort'/>
      <Variable name='CMAKE_GENERATOR_NO_COMPILER_ENV' value='1'/>
      <Variable name='CMAKE_GENERATOR_RC' value='rc'/>
      <Variable name='CMAKE_HOME_DIRECTORY' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_IMPORT_LIBRARY_PREFIX' value=''/>
      <Variable name='CMAKE_IMPORT_LIBRARY_SUFFIX' value='.lib'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_CXX' value='-I'/>
      <Variable name='CMAKE_INCLUDE_FLAG_C_SEP' value=''/>
      <Variable name='CMAKE_INCLUDE_FLAG_RC' value='-I'/>
      <Variable name='CMAKE_INIT_VALUE' value='FALSE'/>
      <Variable name='CMAKE_INSTALL_PREFIX' value='C:/Program Files/DumpInformation'/>
      <Variable name='CMAKE_LIBRARY_PATH_FLAG' value='-LIBPATH:'/>
      <Variable name='CMAKE_LINK_DEF_FILE_FLAG' value='/DEF:'/>
      <Variable name='CMAKE_LINK_LIBRARY_FLAG' value=''/>
      <Variable name='CMAKE_LINK_LIBRARY_SUFFIX' value='.lib'/>
      <Variable name='CMAKE_MAJOR_VERSION' value='2'/>
      <Variable name='CMAKE_MAKE_PROGRAM' value='c:/PROGRA~1/MICROS~2/Common/MSDev98/Bin/MSDEV.COM'/>
      <Variable name='CMAKE_MINOR_VERSION' value='5'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS' value=' /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_DEBUG' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_DEBUG_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_INIT' value=' /STACK:10000000 /machine:I386 /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELEASE' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELEASE_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_NO_BUILD_TYPE' value='1'/>
      <Variable name='CMAKE_NUMBER_OF_LOCAL_GENERATORS' value='1'/>
      <Variable name='CMAKE_PARENT_LIST_FILE' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation/CMakeLists.txt'/>
      <Variable name='CMAKE_PATCH_VERSION' value='0'/>
      <Variable name='CMAKE_PLATFORM_ROOT_BIN' value='C:/Dashboards/My Tests/CMakeVS60/Tests/SystemInformation/CMakeFiles'/>
      <Variable name='CMAKE_PROJECT_NAME' value='DumpInformation'/>
      <Variable name='CMAKE_RANLIB' value=':'/>
      <Variable name='CMAKE_RC_COMPILER' value='c:/Program Files/Microsoft Visual Studio/Common/MSDev98/Bin/RC.EXE'/>
      <Variable name='CMAKE_RC_COMPILER_ARG1' value=''/>
      <Variable name='CMAKE_RC_COMPILER_ENV_VAR' value='RC'/>
      <Variable name='CMAKE_RC_COMPILER_LOADED' value='1'/>
      <Variable name='CMAKE_RC_COMPILER_WORKS' value='1'/>
      <Variable name='CMAKE_RC_COMPILE_OBJECT' value='<CMAKE_RC_COMPILER> <FLAGS>  /fo<OBJECT> <SOURCE>'/>
      <Variable name='CMAKE_RC_FLAGS' value=' '/>
      <Variable name='CMAKE_RC_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_RC_OUTPUT_EXTENSION' value='.res'/>
      <Variable name='CMAKE_RC_SOURCE_FILE_EXTENSIONS' value='rc'/>
      <Variable name='CMAKE_ROOT' value='C:/Dashboards/My Tests/CMake'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_LIBRARY_C_FLAGS' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_LINK_C_FLAGS' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_PREFIX' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG_SEP' value=''/>
      <Variable name='CMAKE_SHARED_LIBRARY_SUFFIX' value='.dll'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS' value=' /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_DEBUG' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_DEBUG_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_INIT' value=' /STACK:10000000 /machine:I386 /STACK:10000000 /machine:I386'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELEASE' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELEASE_INIT' value='/INCREMENTAL:NO'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO_INIT' value='/debug /INCREMENTAL:YES'/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_CXX_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_MODULE_CREATE_C_FLAGS' value='-shared'/>
      <Variable name='CMAKE_SHARED_MODULE_PREFIX' value=''/>
      <Variable name='CMAKE_SHARED_MODULE_SUFFIX' value='.dll'/>
      <Variable name='CMAKE_SIZEOF_VOID_P' value='4'/>
      <Variable name='CMAKE_SKIP_RPATH' value='NO'/>
      <Variable name='CMAKE_SOURCE_DIR' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='CMAKE_STATIC_LIBRARY_PREFIX' value=''/>
      <Variable name='CMAKE_STATIC_LIBRARY_SUFFIX' value='.lib'/>
      <Variable name='CMAKE_SYSTEM' value='Windows-5.1'/>
      <Variable name='CMAKE_SYSTEM_AND_CXX_COMPILER_INFO_FILE' value='C:/Dashboards/My Tests/CMake/Modules/Platform/Windows-CL.cmake'/>
      <Variable name='CMAKE_SYSTEM_AND_C_COMPILER_INFO_FILE' value='C:/Dashboards/My Tests/CMake/Modules/Platform/Windows-CL.cmake'/>
      <Variable name='CMAKE_SYSTEM_AND_RC_COMPILER_INFO_FILE' value='C:/Dashboards/My Tests/CMake/Modules/Platform/Windows-RC.cmake'/>
      <Variable name='CMAKE_SYSTEM_INCLUDE_PATH' value='C:\Program Files;C:\Program Files'/>
      <Variable name='CMAKE_SYSTEM_INFO_FILE' value='C:/Dashboards/My Tests/CMake/Modules/Platform/Windows.cmake'/>
      <Variable name='CMAKE_SYSTEM_LIBRARY_PATH' value='C:\Program Files;C:\Program Files'/>
      <Variable name='CMAKE_SYSTEM_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_NAME' value='Windows'/>
      <Variable name='CMAKE_SYSTEM_PROCESSOR' value='x86'/>
      <Variable name='CMAKE_SYSTEM_PROGRAM_PATH' value='C:\Program Files;C:\Program Files'/>
      <Variable name='CMAKE_SYSTEM_SPECIFIC_INFORMATION_LOADED' value='1'/>
      <Variable name='CMAKE_SYSTEM_VERSION' value='5.1'/>
      <Variable name='CMAKE_USE_RELATIVE_PATHS' value='OFF'/>
      <Variable name='CMAKE_VC_COMPILER_TESTS_RUN' value='1'/>
      <Variable name='CMAKE_VERBOSE_MAKEFILE' value='FALSE'/>
      <Variable name='DumpInformation_BINARY_DIR' value='C:/Dashboards/My Tests/CMakeVS60/Tests/SystemInformation'/>
      <Variable name='DumpInformation_SOURCE_DIR' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='EXECUTABLE_OUTPUT_PATH' value=''/>
      <Variable name='HAVE_CMAKE_SIZEOF_VOID_P' value='TRUE'/>
      <Variable name='LIBRARY_OUTPUT_PATH' value=''/>
      <Variable name='MSVC' value='1'/>
      <Variable name='MSVC60' value='1'/>
      <Variable name='MSVC_IDE' value='1'/>
      <Variable name='PROJECT_BINARY_DIR' value='C:/Dashboards/My Tests/CMakeVS60/Tests/SystemInformation'/>
      <Variable name='PROJECT_NAME' value='DumpInformation'/>
      <Variable name='PROJECT_SOURCE_DIR' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation'/>
      <Variable name='RUN_CONFIGURE' value='ON'/>
      <Variable name='WIN32' value='1'/>
      <Variable name='incl' value='C:/Dashboards/My Tests/CMake/Tests/SystemInformation/This does not exists'/>
     </Dashboard>
    </Dashboards>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Useful_Variables/Get_Variables_From_CMake_Dashboards) in another wiki.
