## '+' (plus) in the file name problem

  - Borland linker when creating executable does not support '+' in the
    name.

<!-- end list -->

  - CMake solves this problem by creating object files by replacing '+'
    with '_p_'.

## Broken streampos implementation

Using free bcc5.5.1.

    #include <iostream>

    int main()
    {
      std::streampos a = 9;
      long c = 10;

      if( a >= c )
       {
       std::cerr << "Impossible happen !" << std::endl;
       }
      else
       {
       std::cerr << "Seems resonable" << std::endl;
       }

      return 0;
    }

On borland a \>= c is true ...

According to the doc:

<http://www.cplusplus.com/ref/iostream/streampos.html>

This type describes a class to contain all the information needed to
restore an arbitrary file-position indicator within a stream. It can be
constructed from or casted to an integer offset value (streamoff).

There is a bug in the definition of std::streampos::operator\<= where
the implementation do the contrary of what expected.

## STL multisets with comparators

The Borland 5.5 compiler's STL implementation does not properly copy
comparators during assignment operations. For an example of how to work
around this, see [vtkLabelHierarchy.cxx
(rev 1.13)](http://public.kitware.com/cgi-bin/viewcvs.cgi/Rendering/vtkLabelHierarchy.cxx?r1=1.12&r2=1.13)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Borland_Compiler_Issues) in another wiki.
