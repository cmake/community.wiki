CMake's framework detection mechanism is now complete. Here is a code
snippet from a CMakeLists.txt file. Suppose our code needs the Carbon,
Quicktime and ApplicationServices Frameworks.

    IF(APPLE)
       #SET(GUI_TYPE MACOSX_BUNDLE)
       INCLUDE_DIRECTORIES ( /Developer/Headers/FlatCarbon )
       FIND_LIBRARY(CARBON_LIBRARY Carbon)
       FIND_LIBRARY(QUICKTIME_LIBRARY QuickTime )
       FIND_LIBRARY(APP_SERVICES_LIBRARY ApplicationServices )
       MARK_AS_ADVANCED (CARBON_LIBRARY
                         QUICKTIME_LIBRARY
                         APP_SERVICES_LIBRARY)
       SET(EXTRA_LIBS ${CARBON_LIBRARY} ${QUICKTIME_LIBRARY} ${APP_SERVICES_LIBRARY})
    ENDIF (APPLE)

... Then later on in the file:

    ADD_EXECUTABLE( ${MHDVIEWER_EXE_NAME} ${GUI_TYPE} ${PROJECT_SRCS} )
    TARGET_LINK_LIBRARIES( ${MHDVIEWER_EXE_NAME} ${EXTRA_LIBS} )

As for the syntax of your \#includes, you have two choices. Generally,
OS X system frameworks are included as:

    #include <Cocoa/Cocoa.h>
    #include <OpenAL/al.h>

But portable libraries may recommend doing flat namespaces because not
everybody uses the same directory name. For example, the OpenAL spec
doesn't define where headers are located. Linux puts them in
\<AL/al.h\>, OS X does \<OpenAL/al.h\>, and Windows doesn't put them in
any directory so it is \<al.h\>. In theory, somebody else could screw
this up even more and put it in \<AL1.1/al.h\>. Thus the 'portable'
solution is:

    #include "al.h"

CMake is really clever about the two cases, so if you have the usage
first case, you want to do:

    FIND_PATH(COCOA_INCLUDE_DIR Cocoa/Cocoa.h)

In the second case you want to do:

    FIND_PATH(OPENAL_INCLUDE_DIR al.h)

CMake will set the include paths correctly depending on which style you
specify.

This is an edited version of helpful posts from Mike Jackson and Eric
Wing. -Drew Wagner

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:HowToUseExistingOSXFrameworks) in another wiki.
