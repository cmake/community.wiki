# MinGW windows path problem

sh.exe must not be in the path for "MinGW Makefiles" to work.

<http://lists.trolltech.com/qt-interest/2006-01/thread00091-0.html>

If make finds sh.exe in your path, it will use it, and then "MinGW
Makefiles" will not work. If sh.exe is in your PATH, then you must use
"MSYS Makefiles" or "Unix Makefiles". I suppose I could put a check in
for this, and if sh.exe is found and you try "MinGW Makefiles" cmake
produces and error.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_MinGW_Compiler_Issues) in another wiki.
