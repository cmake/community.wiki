On Wacom make file all targets that do not produce output must be marked
symbolic. For example:

    install: .SYMBOLIC
       do something

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Wacom_Compiler_Issues) in another wiki.
