This page has been broken out from
[CMake:Bundles_And_Frameworks](doc/cmake/platform_dependent_issues/Bundles-And-Frameworks "wikilink")
so as to better concentrate soley on Mac OS X Framework bundles.

## What is a Framework?

Frameworks are shared libraries that are packaged with associated
resources, such as graphics files, developer documentation, and
programming interfaces. See ["Using Shared Libraries and
Frameworks"](http://developer.apple.com/documentation/DeveloperTools/Conceptual/MachOTopics/Articles/loading_code.html#//apple_ref/doc/uid/TP40001830-96836)
in [“Loading Code at
Runtime”](http://developer.apple.com/documentation/DeveloperTools/Conceptual/MachOTopics/Articles/loading_code.html#//apple_ref/doc/uid/TP40001830-SW1)
for more information.
[1](http://developer.apple.com/documentation/DeveloperTools/Conceptual/MachOTopics/Articles/building_files.html#//apple_ref/doc/uid/TP40001828-97030-TPXREF106)

See also:

  - [Framework Programming
    Guide](http://developer.apple.com/documentation/MacOSX/Conceptual/BPFrameworks/index.html)
  - [Dynamic Library Design
    Guidelines](http://developer.apple.com/documentation/DeveloperTools/Conceptual/DynamicLibraries/Articles/DynamicLibraryDesignGuidelines.html)

## Issues

  - ADD_LIBRARY as it exists now does not provide enough functionality.
  - CMAKE_INSTALL_PREFIX is not enough. When creating frameworks,
    auxilary files (i.e. header files and resources) should be in the
    proper subdirectory structure.

## Current Status

  - Linking to frameworks works.
  - Creating frameworks is just being started. (The plan is to document
    the design and implementation on this wiki page.)
  - 24/Oct/2006 - I have very, very preliminary support for creating
    frameworks with CMake at <http://wtl.wayfarer.org/cmake/>. **Note
    that this is in no way ready to be merged and probably won't even
    work for you\!** But, it does create a framework bundle for me.
    Unfortunately, even though the bundle will link, it won't run as the
    full path to the framework library isn't set correctly. It's set as
    TARGET_INSTALLNAME_DIRframeworkname. It also doesn't support
    header files in the framework yet. It is just meant to be a starting
    place from which to move forward. To ask questions about it or
    comment on it, please e-mail me (Tanner Lovelace) at clubjuggler at
    gmail dot com.

## Milestones

  - Shared library bundled as a framework
      - Should just be a case of setting up the bundle correctly,
        outputting the shared library (using framework flags) and
        copying in the info.plist file.
      - *Should consider install_name issues. I wonder if we can
        default to @executable_name/... as if it were an embedded
        framework and then have it work if it's placed in a standard
        framework location?*
  - Framework versioning working
  - Public header files copied into framework
  - Specified resource files copied into framework
      - *Can we support umbrella frameworks as a special case of
        resource files?*

## Notation

  - In all examples, the applications are named appl1, appl2, ...
  - libraries are named libr1, libr2, ...
  - header files are named appl1_header1, appl2_header2,
    libr1_header1, ...
  - there are auxilary files associated with application and library
    appl1_aux1, appl2_aux2, libr1_aux1, ...
  - and some resource files associated with application and library
    appl1_res1, appl2_res2, libr1_res1, ...
  - All versions are ver1, ver2, ...
  - All libraries have lib in their name, while frameworks do not. To
    differentiate, all frameworks will have names FRlibr1, FRlibr2, ...

## Cases

### Framework only

  - Everything in a same directory:

<!-- end list -->

    /Library/
      Frameworks/
        FRlibr1.framework/
          FRlibr1   -> Versions/Current/FRlibr1
          Resources -> Versions/Current/Resources
          Libraries -> Versions/Current/Libraries
          Headers   -> Versions/Current/Headers
          Versions/
            Current -> ver2
            ver2/
              FRlibr1
              Resources/
              Info.plist
              version.plist
              Libraries/
                libr2.dylib
                libr3.dylib
              Headers/
                FRlibr1_header1.h
                FRlibr1_header2.h
                FRlibr1_header3.h

  - Suggested api:

<!-- end list -->

    ADD_LIBRARY(
      FRlibr1
      SHARED MACOSX_FRAMEWORK
      appl1_src1.cxx
      appl1_src2.cxx
      FRlibr1_header4.h
      FRlibr1_header5.h
      FRlibr1_header6.h
      ...
      MACOSX_FRAMEWORK_PUBLIC_HEADERS
      FRlibr1_header1.h
      FRlibr1_header2.h
      FRlibr1_header3.h
      )

    SET_TARGET_PROPERTIES(FRlibr1
      PROPERTIES
      VERSION ver2
      )

    TARGET_LINL_LIBRARIES(
      FRlibr1
      libr2 libr3)

Comment: (submitted by Tanner Lovelace) We need to have both the means
of versioning frameworks and creating frameworks that aren't versioned.
If a framework doesn't have a version, it should be placed at the top
level of the framework directory structure (or, alternatively, we could
decide that all frameworks must have a version and either silently
default to something like "A" (not recommended\!) or cause an error). If
it does have a version, then it should be placed in the appropriate
version directory and symlinks created to point to it. The version may
be any arbitrary string. Apple itself generally uses capital letters (A,
B, C, etc...) but the major version works just as well. I believe it is
suggested that anything below major version number not be used since
libraries should be compatible within major version numbers.

## Linking Issues

### Link a Framework

Assuming:

    /usr/
      lib/
        libr3.dylib
    /Library/
      Frameworks/
        FRlibr1.framework/
          FRlibr1   -> Versions/Current/FRlibr1
          Resources -> Versions/Current/Resources
          Libraries -> Versions/Current/Libraries
          Headers   -> Versions/Current/Headers
          Versions/
            Current -> ver2
            ver2/
              FRlibr1
              Resources/
              Libraries/
                libr2.dylib
              Headers/

Regular library is linked like this:

    libtool -dynamic libr3_src1.o libr3_src2.o -o libr3.dylib

Framework is linked like this:

    mkdir -p FRlibr1.framework/Versions/ver2
    gcc -dynamiclib -o FRlibr1.framework/Versions/ver2/FRlibr1 FRlibr1_src1.o FRlibr1_src2.o
    cd ./FRlibr1.framework/Versions && ln -sf ver2 Current
    cd ./FRlibr1.framework && ln -sf Versions/Current/FRlibr1 FRlibr1

You link libr2.dylib like
    this:

    libtool -dynamic libr1_src1.o libr1_src2.o -o FRlibr1.framework/Versions/ver2/Libraries/libr2.dylib

  - Looks like the difference between linking framework and linking
    shared library
    is:
      - Framework:

<!-- end list -->

    gcc <b>-dynamiclib</b> -o FRlibr1.framework/Versions/ver2/FRlibr1 <sources>

  -   - Shard library:

<!-- end list -->

    gcc <b>-dynamic</b> -o libr2 <sources>

### Embedded Frameworks

Assuming:

    /Applications/
      appl1.app/
        Contents/
          Info.plist
          MacOS/
            appl1 -> appl1-1
            appl1-1
          Resources/
          Framework/
            FRlibr1.framework/
              FRlibr1   -> Versions/Current/FRlibr1
              Resources -> Versions/Current/Resources
              Versions/
                Current -> ver2
                ver2/
                  FRlibr1
                  Resources/

You have to run: **Add command here to create embedded framework**

## Useful Tools

(From
<http://developer.apple.com/documentation/DeveloperTools/Conceptual/MachOTopics/index.html>)

Tools for analyzing Mach-O files include the following:

  - The <i>/usr/bin/lipo</i> tool allows you to create and analyze
    binaries that contain images for more than one architecture. An
    example of such a binary is a universal binary. Universal binaries
    can be used in PowerPC-based and Intel-based Macintosh computers.
    Another example is a PPC/PPC64 binary, which can be used in 32-bit
    PowerPC–based and 64-bit PowerPC–based Macintosh computers.
  - The file-type displaying tool, <i>/usr/bin/file</i>, shows the type
    of a file. For multi-architecture files, it shows the type of each
    of the images that make up the archive.
  - The object-file displaying tool, <i>/usr/bin/otool</i>, lists the
    contents of specific sections and segments within a Mach-O file. It
    includes symbolic disassemblers for each supported architecture and
    it knows how to format the contents of many common section types.
  - The page-analysis tool, <i>/usr/bin/pagestuff</i>, displays
    information on each logical page that compose the image, including
    the names of the sections and symbols contained in each page. This
    tool doesn’t work on binaries containing images for more than one
    architecture.
  - The symbol table display tool, <i>/usr/bin/nm</i>, allows you to
    view the contents of an object file’s symbol table.

## Related Work

  - On mailing list:
    <http://public.kitware.com/pipermail/cmake/2005-December/007725.html>
  - From
        apple:
      - <http://developer.apple.com/documentation/DeveloperTools/Conceptual/MachOTopics/index.html>
      - <http://developer.apple.com/documentation/DeveloperTools/Conceptual/MachOTopics/Articles/loading_code.html#//apple_ref/doc/uid/TP40001830>
      - <http://developer.apple.com/documentation/CoreFoundation/Conceptual/CFBundles/index.html>
      - <http://developer.apple.com/documentation/MacOSX/Conceptual/BPFrameworks/index.html>
  - From Trolltech:
    <http://doc.trolltech.com/qq/qq09-mac-deployment.html>
  - Here is a good tutorial (via Quicktime movie) on how to create
    Embedded frameworks: <http://rentzsch.com/cocoa/embeddedFrameworks>
  - [How to uninstall software on mac](http://www.mac-how.net/)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:MacOSX_Frameworks) in another wiki.
