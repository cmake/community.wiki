bar.c:

    int i = 0;
    void bar()
     {
     i = 5;
     }

foo.c:

    extern void bar();
    void foo()
      {
      bar();
      }

Compiled with

    rm -f libbar.a *.o
    gcc -c bar.c
    ar cr libbar.a bar.o
    gcc -c foo.c
    gcc -shared -o libfoo.so foo.o -L. -lbar

Will
    fail:

    /usr/bin/ld: bar.o: @gprel relocation against dynamic symbol i collect2: ld returned 1 exit status

But, putting:

    gcc -fPIC -c bar.c

works.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_IA64_FPIC_problem) in another wiki.
