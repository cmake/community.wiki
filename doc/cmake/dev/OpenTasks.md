## Support for major buildsystems

Supported:

  - make, MSVC, XCode, Eclipse, KDevelop3, CodeBlocks

Missing:

  - Some generic file format, maybe XML based, so that it would be easy
    to process. Maybe *ant* may be a good candidate. This might also
    help the Java support in CMake. Any takers ?

## Support for programming languages

Fully supported:

  - C, C++
  - Fortran (CMake \>= 2.6.0)
      - Watch out for mix of free-form / fixed form src code
        ([1](http://www.cmake.org/pipermail/cmake/2009-March/027764.html))

More or less fully supported:

  - Java: there may be some issues relating to Java packages and
    subdirectories
  - [Assembler](doc/cmake/languages/Assembler "wikilink"): CMake \>= 2.6.0, but needs
    more testing and feedback
  - Objective C: works if the compiler (gcc) knows how to handle these
    files
  - Ada: not yet in cvs, but support exists (in PlPlot):
    <http://plplot.cvs.sourceforge.net/plplot/plplot/cmake/modules/>
  - D: not in cvs, but support exists:
    <http://www.dsource.org/projects/cmaked>

Not supported:

The languages listed here don't have built-in support in CMake, still
they can be used via ADD_CUSTOM_COMMAND()

  - C\#: would be nice to have
  - Pascal: shouldn't be too hard, volunteers ?
  - OCaml: really strange to build, may be hard to integrate

## Operating systems

Supported:

  - all major OSs, except the ones listed below

Not supported:

  - OS/2 (not offically supported, but builds are available from
    <http://os2ports.smedley.info>), DOS, DJGPP

## CMake language features

  - some way to define custom command line arguments

<!-- end list -->

  - fine grained flags (per source etc.), AFAIK is being worked on

<!-- end list -->

  - per-target or per-file include dirs and compile flags (being worked
    on AFAIK)

<!-- end list -->

  - better (PCRE) regexps, is being worked on

<!-- end list -->

  - abstraction of compiler flags (being worked on ?)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:OpenTasks) in another wiki.
