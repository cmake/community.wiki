# Overview

Work on a Symbian Carbide.c++ generator is in progress. You can read
more about the progress on
[this](http://public.kitware.com/Bug/view.php?id=8486) bug report.

Carbide is the common IDE for Symbian development, the output of this
generator can be compiled using the Carbide IDE or from command line.

This generator creates a set of .bld.inf/.mpp/.mk files that can be
imported in Carbide as an "Symbian OS Bld.inf file".

A bld.inf file in Carbide is similar to a solution (.sln) file in visual
studio, it contains a list project paths, the order of the projects is
the order that they are going to be built.

An .mmp file in Carbide is similar to a project(.vcproj) file in visual
studio, it contains source file paths, include paths, macro definitions
and all sort of other definitions that defines a c\\c++ compiler\\linker
output (exe, dll, lib and such).

CMake rules are implemented using make files (.mk) project that are
before each project .mmp file in the bld.inf file.

# Using Symbian Carbide.c++ Generator

## System requirements for Carbide.c++

  - A windows OS (XP, Vista, Win7)
  - About 4GB of space on drive C:
  - Fast internet bandwidth - the downloads needed are about 1GB.
  - An hour of free time.

## Creating a Carbide.c++ development environment

Follow the following steps.

### Open up an account on Symbian developer community

This is needed so you can download files from the Symbian site. You can
register on
[this](https://developer.symbian.org/main/user_profile/register.php)
page, make sure you give out a valid e-mail address, if you don't want
to give out your real address - you can use
[this](http://www.mytrashmail.com) site to get a temporary e-mail for
the e-mail activation step in the registration.

### Download and install Carbide

  - Go to
    [this](http://developer.symbian.org/main/tools_and_kits/downloads/view.php?id=2)
    page.
  - Download "ADT" (The link is on the right site of the page).
  - Make sure that no other version of ADT\\Carbide is installed on your
    system - remove the old install if needed.
  - Install the file you downloaded.

### Download and install the S60 Nokia Symbian SDK

  - Go to
    [this](http://developer.symbian.org/main/tools_and_kits/downloads/view.php?id=3)
    page.
  - Download "SDK - (from Nokia)" (The link is on the right site of the
    page).
  - Make sure that no other version of this type of Nokia SDK is
    installed on your system - remove the old install if needed.
  - Install the file you downloaded.
  - you must configure the environment for use with the S60 emulator.
    This is done by locating the Carbide.C++ submenu on the Start menu,
    and choosing "Configure environment for WINSCW command line".

## Create a Carbide project using the generator

You must have a Symbian development environment ready on your computer
as described on this wiki page above.

You also must have a version of CMake that includes this generator.

Generating the project is the same as for any other generator target of
CMake, the name of the generator is "Carbide".

Note however the following limitations (forced by this development
environment):

  - All of the project source files and dependencies must be on drive C:
  - You can't have spaces in folder names.
  - CXX extension is treated as C and not CPP by the compiler (the
    generator solves this by creating a new CPP file with a single
    \#include to the original file and adding it instead to the
    project).
  - Source files with the same name but in different folders are not
    supported (the generator solves this by creating a new source file
    with a unique name and a single \#include to the original file)
  - DLL exports need to be declared per data member in both declaration
    and implementation. Read more about how to export from a DLL
    [here](http://www.newlc.com/en/Writing-a-DLL.html).
  - There is an option to create command line application that will run
    on the Symbian emulation, and even get the result code from them,
    but you can't capture the console output as you would from a normal
    command line application.
  - The working directory of an exe is not the path of exe but a
    different folder on the device\\emulation.

## Importing a project to the Carbide IDE

![Carbide-import1](/uploads/cbfc0c354d7fb99beb6e6da696027206/Carbide-import1.jpg)
![Carbide-import2](/uploads/7b51830c5bfdc703d942d6fdf4358c0e/Carbide-import2.jpg)

As the
Figure shows, by selecting **File \> Import** menu item, the developer
launches an import panel which has various options, amongst which is the
option to import a bld.inf file as shown in figure 1. After selecting
this option, the developer is asked to select the bld.inf file to import
from the host computer's hard dis(s).

When this step is completed, Carbide.c++ displays all the Symbian
Platform SDKs that are installed on the host PC (known as autodetection)
and asks the developer to select the one appropriate for the project, as
shown in figure 2. In the next step, the developer can select which of
the Symbian MMP files referenced in the imported bld.inf file should be
used in order to parse the appropriate MMP file(s). Finally, the
developer can select the project name and the root directory for the
project. At the end of this wizard's operation, a Symbian C++ project is
opened and ready for development in Carbide.c++.

![Carbide-import3](/uploads/6cb14ab6ff7a63843f495a536f831769/Carbide-import3.jpg)
![Carbide-import4](/uploads/f5e17a97c1c531060516f0addc676298/Carbide-import4.jpg)

# Known open issues with this generator

  - Only DLL, EXE and LIB targets are supported for now.
  - There is no support for mmp
    "[Bitmap](http://developer.symbian.org/main/documentation/reference/s%5E3/doc_source/ToolsAndUtilities96/Build-ref/Mmp-ref/bitmap.html)"
    yet.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/Symbian_Carbide_Generator) in another wiki.
