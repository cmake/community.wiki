Project ideas for the Google Summer of Code 2011

## Guidelines

### Students

These ideas were contributed by developers and users of
[CMake](http://www.cmake.org/). If you wish to submit a proposal based
on these ideas you may wish to contact the developers to find out more
about the idea you are looking at, get to know the developers your
proposal will be reviewed by and receive feedback on your ideas.

The Google Summer of Code program is competitive, and accepted students
will usually have thoroughly researched the technologies of their
proposed project, been in frequent contact with potential mentors and
possibly have submitted a patch or two to fix bugs in the project they
intend to work. Kitware makes extensive use of mailing lists, and this
would be your best point of initial contact for any of the proposed
projects you would like to apply for. The mailing lists can be found on
the project pages linked to in the preceding paragraph. Please see [GSoC
proposal guidelines](https://public.kitware.com/Wiki/GSoC_proposal_guidelines) for further
guidelines on writing your proposal.

### Adding Ideas

When adding a new idea to this page, please try to include the following
information:

  - A brief explanation of the idea.
  - Expected results/feature additions.
  - Any prerequisites for working on the project.
  - Links to any further information, discussions, bug reports etc.
  - Any special mailing lists if not the standard mailing list for the
    CMake.
  - Your name and email address for contact (if willing to mentor, or
    nominated mentor).

If you are not a developer for the project concerned, please contact a
developer about the idea before adding it here.

## Project Ideas

[Project page](http://www.cmake.org/), [mailing
lists](http://www.cmake.org/cmake/help/mailing.html),
[dashboard](http://www.cdash.org/CDash/index.php?project=CMake).

### Project: CMake Autoconf Compatibility

**Brief explanation:** One of the things that prevents some projects
from migrating to CMake is their current investment and shared knowledge
of the auto\* family of tools. Creating improved compatibility functions
matching the familiar auto\* syntax for things like header checks,
command line switches to cmake etc would improve this situation
significantly. The student would need to assess syntax that should be
replicated in both the CMake language, and common command line switches
such as --prefix=... Some modules already exist that implement some of
this functionality, and these could be used as a starting point.

**Expected results:** Improved implementation of auto\* like syntax to
aid projects when migrating their build systems over to CMake.
Additional documentation on the migration process, and working with
existing communities that have needs in this area.

**Mentor:** Bill Hoffman (bill dot hoffman at kitware dot com).

### Project: CMake Project Generator

**Brief explanation:** The addition of a facility in CMake to
automatically generate CMakeLists files based upon the source files
present in a directory. So a developer can point CMake at a directory
full of source files with a '--generate-project' argument for example,
and automatically generate a project. This should detect if source files
contain the QOBJECT macro, and need Qt's moc running on them, adding
them to the appropriate CMake variable. This would be limited in scope
to relatively simple projects wanting a single executable to be built
from the source files, with some heuristics or switches to determine
what toolkit etc it being used.

**Expected results:** A new facility added to the CMake suite of tools
to generate simple CMakeLists files when supplied the path to a source
directory. The user should reasonably expect to be able to run generate
CMakeLists files, run CMake on these files and type make to build their
project. Similar in functionality to the feature provided by qmake.

**Prerequisites:** Some familiarity with CMake, C++ and Qt.

**Mentor:** Marcus Hanwell (marcus dot hanwell at kitware dot com).

### Project: CMake ninja generator

**Brief explanation:** CMake supports regular makefiles, and IDE's for
all versions of Visual Studio, and Xcode. However, the new tool Ninja
<http://martine.github.com/ninja/manual.html> has some properties that
might allow faster builds over traditional makefiles. The idea of this
project would be to create a new backend build tool format for CMake.

**Expected results:** A new option for any project using CMake to build
with the Ninja tool.

**Prerequisites:** Experience with C++ programming, and a working
knowledge of build tools like make.

**Mentor:** Bill Hoffman.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/GSoC) in another wiki.
