

<!-- toc -->

- [Introduction](#introduction)
- [New Maintainer](#new-maintainer)
- [Compatibility](#compatibility)
- [Third-Party Modules](#third-party-modules)
- [List](#list)

<!-- tocstop -->

## Introduction

If you want to add a new module to CMake, then you must volunteer to
maintain it, or find someone that will. See the [Call for
maintainers](http://www.cmake.org/pipermail/cmake/2007-July/015258.html)
announcement on the mailing list. This page contains a list of Module
maintainer volunteers, and instructions on how to become a maintainer.

Please [**DO NOT ADD a Bug
Tracker**](http://www.cmake.org/pipermail/cmake/2008-November/025196.html)
entry for a new Module request. Instead, volunteer to become a module
maintainer. Module maintainers get developer access (see
[below](#new-maintainer "wikilink")).

**Do not write find modules for packages that themselves build with
CMake. Instead provide a CMake package configuration file with the
package itself. See our tutorials on [CMake
Packages](doc/Tutorials#cmake-packages "wikilink").**

## New Maintainer

Please follow these steps to become a module maintainer:

1.  Read the
    [cmake-developer(7)](https://cmake.org/cmake/help/git-master/manual/cmake-developer.7.html#modules)
    manual section on Modules
2.  Create a [Kitware GitLab](https://gitlab.kitware.com/users/sign_in)
    account for access to the issue tracker. Use a meaningful name
    rather than a handle or alias. We need to be able to assign issues
    to you when they are related to your module.
3.  Create a [CDash](http://open.cdash.org/) account and subscribe to
    the [CMake](https://open.cdash.org/index.php?project=CMake) project
    to receive notification when your changes may have broken something.
    In your profile add "Your Name" as it appears in your "`git config
    user.name`" as a repository credential.
4.  Subscribe to the [CMake Developers Mailing
    List](https://cmake.org/mailman/listinfo/cmake-developers) and
    introduce yourself and the module. Wait for response, discussion,
    and acceptance before proceeding to the following steps.
5.  Read [CONTRIBUTING.rst](https://gitlab.kitware.com/cmake/cmake/blob/master/CONTRIBUTING.rst)
    at the top of the source tree for contribution instructions.
6.  Update the list below to advertise yourself as a contributor.

## Compatibility

If you are taking over maintenance of an existing module, please ensure
that all changes maintain compatibility:

  - The set of documented output variables such as XXX_INCLUDE_DIRS
    must be maintained. If you add new output variables that supersede
    existing ones please document the old names as for compatibility
    only but still provide them.

<!-- end list -->

  - The set of (un)documented input variables such as XXX_INCLUDE_DIR
    must be maintained. These variables typically appear as the first
    argument to commands like find_path which save them in the cache
    and allow the user to set them on the command line or interactively.
    Build scripts may set them to convince find modules to pick up the
    desired location for a package. If you change the way a package is
    found such that a cache variable is no longer needed, check whether
    the value is set and use it as a hint for the new approach.

## Third-Party Modules

  - Here is a list of "repositories" of 3rd party CMake modules, so
    maintainers can check for already existing modules etc.
      - KDE4:
        <https://projects.kde.org/projects/kde/kdelibs/repository/revisions/master/show/cmake/modules>
      - KDE support e-c-m:
        <https://projects.kde.org/projects/kdesupport/extra-cmake-modules/repository/revisions/master/show/modules>
      - PlPlot:
        <http://plplot.svn.sourceforge.net/viewvc/plplot/trunk/cmake/modules/>
      - <http://cmake-modules.googlecode.com/svn/trunk/Modules/>
      - OpenSceneGraph
        <http://www.openscenegraph.org/svn/osg/OpenSceneGraph/trunk/CMakeModules/>
      - OpenSync
        <http://svn.opensync.org/branches/3rd-party-cmake-modules/modules/>
      - <http://github.com/jedbrown/cmake-modules>
      - <http://github.com/mloskot/cmake-modules>
      - Blender:
        <https://svn.blender.org/svnroot/bf-blender/trunk/blender/build_files/cmake/Modules>

## List

  - NOBODY\!\!\! Please volunteer if you are able and willing. Need new
    maintainers for these
        modules:
      - [FindJNI](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindJNI.cmake)
      - [FindJava](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindJava.cmake)
      - [FindOpenSSL](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindOpenSSL.cmake)
      - [FindDCMTK](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindDCMTK.cmake)
      - Any Find\*.cmake module distributed with CMake that is not
        already listed under a maintainer below.

<!-- end list -->

  - Alan W. Irwin, irwin at beluga dot phys dot uvic dot ca
      - Ada

<!-- end list -->

  - Tim Burrell (tim dot burrell at gmail dot com)
    (http://www.dsource.org/projects/cmaked)
      - D support

<!-- end list -->

  - Marcel Loose, loose at astron dot
        nl
      - [FindSubversion](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindSubversion.cmake)

<!-- end list -->

  - Clinton Stimpson clinton at elemtech dot
        com
      - [FindQt4](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindQt4.cmake)

<!-- end list -->

  - Petr Gotthard, petr dot gotthard at honeywell dot
        com
      - [FindRTI](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindRTI.cmake)

<!-- end list -->

  - Kovarththanan Rajaratnam, kovarththanan.rajaratnam at gmail dot
        com
      - [FindZLIB](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindZLIB.cmake)

<!-- end list -->

  - James Bigler, nvidia.com - jbigler or jamesbigler - gmail dot
        com
      - [FindCUDA](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindCUDA.cmake)

<!-- end list -->

  - Clement Creusot - creusot at cs dot york dot ac dot
        uk
      - [FindArmadillo](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindArmadillo.cmake)

<!-- end list -->

  - Benjamin Eikel - cmake at eikel dot
        org
      - [FindGLEW](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindGLEW.cmake)

<!-- end list -->

  - Daniele E. Domenichelli - daniele dot domenichelli at gmail dot
        com
      - [FindGTK2](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindGTK2.cmake)

<!-- end list -->

  - Nils Gladitz - nilsgladitz at gmail dot com
      - CPack WiX Generator

<!-- end list -->

  - J. Schueller - schueller at phimeca dot
        com
      - [FindSWIG](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindSWIG.cmake)
      - [UseSWIG](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/UseSWIG.cmake)

<!-- end list -->

  - Domen Vrankar - domen dot vrankar at gmail dot com
      - [CPack RPM
        Generator](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/CPackRPM.cmake)
      - [CPack Deb
        Generator](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/CPackDeb.cmake)

<!-- end list -->

  - Raffi Enficiaud - raffi dot enficiaud at free dot
        fr
      - [FindMatlab](http://cmake.org/gitweb?p=cmake.git;a=blob;f=Modules/FindMatlab.cmake)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:Module_Maintainers) in another wiki.
