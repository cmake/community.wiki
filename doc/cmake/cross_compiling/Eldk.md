# How to use a Linux-to-Linux cross compiling GNU toolchain

Linux is in widespread use in embedded systems and handheld devices.
There are many commercial and non-commercial projects which provide
cross compiling toolchains, one of these is [Denx](http://www.denx.de),
which offer the [Embedded Linux Development
Kit](http://www.denx.de/wiki/DULG/ELDK), short ELDK. ELDK supports
PowerPC, ARM and MIPS as target systems, it consists of the cross
compiling toolchains and the quite complete target environment. ELDK
shouldn't be much different from other cross compiling toolchains, so
the information presented here should be usable also for other
Linux-to-Linux cross compiling toolchains.

## Installing ELDK

You can find installation instructions at
<http://www.denx.de/wiki/view/DULG/ELDKDownloadPowerPC> , the packages
are available at <ftp://ftp.sunet.se/pub/Linux/distributions/eldk/> .
E.g. if your target has a MIPS processor, you will download the files
you can find in
<ftp://ftp.sunet.se/pub/Linux/distributions/eldk/4.1/mips-linux-x86/iso>
. The easiest way to use them is to mount these ISO files and then
install ELDK using the *install* executable you will find there:

    ~/ $ mkdir mount-iso/
    ~/ $ sudo mount -tiso9660 mips-2007-01-21.iso mount-iso/ -o loop
    ~/ $ cd mount-iso/
    ~/mount-iso/$ ./install -d /home/alex/eldk-mips/
    ...
    Preparing...                ########################################### [100%]
       1:appWeb-mips_4KCle      ########################################### [100%]
    Done
    ~/mount-iso/$ ls eldk-mips/
    bin  eldk_init  etc  mips_4KC  mips_4KCle  usr  var  version

In eldk-mips/mips_4KC/ and eldk-mips/mips_4KCle/ the target
environments have been installed, i.e. complete Linux filesystems for
the target platforms. In eldk-mips/bin/, eldk-mips/usr/ etc. host side
tools have been installed, so the cross compiler can be found in
eldk-mips/usr/bin/mips-linux-gcc.

## Writing a CMake toolchain file

For CMake to be able to crosscompile software, it requires you to write
a toolchain file, which tells CMake some information about the
toolchain. With the examples used above it will look like:

    # the name of the target operating system
    SET(CMAKE_SYSTEM_NAME Linux)

    # which C and C++ compiler to use
    SET(CMAKE_C_COMPILER   /home/alex/eldk-mips/usr/bin/mips_4KC-gcc)
    SET(CMAKE_CXX_COMPILER /home/alex/eldk-mips/usr/bin/mips_4KC-g++)

    # here is the target environment located
    SET(CMAKE_FIND_ROOT_PATH  /home/alex/eldk-mips/mips_4KC /home/alex/eldk-mips-extra-install )

    # adjust the default behaviour of the FIND_XXX() commands:
    # search headers and libraries in the target environment, search
    # programs in the host environment
    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

Save this file as Toolchain-eldk-mips4KC.cmake to some location where
you will put all your toolchain files, e.g. $HOME.

## Building the software for ELDK

Let's say you have the classical hello world software with a CMake based
buildsystem and want to build this for ELDK. main.c:

    include <stdio.h>

    int main()
    {
       printf("Hello world\n");
       return 0;
    }

CMakeLists.txt:

    ADD_EXECUTABLE(hello main.c)
    INSTALL(TARGETS hello DESTINATION bin)

Then run CMake on it to generate the buildfiles, the important point is
that you tell it to use the toochain file you just wrote:

    ~/src/helloworld/ $ mkdir build
    ~/src/helloworld/ $ cd build
    ~/src/helloworld/build/ $ cmake -DCMAKE_TOOLCHAIN_FILE=~/Toolchain-eldk-mips4KC.cmake -DCMAKE_INSTALL_PREFIX=/home/alex/eldk-mips-extra-install ..
    -- Configuring done
    -- Generating done
    -- Build files have been written to: /home/alex/src/helloworld/build
    ~/src/helloworld/build/ $ make
    Scanning dependencies of target hello
    [100%] Building C object CMakeFiles/hello.dir/main.o
    Linking C executable hello
    [100%] Built target hello
    ~/src/helloworld/build/ $ make install
    ...
    -- Installing /home/alex/eldk-mips-extra-install/bin/hello

So that's all. It actually doesn't matter whether it's just a "hello
world" or some complex piece of software, the only difference is the
usage of the toolchain file. If the software has all required configure
checks, it should just build also for ELDK.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CmakeEldk) in another wiki.
