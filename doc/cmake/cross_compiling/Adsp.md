# How to use CMake to cross compile software with the ADSP cross compiler

## Install the cross compiler

You can download an evaluation version of Analog Device VisualDSP 4.5
for Windows on <http://www.analog.com>.

VisualDSP 4.5 provides cross compilers for the Blackfin, SHARC,
TigerSHARC and ADSP-21xx processors.

## Write a CMake toolchain file

To be able to cross compile software, CMake requires you to write a
toolchain file, which tells CMake some information about the toolchain.
This example shows how to configure CMake to crosscompile for a Blackfin
533 (ADSP-BF533):

    SET( CMAKE_SYSTEM_NAME Generic )

    # these are ADSP specific, so it makes sense to have them separated from CMAKE_SYSTEM_PROCESSOR
    SET( ADSP_PROCESSOR "ADSP-BF533" )
    SET( ADSP_PROCESSOR_SILICIUM_REVISION "0.5" )

    # specify the cross compiler
    SET( CMAKE_C_COMPILER ccblkfn )
    SET( CMAKE_CXX_COMPILER ccblkfn -c++ )

    SET( CMAKE_ASM_COMPILER easmBLKFN )

This example requires that the compiler **ccblkfn** and the linker
**easmBLKFN** are in the path.

Save this file as Toolchain-ADSP-Blackfin.cmake to some location where
you will put all your toolchain files.

## Building the software

You have to choose which make to use: gmake provides by with
VisualDSP4.5 or nmake (to install from Microsoft). If your
CMakeLists.txt is in C:\\src and you build from C:\\build
    then:

    cmake -DCMAKE_TOOLCHAIN_FILE=(location)\Toolchain-ADSP-Blackfin.cmake -G "NMake Makefiles" C:\src

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CmakeAdsp) in another wiki.
