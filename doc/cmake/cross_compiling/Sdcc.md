# How to use sdcc with CMake

SDCC is a free, retargettable, optimizing ANSI - C compiler for 8- and
16bit controllers, for more information visit
<http://sdcc.sourceforge.net>. You can use CMake to generate build files
for sdcc.

MinGW is the GNU toolchain for Windows, it also exists as cross compiler
under Linux (and probably other UNIXes). You can use it to build Windows
software on Linux.

## Installing sdcc

If you want to build (static) libraries, you need sdcc 2.7.1 (not yet
released as of June 2007) or newer or sdcc from svn. If you don't need
libraries, you can also use older versions of sdcc. With Ubuntu/Debian
you can simply install it using apt: "apt-get install sdcc". You can of
course also build this toolchain from sources yourself, you can find
instructions at <http://sdcc.sourceforge.net/index.php#Download> .

## Writing a CMake toolchain file

For CMake to be able to crosscompile software, it requires you to write
a toolchain file, which tells CMake some information about the
toolchain. For sdcc it will look like:

    # the name of the target operating system
    SET(CMAKE_SYSTEM_NAME Generic)

    # which compilers to use for C and C++
    SET(CMAKE_C_COMPILER sdcc)

    # here is the target environment is located
    SET(CMAKE_FIND_ROOT_PATH  /usr/share/sdcc /home/alex/sdcc-install )

    # adjust the default behaviour of the FIND_XXX() commands:
    # search headers and libraries in the target environment, search
    # programs in the host environment
    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

Save this file as Toolchain-sdcc.cmake to some location where you will
put all your toolchain files, e.g. $HOME. As you can see
CMAKE_FIND_ROOT_PATH is set to /usr/share/sdcc, which contains the
headers and libraries installed with sdcc, and /home/alex/sdcc-install/.
This second directory is intended to hold other libraries you will
compile using sdcc, they should be installed under this install prefix.
This way the FIND_XXX() commands in CMake will find both the headers
and libraries coming with the toolchain as well as additional libraries
you have built for this platform.

## Building the software with sdcc

Let's say you have a very simple program with a CMake based buildsystem
and want to build this using sdcc. main.c:

    int main()
    {
       return 42;
    }

CMakeLists.txt:

    ADD_EXECUTABLE(hello main.c)

Then run CMake on it to generate the buildfiles, the important point is
that you tell it to use the toochain file you just wrote:

    ~/src/helloworld/ $ mkdir build
    ~/src/helloworld/ $ cd build
    ~/src/helloworld/build/ $ cmake -DCMAKE_TOOLCHAIN_FILE=~/Toolchain-sdcc.cmake ..
    -- Configuring done
    -- Generating done
    -- Build files have been written to: /home/alex/src/helloworld/build
    ~/src/helloworld/build/ $ make
    Scanning dependencies of target hello
    [100%] Building C object CMakeFiles/hello.dir/main.rel
    Linking C executable hello.ihx
    [100%] Built target hello

So that's all. It actually doesn't matter whether it's just a "hello
world" or some complex piece of software, the only difference is the
usage of the toolchain file. If the software has all required configure
checks (which is quite unlikely for 8/16 bit controller systems), it
should just build also with this toolchain.

## Building simple software with sdcc

If you have a software which is simple to build (which doesn't say
anything about the actual code), you don't really need the toolchain
file. "Simple" means that there are no platform tests, especially no
FIND_XXX() commands in the CMake files. This may be the case for many
embedded projects. Then all you have to set is CMAKE_SYSTEM_NAME and
CMAKE_C_COMPILER:

    $ cmake -DCMAKE_SYSTEM_NAME=Generic -DCMAKE_C_COMPILER=sdcc <source dir>

This is enough to select the correct toolchain and target platform. If
the target platform has no operating system, "Generic" has to be used.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CmakeSdcc) in another wiki.
