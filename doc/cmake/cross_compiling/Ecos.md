# How to use CMake to cross compile software for eCos

## Install the eCos host tools

To build eCos software, you need the host tools for eCos, ecosconfig and
optionally configtool. Make sure both ecosconfig and configtool can be
found via the PATH. There are different way how to get them.

  - For some Linux distributions there exist ready-to-use packages,
    which you can simply install. E.g. for Ubuntu this

is done using "apt-get install ecosconfig".

  - Download and install a recent prebuilt version of the hosttools
    provided by eCosCtentric. You can find it at
    <http://www.ecoscentric.com/devzone/configtool.shtml>
  - Download and install the binary ecos 2.0 releases you can find at
    <ftp://ecos.sourceware.org/pub/ecos/releases/ecos-2.0> . Ignore the
    actual eCos sources in the package and use only configtool and
    ecosconfig which you can find in ecos-2.0/tools/bin/ in the package.
  - Download the sources from cvs and build it yourself. You can find
    instructions at <http://ecos.sourceware.org/anoncvs.html>

and at <http://www.ecoscentric.com/devzone/configtool.shtml> .

## Install a toolchain for building eCos applications

You need to install a suitable cross compiling toolchain to be able to
build software for eCos. You can download them either from
<ftp://ecos.sourceware.org/pub/ecos/gnutools> or build it yourself
following the instructions you can find at
<http://ecos.sourceware.org/build-toolchain.html> . Install the
toolchain to any location you like, e.g. $HOME/ecostoolchains/ . Make
sure the compiler is available via PATH, e.g. EXPORT
PATH=$HOME/ecostoolchains/arm-elf/bin:$PATH.

## Get the eCos sources and set ECOS_REPOSITORY

eCos release 2.0 is very old, it is recommended to use current anonymous
cvs. You can find instructions here:
<http://ecos.sourceware.org/anoncvs.html>

If you can't access cvs you can also download a weekly snapshot of cvs
provided by eCosCentric:
<http://www.ecoscentric.com/devzone/snapshots.shtml>

Once you have the eCos sources installed somewhere on your development
machine, e.g. in $HOME/ecos, set the ECOS_REPOSITORY environment
variable. This is required for running ecosconfig and configtool. It has
to point to the directory where the file ecos.db is located, e.g.
$HOME/ecos/packages/ .

## Build an eCos

eCos is a configurable operating system, so you have to configure the
system you want to use. Here's a simple example for the ARM integrator
board:

    ~/ $ cd src/ecos-arm-integrator
    ~/src/ecos-arm-integrator/ $ ecosconfig new integrator_arm9 default
    ~/src/ecos-arm-integrator/ $ ecosconfig tree
    ~/src/ecos-arm-integrator/ $ make
    ~/src/ecos-arm-integrator/ $ ls install/
    include  lib
    ~/src/ecos-arm-integrator/ $ ls install/lib
    extras.o  libextras.a  libtarget.a  target.ld  vectors.o

The first step will create an ecos.ecc file, which contains the
configuration of the operating system. The second step uses this
information to generate makefiles, which you use to build in the final
make step. Once make has finished, you will find an include/ and a lib/
directory created in the install/ subdirectory. This is the target
environment which will be used for compiling your software for this
eCos.

## Write a CMake toolchain file

For CMake to be able to crosscompile software, it requires you to write
a toolchain file, which tells CMake some information about the
toolchain. With the examples used above it will look like:

    # the name of the target operating system
    SET(CMAKE_SYSTEM_NAME eCos)

    # which compiler to use
    SET(CMAKE_C_COMPILER /home/alex/src/ecostoolchains/arm-elf/bin/arm-elf-gcc)
    SET(CMAKE_CXX_COMPILER /home/alex/src/ecostoolchains/arm-elf/bin/arm-elf-g++)

    # here is the target environment located
    SET(CMAKE_FIND_ROOT_PATH  /home/alex/src/ecos-arm-integrator/install /home/alex/ecos-arm-install )

    # adjust the default behaviour of the FIND_XXX() commands:
    # search headers and libraries in the target environment, search
    # programs in the host environment
    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

Save this file as Toolchain-eCos-arm-elf.cmake to some location where
you will put all your toolchain files, e.g. $HOME.

## Build the software for eCos

Let's say you have the classical hello world software with a CMake based
buildsystem and want to build this for the eCos you built above. main.c:

    include <stdio.h>

    int main()
    {
       printf("Hello world\n");
       return 0;
    }

CMakeLists.txt:

    ADD_EXECUTABLE(hello main.c)
    INSTALL(TARGETS hello DESTINATION bin)

Then run CMake on it to generate the buildfiles, the important point is
that you tell it to use the toochain file you just wrote:

    ~/src/helloworld/ $ mkdir build
    ~/src/helloworld/ $ cd build
    ~/src/helloworld/build/ $ cmake -DCMAKE_TOOLCHAIN_FILE=~/Toolchain-eCos-arm-elf.cmake -DCMAKE_INSTALL_PREFIX=/home/alex/ecos-arm-install ..
    -- Configuring done
    -- Generating done
    -- Build files have been written to: /home/alex/src/helloworld/build
    ~/src/helloworld/build/ $ make
    Scanning dependencies of target hello
    [100%] Building C object CMakeFiles/hello.dir/main.o
    Linking C executable hello.elf
    [100%] Built target hello
    ~/src/helloworld/build/ $ make install
    ...
    -- Installing /home/alex/ecos-arm-install/bin/hello

So that's all. It actually doesn't matter whether it's just a "hello
world" or some complex piece of software, the only difference is the
usage of the toolchain file. If the software has all required configure
checks, it should just build also for eCos.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CmakeEcos) in another wiki.
