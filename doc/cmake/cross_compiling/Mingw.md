# How to use MinGW to cross compile software for Windows

MinGW is the GNU toolchain for Windows, it also exists as cross compiler
under Linux (and probably other UNIXes). You can use it to build Windows
software on Linux.

## Install the mingw cross compiling toolchain

There are several ways to get the mingw cross compiler on your machine.
With Ubuntu/Debian you can simply install it using apt: "apt-get install
mingw32". This will install the toolchain as i586-mingw32msvc-gcc to
/usr/bin/ . You can of course also build this toolchain from sources
yourself, but this is more work.

## Write a CMake toolchain file

For CMake to be able to crosscompile software, it requires you to write
a toolchain file, which tells CMake some information about the
toolchain. With the examples used above it will look like:

    # the name of the target operating system
    SET(CMAKE_SYSTEM_NAME Windows)

    # which compilers to use for C and C++
    SET(CMAKE_C_COMPILER i586-mingw32msvc-gcc)
    SET(CMAKE_CXX_COMPILER i586-mingw32msvc-g++)
    SET(CMAKE_RC_COMPILER i586-mingw32msvc-windres)

    # here is the target environment located
    SET(CMAKE_FIND_ROOT_PATH  /usr/i586-mingw32msvc /home/alex/mingw-install )

    # adjust the default behaviour of the FIND_XXX() commands:
    # search headers and libraries in the target environment, search
    # programs in the host environment
    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

Save this file as Toolchain-mingw32.cmake to some location where you
will put all your toolchain files, e.g. $HOME. As you can see
CMAKE_FIND_ROOT_PATH is set to /usr/i586-mingw32msvc, which contains
the headers and libraries installed with the toolchain, and
/home/alex/mingw-install/. This second directory is intended to hold
other libraries you will compile using mingw32, they should be installed
under this install prefix. This way the FIND_XXX() commands in CMake
will find both the headers and libraries coming with the toolchain as
well as additional libraries you have built for this platform.

You can try this toolchain file as well
[Toolchain-cross-mingw32-linux.cmake](/uploads/265a703419aeaa26bef8044b92911e37/Toolchain-cross-mingw32-linux.cmake)
which is trying to be as
simple as possible by only setting COMPILER_PREFIX var.

## Build the software for Windows

Let's say you have the classical hello world software with a CMake based
buildsystem and want to build this for Windows using mingw32. main.c:

    #include <stdio.h>

    int main()
    {
       printf("Hello world\n");
       return 0;
    }

CMakeLists.txt:

    ADD_EXECUTABLE(hello main.c)

Then run CMake on it to generate the buildfiles, the important point is
that you tell it to use the toolchain file you just wrote:

    ~/src/helloworld/ $ mkdir build
    ~/src/helloworld/ $ cd build
    ~/src/helloworld/build/ $ cmake -DCMAKE_TOOLCHAIN_FILE=~/Toolchain-mingw32.cmake -DCMAKE_INSTALL_PREFIX=/home/alex/mingw-install ..
    -- Configuring done
    -- Generating done
    -- Build files have been written to: /home/alex/src/helloworld/build
    ~/src/helloworld/build/ $ make
    Scanning dependencies of target hello
    [100%] Building C object CMakeFiles/hello.dir/main.o
    Linking C executable hello.exe
    [100%] Built target hello

So that's all. It actually doesn't matter whether it's just a "hello
world" or some complex piece of software, the only difference is the
usage of the toolchain file. If the software has all required configure
checks, it should just build also with this toolchain.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CmakeMingw) in another wiki.
