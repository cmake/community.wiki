**CAUTION: The contents of this page may be obsolete**
![120px-Old_finnish_stop_sign.svg](/uploads/521180f925eeefd545a3a7a761185c2e/120px-Old_finnish_stop_sign.svg.png)

----

## Using CMake Package

    FIND_PACKAGE(CME)
    IF(CME_FOUND)
      INCLUDE(${CME_USE_FILE})
      # ... Use library ...
    ENDIF(CME_FOUND)

## Setup CMake Package

    #-----------------------------------------------------------------------------
    # CMEConfig.cmake - CME CMake configuration file for external projects.
    #
    # This file is configured by CME and used by the UseCME.cmake module
    # to load CME's settings for an external project.

    # The CME include file directories.
    SET(CME_INCLUDE_DIRS "@CMAKE_INSTALL_PREFIX@/include")

    # The CME library directories.
    SET(CME_LIBRARY_DIRS "@CMAKE_INSTALL_PREFIX@/lib")

    # The CME version number.
    SET(CME_VERSION_MAJOR "@CME_VERSION_MAJOR@")
    SET(CME_VERSION_MINOR "@CME_VERSION_MINOR@")

    # The location of the UseCME.cmake file.
    SET(CME_USE_FILE "@CMAKE_INSTALL_PREFIX@/lib/cme/UseCME.cmake")

    # The build settings file.
    SET(CME_BUILD_SETTINGS_FILE
        "@CMAKE_INSTALL_PREFIX@/lib/cme/CMEBuildSettings.cmake")

    # The CME library dependencies.  These can be blocked by projects
    # not interested in linking to CME's library.
    IF(NOT CME_NO_LIBRARY_DEPENDS)
      INCLUDE("@CMAKE_INSTALL_PREFIX@/lib/cme/CMELibraryDepends.cmake")
    ENDIF(NOT CME_NO_LIBRARY_DEPENDS)

    # Additional project-specific configuration settings can be set here.

    #-----------------------------------------------------------------------------
    # CMakeLists.txt

    # Example project "CMake Export".
    PROJECT(CME)

    # Every project should have a version number.
    SET(CME_VERSION_MAJOR 1)
    SET(CME_VERSION_MINOR 0)

    # Example library.
    ADD_LIBRARY(cme cme.cxx)

    # Link to libm on unix for sqrt().  This is to demonstrate library
    # dependency chaining.
    IF(UNIX)
      TARGET_LINK_LIBRARIES(cme m)
    ENDIF(UNIX)

    # Create the CMEConfig.cmake file for installation.
    CONFIGURE_FILE(${CME_SOURCE_DIR}/CMEConfig.cmake.in
                   ${CME_BINARY_DIR}/CMEConfig.cmake @ONLY IMMEIDATE)

    # Save the compiler settings and library dependencies so another
    # project can import them.
    INCLUDE(${CMAKE_ROOT}/Modules/CMakeExportBuildSettings.cmake)
    CMAKE_EXPORT_BUILD_SETTINGS(${CME_BINARY_DIR}/CMEBuildSettings.cmake)
    EXPORT_LIBRARY_DEPENDENCIES(${CME_BINARY_DIR}/CMELibraryDepends.cmake)

    # Install targets.  Make sure the paths configured into
    # CMEConfig.cmake match.
    INSTALL_TARGETS(/lib cme)
    INSTALL_FILES(/include .h cme)
    INSTALL_FILES(/lib/cme .cmake CMEBuildSettings CMELibraryDepends
                  UseCME CMEConfig)

    #-----------------------------------------------------------------------------
    # UseCME.cmake
    #
    # This module is provided as CME_USE_FILE by CMEConfig.cmake.  It can
    # be INCLUDEd in a project to load the needed compiler and linker
    # settings to use CME.
    #

    # Load the compiler settings used for CME.
    IF(CME_BUILD_SETTINGS_FILE)
      INCLUDE(${CMAKE_ROOT}/Modules/CMakeImportBuildSettings.cmake)
      CMAKE_IMPORT_BUILD_SETTINGS(${CME_BUILD_SETTINGS_FILE})
    ENDIF(CME_BUILD_SETTINGS_FILE)

    # Add include directories needed to use CME.
    INCLUDE_DIRECTORIES(${CME_INCLUDE_DIRS})

    # Add link directories needed to use CME.
    LINK_DIRECTORIES(${CME_LIBRARY_DIRS})

    /*--------------------------------------------------------------------------*/
    /* cme.cxx */
    #include <math.h>
    double cme()
    {
      return sqrt(2);
    }

    /*--------------------------------------------------------------------------*/
    /* cme.h */
    #ifndef _cme_h
    #define _cme_h

    extern double cme();

    #endif

## Reference

  - <http://public.kitware.com/pipermail/cmake/2003-March/003554.html>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Package) in another wiki.
