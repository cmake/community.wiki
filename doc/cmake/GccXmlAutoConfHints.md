When converting from autoconf, the generation of "config.h" files can be
complex. You may wish to examine the CMake scripts within the gcc-xml
project at [gcc-xml (XML output of gcc's internal
representation)](http://www.gccxml.org/HTML/Index.html). The project
builds a modified gcc compiler using CMake. This is an impressive feat
of autoconf conversion.

For example, the HAVE_THIS/HAVE_THAT defines used within autoconf
projects can be overwhelming when converting a large project. Below is a
snippet from the "config.h.in" file within gcc-xml:

    /* Define to 1 if you have the `bcopy' function. */
    #cmakedefine HAVE_BCOPY @HAVE_BCOPY@

    /* Define to 1 if you have the `bsearch' function. */
    #cmakedefine HAVE_BSEARCH @HAVE_BSEARCH@

    /* Define to 1 if you have the `bzero' function. */
    #cmakedefine HAVE_BZERO @HAVE_BZERO@

While not a drop-in tool, their scripts are an excellent starting point.
*Be sure to reference the code from their CVS repository instead of the
out-of-date tarball listed.*

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/GccXmlAutoConfHints) in another wiki.
