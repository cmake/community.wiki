See also [CMake Generator Specific
Information](doc/cmake/Generator-Specific-Information "wikilink")

## Specific Platforms

### Linux

  - [IA64 Issues](doc/cmake/platform_dependent_issues/IA64-Issues "wikilink")

### Mac OSX

  - [Bundles and Frameworks](doc/cmake/platform_dependent_issues/Bundles-And-Frameworks "wikilink")
  - [Linker and XCode stuff](doc/cmake/MacOSXLinkerAndXCodeStuff "wikilink")
  - [How To Use Existing OS X
    Frameworks](doc/cmake/platform_dependent_issues/HowToUseExistingOSXFrameworks "wikilink")
  - <http://www.kernelthread.com/mac/apme/optimizations/>
  - <http://0xfe.blogspot.com/2006/03/how-os-x-executes-applications.html>

### Windows

  - [Borland Compiler Issues](doc/cmake/platform_dependent_issues/Borland-Compiler-Issues "wikilink")

<!-- end list -->

  - [MinGW Compiler Issues](doc/cmake/platform_dependent_issues/MinGW-Compiler-Issues "wikilink")

<!-- end list -->

  - [Wacom Compiler
Issues](doc/cmake/platform_dependent_issues/Watcom-Compiler-Issues "wikilink")

## OS Specific Programs

| Platform         | System Info                | OS Distribution Version          |
| ---------------- | -------------------------- | -------------------------------- |
| Linux            | uname -a                   |                                  |
| HP-UX            | uname -a                   |                                  |
| AIX              | uname -a                   |                                  |
| SunOS            | uname -a                   |                                  |
| IRIX             | hinv, uname -a             |                                  |
| Max OSX / Darwin | system_profiler, uname -a | About This Mac, /usr/bin/sw_ver |
| Cygwin           | uname -a                   |                                  |
| Windows          | ver                        | ver                              |

## The Platforms / Compilers Table

| Platform                | Compilers                                               | Macros          | List Dependencies   | List Symbols | Trace Syscal/Signal                      | Runtime Library Path                                                          |
| ----------------------- | ------------------------------------------------------- | --------------- | ------------------- | ------------ | ---------------------------------------- | ----------------------------------------------------------------------------- |
| Linux                   | gcc, g++, icc                                           | __linux       | ldd program         | nm           | strace                                   | LD_LIBRARY_PATH                                                             |
| HP-UX                   | cc, [aCC](http://docs.hp.com/en/B3901-90019/index.html) | __hpux        | chatr program       | nm           |                                          | SHLIB_PATH (32), LD_LIBRARY_PATH (64)                                      |
| AIX                     | xlc, xlC                                                | _AIX           | dump -H program     | nm           |                                          | LIBPATH                                                                       |
| SunOS                   | cc, CC                                                  | __sparc       | ldd program         | nm           |                                          | LD_LIBRARY_PATH (32), LD_LIBRARY_PATH_64 (64)                            |
| IRIX                    | cc, CC                                                  | __sgi         | ldd program         |              |                                          | LD_LIBRARY_PATH (o32), LD_LIBRARYN32_PATH (n32), LD_LIBRARY64_PATH (64) |
| Max OSX / Darwin        | gcc, g++                                                | __APPLE__   | otool -L program    | nm           | ktrace -f outfile program; kdump outfile | DYLD_LIBRARY_PATH                                                           |
| Cygwin                  | gcc, g++                                                | __CYGWIN__  | depends program.exe | nm           |                                          | PATH                                                                          |
| gcc, g++ w/ -mwin32     | __CYGWIN__, _WIN32                                 |
| gcc, g++ w/ -mno-cygwin | _WIN32                                                 |
| MinGW                   | gcc, g++                                                | __MINGW32__ | depends program.exe | nm           |                                          | PATH                                                                          |
| Windows                 | Visual Studio 6, cl                                     | _WIN32         | depends program.exe | dumpbin      |                                          | PATH                                                                          |
| Visual Studio 7, cl     | depends program.exe **†**                               |
| Visual Studio 7.1, cl   | depends program.exe **†**                               |

  - **†** In order for *depends* to be installed, the "Win32 Platform
    SDK Tool" needs to be selected when installing Visual Studio (See
    [Determining Which DLLs to
    Redistribute](http://msdn.microsoft.com/library/default.asp?url=/library/en-us/vccore/html/vccondeterminingwhichdllstoredistribute.asp)).

This is a useful reference for predefined macros:
<http://predef.sourceforge.net/precomp.html>

## Architecture Modes

| Compiler      | Command | Languages      | Architecture Flags | Macros        |
| ------------- | ------- | -------------- | ------------------ | ------------- |
| GNU           | gcc     | C              |                    |               |
| g++           | C++     |                |                    |
| SGI MIPSpro   | cc      | C              | \-o32, -n32, -64   |               |
| CC            | C++     |                |
| SunPro        | cc      | C              | \-xarch=...        | __SUNPRO_C |
| CC            | C++     | __SUNPRO_CC |
| HP            | cc      | C              | \+DD64             |               |
| aCC           | C++     | __HP_aCC    |
| IBM VisualAge | xlc     | C              | \-q32, -q64        | __IBMC__  |
| xlC           | C++     | __IBMCPP__ |                    |

## Compiler Options and Flags

| Compiler  | Full Warnings                | No Warnings                  | Warnings as Errors | Suppress Warning \#n                                                | Warning \#n as Error                                                   |
| --------- | ---------------------------- | ---------------------------- | ------------------ | ------------------------------------------------------------------- | ---------------------------------------------------------------------- |
| gcc       | \-W -Wall -Wshadow           | \-w                          | \-Werror           | \-Wno-<warning-name>                                                |                                                                        |
| MIPS Pro  | \-fullwarn                   | \-w or -woffall or -woff all | \-w2               | \-woff (\#n)\[,n0..n1,...\] or -diag_suppress (\#n)\[,n0..n1,...\] | \-diag_error (\#n)\[,n0..n1,...\]                                     |
| icc       | \-Wall -w2 -Wcheck           | \-w                          | \-Werror           | \-wd(\#n)\[,arg2,..argn\]                                           | \-we(\#n)\[,arg2,..argn\]                                              |
| bcc32     | \-w+                         | \-w-                         | \-g1               | \-w-(\#n)                                                           |                                                                        |
| xlc       | \-qlanglv=ansi or -qflag=w:w | \-w or -qflag=e:e            | \-qhalt=w          | \-qsuppress=(\#n)\[:arg2:..argn\]                                   |                                                                        |
| xlC       | \-qlanglv=ansi or -qflag=w:w | \-w or -qflag=e:e            | \-qhalt=w          | \-qsuppress=(\#n)\[:arg2:..argn\]                                   | \-qhaltonmsg=(\#n)\[,arg2,..argn\]                                     |
| VS6: cl   | /W4                          | /w                           | /WX                |                                                                     |                                                                        |
| VS7-8: cl | /Wall /Wp64                  | /w                           | /WX                | /wd(\#n)                                                            | /we(\#n)                                                               |
| Sun cc    |                              | \-w                          | \-errwarn=%all     | \-erroff=<warning-tag>\[,arg2,..argn\]                              | \-errwarn=<warning-tag>\[,arg2,..argn\] (use -errtags=yes to see tags) |
| HP cc     | \+w1                         | \-w or +w3                   | \+We               | \+W (\#n)\[,arg2,..argn\]                                           | \+We (\#n)\[,arg2,..argn\]                                             |
| HP aCC    | \+w                          | \-w                          | \+We               | \+W(\#n)\[,arg2,..argn\]                                            | \+We(\#n)\[,arg2,..argn\]                                              |
| wcl386    | \-wx                         | \-w=0                        | \-we               | \-wcd=(\#n)                                                         | \-wce=(\#n)                                                            |

  - More platform specific information can be found in [ROSETTA
    STONE](http://bhami.com/rosetta.html) platforms table (Mostly for
    system administrators)

<!-- end list -->

  - Another even more complete table: [Using static and shared libraries
    across
    platforms](http://www.fortran-2000.com/ArnaudRecipes/sharedlib.html)
    (For various compiler flags and options)

## Debugging Tips

### Using special debug libraries on various systems

| Platform         | Operation                               |
| ---------------- | --------------------------------------- |
| Debian GNU/Linux | export LD_LIBRARY_PATH=/usr/lib/debug |
| Mac OS X         | export DYLD_IMAGE_SUFFIX=_debug      |

### The gdbrun Script for UNIX-like Platforms

The following is an extremely useful script that will run any command
line in a gdb debugger. Put this text in an executable file called
"gdbrun":

    <nowiki>
    #!/bin/bash

    extra_text=""
    if [ "$1" == "--break-main" ]; then
      extra_text="break main"
      shift
    fi

    EXEC="$1"

    shift

    run_text="run"
    for a in "$@"; do
      run_text="${run_text} \"$a\""
    done

    TMPFILE=/tmp/gdbrun.$$.$#.tmp
    cat > ${TMPFILE} <<EOF
    ${extra_text}
    ${run_text}
    EOF

    gdb -x ${TMPFILE} "${EXEC}"
    rm -f "${TMPFILE}"

    </nowiki>

Then one may debug a test like this:

    <nowiki>
    gdbrun /path/to/myexe --some-arg --some-other-arg
    </nowiki>

Notes about this script:

  - It supports spaces in argument names (note the for loop)
  - Takes extra argument --break-main, which causes the program to stop
    once all the libraries are loaded
  - It always run debugger, even when program exits normally
  - Cannot be used with MPI or any other system that runs your program
    from a shell script

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Platform_Dependent_Issues) in another wiki.
