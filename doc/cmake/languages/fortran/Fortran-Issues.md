This page used to contain discussion related to building Fortran90
projects with CMake 2.4 and lower. As of CMake 2.6 there is first-class
support for building Fortran90 projects.

See this bug report for details:
<http://www.cmake.org/Bug/view.php?id=5809>

See the wiki history of this page for the old discussion.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Fortran_Issues) in another wiki.
