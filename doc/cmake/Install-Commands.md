You are now on the CMake community wiki.
Many pages are now **DEPRECATED** and sometimes even considered bad
practice. Those are only kept as reference for previous CMake versions.
The documentation for the latest version is available here:
[CMake Documentation](https://cmake.org/documentation/).

-----

## Overview

CMake has an elaborate install process that simplifies installation of
programs, libraries, and other files. In CMake version 2.4 the new
**INSTALL** command was introduced that encompasses the functionality of
all the old **install** commands. This page describes the new
**INSTALL** command and the transition from the old one.

## Old INSTALL_\* Commands

  - INSTALL_FILES : Create rules to install the listed files with the
    given extension into the given directory. Only files existing in the
    current source tree or its corresponding location in the binary tree
    may be listed. If a file specified already has an extension, that
    extension will be removed first. This is useful for providing lists
    of source files such as foo.cxx when you want the corresponding
    foo.h to be installed. A typical extension is '.h'.

<!-- end list -->

    INSTALL_FILES(<dir> extension file file ...)
    INSTALL_FILES(<dir> regexp)

  -
    Any files in the current source directory that match the regular
    expression will be installed.

<!-- end list -->

    INSTALL_FILES(<dir> FILES file file ...)

  -
    Any files listed after the FILES keyword will be installed
    explicitly from the names given. Full paths are allowed in this
    form. The directory
    <dir>
    is relative to the installation prefix, which is stored in the
    variable CMAKE_INSTALL_PREFIX.

<!-- end list -->

  - INSTALL_PROGRAMS : Create rules to install the listed programs into
    the given directory. Use the FILES argument to guarantee that the
    file list version of the command will be used even when there is
    only one argument.

<!-- end list -->

    INSTALL_PROGRAMS(<dir> file1 file2 [file3 ...])
    INSTALL_PROGRAMS(<dir> FILES file1 [file2 ...])
    INSTALL_PROGRAMS(<dir> regexp)

  -
    In the third form any program in the current source directory that
    matches the regular expression will be installed. This command is
    intended to install programs that are not built by cmake, such as
    shell scripts. See the TARGETS form of the INSTALL command to create
    installation rules for targets built by cmake. The directory
    <dir>
    is relative to the installation prefix, which is stored in the
    variable CMAKE_INSTALL_PREFIX.

; INSTALL_TARGETS : Create rules to install the listed targets into the
given directory. The directory

<dir>

is relative to the installation prefix, which is stored in the variable
CMAKE_INSTALL_PREFIX. If RUNTIME_DIRECTORY is specified, then on
systems with special runtime files (Windows DLL), the files will be
copied to that directory.

    INSTALL_TARGETS(<dir> [RUNTIME_DIRECTORY dir] target target)

## New INSTALL Command

The new **INSTALL** command replaces all the old commands with a single
command. This command is much more flexible and provide a common
interface for all installation requirements.

In addition to all the old functionality, this command also provides a
way to install directories, run CMake scripts and inline CMake code, as
well as a **component** framework.

## Replacing The Old INSTALL_\* Commands

Each of the old commands can be replaced with a proper signature of the
new **INSTALL** command.

It is important to note that the new INSTALL command takes all the paths
as relative paths. If absolute path is provided, the absolute path will
be used.

### Replace INSTALL_FILES

Let say that we have files in source and build directory that we are
trying to install. These files are: file1.h, file2.h, file3.h,
file4.hxx, file5.txt, and file6.cmake. Assume file1.h, file2.h,
file4.hxx, and file5.txt are in the source tree, while all the other
files are in the build tree.

    INSTALL_FILES(/include .h file1.cxx file2.c file3.h)
    INSTALL_FILES(/include "\.hxx")
    INSTALL_FILES(/share/special_files FILES file5.txt
      ${CMAKE_CURRENT_BINARY_DIR}/file6.cmake)

These lines are replaced with:

    INSTALL(FILES file1.h file2.h ${CMAKE_CURRENT_BINARY_DIR}/file3.h
      DESTINATION include)
    FILE(GLOB files "${CMAKE_CURRENT_SOURCE_DIR}/*.hxx")
    INSTALL(FILES ${files} DESTINATION include)
    INSTALL(FILES file5.txt ${CMAKE_CURRENT_BINARY_DIR}/file6.cmake
      DESTINATION share/special_files)

### Replace INSTALL_PROGRAMS

Assuming there are scripts scr1 and scr2, where scr2 is in the build
directory. The code in CMake list file would look like this:

    INSTALL_PROGRAMS(/bin FILES scr1 ${CMAKE_CURRENT_BINARY_DIR}/scr2)

This line is replaced with:

    INSTALL(PROGRAMS scr1 ${CMAKE_CURRENT_BINARY_DIR}/scr2
      DESTINATION bin)

### Replace INSTALL_TARGETS

Assuming there are targets: ExecutableTarget, StaticLibraryTarget, and
SharedLibraryTarget. Typically there would be code in CMake list file
that would look like this:

    INSTALL_TARGETS(/bin ExecutableTarget)
    INSTALL_TARGETS(/lib StaticLibraryTarget)
    INSTALL_TARGETS(/lib RUNTIME_DIRECTORY /bin SharedLibraryTarget)

These lines are replaced
    with:

    INSTALL(TARGETS ExecutableTarget StaticLibraryTarget SharedLibraryTarget
      RUNTIME DESTINATION bin
      LIBRARY DESTINATION lib
      ARCHIVE DESTINATION lib
    )

**RUNTIME** refers to the runtime piece of the target. Runtime piece
only applies to executable targets and DLL (Windows) style shared
libraries. **LIBRARY** refers to all the other (non DLL) shared
libraries and modules. **ARCHIVE** refers to the static libraries and
the import parts of DLL libraries (LIB files).

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:Install_Commands) in another wiki.
