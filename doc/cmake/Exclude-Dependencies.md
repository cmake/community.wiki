Assume there are series of directories with several targets. This page
will describe things that should be build and things that should be
ignored.

  - T1 - T8 are targets
  - T6 -\> T2 means T6 depends on T2
  - A: 1, 2, 3, 4 means in directory A the targets T1 - T4 will be build
    by default
  - F: 7, 8, (4) means that targets T7, T8, T4 will be build by default
    and T4 is pulled in from excluded directory

<graphviz> digraph untitled {

` graph [`
`   rankdir=LR;`
` ];`
` node [`
`   shape="record";`
` ];`

` nodeA [ label="A: 1, 2, 3, 4, 5|T1|T2|T3|T4|T5 -\> T1" ];`
` nodeB [ label="B: 6, 7, (3)|T6|T7 -\> T2|Set T3 IN_ALL" ]; `
` nodeC [ label="C: 6, 7, (3)" ];`
` nodeD [ label="D: 8, 9, (4)|T8|T9 -\> T6|Set T4 IN_ALL" ];`
` nodeF [ label="F: 8, 9, (4)" ];`

` nodeC -> nodeA [ label="EX_ALL" ];`
` nodeC -> nodeB;`
` nodeF -> nodeC [ label="EX_ALL" ];`
` nodeF -> nodeD;`

}

</graphviz>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:Exclude_Dependencies) in another wiki.
