  - When you open CMake, you will see this:

![CMake_WinXPCMake24_Setup](/uploads/6eaa7de4dffd0d2befb9b6e79e4fb14d/CMake_WinXPCMake24_Setup.jpg)

  - Now there will be an entry for CMake on your start menu. Click the
    CMake shortcut there to run CMake. You will see a window like the
    one shown
below:

![CMake_WinXPCMake24_SourceCode](/uploads/13fb453442aacaae57e1ea4007294d8f/CMake_WinXPCMake24_SourceCode.jpg)

  - This dialog box is asking two simple questions. It wants to know
    where the source code is and where you want to place the output.
    Populate the form as shown above and hit the configure button.

Next you will see a screen like the one shown below:

![CMake_WinXPCMake24_Generator](/uploads/5d387a63b0144c90e890b8afcfd8afee/CMake_WinXPCMake24_Generator.jpg)

  - Select the environment (IDE) you plan to use and click OK. You will
    see a bunch of status messages in the status bar of the main form.
    It will take a while for this configuration process to complete.

Next you will see a screen that looks like
this:

![CMake_WinXPCMake24_Variables](/uploads/576db4fbf80e46b7126506c9153be2f5/CMake_WinXPCMake24_Variables.jpg)

  - Click "Configure"

The red bars will change to little gray ones as shown
below:

![CMake_WinXPCMake24_Configured](/uploads/51d6cb99b7371dbfd6e253fd6360c1ee/CMake_WinXPCMake24_Configured.jpg)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/Obtaining/ExampleWindowsXPCMake24) in another wiki.
