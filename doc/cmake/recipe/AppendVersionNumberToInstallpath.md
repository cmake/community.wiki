In some situations, it's necessary to keep older versions of a program
along with the newer. Here is a recipe for appending the version number
to your install path:

    IF(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
        SET(ORIGINAL_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX} CACHE STRING "Default prefix path" FORCE)
    ENDIF()
    MARK_AS_ADVANCED(ORIGINAL_INSTALL_PREFIX)

    IF( NOT( VERSION VERSION_EQUAL PROJECT_VERSION) )
        SET(CMAKE_INSTALL_PREFIX "${ORIGINAL_INSTALL_PREFIX}-${VERSION}" CACHE STRING "Install path" FORCE)
        SET(PROJECT_VERSION ${VERSION})
    ENDIF()

This ensures that the CMAKE_INSTALL_PREFIX is updated whenever the old
version number and the new version number doesn't match. This does have
some side-effects, such as situations where the user is customizing the
CMAKE_INSTALL_PREFIX to some custom location. Such users would need to
modify the ORIGINAL_INSTALL_PREFIX instead to ensure that the project
is going to the desired place.

The CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT is necessary here
because if it's missing, the path becomes recursive and will continue to
append ${PROJECT_NAME}-${VERSION} to the path infinitely. The
conditional ensures that it only does this once.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/RecipeAppendVersionNumberToInstallpath) in another wiki.
