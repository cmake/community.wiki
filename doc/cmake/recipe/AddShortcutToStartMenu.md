If you application builds for multiple platforms and you'd like your
application to show up in the Start Menu in Windows XP, add this logic
to the end of your CMakeLists.txt file before "include(CPack)":

    if( WIN32 AND NOT UNIX )
        SET(CPACK_PACKAGE_EXECUTABLES "Target_Name" "Target Name")
    endif()

Now the application shows up with a shortcut in the Start Menu on
Windows only with the name "Target Name"

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/RecipeAddShortcutToStartMenu) in another wiki.
