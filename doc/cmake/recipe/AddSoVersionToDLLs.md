When building for platforms that don't allow .so versioning, it's
sometimes nice to be able to see at a glance what version of the API the
.so library supports. Here's some example code for adding this to
non-linux platforms and not breaking linux platforms in the process:

    SET( GENERIC_DLL_VERSION ${VERSION} )
    SET( MYDLL_SOVERSION 1 )
     ...
    ADD_LIBRARY(mydll SHARED ${mydll_SRCS})
    target_link_libraries(mydll otherlib)
    IF( WIN32 )
        SET_TARGET_PROPERTIES( mydll PROPERTIES
            OUTPUT_NAME "mydll-${MYDLL_SOVERSION}"
            VERSION ${GENERIC_DLL_VERSION} )
    ELSE()
        SET_TARGET_PROPERTIES( mydll PROPERTIES
            VERSION ${GENERIC_DLL_VERSION}
            SOVERSION ${MYDLL_SOVERSION} )
    ENDIF()

You will need to be sure to increment the VERSION and MYDLL_SOVERSION
every time the program or the SOVERSION changes (respectively).

One thing I'd like to try in the future is add something to my unit
tests that grep the source headers for the API and complain when it's
changed to remind me to update the SOVERSION.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/RecipeAddSoVersionToDLLs) in another wiki.
