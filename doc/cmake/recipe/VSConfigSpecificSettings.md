This was taken from a thread on the CMake users mailing list found here:

[Mail
Thread](http://www.cmake.org/pipermail/cmake/2008-October/024468.html)

Basically the following is needed in your cmakelists.txt file.

    if(WIN32)
       set_target_properties(WindowApplicationExample PROPERTIES LINK_FLAGS_DEBUG "/SUBSYSTEM:CONSOLE")
       set_target_properties(WindowApplicationExample PROPERTIES COMPILE_DEFINITIONS_DEBUG "_CONSOLE")
       set_target_properties(WindowApplicationExample PROPERTIES LINK_FLAGS_RELWITHDEBINFO "/SUBSYSTEM:CONSOLE")
       set_target_properties(WindowApplicationExample PROPERTIES COMPILE_DEFINITIONS_RELWITHDEBINFO "_CONSOLE")
       set_target_properties(WindowApplicationExample PROPERTIES LINK_FLAGS_RELEASE "/SUBSYSTEM:WINDOWS")
       set_target_properties(WindowApplicationExample PROPERTIES LINK_FLAGS_MINSIZEREL "/SUBSYSTEM:WINDOWS")
    endif(WIN32)

This will cause debug builds to use the console and release builds to
NOT show a console.

Search Engine: Visual Studio 2005 2008 console show hide debug release
how to configuration

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/VSConfigSpecificSettings) in another wiki.
