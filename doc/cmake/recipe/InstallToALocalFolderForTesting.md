This recipe is meant to install a local copy of all install files into a
directory within the build directory. This is usually not needed in
linux because you can use the rpath to set the location of libraries,
but when doing cross-platform development (i.e. windows or cygwin), the
files all need to be in the same directory. Now you can use CTest to
call the programs without installing them to their destination. Here's
the recipe:

    add_custom_target(fake_install
        ALL
        "${CMAKE_COMMAND}"
        -D CMAKE_INSTALL_PREFIX:string=${CMAKE_CURRENT_BINARY_DIR}/testdir
        -P "${CMAKE_CURRENT_BINARY_DIR}/cmake_install.cmake"
        DEPENDS Target1 Target2 Target2  )

Note that I had to manually set it to depend on all of my build system's
targets to avoid situations where it would try to run the installer
before finishing the build of everything else. Also worth noting is that
this will run every time you "make all" and has a strange side effect of
also running whenever doing "make install" (because the target is ALWAYS
considered out of date), but that shouldn't be an issue. Note that I put
this in my top level CMakeLists.txt. IF you put it somewhere else, you
may need to use PROJECT_BINARY_DIR instead of
CMAKE_CURRENT_BINARY_DIR.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/RecipeInstallToALocalFolderForTesting) in another wiki.
