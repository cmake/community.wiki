

<!-- toc -->

- [CMake](#cmake)
    + [CMake Packages](#cmake-packages)
    + [Object Libraries](#object-libraries)
    + [Cross-platform Development](#cross-platform-development)
    + [Convenience Techniques](#convenience-techniques)

<!-- tocstop -->

This page is not yet fully organized. Please see other tutorials on the
[top page](Home#tutorials-1 "wikilink").

# CMake

### CMake Packages

  - [Exporting and Importing
    Targets](doc/tutorials/Exporting-and-Importing-Targets "wikilink")
  - [Packaging](doc/tutorials/Packaging "wikilink")
  - [Package Registry](doc/tutorials/Package-Registry "wikilink")
  - [How to create a ProjectConfig.cmake
    file](doc/tutorials/How-to-create-a-ProjectConfig.cmake-file "wikilink")

### Object Libraries

  - [Object Library Overview](doc/tutorials/Object-Library "wikilink")

### Cross-platform Development

  - [Change behavior based on the available c++
    environment](doc/tutorials/C++Compilers "wikilink")
  - [Change c++11 flags based on the available c++
    environment](doc/tutorials/C++11Flags "wikilink")
  - [Using Qt through CMake on different
    platforms](doc/tutorials/Qt "wikilink")

### Convenience Techniques

  - [Set a group of variables all at the same
    time](doc/tutorials/SettingVariableGroups "wikilink")

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/Tutorials) in another wiki.
