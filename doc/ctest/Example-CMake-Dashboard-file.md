(Maybe these are not necessary? In ITK, the latest source is downloaded
automatically if you run a nightly build - can anyone comment if that is
also the case with VTK? Is this a function of CTest or is it specific to
the project?)

``` text
cvs -d :pserver:anonymous:vtk@public.kitware.com:/cvsroot/VTK co VTK
cvs -d :pserver:anonymous:vtk@public.kitware.com:/cvsroot/VTKData co VTKData
```

The source code is now in /home/username/Dashboards/VTK/VTK and the
testing data sets are in /home/username/Dashboards/VTK/VTKData

Create a file called dashboard.cmake, the contents of which should be
similar to this example:

``` text
cmake_minimum_required(VERSION 2.6.3)

SET(DASHBOARD Nightly)

set(CTEST_DASHBOARD_ROOT "/home/username/Dashboards/VTK")
set(CTEST_SOURCE_DIRECTORY "${CTEST_DASHBOARD_ROOT}/VTK") #expands to /home/username/Dashboards/VTK/VTK

set(CTEST_SITE "your_name.your_organization") # this can be whatever you like
set(CTEST_BUILD_NAME "Linux-gcc-4.4.0") # this should be a description of your system - e.g. operating system, compiler version


set(CTEST_BINARY_DIRECTORY "${CTEST_DASHBOARD_ROOT}/bin") #expands to /home/username/Dashboards/VTK/bin
set(CTEST_DATA_DIRECTORY "${CTEST_DASHBOARD_ROOT}/VTKData") #expands to /home/username/Dashboards/VTK/VTKData

# do not change these
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_TEST_TIMEOUT 360)
set(CTEST_COVERAGE_COMMAND "/usr/bin/gcov") # REQUIRED with new ctest script style.

#this should be changed to match your system
set(CTEST_CMAKE_GENERATOR "Unix Makefiles") #this example is for linux, maybe someone can provide a similar file for windows?
set(CTEST_BUILD_COMMAND "make")

find_program(CTEST_CVS_COMMAND NAMES cvs) #tell cmake to use cvs to update
set(CTEST_UPDATE_COMMAND "cvs")
set(CTEST_UPDATE_OPTIONS "-dAP")

ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY}) #start fresh each time the dashboard build tests are performed

ctest_start(Experimental) # I think you can change this to "Nightly"?

#updates
ctest_update(SOURCE "${CTEST_DATA_DIRECTORY}") #update (using cvs) the data sets before testing
ctest_update(SOURCE "$${CTEST_SOURCE_DIRECTORY}") #update (using cvs) the source code before building

#setup and build
ctest_configure(BUILD "${CTEST_BINARY_DIRECTORY}")
ctest_read_custom_files("${CTEST_BINARY_DIRECTORY}")
ctest_build(BUILD "${CTEST_BINARY_DIRECTORY}")

#run tests
ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}")

#not sure what this is?
ctest_coverage(BUILD "${CTEST_BINARY_DIRECTORY}")

#submit the results to the dashboard
ctest_submit()
```

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/Example_CMake_Dashboard_file) in another wiki.
