## General information and availability

### What is CTest

CTest is a cross-platform, open-source testing system distributed with
[CMake](http://www.cmake.org). CTest can peform several operations on
the source code, that include retrieving from CVS or Subversion
repository, configure, build, perform set of predefined runtime tests.
It also includes several advanced tests such as coverage and memory
checking. The results can be submitted to a
[Dart](http://public.kitware.com/Dart) testing dashboard.

### What is the current release?

CTest release schedule is tied with CMake. This release is available at:

<http://www.cmake.org/HTML/Download.html>

Nightly development can be accessed through CVS.

See <http://www.cmake.org/HTML/Download.html> for more information.

### I found a Bug\! What should I do?

Please report the bug in our bug tracker: <http://www.cmake.org/Bug> .

Please make sure to look at the old bugs not to include duplicates,
include detailed instructions of the bug and how to reproduce it.

### I want a new feature in CTest. What should I do?

Report a feature request in our Bug tracker <http://www.cmake.org/Bug> .

Please make sure to look at the old feature requests not to include
duplicates, include detailed instructions of the feature and proposed
implementation.

## Using CTest

### My test does not return non-zero on failure. What can I do?

After adding test using:

    ADD_TEST(MyTest ${EXEC_DIR}/MyTest)

Add test properties for pass and
    failure:

    SET_TESTS_PROPERTIES(MyTest PROPERTIES FAIL_REGULAR_EXPRESSION "ERROR;FAIL;Test failed")
    SET_TESTS_PROPERTIES(MyTest PROPERTIES PASS_REGULAR_EXPRESSION "Test passed")

## Platform-specific questions

### How can I perform coverage test?

Currently coverage is only supported on gcc compiler. To perform
coverage test, make sure that your code is build with debug symbols,
without optimization, and with special flags. These flags are:

    -fprofile-arcs -ftest-coverage

Also make sure to pass these flags to C compiler, CXX compiler, and the
linker. For
    example:

    CXXFLAGS="-g -O0 -Wall -W -Wshadow -Wunused-variable -Wunused-parameter -Wunused-function -Wunused -Wno-system-headers -Wno-deprecated -Woverloaded-virtual -Wwrite-strings -fprofile-arcs -ftest-coverage"
    CFLAGS="-g -O0 -Wall -W -fprofile-arcs -ftest-coverage"
    LDFLAGS="-fprofile-arcs -ftest-coverage"

## Nightly Dashboards

### Can I script CTest?

Yes, please look at the [CTest Scripting
Tutorial](doc/ctest/Scripting-Of-CTest "wikilink").

### How do I shutdown / reboot the system after the dashboard?

#### Linux

Add a cron entry as **root** with line:

    /sbin/shutdown -r now

#### Windows

  - <http://www.robvanderwoude.com/index.html>

#### HP-UX

Add a cron entry as **'root** with line:

    /sbin/shutdown -r -y 60

#### SunOS

Add a cron entry as **'root** with line:

    /usr/sbin/sync && /usr/sbin/sync && /usr/sbin/reboot

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CTest:FAQ) in another wiki.
