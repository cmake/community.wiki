**CAUTION: The contents of this page may be obsolete**
![120px-Old_finnish_stop_sign.svg](/uploads/521180f925eeefd545a3a7a761185c2e/120px-Old_finnish_stop_sign.svg.png)

----

CTest supports four submission methods:

Dart Classic:

  - FTP with HTTP client initiated trigger
  - HTTP with HTTP client initiated trigger
  - SCP with server side trigger

Dart2:

  - XML-RPC with no trigger

The submission methods are configured by a set of variables, which
should be set in CTestConfig.cmake in your source tree. For backward
compatibility also DartConfig.cmake is supported, it has the same syntax
as CTestConfig.cmake, except that the variables don't have the "CTEST_"
prefix (e.g. DROP_METHOD instead of CTEST_DROP_METHOD). CMake will
read this file and create a DartConfiguration.tcl with the respective
settings in the build tree.

## XML-RPC

Dart2 uses XML-RPC over HTTP to perform submission of data and does not
require triggering.

Example CTestConfig.cmake flags:

    SET (CTEST_DROP_METHOD "xmlrpc")
    SET (CTEST_DROP_SITE "http://www.na-mic.org:8081/")
    SET (CTEST_DROP_LOCATION "Insight")

Example DartConfiguration.tcl flags:

    DropMethod: xmlrpc
    DropSite: http://www.na-mic.org:8081/
    DropLocation: Insight

## HTTP

This is preferred submission method for the original Dart dashboards. It
uses Hypertext Transfer Protocol's (HTTP) PUT option to send submission
data to the server. Once data is uploaded, it uses HTTP to trigger the
actual submission of the data to the dashboard.

Example CTestConfig.cmake flags:

    SET (CTEST_DROP_METHOD "http")
    SET (CTEST_DROP_SITE "public.kitware.com")
    SET (CTEST_DROP_LOCATION "/cgi-bin/HTTPUploadDartFile.cgi")
    SET (CTEST_TRIGGER_SITE "http://${DROP_SITE}/cgi-bin/Submit-Random-TestingResults.cgi")

Example DartConfiguration.tcl flags:

    DropMethod: http
    DropSite: public.kitware.com
    DropLocation: /cgi-bin/HTTPUploadDartFile.cgi
    TriggerSite: http://public.kitware.com/cgi-bin/Submit-Random-TestingResults.cgi

## FTP

Default submission method for legacy purposes. This is the method
original Dart preferred. It uses File Transfer Protocol (FTP) to send
submission data to the server. Once data is uploaded, it uses HTTP to
trigger the actual submission of the data to the dashboard.

Example CTestConfig.cmake flags:

    SET (CTEST_DROP_METHOD "ftp")
    SET (CTEST_DROP_SITE "public.kitware.com")
    SET (CTEST_DROP_LOCATION "/incoming")
    SET (CTEST_DROP_SITE_USER "ftpuser")
    SET (CTEST_DROP_SITE_PASSWORD "public")
    SET (CTEST_TRIGGER_SITE "http://${DROP_SITE}/cgi-bin/Submit-Random-TestingResults.cgi")

Example DartConfiguration.tcl flags:

    DropMethod: ftp
    DropSite: public.kitware.com
    DropLocation: /incoming
    DropSiteUser: ftpuser
    DropSitePassword: public
    TriggerSite: http://public.kitware.com/cgi-bin/Submit-Random-TestingResults.cgi

## SCP

When security is important SCP submission method can be used.

Example CTestConfig.cmake flags:

    SET (CTEST_DROP_METHOD "scp")
    SET (CTEST_DROP_SITE "public.kitware.com")
    SET (CTEST_DROP_LOCATION "/incoming")
    SET (CTEST_DROP_SITE_USER "ftpuser")

Example DartConfiguration.tcl flags:

    DropMethod: scp
    DropSite: public.kitware.com
    DropLocation: /incoming
    DropSiteUser: someuser
    DropSitePassword: public
    TriggerSite: http://public.kitware.com/cgi-bin/Submit-Random-TestingResults.cgi
    ScpCommand: /usr/bin/scp

This method requires external tool to do the actual transfer and does
not perform the trigger. Trigger has to be done on the server. Example
script that will do it is this:

    <nowiki>#!/usr/bin/perl -ws
    #
    # Script that will move testing results from the project incomming
    # directory to the appropriate location for dashboard summarization.
    #
    ###

    use File::Basename;
    use File::Copy;
    use File::Path;

    if ( $#ARGV < 0 )
    {
        die "No project specified";
        $project = "Xdmf";
    }
    $project = $ARGV[0];


    $dropLocation = "/projects/FTP/incoming/".$project;
    $destination = "/projects/".$project."/Testing/".$project."-Testing/Testing/HTML/TestingResults/Sites";

    opendir(DIR, $dropLocation) || die "Cannot open directory ".$dropLocation." - ".$!;
    @files = grep { ! /^\./ && -f "$dropLocation/$_" } readdir(DIR);
    closedir DIR;

    foreach $file (@files) {
        $xmlfile = $file;
        $xmlfile =~ s/%(..)/sprintf("%c", hex($1))/g;   # unquote %-quoted

            print "Processing file: ".$xmlfile."\n";

        $fullPathToIncomingXMLFile = $dropLocation . "/" . $xmlfile;
        if (-e $fullPathToIncomingXMLFile) {
    # file exists, so lets move it

    # first, translate the xml filename to a directory path
            $xmlfile =~ s|___|/|g;

    # for security reasons, disallow any file with ".." in the user
    # specified path to keep people from storing files just anywhere in
    # the host directory structure
            $securityCheck = $xmlfile;
            if ( ($securityCheck =~ /\.\./) ) {
                print "For security reasons, $xmlfile cannot be accepted.\n";
                exit;
            }

    # construct destination path and filename
            $fullPathToDestinationXMLFile = $destination . "/" . $xmlfile;
            mkpath( dirname( $fullPathToDestinationXMLFile ) );

    # now copy the file to destination
            move( $fullPathToIncomingXMLFile, $fullPathToDestinationXMLFile);

            print "$xmlfile submission successful.\n";
        }
        else {
    # specified file does not exist
            print "$xmlfile submission failed. $xmlfile is not in the dropbox.\n";
        }

    }</nowiki>

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CTest:Submission_Issues) in another wiki.
