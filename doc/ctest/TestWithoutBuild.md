## Carrying out tests on a machine without building first

This page describes, what can be done to perform tests on a different
machine than the one where the software was built.

I created an installer image (BitRock installer in our case) containing
the normal deliverables of our software plus the following items:

  - Some scripts needed to carry out our tests. This also contains
    software needed to carry out the tests (Squish)
  - A complete CMake installation
  - The following CMake-generated files from the build directory:
      - CTestCustom.cmake
      - DartConfiguration.tcl
      - CTestTestfile.cmake
  - Post-installation script

The last item is a script, which is run by the installer as a
post-install step. This script adjusts all paths in the above CMake
files from the build directory to the installation directory. It also
adjusts the host name contained in DartConfiguration.tcl, so the results
show up in CDash under the correct name. The final step of the
post-installation script is to call ctest with the following arguments:

    ctest -T Test -T Submit --track Nightly

For this to work the environment variable CMAKE_ROOT is set to the
just-installed CMake share directory in the script.

This causes a simple installation, which can be done automatically via
cron or the Windows task planner, to perform the installation, carry out
the tests and submit the results to CDash.The only requirement for the
test machines is to have standard software installed, we need perl and
python for our tests.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CTest:TestWithoutBuild) in another wiki.
