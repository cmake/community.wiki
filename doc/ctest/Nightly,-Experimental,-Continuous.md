CTest currently supports three different running submission modes:
Experimental, Nightly, and Continuous.

# Experimental

  - By default no update happens. If update is explicitly issued using
    *-D ExperimentalUpdate* then it updates to the latest version in the
    repository
  - Experimental is typically used for:
      - Testing if the changes were succesfull
      - New dashboards that are not yet tested if everything works fine
      - Trying new features without poluting the Nightly section

# Nightly

  - CTest will update to the latest nightly start time specified in
    DartConfiguration.tcl as NightlyStartTime. Assuming the nightly
    start time is 9pm EST and currently it is before 9pm EST, anything
    between 9pm EST yesterday and 9pm EST today will be updated to 9pm
    yesterday. This way all nightly submissions are updated to the same
    version of repository.

# Continuous

  - Repository is updated to the latest version and if there are no
    updates, the processing will stop. If there are any changes, that
    the processing is similar to Nightly or Experimental.

Here are some variables you may find useful:

    # should ctest wipe the binary tree before running
    SET (CTEST_START_WITH_EMPTY_BINARY_DIRECTORY_ONCE TRUE)

    # specify how long to run the continuous in minutes
    SET (CTEST_CONTINUOUS_DURATION 650)
    SET (CTEST_CONTINUOUS_MINIMUM_INTERVAL 15)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CTest:Nightly,_Experimental,_Continuous) in another wiki.
