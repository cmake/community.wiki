# Parallel execution

Like the effect -j N has on make

    * See: [http://public.kitware.com/Bug/view.php?id=1113 mantis-entry]

# Other xml output formats

The test results are stored into a bunch of xml files, which are tied to
dart/cdash.

## Junit

There are many continuous integration suites with dashboard facilities
out there in the Java world. Apache ant provides two taskdefs (<junit>,
<junitreport>) which produce xml. This format has become most popular.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CTest:OpenTasks) in another wiki.
