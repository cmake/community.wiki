## A simple example

Let's just start with a simple example for an application, let's name
it, well, ksimple. The CMakeLists.txt below gives the project a name, so
that the project files for KDevelop/XCode/MSVC will have a good name. It
will then find the KDE 4 installation on the system and setup the
required include directories. The list of source files will be put into
a variable named mySources, which will be automoced and then used to
build an executable from it. The executable and its desktop file will
finally be installed.

    project(ksimple)

    find_package(KDE4 REQUIRED)

    include_directories( ${KDE4_INCLUDES} )

    set(mySources main.cpp mywidget.cpp mypart.cpp)

    kde4_automoc( ${mySources} )

    kde4_add_executable(ksimple ${mySources})

    target_link_libraries(ksimple ${KDE4_KDEUI_LIBS} ${KDE4_KPARTS_LIBS} )

    install(TARGETS ksimple DESTINATION bin)
    install(FILES kfoo.desktop DESTINATION ${XDG_APPS_DIR})

## A full-featured example

Ok, and now for a full-featured but non-working example.

Give the project a name, find KDE 4 and setup CMAKE_MODULE_PATH. This
has the effect that cmake will also use check the
${CMAKE_SOURCE_DIR}/cmake directory for files if you use
include(somefile) or find_package(something). Please note that this
directory is added to the former contents of CMAKE_MODULE_PATH. This
is important, since if find_package(KDE4) was successfully, it will
have setup CMAKE_MODULE_PATH so that it points to the directory where
the cmake files installed with kdelibs are located.

    project(kfoo)

    find_package(KDE4 REQUIRED)

    set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake ${CMAKE_MODULE_PATH})

Then apply the required settings:

    include_directories( ${KDE4_INCLUDE_DIRS})
    add_definitions( ${KDE4_DEFINITIONS} )

Create a variable which holds your source files:

    set(kfooSources main.cpp myappl.cpp view.cpp)

If you have Qt designer ui files version 3 or 4 add them to the variable
we just created:

    kde4_add_ui_files( kfooSources maindialog.ui logindialog.ui)

    kde4_add_ui3_files( kfooSources printerdlg.ui previewdlg.ui)

If you have files for the kconfig_compiler add them this way:

    kde4_add_kcfg_files( kfooSources settings.kcfg )

You have some DBUS stuff too ? ...not done yet...

So, if you have listed everything, you probably want automoc:

    kde4_automoc( ${kfooSources} )

Please note that kde4_automoc() doesn't take the name of a variable as
argument, but just a list of files. That's why kfooSource is
"dereferenced" here.

Finally, tell cmake what you want to build:

    kde4_add_executable(kfoo ${kfooSources} )

This is a slightly extended version of the original CMake
add_executable(). Additionally it does some more RPATH handling and
supports KDE4_ENABLE_FINAL. Just as add_executable() the first
argument is the name of the executable followed by the list of source
files. If you want to create a library instead of an executable, it
works similar:

    kde4_add_library(kfoo ${kfooSources} )
    set_target_properties( kfoo PROPERTIES VERSION 5.0.0 SOVERSION 5 )

As above with add_executable(), kde4_add_library() is an extended
version of the add_library() command. It adds support for
KDE4_ENABLE_FINAL and under windows it adds "-DMAKE_KFOO_LIB" to the
compile flags. The set_target_properties() command is optional, if you
don't need a version number for the library you can just skip it.

KDE is very modular, so if you want to create a KPart, or a control
center module, or an ioslave, here's how to do it:

    kde4_add_plugin( kfoo ${kfooSources} )

Now your application/library/plugin probably needs to link to some
libraries. For this use the standard cmake target_link_libraries()
command. For every KDE library there are variables in the form
"KDE4_FOO_LIBS" available. Use them and you get also all depending
libraries:

    target_link_libraries( kfoo ${KDE4_KDEUI_LIBS} ${KDE4_KIO_LIBS})

And now some words about kdeinit-executables. They can also be created
with cmake. Here's an example:

    kde4_add_kdeinit_executable(kbar ${kbarSources} )
    target_link_libraries(kdeinit_kbar ${KDE4_KIO_LIBS} )
    target_link_libraries( kbar kdeinit_kbar)

    install(TARGETS kbar DESTINATION ${BIN_INSTALLDIR})
    install(TARGETS kdeinit_kbar DESTINATION ${LIB_INSTALL_DIR})

As you can see, here's a bit more going on.
kde4_add_kdeinit_executable() creates both an executable with the
given name and a library with the given name prefixed with "kdeinit_".
Use target_link_libraries() to add all required libraries to the
kdeinit_kbar library, and then link kbar against kdeinit_kbar.

In the examples above the install commands are missing. The KDE 4
support in cmake provides a bunch of predefined variables you should
use, e.g. if it's a library:

    install(TARGETS kfoo DESTINATION ${LIB_INSTALL_DIR})

There is also PLUGIN_INSTALL_DIR and a bunch more variables, see the
top of FindKDE4Internal.cmake for details.

## Some useful functions

  - Sometimes, you can want to remove a default argument passed to the
    compiler, then the remove_definitions is for you. For example, by
    default, the KDE4 build system sets the -DQT_NO_STL flag. If your
    code uses some of the Qt STL compatibility layer, you will want to
    remove this flag. Simply do:

<!-- end list -->

    remove_definitions(-DQT_NO_STL)

## Where to find more information

The KDE 4 support of cmake consists of the file FindKDE4.cmake, which
comes with cmake and is mainly a forwarder to the file
FindKDE4Internal.cmake. In FindKDE4Internal.cmake system-dependent
settings are set and the KDE libraries, executables and include
directories reqauired for building KDE 4 software are searched.
Additionally to FindKDE4Internal.cmake there are the files
KDE4Macros.cmake. This file implements all the KDE4-specific commands we
used above. If you do

    find_package(KDE4)

these macros are automatically available to you. There is also a file
KDE4Defaults.cmake which sets some minor cmake options which are good,
but not absolutely required for building KDE 4 software. If you want to
use these settings, include this file:

    include(KDE4Defaults)

These three files come with the KDE libraries and are installed to
share/apps/cmake/modules/. If you want to know more details, it is
always a good idea to have a look at these files directly.

At the top of FindKDE4Internal.cmake you can find the complete
documentation for all variables and macros these files provide.


----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:How_To_Build_KDE4_Software) in another wiki.
