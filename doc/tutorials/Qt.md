A typical CMakeLists.txt file for a Qt project looks like this:

``` cmake
cmake_minimum_required(VERSION 2.6)
PROJECT(TestProject)

FIND_PACKAGE(Qt4 REQUIRED)
INCLUDE(${QT_USE_FILE})

QT4_WRAP_UI(UISrcs MyWidget.ui)
QT4_WRAP_CPP(MOCSrcs MyWidget.h)

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

ADD_EXECUTABLE(MyProject main.cpp MyWidget.cpp ${MOCSrcs} ${UISrcs})
TARGET_LINK_LIBRARIES(MyProject ${QT_LIBRARIES})
```

However, when you build and run in Windows, you will get a console
window that appears along with your GUI program. To fix this, you must
do two things. First, add ${QT_QTMAIN_LIBRARY} to your
target_link_libraries command. Second, you must specify
/SUBSYSTEM:WINDOWS for all build configurations (Release, Debug, etc)
that you want to not pop up the console window. An example is below,
where when built in release mode, no console will appear:

``` cmake
cmake_minimum_required(VERSION 2.6)

PROJECT(FileMenu)

FIND_PACKAGE(Qt4 REQUIRED)
INCLUDE(${QT_USE_FILE})

QT4_WRAP_UI(UISrcs MyWidget.ui)
QT4_WRAP_CPP(MOCSrcs MyWidget.h)

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

ADD_EXECUTABLE(TestProject main.cpp MyWidget.cpp ${MOCSrcs} ${UISrcs})
TARGET_LINK_LIBRARIES(TestProject ${QT_QTMAIN_LIBRARY} ${QT_LIBRARIES})

IF(WIN32) # Check if we are on Windows
  if(MSVC) # Check if we are using the Visual Studio compiler
    set_target_properties(TestProject PROPERTIES LINK_FLAGS_RELEASE "/SUBSYSTEM:WINDOWS")
  elseif(CMAKE_COMPILER_IS_GNUCXX)
    # SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mwindows") # Not tested
  else()
    message(SEND_ERROR "You are using an unsupported Windows compiler! (Not MSVC or GCC)")
  endif()
elseif(UNIX)
  # Nothing special required
else()
  message(SEND_ERROR "You are on an unsupported platform! (Not Win32 or Unix)")
ENDIF()
```

If you want to set this for all build modes, instead of a line
like:

``` cmake
set_target_properties(TestProject PROPERTIES LINK_FLAGS_RELEASE "/SUBSYSTEM:WINDOWS")
```

use:

``` cmake
set_target_properties(FileMenu PROPERTIES LINK_FLAGS "/SUBSYSTEM:WINDOWS") # works for all build modes
```

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/Tutorials/Qt) in another wiki.
