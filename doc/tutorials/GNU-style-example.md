[back to tutorials list](Home#more-topics "wikilink")

GNU style projects typically have a main directory that contains
*include* and *src* subdirectories. The *src* directory in turn contains
directories for libraries and applications. This is a brief example of
such a project using CMake. The heirarchy is as follows:

  - CMakeLists.txt
  - include
      - CMakeLists.txt
      - yo.h (the header file for the library)
  - src
      - CMakeLists.txt
      - app
          - CMakeLists.txt
          - hello.c (the application)
      - libyo
          - CMakeLists.txt
          - yo.c (the trivial library)

The tarball for the project is available
[here](http://www.bzflag.bz/~butler/cmake_ex.tgz)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:GNU_style_example) in another wiki.
