``` cmake
cmake_minimum_required(VERSION 2.6)

# The idea of this demo is to show how to set a group of CMake variables together.
# A common use case is to allow easy enabling/disabling of a large set of targets, while
# maintaining manual individual target control. This is in contrast to a BUILD_ALL style variable,
# where targets are build if a corresponding variable to the target is set, OR the BUILD_ALL variable
# is set. The problem with this method is that if you want to set "all but one" of the targets to ON,
# it is not possible without manually setting all N-1 of the targets to OFF.

PROJECT(VariablePrefix)

# Add options to build specific targets
option(build_Green "Green doc string.")
option(build_Blue "Blue doc string.")

# Add options to allow enabling/disabling of a set of targets
option(TurnOnAll "Turn on all build_ variables.")
option(TurnOffAll "Turn off all build_ variables.")

# A function to get all user defined variables with a specified prefix
function (getListOfVarsStartingWith _prefix _varResult)
    get_cmake_property(_vars CACHE_VARIABLES)
    string (REGEX MATCHALL "(^|;)${_prefix}[A-Za-z0-9_]*" _matchedVars "${_vars}")
    set (${_varResult} ${_matchedVars} PARENT_SCOPE)
endfunction()

# Mass toggle "commands". These variables (TurnOnAll and TurnOffAll) let you
# enable or disable all of the variables starting with "build_". This is an abuse
# of a CMake "variable" to be used as an instruction to issue a command once.
# For example, when TurnOnAll is set to ON and then configure is run, the following
# block is run once, and then TurnOnAll sets itself back to OFF.
if(TurnOnAll)
    getListOfVarsStartingWith("build_" matchedVars)
    foreach (_var IN LISTS matchedVars)
        #message("Turn on ${item}")
        get_property(currentHelpString CACHE "${_var}" PROPERTY HELPSTRING)
        set(${_var} ON CACHE BOOL ${currentHelpString} FORCE)
    endforeach()

    get_property(turnOnHelpString CACHE "TurnOnAll" PROPERTY HELPSTRING)
    set(TurnOnAll OFF CACHE BOOL ${turnOnHelpString} FORCE)  # Set itself back to off, as this is a one time thing.
endif()

# In exactly the same manner, if TurnOffAll is set to ON, all of the variables starting with build_ are set to OFF.
if(TurnOffAll)
    getListOfVarsStartingWith("build_" matchedVars)
    foreach (_var IN LISTS matchedVars)
        #message("Turn off ${item}")
        get_property(currentHelpString CACHE "${_var}" PROPERTY HELPSTRING)
        set(${_var} OFF CACHE BOOL ${currentHelpString} FORCE)
    endforeach()

    get_property(turnOffHelpString CACHE "TurnOffAll" PROPERTY HELPSTRING)
    set(TurnOffAll OFF CACHE BOOL ${turnOffHelpString} FORCE)  # Set itself back to off, as this is a one time thing.
endif()

# Of course turning on all variables at the same time as turning off all variables does not make sense.
if(TurnOnAllTargets AND TurnOffAllTargets)
  message(FATAL_ERROR "You cannot turn targets on and off at the same time!")
endif()

# Conditional executables
if(build_Green)
   ADD_EXECUTABLE(Green GroupVariables.cpp)
endif()

if(build_Blue)
   ADD_EXECUTABLE(Blue GroupVariables.cpp)
   # other options here
endif()
```

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/Tutorials/SettingVariableGroups) in another wiki.
