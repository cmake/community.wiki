**Creating a Stand Alone OS X application Bundle using CMake 2.6**

[Get the Project](http://www.cmake.org/Wiki/images/e/e3/QTTest.zip)

The first requirement is to have CMake 2.6 CVS as it includes a new
Module called "BundleUtilities" the makes this process much easier than
it used to be.

The example uses Qt4 as its demonstration but any project that uses any
non-system libraries should actually work.

The basic cmake process that I have setup in the example project goes
something like this:

  - Setup a CMake based project as usual.
  - Add some extra files to your project to support the building of the
    bundle
  - Create a CMake function to configure those files
  - Call the function from your CMake code.

The first file to add is a shell script that gets configured by CMake
during cmake time. The name of our shell script input file is
*QtTest/Resources/OSX_Tools/CreateBundle.sh.in*. This script basically
copies the skeleton .app bundle that the build system produced into the
top level of the Installation directory (CMAKE_INSTALL_PREFIX) and
then creates the proper subdirectories (Libraries, Frameworks, Support)
in that copied app bundle.

The Second file to add is a CMake script file that also gets configured.
The name of this file is
*QtTest/Resources/OSX_Tools/CompleteBundle.cmake.in*. This file
contains a couple of items of note. The first is a cmake function over
ride from the Bundle Utilities that sets up where to copy our libraries
and frameworks. The default is to just copy them next to the application
executable which isn't in adherence to Apple's general guidelines. Next
in the file is an "Execute_process" command where the shell script that
was configured from above is now executed. Lastly we finally call the
"fixup_bundle" function that is in the "BundleUtilities" module. This
function will find all the non-system libraries that are required by
your application, copy them into the correct location within the
application bundle and then run the "install_name_tool" on all the
libraries and the executable.

After a successful build, run **make install** from the terminal (if you
used makefiles) or run the "Install" target from Xcode and your
application bundle will be copied and updated.

The CMake files in the example project have comments throughout to help
you with this process. Some of the shell script code could probably be
factored out and replaced with cmake code but for now this process
works.

This was tested on OS X 10.5.5 using Qt 4.4.2 built as Dynamic Libraries
and CMake CVS.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/BuildingOSXApplications) in another wiki.
