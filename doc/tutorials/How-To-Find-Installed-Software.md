See [How to find Libraries](doc/tutorials/How-To-Find-Libraries "wikilink")


----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:How_To_Find_Installed_Software) in another wiki.
