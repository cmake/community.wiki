``` cmake
cmake_minimum_required(VERSION 2.6)

PROJECT(Test)

if(UNIX)
<pre>
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=gnu++0x")
</pre>
endif()

# MSVC does not require any special flags
```

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/Tutorials/C++11Flags) in another wiki.
