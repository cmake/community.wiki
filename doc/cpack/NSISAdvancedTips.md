# CPack NSIS Advanced Tips

Those who are comfortable with both CMake/CPack and NSIS may wish to
customize their own NSIS scripts to pass through CPack.

## NSIS script file creation

When CPack uses the NSIS generator, it reads the following files from
the modules path:

    NSIS.template.in
    NSIS.InstallOptions.ini.in

CPack fills in the variables and outputs it to this
    directory:

    ${CMAKE_BINARY_DIR}_CPack_Packages\${CPACK_TOPLEVEL_TAG}\${CPACK_GENERATOR}\

Both files get their name changed to

    Project.nsi
    NSIS.InstallOptions.ini

To start work on a custom NSIS script, simply create a file (or copy)
into your module path (e.g. CPACK_MODULE_PATH) called
NSIS.template.in. CPack will automatically use your path over the
default one.

**NOTE:** It may be tempting to create custom Install options using
NSIS.InstallOptions.ini.in, but it is **NOT** recommended. CPack uses
this file to provide the option of modifying the PATH environment
variable. Modifying this file or the references to it within
NSIS.template.in may render this option useless. If custom
InstallOptions files are desired, read the next section.

## Integrating Custom InstallOptions files

At first glance, integrating a custom option file appears to be
difficult, since CPack copies 2 specific files to a separate generated
directory. However, integration is really quite simple.

With the NSIS.template.in, look for the Function .onInit Within this
function insert the following
    line:

    !insertmacro INSTALLOPTIONS_EXTRACT_AS "@MY_PATH_TO_FILE@\@MY_FILENAME@" "@MY_FILENAME@"

Next, look for a section that says ReserveFile. Again, just add the
line:

    ReserveFile "@MY_PATH_TO_FILE@\@MY_FILENAME@"

Don't forget to define MY_PATH_TO_FILE and MY_FILENAME in either
your CPackConfig file or your CMakeLists.txt

Now, @MY_FILENAME@ can be referenced directly anywhere in the script.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:CPackNSISAdvancedTips) in another wiki.
