I just wanted to jot down some notes that I took during my "learning
curve" of CPack on Windows XP.

First, what would seem like obvious things to do but worth mentioning.

# Pre-requisite Software

1.  Download and install the NSIS installer from [NSIS Downloads
    page](http://nsis.sourceforge.net/Download)
2.  Download and install a Zip package. I used 7zip from [7Zip Downloads
    page](http://www.7-zip.org/download.html)

<!-- end list -->

  -

      -
        <b>After you install 7zip (or what ever package you want for zip
        compression) be sure to add that program to the "PATH" variable
        for windows.
        Instructions for that can be found at Microsoft's web site [How
        To Manage Environment Variables in Windows
        XP](http://support.microsoft.com/kb/310519)</b>

Now you should be ready to start writing some CPack Code.

# Bumps in the Road

Early on I had some trouble getting some support libraries to show up in
the installer. This was because of a <b>bug</b> in my own cmake code
which I thought might be good to show here so that others do not make
the same mistake.

<B> WRONG CMAKE CODE </B>

    INSTALL(FILES MyLibrary.dll
       DESTINATION ${CMAKE_INSTALL_PREFIX}/bin
       CONFIGURATIONS Debug
       COMPONENT Runtime)

Note the `${CMAKE_INSTALL_PREFIX}` in the Destination property. Having
this was a bug and caused all sorts of problems. Basically CPack runs
the "install" command first and then does the packaging. By using an
Absolute path in the "Destination" property CMake was dutifully copying
the files into the installation directory and NOT the staging area for
CPack. OOPS. Here is the corrected code.

<B> CORRECT CMAKE CODE </B>

    INSTALL(FILES MyLibrary.dll
       DESTINATION bin
       CONFIGURATIONS Debug
       COMPONENT Runtime)

Also note that I am using a "CONFIGURATION" property because I actually
name outputs based on the build configuration. Both the "CONFIGURATION"
and the "COMPONENT" are optional. Using the COMPONENT Property will
allow you to eventually do component based installs. See
[CMake:Component Install With
CPack](doc/cpack/Component-Install-With-CPack "wikilink") for more
information.

There are ways to use Absolute file paths but there seems to be lots of
issues with this approach and the approach is not supported on NSIS
anyway. For more information on the subject take a look [At this
post](http://www.cmake.org/pipermail/cmake/2008-May/021638.html) on the
CMake mailing list.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:CPackWin32NewbiesChecklist) in another wiki.
