## Overview

CMake 2.6.2 includes a new module called 'BundleUtilities'. This module
is intended to make the task of creating a fully standalone OS X
application bundle much easier than before. It still requires some setup
on your part though. In CMake 2.8, the module was ported to Linux and
Windows. In this example we will walk through the necessary files and
CMake code that are needed to use the 'BundleUtilities.cmake'
effectively. Get the project
[QtTest-Package-Example.zip](/uploads/f5df17969b0047ec24c393d4637d017e/QtTest-Package-Example.zip).

## Basic Flow

The CMake code in the example does a few things.

  - It has install commands for targets built by the CMake code.
  - It has an install command for Qt plugins
  - It has an install command for creating a qt.conf file
  - It has an install command for executing CMake code at install time.
    This code uses BundleUtilities which gathers and includes
    prerequisites into the application bundle. On Mac OS X,
    BundleUtilities also modifies the install names.

## Additions to your CMakeLists.txt File

A simpler version of this example would have something like:

    set(APPS ...)  # paths to executables
    set(DIRS ...)   # directories to search for prerequisites
    INSTALL(CODE "
       include(BundleUtilities)
       fixup_bundle(\"${APPS}\"   \"\"   \"${DIRS}\")
       " COMPONENT Runtime)

But since we include Qt plugins, we use the second argument to
fixup_bundle().

After all this is added to your project, rerun cmake on your project,
build your project and finally try 'make install' or run the
'Installation' target from Xcode. You will see lots of text output
during the installation phase indicating what is currently being
performed. After all the libraries are copied to the install tree, a
verification phase will be performed to make sure the application bundle
is properly created. Assuming the verification has passed you should now
have a QtTest.app that you can place on another computer and
successfully run.

To create a package, one can execute cpack -G <generator name> on the
CMake generated CPackConfig.cmake file.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/BundleUtilitiesExample) in another wiki.
