[Back](Contrib "wikilink")

## DEPRECATED

The information on this page is now deprecated. See [CPack DEB
Generator](doc/cpack/PackageGenerators#deb-unix-only "wikilink")
instead.

## Description

The **UseDebian** CMake module simplifies the Debian package generation
using both CPack and CMake. It has been done because CPack does not have
an deb generator. This CMake macro can be used anywhere `ar` is
available (no debian specific tool are required).
<http://www.itk.org/Wiki/index.php?title=CMakeUserUseDebian&action=edit&section=1>

## Usage

The usage is simple:

  - put the macro file
    "[UseDebian.cmake](http://gdcm.svn.sourceforge.net/viewvc/gdcm/trunk/CMake/UseDebian.cmake?view=markup)"
    in your CMAKE_MODULE_PATH, as well as
    [FindMd5sum.cmake](http://gdcm.svn.sourceforge.net/viewvc/gdcm/trunk/CMake/FindMd5sum.cmake?view=markup)
    (until cmake -E can do md5sum
    [ref](http://public.kitware.com/pipermail/cmake/2007-April/013629.html)).
  - Then add something like this to your CMakeLists.txt

<!-- end list -->

```
 IF(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")
   SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Grass Root DiCoM")
   SET(CPACK_PACKAGE_VENDOR "gdcm")
   SET(CPACK_PACKAGE_VERSION_MAJOR "${GDCM_VERSION_MAJOR}")
   SET(CPACK_PACKAGE_VERSION_MINOR "${GDCM_VERSION_MINOR}")
   SET(CPACK_PACKAGE_VERSION_PATCH "${GDCM_VERSION_BUILD}")
   SET(CPACK_PACKAGE_INSTALL_DIRECTORY "gdcm ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}")
   SET(CPACK_SOURCE_PACKAGE_FILE_NAME "gdcm-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
   # List executables
   INCLUDE(CPack)
 ENDIF(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")

 INCLUDE(UseDebian)
 IF(DEBIAN_FOUND)
  ADD_DEBIAN_TARGETS(GDCM)
 ENDIF(DEBIAN_FOUND)
```

This CMake macro was inspired from UseRPMTools.

## Debian

To inspect your deb file:

```
 $ dpkg-deb -c gdcm_1.2.0-1_i386.deb

 $ dpkg-deb -f gdcm_1.2.0-1_i386.deb

 tar: Read 8192 bytes from -
 Package: gdcm
 Version: 1.2.0
 Section: devel
 Priority: optional
 Architecture: i386
 Depends: libc6 (>= 2.3.1-6), libgcc1 (>= 1:3.4.2-12)
 Maintainer:
 Description: Grass Root DiCoM
  .
  gdcm was packaged by UseDebian and CMake.
  .
```

## Errors

If you are getting
    a:

    "CMake Error: Md5sum_DIR is not set.  It must be set to the directory containing Md5sumConfig.cmake in order to use Md5sum."

This is mostly beause you did:

    include(../CMake/UseDebian.cmake)

instead of setting the `CMAKE_MODULE_PATH`. Indeed the UseDebian.cmake
rely on another cmake script: FindMd5sum.cmake which need to be found by
cmake \!

## Derived work

Mehdi Rabah (at INRIA) reworked the cmake debian module:

  - <http://mehdi.rabah.free.fr/DpkgDeb.cmake>
  - <http://mehdi.rabah.free.fr/Rpmbuild.cmake>
  - example : <http://mehdi.rabah.free.fr/CMakeLists.txt>

## Reference

  - <http://public.kitware.com/pipermail/cmake/2007-April/013650.html>
  - <http://gdcm.svn.sourceforge.net/viewvc/gdcm/CMake/>
  - <http://cvs.savannah.gnu.org/viewvc/tsp/tsp/make/>

[Back](Contrib "wikilink")

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeUserUseDebian) in another wiki.
