Note that this VTK example is simply a filler so we can do VTK packaging
in the CMake (below).

## DistanceBetweenPoints.cxx

``` cpp
#include <vtkMath.h>

int main(int, char *[])
{
  // Create two points.
  double p0[3] = {0.0, 0.0, 0.0};
  double p1[3] = {1.0, 1.0, 1.0};

  // Find the squared distance between the points.
  double squaredDistance = vtkMath::Distance2BetweenPoints(p0, p1);

  // Take the square root to get the Euclidean distance between the points.
  double distance = sqrt(squaredDistance);

  // Output the results.
  std::cout << "SquaredDistance = " << squaredDistance << std::endl;
  std::cout << "Distance = " << distance << std::endl;

  return EXIT_SUCCESS;
}
```

## CMakeLists.txt

``` cmake
cmake_minimum_required(VERSION 2.6)

project(DistanceBetweenPoints)

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

add_executable(DistanceBetweenPoints DistanceBetweenPoints.cxx)
install(TARGETS DistanceBetweenPoints DESTINATION distance)

set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "David Doria") # required

include(CPack)

# To use this:
# make package
# sudo dpkg -i DistanceBetweenPoints-0.1.1-Linux.deb

# This will result in the file:
# /usr/distance/DistanceBetweenPoints
```

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/CPack/Examples/Linux/DEB) in another wiki.
