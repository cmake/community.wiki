## VTKIncludeTest.cpp

``` cpp
#include <vtkMath.h>

int main(int, char *[])
{
  double p0[3] = {0.0, 0.0, 0.0};
  double p1[3] = {1.0, 1.0, 1.0};

  double squaredDistance = vtkMath::Distance2BetweenPoints(p0, p1);

  std::cout << "SquaredDistance = " << squaredDistance << std::endl;

  return EXIT_SUCCESS;
}
```

## CMakeLists.txt

``` cmake
PROJECT (VTKIncludeTest)
cmake_minimum_required(VERSION 2.8)

FIND_PACKAGE(VTK REQUIRED)
INCLUDE(${VTK_USE_FILE})

ADD_EXECUTABLE(VTKIncludeTest VTKIncludeTest.cpp)
TARGET_LINK_LIBRARIES(VTKIncludeTest vtkHybrid)

# The installation commands start here
SET(APPS "\${CMAKE_INSTALL_PREFIX}/bin/VTKIncludeTest")

# Install the application
INSTALL(TARGETS VTKIncludeTest
    BUNDLE DESTINATION . COMPONENT Runtime
    RUNTIME DESTINATION bin COMPONENT Runtime
    )

set(YOUR_PLUGINS "") #empty for now
set(YOUR_LIBRARY_DIRS ${VTK_BIN})
install(CODE "
  include(BundleUtilities)
  fixup_bundle(\"${APPS}\" \"\${YOUR_PLUGINS}\" \"${YOUR_LIBRARY_DIRS}\")
  " COMPONENT Runtime)

SET(CPACK_GENERATOR "DEB")
SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "David Doria") #required

include(CPack)
```

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/CPack/BundleUtilities/Examples/Linux/Automatic) in another wiki.
