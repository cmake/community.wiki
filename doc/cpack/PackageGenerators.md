# CPack Package Generators

CPack may generate several kinds of package on behalf of various *CPack
Generators*. All generators share common properties explained below.
Some if not all generators do have specific features or restrictions
which are explained in specific sections.

Since CPack 2.8.8, CPack has builtin documentation just like CMake so
that one can use:

    cpack --help-variable-list
    cpack --help-variable <VARNAME>
    cpack --help-command-list
    cpack --help-command <CMDNAME>
    ...
    cpack --help

in order to get specific documentation. This [builtin
documentation](http://www.cmake.org/Wiki/CMake_builtin_documentation_handling)
should be the most up to date documentation for CPack. The current Wiki
is a convenient place to find informations but may not be as up to date
as the builtin doc.

If ever you find undocumented variable and/or innaccurate documentation
please file a [bug report](http://public.kitware.com/Bug/main_page.php).
The main part of CPack variable or command documentation may be found in
CPack<GEN>.cmake file in the CMake source, the documentation is
extracted from those files automatically and dynamically by CPack, so
you can submit a documentation patch using one of those files.

## Overall usage (common to all generators)

Information about CPACK_xxx variables used for CPack configuration may
be found there:
[CPackConfiguration](http://www.cmake.org/Wiki/CMake:CPackConfiguration)

The CPACK_GENERATOR variable has different meanings in different
contexts. In your CMakeLists.txt file, CPACK_GENERATOR is a **list of
generators**: when run with no other arguments, CPack will iterate over
that list and produce one package for each generator. In a
CPACK_PROJECT_CONFIG_FILE, though, CPACK_GENERATOR is a **string
naming a single generator**. If you need per-cpack-generator logic to
control **other** cpack settings, then you need a
CPACK_PROJECT_CONFIG_FILE. For example it may be used to switch
on/off [component
packaging](https://gitlab.kitware.com/cmake/community/-/wikis/doc/cpack/Component-Install-With-CPack)
for a specific generator.

The CMake source tree itself contains a CPACK_PROJECT_CONFIG_FILE.
See the top level file
[CMakeCPackOptions.cmake.in](https://gitlab.kitware.com/cmake/cmake/-/blob/master/CMakeCPackOptions.cmake.in)
for an example.

If set, the CPACK_PROJECT_CONFIG_FILE is included automatically on a
per-generator basis. It only needs to contain overrides.

Here's how it works:

1.  cpack runs
2.  it includes CPackConfig.cmake (usually found in the build tree)
3.  it iterates over the generators listed in that file's
    CPACK_GENERATOR list variable (unless told to use just a specific
    one via -G on the command line...), then for each generator it:
    1.  sets CPACK_GENERATOR to the one currently being iterated over.
        This is the key: For each generator listed in CPACK_GENERATOR
        in CPackConfig.cmake, cpack will **reset** CPACK_GENERATOR
        internally to **the one currently being used** and then include
        the CPACK_PROJECT_CONFIG_FILE.
    2.  includes the CPACK_PROJECT_CONFIG_FILE
    3.  produces the package for that generator

## Archive Generators

The archive generators are a family of CPack generators derived from the
same base class generator. These generators use the
[libarchive](http://code.google.com/p/libarchive/) library to produce
various archive file formats. These generators share common properties
which are explained in this section. Specific features are explained in
sections corresponding to the specific generator.

Please note that one might want to try to avoid packaging symlinks
within .tgz, .tbz2 or similar archives, the reason being that libarchive
converts them to hard links, and, to add insult to injury, it then even
*clips* (read: **corrupts**) even moderately long paths, causing tar
**extraction failure** (observed in CMake 2.6.4 Linux, TODO list
experience with newer versions).

### TGZ

Tar GZip compressed packages.

### STGZ

Self extracting Tar GZip compressed packages (needs /bin/sh, tar, gunzip
and tail for extracting).

### TBZ2

Tar BZip2 compressed packages. Requires bzip2 for creating the package.

### TZ

Tar UNIX compress compressed packages.

### ZIP

ZIP compressed packages. CMake 2.4.x required zip, WinZip or 7Zip for
creating the package. CMake 2.8.x built-in libarchive support does not
need external programs in order to build ZIP.

## NSIS

Nullsoft Installer. Requires [NSIS](http://nsis.sourceforge.net/) for
creating the package. See the [Advanced
Tips](doc/cpack/NSISAdvancedTips "wikilink") for details on customizing
an NSIS script through
CPack.

### NSIS Generator Specifics settings

| Variable Name                           | Description                                                                                                                                                                                                                                                                                                                                                                 | Example                                                                                                                                                         |
| --------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| CPACK_NSIS_MUI_ICON                  | The icon file (.ico) for the generated install program. Both this and CPACK_NSIS_MUI_UNIICON need to set for this to have any effect.                                                                                                                                                                                                                                    | installer.ico                                                                                                                                                   |
| CPACK_NSIS_MUI_UNIICON               | The icon file (.ico) for the generated uninstall program. Both this and CPACK_NSIS_MUI_ICON need to set for this to have any effect.                                                                                                                                                                                                                                     | uninstaller.ico                                                                                                                                                 |
| CPACK_PACKAGE_ICON                    | A branding image that will be displayed on the top bar inside the installer.                                                                                                                                                                                                                                                                                                | installer.bmp                                                                                                                                                   |
| CPACK_NSIS_EXTRA_INSTALL_COMMANDS   | Extra NSIS commands that will be added to the install Section.                                                                                                                                                                                                                                                                                                              | ExecWait '\\\\\\"$INSTDIR\\\\\\\\vcredist_x86.exe\\\\\\" /q:a'                                                                                                 |
| CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS | Extra NSIS commands that will be added to the uninstall Section.                                                                                                                                                                                                                                                                                                            |                                                                                                                                                                 |
| CPACK_NSIS_COMPRESSOR                 | The arguments that will be passed to the NSIS SetCompressor command.                                                                                                                                                                                                                                                                                                        | /SOLID lzma                                                                                                                                                     |
| CPACK_NSIS_MODIFY_PATH               | If this is set to "ON", then an extra page will appear in the installer that will allow the user to choose whether the program directory should be added to the system PATH variable. It will also display a checkbox allowing to install desktop shorcut icons configured with CPACK_CREATE_DESKTOP_LINKS.                                                              | ON                                                                                                                                                              |
| CPACK_NSIS_DISPLAY_NAME              | Undocumented.                                                                                                                                                                                                                                                                                                                                                               | "${CPACK_PACKAGE_INSTALL_DIRECTORY} My Famous Project"                                                                                                       |
| CPACK_NSIS_INSTALLED_ICON_NAME      | Set the icon used for the Windows "Add or Remove Programs" tool.                                                                                                                                                                                                                                                                                                            | "bin\\\\\\\\MyExecutable.exe"                                                                                                                                   |
| CPACK_NSIS_HELP_LINK                 | Adds link to registry. URI.                                                                                                                                                                                                                                                                                                                                                 | "<http:\\\\\\\\www.my-project-home-page.org>"                                                                                                                   |
| CPACK_NSIS_URL_INFO_ABOUT           | Adds link to registry and the vendor in add/remove programs' "Click here for support information" in program entry links here.                                                                                                                                                                                                                                              | "<http:\\\\\\\\www.my-personal-home-page.com>"                                                                                                                  |
| CPACK_NSIS_CONTACT                    | Adds link to add/remove programs' "Click here for support information" in program entry.                                                                                                                                                                                                                                                                                    | "me@my-personal-home-page.com"                                                                                                                                  |
| CPACK_NSIS_CREATE_ICONS_EXTRA       | Additional NSIS commands for creating start menu shortcuts.                                                                                                                                                                                                                                                                                                                 | set(CPACK_NSIS_CREATE_ICONS "CreateShortCut '\\$SMPROGRAMS\\\\\\\\$STARTMENU_FOLDER\\\\\\\\${PROJECT_NAME}.lnk' '\\$INSTDIR\\\\\\\\${PROJECT_NAME}.exe'") |
| CPACK_NSIS_DELETE_ICONS_EXTRA       | Undocumented. Possibly: Additional NSIS commands to uninstall start menu shortcuts.                                                                                                                                                                                                                                                                                         |                                                                                                                                                                 |
| CPACK_NSIS_MENU_LINKS                | Used to override the Start Menu links.                                                                                                                                                                                                                                                                                                                                      | "doc/cmake-@CMake_VERSION_MAJOR@.@CMake_VERSION_MINOR@/CMakeSetup.html" "CMakeSetup Help"                                                                   |
| CPACK_NSIS_EXECUTABLES_DIRECTORY     | Override the default path ("bin/") where NSIS will find executables, used by CPACK_NSIS_MUI_FINISHPAGE_RUN                                                                                                                                                                                                                                                              | set(CPACK_NSIS_EXECUTABLES_DIRECTORY ".")                                                                                                                    |
| CPACK_NSIS_MUI_FINISHPAGE_RUN       | If used, will make it possible for user to choose (on an additional page, displayed at the end of the installation) to run intalled program. Should point to program name to run, seemingly without any sub-directories of the installation directory in case program installed in such sub-directories (but please check generated NSIS script if you can't make it work). | "MyExecutable.exe"                                                                                                                                              |
|  |

## DragNDrop (OSX only)

Mac OSX Drag and Drop generator. This generator creates a .DMG
disk image file that is populated by install() commands, as if it were
the install prefix. 

To use this generator, create executable(s) with the **MACOSX_BUNDLE** option, and install them to the root of the package.  They will then show up when the DMG is opened, and the user can drag and drop them into their Applications folder.

You will probably want to add extra files to each application bundle, like an `Info.plist` and an icon.  To add your plist, specify it by setting the target property [MACOSX_BUNDLE_PLIST](https://cmake.org/cmake/help/latest/prop_tgt/MACOSX_BUNDLE.html) on your bundle target.  To add other resource files, add them as source files to the bundle, and set the source property [MACOSX_PACKAGE_LOCATION](https://cmake.org/cmake/help/latest/prop_sf/MACOSX_PACKAGE_LOCATION.html) on them to choose where inside the bundle they'll go. 

It is impossible to place multiple executables inside one single bundle
using this generator; it enforces a 1:1 bundle-to-executable relationship.  If you need this functionality, you should use the Bundle generator.  If your executable depends on additional shared libraries, you can include them by using **fixup_bundle()** from BundleUtilities.

## PackageMaker (OSX only)

Mac OSX Package Maker packages. Requires Package Maker for creating the
package.

Some of the variables that you can use to customize the Package Maker
generated
package:

| Variable Name                | Description                                                                                                                                                                                                                                                          | Default value                                                                                                          |
| ---------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------- |
| CPACK_PREFLIGHT_SCRIPT     | This script is launched just after the user clicked on the "Install" button.                                                                                                                                                                                         | \-                                                                                                                     |
| CPACK_POSTFLIGHT_SCRIPT    | This script is launched after the postinstall / postupgrade script or when the package has been                                                                                                                                                                      | \-                                                                                                                     |
| CPACK_POSTUPGRADE_SCRIPT   | This script is launched after the files in the package have been installed. (Isn't an option for a postinstall script missing? According to Apple documentation, postinstall is executed the first time a package is installed and postupgrade all subsequent times) | \-                                                                                                                     |
| CPACK_OSX_PACKAGE_VERSION | Set to the minimum OS X version you support. Choices are currently 10.3, 10.4 and 10.5                                                                                                                                                                               | 10.3, unless you specified features that require a higher OS X version (CPACK_DOWNLOAD_SITE, CPACK_COMPONENTS_ALL) |

This is a useful site that describes where the scripts get executed and
what the arguments to the scripts are. See section "What about
scripts?". <http://s.sudre.free.fr/Stuff/PackageMaker_Howto.html> . In
summary the arguments correspond to $0 Script path, $1 Package path, $2
Target location, and $3 Target Volume.

_NOTE: in recent OS X versions, PackageMaker has been deprecated and was recently removed.  As a result, this generator will not work with recent OS X versions.  The [ProductBuild](https://cmake.org/cmake/help/v3.7/module/CPackProductBuild.html) generator replaces it, and should offer similar functionality._

## OSXX11 (OSX only)

Mac OSX X11 Bundle. Requires hdiutil for creating the package.

## Bundle (OSX only)

### Overview

The Bundle generator (introduced in CMake 2.6) creates a compressed disk
image containing an OSX bundle, whose contents are populated with CMake
INSTALL() commands. This makes it possible to create bundles of
arbitrary complexity while minimizing differences with installation on
other platforms. For example, a bundle created with the bundle generator
can contain multiple executable files, libraries generated by the build,
third-party dependencies, etc.

***Important Note:*** Do **not** use the MACOSX_BUNDLE property on
executables that will be packaged using the bundle-generator\!
Specifying MACOSX_BUNDLE creates a separate bundle for each individual
executable at build-time; the structure of these bundles becomes
redundant when the bundle generator consolidates multiple executables
into a single bundle.

### Bundle Layout

    ${CPACK_BUNDLE_NAME}/
      Contents/
        Info.plist (copied from ${CPACK_BUNDLE_PLIST})
        MacOS/
          ${CPACK_BUNDLE_NAME} (copied from ${CPACK_BUNDLE_STARTUP_COMMAND})
        Resources/
          (file contents defined by CMake INSTALL() commands)

  - CPACK_BUNDLE_PLIST is the name of a file that becomes the
    Info.plist for the bundle. This could be a hard-coded file included
    with the program sources, a file generated with CONFIGURE_FILE,
    etc. Rationale: Info.plist can become arbitrarily complex,
    applications need to be able to specify its contents directly.

<!-- end list -->

  - The bundle's Resources/ directory is populated with the files
    installed with CMake INSTALL() commands. Rationale: integrate well
    with CMake and other package generators (such as NSIS). Makes it
    easy to incorporate external dependencies (Qt, GTK) into the bundle.

<!-- end list -->

  - CPACK_BUNDLE_STARTUP_COMMAND is the name of a file that will be
    executed when the user opens the bundle. It could be a binary or a
    script. This file should be present in the source or build tree
    after a build; CPackBundle will copy it into the correct place in
    the bundle. Rationale: for most non-trivial applications, simply
    running a binary is not enough. The following sample script
    demonstrates several common startup operations:
      - Starts X11 (required by GTK).
      - Updates DYLD_LIBRARY_PATH so that the application can locate
        libraries that are included in the bundle. This eliminates the
        need to run install_name_tool on libraries in the bundle,
        which is messy and error-prone. Useful for either Qt or GTK.
      - Updates PATH so the "main" application can easily run "child"
        binaries included in the bundle.
      - Sets-up some temporary files and environment variables required
        by (in this case) GTK.
      - Passes information to the application via the command line (in
        this case, paths to several application resources located in the
        bundle).

<!-- end list -->

    #!/bin/sh
    #
    # Author: Aaron Voisine <aaron@voisine.org>
    # Inkscape Modifications: Michael Wybrow <mjwybrow@users.sourceforge.net>
    # K-3D Modifications: Timothy M. Shead <tshead@k-3d.com>

    K3D_BUNDLE="`echo "$0" | sed -e 's/\/Contents\/MacOS\/K-3D//'`"
    K3D_RESOURCES="$K3D_BUNDLE/Contents/Resources"
    K3D_TEMP="/tmp/k3d/$UID"
    K3D_ETC="$K3D_TEMP/etc"
    K3D_PANGO_RC_FILE="$K3D_ETC/pango/pangorc"

    echo "running $0"
    echo "K3D_BUNDLE: $K3D_BUNDLE"

    # Start X11 ...
    ps -wx -ocommand | grep -e '[X]11.app' > /dev/null
    if [ "$?" != "0" -a ! -f ~/.xinitrc ]; then
        echo "rm -f ~/.xinitrc" > ~/.xinitrc
        sed 's/xterm/# xterm/' /usr/X11R6/lib/X11/xinit/xinitrc >> ~/.xinitrc
    fi

    mkdir -p $K3D_TEMP
    cat << __END_OF_GETDISPLAY_SCRIPT__ > "$K3D_TEMP/getdisplay.sh"
    #!/bin/sh
    mkdir -p "$K3D_TEMP"

    if [ "\$DISPLAY"x == "x" ]; then
        echo :0 > "$K3D_TEMP/display"
    else
        echo \$DISPLAY > "$K3D_TEMP/display"
    fi
    __END_OF_GETDISPLAY_SCRIPT__
    chmod +x "$K3D_TEMP/getdisplay.sh"
    rm -f $K3D_TEMP/display
    open-x11 $K3D_TEMP/getdisplay.sh || \
    open -a XDarwin $K3D_TEMP/getdisplay.sh || \
    echo ":0" > $K3D_TEMP/display

    while [ "$?" == "0" -a ! -f $K3D_TEMP/display ];
    do
      #echo "Waiting for display $K3D_TEMP/display"
      sleep 1;
    done
    export "DISPLAY=`cat $K3D_TEMP/display`"

    ps -wx -ocommand | grep -e '[X]11' > /dev/null || exit 11

    # Setup temporary runtime files
    rm -rf "$K3D_TEMP"

    # Because the bundle could be located anywhere at runtime, we have to
    # create temporary copies of the Pango configuration files that
    # reflect our current location
    mkdir -p "$K3D_ETC/pango"
    sed -e 's|/opt/local/etc|'"$K3D_ETC|g" "$K3D_RESOURCES/etc/pango/pangorc" > "$K3D_ETC/pango/pangorc"
    sed -e 's|/opt/local|\"'"$K3D_RESOURCES|g" -e "s/\.so/.so\"/g" "$K3D_RESOURCES/etc/pango/pango.modules" > "$K3D_ETC/pango/pango.modules"
    cp -f "$K3D_RESOURCES/etc/pango/pangox.aliases" "$K3D_ETC/pango/pangox.aliases"

    export "DYLD_LIBRARY_PATH=$K3D_RESOURCES/lib"
    export "FONTCONFIG_PATH=$K3D_RESOURCES/etc/fonts"
    export "PANGO_RC_FILE=$K3D_PANGO_RC_FILE"
    export "PATH=$K3D_RESOURCES/bin:$PATH"

    #export
    exec "$K3D_RESOURCES/bin/k3d" "--log-level=debug" "--plugins=$K3D_RESOURCES/lib/k3d/plugins" "--share=$K3D_RESOURCES/share/k3d" "--ui=$K3D_RESOURCES/lib/k3d/uiplugins/k3d-ngui.module"

  - The bundle is then stored in a compressed disk image. Rationale:
    de-facto standard mechanism for distributing bundles.

### Required CMake Variables

The prototype bundle generator uses the following variables:

  - CPACK_PACKAGE_FILE_NAME - provides the name of the final
    compressed disk image (the name of the file that is distributed).
  - CPACK_PACKAGE_ICON - provides the icon for the mounted disk image
    (appears after the user mounts the disk image).  NOTE: Icon must be [an approved OS X application icon resolution](http://iconhandbook.co.uk/reference/chart/osx/).  Attempting to specify an icon that is not one of these sizes will result in the package having no icon.  
  - CPACK_BUNDLE_NAME - provides the bundle name (displayed in the
    finder underneath the bundle icon).
  - CPACK_BUNDLE_ICON - provides the bundle icon (displayed in the
    /Applications folder, on the dock, etc).
  - CPACK_BUNDLE_PLIST - path to a file that will become the bundle
    plist.  An example of generating a plist automatically from CMake variables can be found [here](https://github.com/Amber-MD/cmake-buildscripts/blob/9571b0236a86f11c52463853614ae7809dc4d0ac/Packaging.cmake#L100) and [here](https://github.com/Amber-MD/cmake-buildscripts/blob/master/packaging/Info.in.plist).
  - CPACK_BUNDLE_STARTUP_COMMAND - path to a file (in your current build directory) that should be
    executed when the user opens the bundle. Could be a shell-script or
    a binary.

_NOTE: Since the the bundle file is not created until after all install scripts are run, it is impossible to use BundleUtilities' `fixup_bundle()` function automatically with the Bundle generator.  Instead, I have had better luck with an [alternate script](https://gist.github.com/multiplemonomials/35b4722b1caae14be39c5f1e291a8aee) I wrote to work with CPackBundle.  It functions very similarly, but does all of its work during the install phase, so the dependencies it finds get packaged into the bundle without any extra work._

### TODO

  - Detect attempts to use the bundle generator with MACOSX_BUNDLE.
  - Generate a default Info.plist file if CPACK_BUNDLE_PLIST is not
    defined.
  - Support fixing-up binaries with install_name_tool, eliminating the
    need to run a script that sets DYLD_LIBRARY_PATH.
  - Add arbitrary files (such as background images, READMEs, etc) to the
    disk image - this use-case is distinct from adding files to the
    bundle with INSTALL().
  - Provide an option or alternative to CPACK_BUNDLE_STARTUP_COMMAND
    that simply executes one of the files installed in the bundle.
    Presumably, this should be a symlink to a binary or script.

## CygwinBinary (Cygwin only)

Tar Bzip2 compressed Cygwin package. Requires bzip2 for creating the
package.

## CygwinSource (Cygwin only)

Tar Bzip2 compressed Cygwin source package. Requires bzip2 for creating
the package.

## DEB (UNIX only)

Debian packages (2.0 version only, see the debian-binary file). In CMake
cvs since July 2007, will be in 2.6.0. With CPack 2.4.x you can use the
approach described in
[CMakeUserUseDebian](doc/cpack/UseDebian "wikilink") (Requires only ar
for creating the package). Warning: due to an incompatibility between
GNU-ar and BSD-ar this is not a long-term recommended solution. Instead
you should switch to the solution implemented in 2.6.x where a BSD-ar
implementation was integrated in CPack.

Reference: \[libapt-inst\] Should support both BSD and SysV ar formats

  - <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=161593>

Note: Only binary package are supported. source package do not really
make sense since build process is cmake driven.

Here are the variables needed for a binary package:

### control file (aka DEBIAN/control) for binary package

Specific variables are needed to generate the control file for debian
package. This file is created automatically using the following
variables, some of which are mandatory and some are optional. See also:
[1](http://www.debian.org/doc/debian-policy/ch-controlfields.html#s-binarycontrolfiles)

**package name**

  - debian policy enforce lower case for package name
  - Package: (mandatory)
  - if CPACK_DEBIAN_PACKAGE_NAME is not set CPACK_PACKAGE_NAME
    (lower case will be used)

**version**

  - Version: (mandatory)
  - if CPACK_DEBIAN_PACKAGE_VERSION is not set
    CPACK_PACKAGE_VERSION

**arch**

  - Architecture: (mandatory)
  - if not set CPACK_DEBIAN_PACKAGE_ARCHITECTURE will be set to i386

Notes:

  - should be set via: dpkg --print-architecture
  - There is no such thing as i686 architecture on debian, you should
    use i386 instead

**depends**

  - Depends:
  - You should set:
    CPACK_DEBIAN_PACKAGE_DEPENDS
  - eg.:

<!-- end list -->

    SET(CPACK_DEBIAN_PACKAGE_DEPENDS "libc6 (>= 2.3.1-6), libgcc1 (>= 1:3.4.2-12)")

Notes:

  - have a look at GET_PROPERTY(result GLOBAL ENABLED_FEATURES), this
    returns the successful FIND_PACKAGE() calls, maybe this can help
  - TODO: automate 'objdump -p | grep NEEDED'

**maintaner**

  - Maintainer: (mandatory)
      - valid email is required
  - if CPACK_DEBIAN_PACKAGE_MAINTAINER is not set,
    CPACK_PACKAGE_CONTACT will be used instead

**description**

  - Description: (mandatory)
  - if CPACK_DEBIAN_PACKAGE_DESCRIPTION is not set
    CPACK_PACKAGE_DESCRIPTION_SUMMARY will be used instead.

**section**

  - Section: (recommended)
  - if not set CPACK_DEBIAN_PACKAGE_SECTION will default to 'devel'

**priority**

  - Priority: (recommended)
  - if not set CPACK_DEBIAN_PACKAGE_PRIORITY will be set to
    "optional"

**recommends**

  - Recommends:
  - You should set: CPACK_DEBIAN_PACKAGE_RECOMMENDS

**Suggests**

  - Suggests:
  - You should set: CPACK_DEBIAN_PACKAGE_SUGGESTS

**Control Extra**

  - Additional control files (optional)
  - In order to perform pre-, or post-install configuration, certain
    files can be provided in the DEBIAN/ folder in the debian package
    (postinst, preinst, postrm, prerm)
  - You should set:
    CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA
  - E.g.

<!-- end list -->

    set( CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_CURRENT_SOURCE_DIR}/CMake/debian/postinst;${CMAKE_CURRENT_SOURCE_DIR}/CMake/debian/prerm;" )

### Source (for reference only)

Here are the variables needed for a source package (not implemented):

  - For debian source packages:
  - see also
    [debian/control](http://www.debian.org/doc/debian-policy/ch-controlfields.html#s-sourcecontrolfiles)

<!-- end list -->

  - .dsc
  - see also
    [2](http://www.debian.org/doc/debian-policy/ch-controlfields.html#s-debiansourcecontrolfiles)

Most of them are identical with the binary package, with
exception:

**builds-depends**

  - DEBIAN_PACKAGE_BUILDS_DEPENDS
  - eg.:

<!-- end list -->

    "debhelper (>> 5.0.0), libncurses5-dev, tcl8.4"

### Debian Generator specific settings

| Variable Name                          | Description                                                                                            | Example                                  |
| -------------------------------------- | ------------------------------------------------------------------------------------------------------ | ---------------------------------------- |
| CPACK_DEBIAN_PACKAGE_MAINTAINER     | The maintenainer informations                                                                          | Firstname Lastname \<email@example.com\> |
| CPACK_PACKAGE_DESCRIPTION_SUMMARY   | Package short description                                                                              | Here is my short description             |
| CPACK_PACKAGE_DESCRIPTION            | Package description                                                                                    | Here is my long description              |
| CPACK_DEBIAN_PACKAGE_DEPENDS        | Package dependencies (use dpkg -s <packagename> to retrieve version)                                   | libc6 (\>= 2.7-18)                       |
| CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA | This variable allow advanced user to add custom script to the control.tar.gz (inside the .deb archive) | ${CMAKE_CURRENT_SOURCE_DIR}/postinst  |
| CPACK_DEBIAN_PACKAGE_SECTION        | Package section (see <http://packages.debian.org/stable/>)                                             | Network                                  |
| CPACK_DEBIAN_PACKAGE_VERSION        | Package version                                                                                        | ${CPACK_PACKAGE_VERSION}+lenny1        |
|  |

### External references

  - <http://wiki.debian.org/HowToPackageForDebian>

## RPM (Unix Only)

Binary RPM packages are supported by CMake (more precisely by CPack)
since CMake 2.6.0. If you use CMake 2.4.x (or you want to build source
RPM) you may use the
[CMakeUserUseRPMTools](doc/cpack/UseRPMTools "wikilink") module.

The CPack RPM generator supports [CPack Component
installation](doc/cpack/Component-Install-With-CPack "wikilink"). You can
find details about the implementation
[here](http://public.kitware.com/Bug/view.php?id=7645).

### CPack RPM usage

The CPack RPM generator is similar to other CPack generators. Its
execution is controlled using:

  - generic CPACK_xxxx variables see [CPack
    variables](doc/cpack/Configuration "wikilink")
  - specific CPACK_RPM_xxxx variables see generator specific wiki
    pages

Since CMake/CPack 2.8.0, a detailed description of how to use the
CPACK_RPM_xxxx is available in this article or from the CMake command
line:

    cmake --help-module CPackRPM

If you use [CPack with
CMake](doc/cpack/Packaging-With-CPack#using-cpack-with-cmake "wikilink")
with the Makefile generator you usually launch:

    cd build_dir
    make package

However if you want more command line control over the CPack run you may
launch:

    cd build_dir
    cpack -D CPACK_RPM_PACKAGE_DEBUG=1 -D CPACK_RPM_SPEC_INSTALL_POST="/bin/true" -G RPM

this will launch CPack with additionnal CPACK_RPM_xxxx variables
definitions which may be used by CPack RPM generator.

### CPack RPM generators specific settings

CPack RPM specific variables are used to generate an RPM spec file which
will be processed by the *rpmbuild* tool. A specific variable may be

  - *optional*, the variable may or may not be set and its value is not
    needed for building a valid spec file.
  - **mandatory**, the variable must be set because we need a value for
    building a valid spec file.
      - **mandatory with default value**, the variable must be set but a
        default value is provided.
      - **mandatory**, the variable must be set and no default value is
        provided.

Here is the list of CPack RPM specific variables (*some variable are not
yet supported because there are some patches pending*):

<table>
<thead>
<tr class="header">
<th><p>Variable Name</p></th>
<th><p>Description</p></th>
<th><p>Default value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>CPACK_RPM_PACKAGE_SUMMARY</strong></p></td>
<td><p>The RPM package summary</p></td>
<td><p>CPACK_PACKAGE_DESCRIPTION_SUMMARY</p></td>
</tr>
<tr class="even">
<td><p><strong>CPACK_RPM_PACKAGE_NAME</strong></p></td>
<td><p>The RPM package name</p></td>
<td><p>CPACK_PACKAGE_NAME</p></td>
</tr>
<tr class="odd">
<td><p><strong>CPACK_RPM_PACKAGE_VERSION</strong></p></td>
<td><p>The RPM package version</p></td>
<td><p>CPACK_PACKAGE_VERSION</p></td>
</tr>
<tr class="even">
<td><p><em>CPACK_RPM_PACKAGE_ARCHITECTURE</em></p></td>
<td><p>The RPM package architecture. This may be set to &quot;noarch&quot; if you know you are building a noarch package.</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><strong>CPACK_RPM_PACKAGE_RELEASE</strong></p></td>
<td><p>The RPM package release. This is the numbering of the RPM package itself, i.e. the version of the packaging and not the version of the content (see CPACK_RPM_PACKAGE_VERSION). One may change the default value if the previous packaging was buggy and/or you want to put here a fancy Linux distro specific numbering.</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p><strong>CPACK_RPM_PACKAGE_LICENSE</strong></p></td>
<td><p>The RPM package license policy.</p></td>
<td><p>&quot;unknown&quot;</p></td>
</tr>
<tr class="odd">
<td><p><strong>CPACK_RPM_PACKAGE_GROUP</strong></p></td>
<td><p>The RPM package group (see /usr/share/doc/rpm-*/GROUPS )</p></td>
<td><p>&quot;unknown&quot;</p></td>
</tr>
<tr class="even">
<td><p><strong>CPACK_RPM_PACKAGE_VENDOR</strong></p></td>
<td><p>The RPM package vendor</p></td>
<td><p>CPACK_PACKAGE_VENDOR if set or &quot;unknown&quot; if not set</p></td>
</tr>
<tr class="odd">
<td><p><strong>CPACK_RPM_PACKAGE_DESCRIPTION</strong></p></td>
<td><p>The RPM package description</p></td>
<td><p>The content of CPACK_PACKAGE_DESCRIPTION_FILE if set or &quot;no package description available&quot; if not set</p></td>
</tr>
<tr class="even">
<td><p><em>CPACK_RPM_PACKAGE_REQUIRES</em></p></td>
<td><p>May be used to set RPM dependencies. see <a href="http://www.rpm.org/max-rpm/s1-rpm-depend-manual-dependencies.html">RPM dependencies specification</a>) for precise syntax. Note that you must enclose the complete requires string between quotes, for example:<br />
set(CPACK_RPM_PACKAGE_REQUIRES &quot;python &gt;= 2.5.0, cmake &gt;= 2.8&quot;)</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><em>CPACK_RPM_PACKAGE_PROVIDES</em></p></td>
<td><p>May be used to set the virtual packages provided by the RPM. It is somewhat complimentary to CPACK_RPM_PACKAGE_REQUIRES, but note that you do not need to list things like libraries, etc. here, since rpmbuild will figure that out by itself when generating the RPM. Most packages leave this blank. NOTE: This variable was added in cmake 2.8.1 (see <a href="http://public.kitware.com/Bug/view.php?id=9584">Bug9584</a>).</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><em>CPACK_RPM_SPEC_INSTALL_POST</em></p></td>
<td><p>May be used to set an RPM post-install command inside the spec file. For example setting it to &quot;/bin/true&quot; may be used to prevent rpmbuild to strip binaries (see <a href="http://public.kitware.com/Bug/view.php?id=7435">Bug7435</a>). This is <strong>not to be confused</strong> with .spec %post section (the action specified here is being invoked at <em>rpmbuild</em> time, not <em>post-install-package</em> time at user side)</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><em>CPACK_RPM_SPEC_MORE_DEFINE</em></p></td>
<td><p>May be used to add any %define lines to the generated spec file.</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><em>CPACK_RPM_USER_BINARY_SPECFILE</em></p></td>
<td><p>May be used to specify a user provided spec file instead of generating one. This is an feature which currently needs a patch see <a href="http://public.kitware.com/Bug/view.php?id=9679">Bug9679</a></p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><em>CPACK_RPM_GENERATE_USER_BINARY_SPECFILE_TEMPLATE</em></p></td>
<td><p>May be used to generate a <strong>template</strong> for a user provided spec file. This is an feature which currently needs a patch see <a href="http://public.kitware.com/Bug/view.php?id=9679">Bug9679</a></p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><em>CPACK_RPM_<POST/PRE>_<UN>INSTALL_SCRIPT_FILE</em></p></td>
<td><p>The content of the specified files will be embedded in the RPM spec file in the appropriate sections. This is an feature which currently needs a patch see <a href="http://public.kitware.com/Bug/view.php?id=8988">Bug8988</a></p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><em>CPACK_RPM_PACKAGE_DEBUG</em></p></td>
<td><p>May be set when invoking cpack in order to trace debug informations during CPack RPM run. For example you may launch CPack like this <em>cpack -D CPACK_RPM_PACKAGE_DEBUG=1 -G RPM</em></p></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>
