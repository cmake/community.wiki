# Introduction

CPack is a powerful, easy to use, cross-platform software packaging
tool distributed with [CMake](http://www.cmake.org) since version 2.4.2.
It uses the [generators](doc/cpack/PackageGenerators "wikilink")
concept from CMake, to abstract package generation on specific
platforms, and it can be used with or without CMake.

Using either a simple configuration file or the CMake module, a complex
project can be packaged into an installer.

# Using CPack without CMake

CPack can be used directly by specifying a CPackConfig.cmake file, which
uses CMake syntax and defines several variables. Here is an example
CPackConfig.cmake file for a Linux system:

```cmake
set(CPACK_CMAKE_GENERATOR "Unix Makefiles")
set(CPACK_GENERATOR "STGZ;TGZ;TZ")
set(CPACK_INSTALL_CMAKE_PROJECTS "/home/andy/vtk/CMake-bin;CMake;ALL;/")
set(CPACK_NSIS_DISPLAY_NAME "CMake 2.5")
set(CPACK_OUTPUT_CONFIG_FILE "/home/andy/vtk/CMake-bin/CPackConfig.cmake")
set(CPACK_PACKAGE_DESCRIPTION_FILE "/home/andy/vtk/CMake/Copyright.txt")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "CMake is a build tool")
set(CPACK_PACKAGE_EXECUTABLES "ccmake;CMake")
set(CPACK_PACKAGE_FILE_NAME "cmake-2.5.0-Linux-i686")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "CMake 2.5")
set(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "CMake 2.5.0")
set(CPACK_PACKAGE_NAME "CMake")
set(CPACK_PACKAGE_VENDOR "Kitware")
set(CPACK_PACKAGE_VERSION "2.5.0")
set(CPACK_PACKAGE_VERSION_MAJOR "2")
set(CPACK_PACKAGE_VERSION_MINOR "5")
set(CPACK_PACKAGE_VERSION_PATCH "0")
set(CPACK_RESOURCE_FILE_LICENSE "/home/andy/vtk/CMake/Copyright.txt")
set(CPACK_RESOURCE_FILE_README "/home/andy/vtk/CMake/Templates/CPack.GenericDescription.txt")
set(CPACK_RESOURCE_FILE_WELCOME "/home/andy/vtk/CMake/Templates/CPack.GenericWelcome.txt")
set(CPACK_SOURCE_GENERATOR "TGZ;TZ")
set(CPACK_SOURCE_OUTPUT_CONFIG_FILE "/home/andy/vtk/CMake-bin/CPackSourceConfig.cmake")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "cmake-2.5.0")
set(CPACK_SOURCE_STRIP_FILES "")
set(CPACK_STRIP_FILES "bin/ccmake;bin/cmake;bin/cpack;bin/ctest")
set(CPACK_SYSTEM_NAME "Linux-i686")
set(CPACK_TOPLEVEL_TAG "Linux-i686")
```
These variables can also be overwritten on the command line using the
option `-D`:

```sh
cpack -D CPACK_PACKAGE_VENDOR=Me -D CPACK_SYSTEM_NAME=super-duper-linux ...
```

# Using CPack with CMake

CMake comes with a CPack module, which will automatically generate an
appropriate CPack configuration file. To use the module, simply invoke
the following command (BTW, failure to do so will result in an annoying
"CPack Error: CPack project name not specified" message...):
```cmake
include(CPack)
```

This generates a new target called *"package"* in your build system.
When this target is built, CPack will be invoked to generate all of the
packages. Internally, CPack will use [CMake's install
mechanism](doc/cmake/Install-Commands "wikilink") to automatically populate
the package.

An example output of the *"package"* target (from a Linux Makefile) is:

    Run CPack packaging tool...
    CPack: Create package using STGZ
    CPack: Install projects
    CPack: - Run preinstall target for: CMake
    CPack: - Install project: CMake
    CPack: - Strip files
    CPack: Compress package
    CPack: Finalize package
    CPack: Package /home/andy/CMake-bin/cmake-2.5.0-Linux-i686.sh generated.
    CPack: Create package using TGZ
    CPack: Install projects
    CPack: - Run preinstall target for: CMake
    CPack: - Install project: CMake
    CPack: - Strip files
    CPack: Compress package
    CPack: Finalize package
    CPack: Package /home/andy/CMake-bin/cmake-2.5.0-Linux-i686.tar.gz generated.
    CPack: Create package using TZ
    CPack: Install projects
    CPack: - Run preinstall target for: CMake
    CPack: - Install project: CMake
    CPack: - Strip files
    CPack: Compress package
    CPack: Finalize package
    CPack: Package /home/andy/CMake-bin/cmake-2.5.0-Linux-i686.tar.Z generated.

## Using CMake variables to configure CPack

To configure CPack, it is possible to define [CPack
variables](doc/cpack/Configuration "wikilink") inside a CMake file.
These variables will be copied across to the generated CPackConfig.cmake
file before CPack is invoked.

This is an example CMake list section for CPack configuration:
```cmake
include(InstallRequiredSystemLibraries)

set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "My funky project")
set(CPACK_PACKAGE_VENDOR "Me, myself, and I")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/ReadMe.txt")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/Copyright.txt")
set(CPACK_PACKAGE_VERSION_MAJOR "1")
set(CPACK_PACKAGE_VERSION_MINOR "3")
set(CPACK_PACKAGE_VERSION_PATCH "2")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "CMake ${CMake_VERSION_MAJOR}.${CMake_VERSION_MINOR}")
if(WIN32 AND NOT UNIX)
  # There is a bug in NSI that does not handle full UNIX paths properly.
  # Make sure there is at least one set of four backlashes.
  set(CPACK_PACKAGE_ICON "${CMake_SOURCE_DIR}/Utilities/Release\\\\InstallIcon.bmp")
  set(CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\MyExecutable.exe")
  set(CPACK_NSIS_DISPLAY_NAME "${CPACK_PACKAGE_INSTALL_DIRECTORY} My Famous Project")
  set(CPACK_NSIS_HELP_LINK "http:\\\\\\\\www.my-project-home-page.org")
  set(CPACK_NSIS_URL_INFO_ABOUT "http:\\\\\\\\www.my-personal-home-page.com")
  set(CPACK_NSIS_CONTACT "me@my-personal-home-page.com")
  set(CPACK_NSIS_MODIFY_PATH ON)
else()
  set(CPACK_STRIP_FILES "bin/MyExecutable")
  set(CPACK_SOURCE_STRIP_FILES "")
endif()
set(CPACK_PACKAGE_EXECUTABLES "MyExecutable" "My Executable")
include(CPack)
```

## CPack Generators

There are several [generators](doc/cpack/PackageGenerators "wikilink")
usable with CPack.

# Example

Here is an [example](doc/cpack/examples/linux/DEB "wikilink").

# Debugging

A very detailed log of CPack execution steps can be obtained via `cpack
--debug --verbose` (which, as a side note, are features that are awfully
hidden at least in older versions of CPack).

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:Packaging_With_CPack) in another wiki.
