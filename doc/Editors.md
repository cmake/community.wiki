## CMake Editor Modes

There are CMake syntax highlighting and indentation supports for many
editors:

  - **[Eclipse](http://www.eclipse.org/cdt/)** There are two plugins for
    Eclipse:
      - The [CMakeEd](https://github.com/15knots/cmakeed) plugin for
        Eclipse provides syntax coloring and content assist for editing
        CMakeLists.txt and any file ending in a .cmake extension. It
        also integrates the CMake command reference documentation into
        the Eclipse Help system. This plugin does NOT do project
        management for you or generate CMake files for you. You are
        still responsible for this part. CMakeEd just makes writing the
        CMakeLists.txt files easier. Unfortunately, the built-in
        documentation has not been updated for several years and is
        badly out of date.
      - The
        [cmake4eclipse](https://marketplace.eclipse.org/content/cmake4eclipse)
        plugin generates the build scripts when you build the project
        from eclipse -- no need to manually invoke cmake with the CDT
        generator. It does not provide project management, your
        `CMakeLists.txt` files control the build. The plugin comes with
        a Language Settings Provider that feeds (non standard) include
        paths and pre-processor symbols defined in your CMake scripts to
        the CDT-Indexer.
      - The [CMakeBuilder](http://www.cmakebuilder.com/) plugin provides
        a user friendly interface to easily manage CMake-based projects,
        with the following features: advanced parser, Advanced CMake
        outline, CMakeBuilder perspective, symbol table and environment
        inspector, CMake files editor with syntax highlighting, code
        assist, wizard-oriented project management, Project Nature
        CMakeBuilder for CDT projects, and incremental project
        builders.
        As of 2017, this **seems to be
        [dead](https://marketplace.eclipse.org/content/cmakebuilder)**:
        No downloads page, apparently no downloads in the past year.
      - More plugins can be found in the **[Eclipse
        Marketplace](https://marketplace.eclipse.org/search/site/cmake)**.
  - Eclipse C/C++ Development Tooling (CDT) 9.2.0 claims to provide an
    experimental [CMake
    feature](https://projects.eclipse.org/projects/tools.cdt/releases/9.2.0).
      - **[Here](doc/editors/Eclipse-CDT4-Generator "wikilink")** you find
        documentation how to use the Eclipse CDT project generator of
        CMake.
      - **[Here](doc/editors/Eclipse-UNIX-Tutorial "wikilink")** you find
        documentation how to use Eclipse with the regular Makefile
        generator of CMake (for versions \< 2.6 of CMake).

<!-- end list -->

  - **[KDevelop 4](http://www.kdevelop.org)** supports CMake-based
    projects natively.

<!-- end list -->

  - **[CLion](https://www.jetbrains.com/clion/)**, though not free, uses CMake-based projects natively. There are some plugins for JetBrains IDEs (IntelliJ IDEA, Android Studio, CLion, etc.):
      - The [CMake simple highlighter](https://plugins.jetbrains.com/plugin/10089-cmake-simple-highlighter) plugin provides(extends) CMake syntax highlight for Scripting, Project and Utility Modules command names; user defined function and macros name's reference and definition; bracket argument and quoted arguments.
      - The [CMake Plus](https://plugins.jetbrains.com/plugin/12869-cmake-plus) plugin provides:
        - Highlighting/recognition of such command arguments as: variables, properties, targets, modules, command's named parameters (aka keyword arguments), generator expressions, path or url structures, policies.
        - Variables, Properties and Targets navigation (Go to definition / Find usages) and code completion.
        - Generator expressions quick docs and completion.
        - Parameters info for Scripting, Project and Utility Modules commands (use `Ctrl/Cmd + P`).
        - Regex language injection: highlighting, inspections, hints and suggestions provided by IDE built-in Regex language support.
        - Structure View (for the current file) with: Function/Macros declarations; Variables, Properties and Targets (re)definitions; Filtering and Sorting; Navigation (Structure View <-> Editor).
        - Extra documentation for CMake commands, modules, policies, properties, and variables.
        - Various Syntax and Operation inspections;

<!-- end list -->

  - **\[<http://qt-project.org/wiki/category:tools>::qtcreator
    QtCreator\]** supports CMake-based projects [natively since
    version 1.1](https://qt-project.org/doc/qtcreator-2.8/creator-project-cmake.html).

<!-- end list -->

  - **[NetBeans](http://www.netbeans.org)** supports CMake-based
    projects [natively since
    version 6.8](http://forums.netbeans.org/ntopic26390.html).

<!-- end list -->

  - **[Emacs](http://www.gnu.org/software/emacs)**: See the [CMake Emacs
    Support](doc/editors/Emacs "wikilink") page.

<!-- end list -->

  - **Enscript** [syntax highlighting
    rules](http://tristancarel.com/pub/patches/enscript/cmake.st). To
    enable it:
    1.  copy `cmake.st` in the `hl/` directory.
    2.  add the following in the `namerules` section of the
        `hl/enscript.st` file:

<!-- end list -->

    /CMakeLists\.txt/               cmake;
    /\.cmake.*$/                    cmake;
    /\.ctest.*$/                    cmake;

  - **[Epsilon](http://www.lugaru.com)** has a CMake
    [extension](http://lugaru.com/ftp/for-v13/cmake.e) that supports
    syntax highlighting, indentation, and auto-completion of expressions
    for control statements such as if-else-endif, foreach-endforeach,
    and while-endwhile.

<!-- end list -->

  - **[Geany](http://www.geany.org)** added CMake support in
    [version 0.16](http://www.geany.org/Main/20090215)

<!-- end list -->

  - **[Kate](http://kate.kde.org)**, **KWrite**,
    **[KDevelop](http://www.kdevelop.org)** and all other
    [KDE](http://www.kde.org) applications, which use the kate
    text-editing component support cmake syntax highlighting since KDE
    3.4.

<!-- end list -->

  - **NEdit** [syntax highlighting
    support](http://www.cmake.org/Wiki/images/c/c6/NEditCMakeHighlighting-0001.tar.gz)
    was added by [Philippe
    Poilbarbe](http://public.kitware.com/pipermail/cmake/2007-May/014267.html)

<!-- end list -->

  - **[Notepad++](http://notepad-plus.sourceforge.net/uk/site.htm)**
    added CMake support in version 4.1

<!-- end list -->

  - **[SciTE](http://scintilla.sourceforge.net/SciTEDownload.html)**
    version 1.73 has CMake support. To enable the feature edit
    SciTEGlobal.Properties and remove the comment before the CMake
    lines.

<!-- end list -->

  - **[Sublime Text](http://www.sublimetext.com/)** Sublime Text is a
    sophisticated text editor for code, markup and prose. You'll love
    the slick user interface, extraordinary features and amazing
    performance. CMake synatx support is provided through the [Package
    Control](https://sublime.wbond.net/) and the [CMake
    Package](https://sublime.wbond.net/packages/CMake).

<!-- end list -->

  - **[TextMate](http://www.macromates.com)** is a wonderful text editor
    for OS X. [CMake
    Bundle](http://www.bluequartz.net/binaries/CMake.tmbundle.zip). This
    plugin adds syntax highlighting for CMake files and rudimentary
    completion for command, properties and cmake variables.

<!-- end list -->

  - **UltraEdit** syntax highlighting [word
    file.](http://www.cmake.org/Wiki/images/5/56/UltraEditWordfile.tar.gz)

<!-- end list -->

  - **VIM** [syntax
    highlighting](http://cmake.org/gitweb?p=cmake.git;a=blob_plain;hb=master;f=Docs/cmake-syntax.vim)
    and [indentation
    mode](http://cmake.org/gitweb?p=cmake.git;a=blob_plain;hb=master;f=Docs/cmake-indent.vim).
    To enable indentation, copy indentation file to your .vim/indent
    directory, syntax highlighting file to your .vim/syntax directory
    and add the following to your
    .vimrc:

<!-- end list -->

    :autocmd BufRead,BufNewFile *.cmake,CMakeLists.txt,*.cmake.in runtime! indent/cmake.vim
    :autocmd BufRead,BufNewFile *.cmake,CMakeLists.txt,*.cmake.in setf cmake
    :autocmd BufRead,BufNewFile *.ctest,*.ctest.in setf cmake

  - **Visual Studio 2010 Professional** and above have two extensions
    available for editing CMake files.
    [Vissemee](http://code.google.com/p/vissemee) provides syntax
    highlighting for CMake. [CMake Tools for Visual
    Studio](http://cmaketools.codeplex.com) provides both syntax
    highlighting and IntelliSense for CMake.

<!-- end list -->

  - **Visual Studio 2017** and above [natively
    support](https://blogs.msdn.microsoft.com/vcblog/2016/10/05/cmake-support-in-visual-studio/)
    CMake projects.

## Creating New Editor Mode

The best way to start is to check the logic in existing ones. Make sure
to enable indentation for files that match the following file names:

  - CMakeLists.txt
  - \*.cmake
  - \*.cmake.in
  - \*.ctest
  - \*.ctest.in

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_Editors_Support) in another wiki.