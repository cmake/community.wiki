# CMake with Eclipse

There are currently 3 options for the users wanting to use CMake and
Eclipse for CMake enabled projects:

**Option 1:** Use the CMake Eclipse CDT4 generator (since CMake 2.6.0)
which generates projects Eclipse CDT4 or newer , which you can easily
import in Eclipse. This solution works fairly well and is recommended.
How to do this is documented extensively at this page:
[Eclipse_CDT4_Generator](doc/editors/Eclipse-CDT4-Generator "wikilink").

**Option 2:** Use the "Unix Makefile Generator" for your project and
then create the corresponding Eclipse project *by hand*. This solution
works well for *any* CMake version and you may find detailed and
accurate instructions below in this page.

**Option 3:** Use the
[cmake4eclipse](https://marketplace.eclipse.org/content/cmake4eclipse)
Eclipse plugin, which automatically generates build-scripts for the
Eclipse CDT managed build system from CMake scripts. This plugin also
works on other OSses than UNIX.

Every solution has **pros** and **cons**.

# Some words before we begin

## Target Audience

CMake users with a basic understanding of what CMake can and can NOT do
for your project. Users should have a basic idea of what Eclipse is and
how to install it. These instructions are written with OS X /Linux in
mind but should be applicable to Windows Operating Systems.

## Obtaining Eclipse + CDT

[Eclipse IDE](http://www.eclipse.org) is an IDE written in Java that was
initially used for developing java programs. Eclipse is also a "Rich
Client Platform" on which almost any type of application can be built.
One such project is the "C/C++ Development Tools" or CDT. The home page
is [here](http://www.eclipse.org/downloads/). Look for the C/C++
download link for your platform.

You can download a version of Eclipse that does NOT include all the
other Java Development tools and only includes the C/C++ tools
[here](http://www.eclipse.org/downloads/). Just select the "Eclipse IDE
for C/C++ Developers" link on that page.

There is also an Eclipse/CDT distribution for Windows, now called
Wascana, that specifically includes MinGW/MSys/JRE/CDT/WxWidget. The
home page is [here](http://wascana.sourceforge.net/).

The CMakeEd Plugin for Eclipse will open CMake files with syntax
coloring options, command and variable completion and full cmake
documentation integrated into the Eclipse Help system. CMakeEd is found
on the internet [here](http://cmakeed.sourceforge.net). There is also a
wiki entry for it. [CMakeEd](doc/Editors "wikilink").

## Eclipse + CDT Version Information

  - CDT 3.1.2 goes with Eclipse 3.2
  - CDT 4.0 goes with Eclipse 3.3. (Europa)
  - CDT 5.x goes with Eclipse 3.4 (Ganymeade)
  - CDT 6.x goes with Eclipse 3.5 (Galileo)
  - CDT 7.x goes with Eclipse 3.6 (Helios)

I highly recommend the latest CDT version as the feature set is vastly
improved, most notably the indexer which has become super fast. The
syntax highlighting improvements are also very nice. There are now at
least 3 C/C++ code formatting options available (or edit/create your own
style rules). Numerous other improvements are also incorporated into the
release. Eclipse 3.6 does require Java 1.5 (5.0) jvm.

# The Options

## Option 1: CDT4 Generator

This option is recommended. It requires CMake 2.6.0.

Follow the directions located on this page:
[Eclipse_CDT4_Generator](doc/editors/Eclipse-CDT4-Generator "wikilink").

## Option 2: UNIX Makefile Generator

### File System Setup

Eclipse CDT supports projects that already provide a makefile so this is
good news for CMake users.

<B>The basic steps to get Eclipse + CDT + CMake working are:</b>

1.  Use CMake to generate makefiles for your project
2.  Create a new project in Eclipse using the top level directory of
    your project source code as the "Project Directory".
3.  Create an "External Tool" to run cmake from within Eclipse
    (Optional)

<b>Note Regarding Out-Of-Source Builds:</b> Eclipse CDT will work better
if the the build directory is located within the source directory. Doing
so makes it easier to configure Eclipse to Debug and Run your program.
It <i>is</i> possible to store your build directory completely outside
of the source directory however setting up the ability to debug and run
your targets will be slightly more complicated. Regardless of how you
set things up, it is currently <b>not</b> possible to use the same
source directory with multiple Eclipse projects. Eclipse stores a
.project and .cproject file in the root of each project (your source
tree in this case) and keeps track of active projects. It will not allow
you to reuse a source directory twice (as it views them as project
directories).

A typical project setup workflow would be something like this.

<b>Initial Setup of your Project</b> In a terminal program, "cd" into
the top level of your project (source) directory ideally where your
CMakeLists.txt file resides. Create a directory called "Build". cd into
Build and then run 'ccmake' or 'cmake' using the parent directory as the
target for cmake. Let us use the 'Example' directory that is a part of
the CMake source distribution as the source folder for this tutorial.

    [mjackson@Thor:]$ cd CMake-2.4.6/Example
    [mjackson@Thor:]$ mkdir Build
    [mjackson@Thor:]$ cd Build
    [mjackson@Thor:]$ cmake ../

![Basic Project Setup](/uploads/0a495cc156fe3bc3c5327102b39df6f7/Eclipse_Tutorial_1.png)

**Image showing the basic Project setup for our tutorial.**

At this point we have bootstrapped the process somewhat so Eclipse
throws less errors when we setup the Eclipse Projects.

### Eclipse Project Setup

**Launch Eclipse and go to "New-\>C++ Project" Menu**

![New Project Menu](/uploads/f5716243f8d5670ff84eaff110b921b9/Eclipse_NewProjectMenu.png)

**Select the MakeFile Project type, choose Other Toolchain and click
Next**

![Project Wizard Makefile Selection](/uploads/f1d7b3f1a95517047d176a90f37efc35/Repl_eclipse_wizard_1.png)

**Select the *Advanced Settings* Button**

![Project Wizard Setting Project Type](/uploads/d9b9ab8e015c4faa47ebe77df53b8392/Eclipse_wizard_2.png)

**Uncheck the *Use Default Build Command**

'''Insert the following in the *Build Command* Text Field:

    make -C ${ProjDirPath}/Build VERBOSE=1

(or whatever path gets you to your build directory)

<b>In the picture below, the name of the project is 'MXATools'</b>

![Project Wizard Advanced Options](/uploads/978d05ed1faa851f1b514619bba75c32/Eclipse_wizard_3a.png)

<b>Open the "C/C++ Build" node, and select "Settings".

In the Right Side pane select the binary format for your system.

Click on the OK Button. Your project is now ready.</b>

![Project Wizard Setting binary Parser](/uploads/9948f1f53c00ad5cdeb7e07da3a7e0a7/Eclipse_wizard_4.png)

Our Finished Project showing off some of the C++ semantic highlighting capabilities:
![Project Wizard](/uploads/e48b2670b56793492d047dd3b3ce4655/Eclipse_project_finished.png)

Eclipse showing the CMakeEditor Plugin:
![CMakeEditor Screen Shot](/uploads/c05b80a3ebbf396caf11abb75d27fdfc/Eclipse_Cmake_Editor.png)

### Creating an External Tool to run cmake (Optional)

In the course of normal development it is common to edit your
CMakeLists.txt files. The makefile should pick up this difference and
rerun cmake again. Most of the time this is perfectly sufficient and
works just fine. Other times however this is NOT sufficient so having a
convenient way to run CMake from within Eclipse is handy.

Times when this step is needed:

  - You do <b>major</b> edits to the CMakeLists.txt files and you want
    to be sure that cmake is picking everything up.
  - You would like to start from a <i>clean</i> build state with nothing
    in the build directory
  - You are developing your CMakeLists.txt file and you just want to see
    the outcome of running cmake on your project but do <b>NOT</b> need
    to actually build your project. For example you are configuring a
    file and just want to see the newly created/configured file

#### Creating an External Tool

  - To do this select the "Run-\>External Tools-\>Show External Tools
    Dialog..." menu.
  - Create a new "Program" and call it Cmake.
  - In the "Location" text field, type the absolute path to cmake
    (/usr/local/bin/cmake).
  - In the "Working Directory" test field insert the following:
    `"${workspace_loc}/[NAME OF YOUR PROJECT DIRECTORY/Build"` and in
    the Arguments section insert the following: `"../"`.
  - In the "Refresh" tab select <i>The project containing the selected
    resources</i>.
  - In the "Common" tab check the "External Tools" selection.
  - This will put a shortcut in the "Run" menu for you.
  - Click the Apply Button and then run.
  - CMake will now run on your project directory.

### Creating A Make Target To Run CMake (Optional)

Creating an external tool is one solution for running CMake from within
Eclipse. However, a <i>Make Target</i> is even better for a couple of
reasons:

1.  <i>Make Target</i>s are stored in the Eclipse .project file. Check
    this file into your version control system, and your <i>Make
    Target</i> will work in every one of your working copies, on every
    computer.
2.  It is a lot less complicated

You can create a <i>Make Target</i> in just a few easy steps:

1.  Find the Make Targets window (or open it using the menu command
    Window-\>Show View-\>Make Targets). It should appear on the right,
    with the Outline window.
2.  Select the folder that you want to use as the working directory in
    which to start the CMake command. Usually the project folder is what
    you want. Right click that folder and select <i>Add Make Target</i>
3.  Choose whatever name you want, like <i>Run CMake</i>
4.  Leave the *Make Target* field empty
5.  Set the build command to `cmake -E chdir Build/ cmake -G "Unix
    Makefiles" ../`

That's it. The <i>Make Target</i> feature of Eclipse CDT is very useful
because it can be used to execute any command, not just build commands.
Take advantage of the CMake -E option to make your <i>Make Target</i>s
portable across platforms.

### Parsing Errors more efficiently

The CDT Error Parser cannot handle error messages that span more than
one line, which is the default gcc behavior. In order to force gcc to
generate single line error messages with no line wrapping, add to your
CMakeLists.txt:

    IF(CMAKE_COMPILER_IS_GNUCC)
      SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fmessage-length=0")
    ENDIF(CMAKE_COMPILER_IS_GNUCC)
    IF(CMAKE_COMPILER_IS_GNUCXX)
      SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fmessage-length=0")
    ENDIF(CMAKE_COMPILER_IS_GNUCXX)

### Automatic Discovery of Include directories (Optional, but handy)

Eclipse relies on default include paths such as `/usr/include` to find
and index header files. One can manually enter more include paths from
the "Project Properties" dialog but there is a much easier way for
Eclipse to pick up on any 'non-default' include paths needed by your
project.

Eclipse has a mechanism in place to 'Discover' these locations. The
mechanism works by parsing the compiler invocation looking for "-I"
arguments then adds these directories to the list of include paths to
index. In order for this discovery process to work you need to build
your project at least once with the CMAKE_VERBOSE_MAKEFILE to true
either by setting the variable in your CMakeLists.txt file:

    SET(CMAKE_VERBOSE_MAKEFILE ON)

OR by setting the "make" command to the
    following:

    make -C ${workspace_loc}/[NAME OF YOUR PROJECT DIRECTORY/Build VERBOSE=1

### Multiple Make Jobs for Multi-Core Hardware

If you have a multi-core hardware system (Intel Core Duo or the like)
you can use the following to invoke more than one compile job:

    make -C ${workspace_loc}/[NAME OF YOUR PROJECT DIRECTORY/Build -j2

Where '2' is the number of jobs to have make run.

### True Out of Source Builds

You can <i>still</i> do out of source builds if you want. The make
command would be the following if you have created your build directory
at the same level as your project directory and called it
"MyProjectBuild"

    make -C ${workspace_loc}/[NAME OF YOUR PROJECT DIRECTORY/../MyProjectBuild

While this will work, what you will be missing is the list of binaries
in your project view which makes setup of Debugging and Running your
executable easier. All this can <i>still</i> be setup through custom run
and debug commands in eclipse. I will leave that as an exercise to the
reader.

## Option 3: cmake4eclipse build-script generation

Make sure your project is configured to use the <i>External builder</i>
(on the <i>C/C++Buld | Builder Settings</i> tab) and select the <i>CMake
Builder (portable)</i> on the <i>C/C++Buld | Tool Chain Editor </i> tab.
For more instructions inside the Eclipse workbench, go to Help:Help
Contents, then open the 'CMake for CDT User Guide' node.

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:Eclipse_UNIX_Tutorial) in another wiki.
