**[GNU Emacs](http://www.gnu.org/software/emacs)** is the king of text
editors, it has been around since the mid-1970:s and it still actively
being developed. It is available on multiple platforms, including
Microsoft Windows, [Mac OS X](http://www.emacsformacosx.com), and Linux.
It runs either as a modern windows-based application or you can use it
in a terminal to edit on a remote machine. It can be extended using it's
built-in programming language Emacs Lisp and there are literarily
thousands of extensions available.

See the page [CMake Editors Support](doc/Editors "wikilink")
for CMake support in other editors.

# Modes

## cmake-mode

See the [combined syntax highlighting and indentation
mode](http://cmake.org/gitweb?p=cmake.git;a=blob_plain;hb=master;f=Auxiliary/cmake-mode.el)
in the CMake source tree. To enable it, add the following to your
*.emacs* file:

    ; Add cmake listfile names to the mode list.
    (setq auto-mode-alist
          (append
           '(("CMakeLists\\.txt\\'" . cmake-mode))
           '(("\\.cmake\\'" . cmake-mode))
           auto-mode-alist))

    (autoload 'cmake-mode "~/CMake/Auxiliary/cmake-mode.el" t)

## cmake-font-lock

The package
[cmake-font-lock](https://github.com/Lindydancer/cmake-font-lock),
written by Anders Lindgren, provides advanced font-lock support for
CMake scripts.

Although the standard package have some syntax coloring support, this
package raises the bar quite a bit.

The following is colored:

  - Function arguments are colored according to it's use, An argument
    can be colored as a *keyword*, a *variable*, a *property*, or a
    *target*. This package provides information on all built-in CMake
    functions. Information on user-defined functions can be added.
  - All function names are colored, however, special functions like
    `if`, `while`, `function`, and `include` are colored using a
    different color.
  - The constants *true*, *false*, *yes*, *no*, *y*, *n*, *on*, and
    *off*.
  - The constructs `${...}`, `$ENV{...}`, and `$<name:...>`.
  - In preprocessor definitions like `-DNAME`, `NAME` is colored.
  - Comments and quoted strings.

Example:

![CMakeFontLockScreenshot](/uploads/02533a0a852429c65d44ce90d6a2ed89/CMakeFontLockScreenshot.png)

## company-mode (code completion in Emacs)

See [company-mode](http://company-mode.github.io/) , company-cmake.el is
incorporated into company-mode since v0.6.12

## Jump focus between tags (if/else/endif, foreach/endforeach, ...)

See [evil-matchit](https://github.com/redguardtoo/evil-matchit), install
evil and evil-matchit, then just press %

## syntax check and other handy utilities

Install [cpputils-cmake](https://github.com/redguardtoo/cpputils-cmake),
then you can:

  - open C++ header file at correct location,
  - open gdb and append the full path of executable automatically
  - set up include directories automatically for flymake, flycheck
  - set up include directories for code completion (auto-complete,
    company-mode)

## alternative emacs package for syntax checking, autocompletion and more

Install [cmake-ide](https://github.com/atilaneves/cmake-ide).

# User Suggestions

## Buffer Names

I've been long irritated with having to deal with multiple buffers all
name CMakeLists.txt. Emacs by default will call them CMakeLists.txt,
CMakeLists.txt\<2\>, CMakeLists.txt\<3\>, etc.. This is really hard to
switch back and forth when the buffer names are difficult to associate
with location.

I've found a couple of solutions to this problem.

1.  Use uniquify emacs package. This gives several options to
    automatically rename buffers based on their location on
        disk.
        ;; uniquify.el is a helper routine to help give buffer names a better unique name.
        (when (load "uniquify" 'NOERROR)
          (require 'uniquify)
          (setq uniquify-buffer-name-style 'forward)
          ;(setq uniquify-buffer-name-style 'post-forward)
          )
2.  Rename the buffer as part of the cmake-mode
        (defun cmake-rename-buffer ()
          "Renames a CMakeLists.txt buffer to cmake-<directory name>."
          (interactive)
          ;(print (concat "buffer-filename = " (buffer-file-name)))
          ;(print (concat "buffer-name     = " (buffer-name)))
          (when (and (buffer-file-name) (string-match "CMakeLists.txt" (buffer-name)))
              ;(setq file-name (file-name-nondirectory (buffer-file-name)))
              (setq parent-dir (file-name-nondirectory (directory-file-name (file-name-directory (buffer-file-name)))))
              ;(print (concat "parent-dir = " parent-dir))
              (setq new-buffer-name (concat "cmake-" parent-dir))
              ;(print (concat "new-buffer-name= " new-buffer-name))
              (rename-buffer new-buffer-name t)
              )
          )

        (add-hook 'cmake-mode-hook (function cmake-rename-buffer))

I actually prefer renaming my buffers with my cmake-rename-buffer
function, because the buffer names start with a lower case letter. :)

## Better handling of _ for movement commands

The following will make `M-right` and `M-left` stop after each word of a
multiword identifier like `add_custom_target`. Unfortunately, this
breaks the font-lock support of the standard CMake package. However, it
is compatible with the cmake-font-lock package.

    (defun my-cmake-fix-underscrore ()
      (modify-syntax-entry ?_  "_" cmake-mode-syntax-table))
    (add-hook 'cmake-mode-hook 'my-cmake-fix-underscrore)

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake/Editors/Emacs) in another wiki.
