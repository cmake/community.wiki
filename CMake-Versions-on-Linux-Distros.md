One of the main problems facing new CMake developers is that the CMake version available on many common Linux distros tends to be years behind the current release.  Due to the rapid pace of feature development, it's important to be aware of which features were added when, and to decide early on a minimum CMake version to require.  This page will help you make that choice by documenting which CMake version is available on which Linux distros.

All distros have a version of CMake available by default in their package manager, this is the "Standard Package".  Additionally, some distros have first- or third-party alternate packages available that add support for newer versions of CMake, these are the "Alternate Package" columns.  Lastly, any distro with snap package support can use the [CMake snap package](https://snapcraft.io/cmake), which is kept up to date.

Note that for distros that release lots of versions, only a subset are listed here.  Also, this list doesn't include rolling release distros like Arch and Gentoo, as these are assumed to have the latest version.

| Distro | Distro Release Date | Standard Package | Alternate Package | Snap Support |
|--------|---------------------|------------------|-------------------|--------------|
|**Red Hat/CentOS**|
| Red Hat 6 | Nov 2010 | 2.8.12 | 3.6.1 (`epel/cmake3`) | ✗
| Red Hat 7 | Jun 2014 | 2.8.12 | 3.14.6 (`epel/cmake3`) | ✓
| Red Hat 8 | May 2019 | 3.11.4 | | ✓
|**Fedora**|
| Fedora 28 | May 2018 | 3.14.4 | | ✓
| Fedora 30 | May 2019 | 3.14.5 | | ✓
| Fedora 32 | April 2020 | 3.17.4 | | ✓
| Fedora 33 | October 2020 | 3.19.7 | | ✓
| Fedora 34 | April 2021 | 3.20.5 | | ✓
|**openSUSE**|
| openSUSE 13.2 | Nov 2014 | 3.0.2 | 3.8.1 ([KDE PPA](https://software.opensuse.org/download/package?package=cmake&project=KDE%3AQt55)) | ✗
| openSUSE 42.3 | Jul 2017 | 3.5.2 | 3.14.1 ([DLR PPA](https://software.opensuse.org/download/package?package=cmake&project=science%3Adlr)) | ✗
| openSUSE 15.1 | May 2019 | 3.10.2 | | ✓
|**Ubuntu**|
| Ubuntu 12.04 Precise | Apr 2012 | 2.8.7 | 3.2.2 ([PPA](https://launchpad.net/~george-edison55/+archive/ubuntu/cmake-3.x)) | ✗
| Ubuntu 14.04 Trusty | Apr 2014 | 2.8.12 | 3.14.1 ([DLR PPA](https://software.opensuse.org/download/package?package=cmake&project=science%3Adlr)) | ✓
| Ubuntu 16.04 Xenial | Apr 2016 | 3.5.1 | 3.24.1 ([Kitware PPA](https://apt.kitware.com/)) | ✓
| Ubuntu 18.04 Bionic | Apr 2018 | 3.10.2 | 3.24.1 ([Kitware PPA](https://apt.kitware.com/)) | ✓
| Ubuntu 20.04 Focal | Apr 2020 | 3.16.3 | 3.24.1 ([Kitware PPA](https://apt.kitware.com/)) | ✓
| Ubuntu 22.04 Jammy | Apr 2022 | 3.22.1 | | ✓
|**Debian**|
| Debian 6 Squeeze | Feb 2011 | 2.8.2 | | ✗
| Debian 7 Wheezy | May 2013 | 2.8.9 | | ✗
| Debian 8 Jessie | Apr 2015 | 3.0.2 | | ✗
| Debian 9 Stretch | Jun 2017 | 3.7.2 | | ✓
| Debian 10 Buster | Jul 2019 | 3.13.4 | | ✓
| Debian 11 Bullseye | Aug 2021 | 3.18.4 | | ✓