Welcome to the CMake community wiki!  Anyone is welcome to
[sign in](https://gitlab.kitware.com/users/sign_in), request access
to this wiki using the button on
[this page](https://gitlab.kitware.com/cmake/community),
and contribute new pages or updates to existing pages.
Please use the following guidelines for editing.

# Creating New Pages

* Please find a suitable place under the `contrib/` or `doc/` hierarchy.
  New subdirectories may be created below these, but please do not create
  new top-level directories or documents without first
  [filing an issue](https://gitlab.kitware.com/cmake/cmake/issues)
  to propose it.

* Please use lower-case names for directories and hyphenated Camel-Case
  names for documents, e.g. `doc/mydir/My-Document`.

* Please select the `Markdown` format.

# Editing Existing Pages

* Please do not rename or move existing pages.

* If a page has a table of contents, please maintain it when adding,
  removing, or renaming sections.
