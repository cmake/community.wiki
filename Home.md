# CMake Community Wiki

Welcome to the CMake community wiki!  Anyone is welcome to
[sign in](https://gitlab.kitware.com/users/sign_in), request access
to this wiki using the button on
[this page](https://gitlab.kitware.com/cmake/community),
and contribute new pages or updates to existing pages.
Please follow the [Editing Guidelines](Editing-Guidelines "wikilink").

*Please note that not all pages are maintained.  Material on some pages may be
deprecated or considered bad practice.  Other pages may cover long-outdated
material but are kept for historical reference.  Updates are welcome on all
pages, even if just to identify their status.*

See the official [CMake Documentation](https://cmake.org/documentation/)
for authoritative reference material maintained by CMake developers and
contribuors.

This wiki contains pages on all CMake-related tools including:

* [CMake](#cmake)
* [CTest](#ctest)
* [CPack](#cpack)

# CMake

## Primary Resources - Look here first\!

  - Where can I [download CMake](https://cmake.org/HTML/Download.html)?
  - [CMake Documentation](https://cmake.org/documentation/)
  - [CMake
    Variables](https://cmake.org/cmake/help/latest/manual/cmake-variables.7.html)
  - [Structure of a CMake Build
    System](https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html)
  - [FAQ (Frequently asked questions)](FAQ "wikilink")
  - [CMake Discourse Forum](https://discourse.cmake.org)
  - [Release
    Notes](https://cmake.org/cmake/help/latest/release/index.html)

## Reference Material

  - [CMake
    Variables](https://cmake.org/cmake/help/latest/manual/cmake-variables.7.html)
  - [List of CMake
    Properties](https://cmake.org/cmake/help/latest/manual/cmake-properties.7.html?highlight=properties)

#### The CMake Language

  - [A quick introduction to CMake
    syntax](https://cmake.org/cmake/help/latest/manual/cmake-language.7.html#syntax)
  - [On variables, lists, strings, maps, regexps,
    etc.](doc/cmake/VariablesListsStrings "wikilink")
  - [Language syntax](doc/cmake/Language-Syntax "wikilink")

## Guides

#### General

  - [Cross compiling](doc/cmake/CrossCompiling "wikilink")
  - [How to write platform checks with
    CMake](doc/tutorials/How-To-Write-Platform-Checks "wikilink")
  - [How to find libraries](doc/tutorials/How-To-Find-Libraries "wikilink")
  - [How to install things](doc/cmake/Install-Commands "wikilink")

#### Specific

  - [RPATH handling](doc/cmake/RPATH-handling "wikilink")
  - [Editors/IDEs with CMake syntax
    support](doc/Editors "wikilink")
  - [How to export symbols from a Windows DLL for the non-Windows
    Developer](doc/tutorials/BuildingWinDLL "wikilink")
  - [Appending the SO version to
    DLLs](doc/cmake/recipe/AddSoVersionToDLLs "wikilink")
  - [Advanced Usage of CMake Build Rules](doc/cmake/Build-Rules "wikilink")
  - Choosing a CMake Version
    - [CMake Versions and Life Cycles](doc/cmake/Life-Cycle-Considerations "wikilink")
    - [List of CMake Versions on Linux Distros](CMake-Versions-on-Linux-Distros)

## Tutorials

### Basic Introductions

  - [A Simple CMake Example](https://cmake.org/HTML/Examples.html)
  - [Cross-Platform Software Development Using
    CMake](http://www.linuxjournal.com/article/6700)
  - [CMake: The Cross Platform Build
    System](http://clubjuggler.livejournal.com/138364.html)
  - ["Learning CMake"](http://www.elpauer.org/stuff/learning_cmake.pdf)
    - Slides of a CMake workshop, including CPack, CTest and CDash
  - [CMake tutorial](https://github.com/TheErk/CMake-tutorial) - Slides
    (with LaTeX bearmer source) of a CMake tutorial including CPack,
    CTest.
  - ["CMake: Behind the Scenes of Code
    Development"](http://www.visgraf.impa.br/seminar/slides/rodlima_cmake_presentation.pdf)
    - Slides of an introductory talk/tutorial about CMake and its
    benefits
  - [The Hacker Within: Build
    Systems](http://hackerwithin.org/thw/plugin_wiki/page/buildsystems)
    Explains why and how to use build systems with a CMake example.

<!-- end list -->

  - How CMake simplifies the build process by Bruno Abinader
      - [Part 1 - Basic build
        system](https://brunoabinader.github.io/2009/12/07/how-cmake-simplifies-the-build-process-part-1-basic-build-system/)
      - [Part 2 - Advanced build
        system](https://brunoabinader.github.io/2009/12/09/how-cmake-simplifies-the-build-process-part-2-advanced-build-system/)
  - [Empirical approach to
    CMAKE](http://rachid.koucha.free.fr/tech_corner/cmake_manual.html)
    by Rachid Koucha
  - [Minimal examples](doc/cmake/MinimalExamples "wikilink") (wiki page)

### Finding stuff and platform checking

  - [How to package your project for use by
    others](doc/Tutorials#cmake-packages "wikilink"), create
    FooConfig.cmake files, and exporting and importing targets.

<!-- end list -->

  - [How to find and use existing frameworks on OS
    X](doc/cmake/platform_dependent_issues/HowToUseExistingOSXFrameworks "wikilink")
    A quick example to help OS X users find frameworks automatically.

### How to use CMake with specific Libraries

  - [How to build Qt4 software with
    CMake](doc/tutorials/How-To-Build-Qt4-Software "wikilink")

<!-- end list -->

  - [Qt with CMake](http://qtnode.net/wiki?title=Qt_with_cmake)
    Explains how to use CMake to build software with Qt4, Qt3 and KDE3.

<!-- end list -->

  - [Deploying Qt4 applications with
    CMake](http://mikemcquaid.com/2012/01/deploying-qt-applications-with-deployqt4/)
    Explains how to use the DeployQt4.cmake module coming with CMake
    2.8.7.

<!-- end list -->

  - [How to build KDE4 software with
    CMake](doc/tutorials/How-To-Build-KDE4-Software "wikilink")

<!-- end list -->

  - [How to use CMake to create Matlab MEX
    files](doc/tutorials/MatlabMex "wikilink")
    Describes how to use CMake when developing Matlab Executable (MEX)
    files for use with The Mathworks Matlab scripting language.

<!-- end list -->

  - [How to use CMake for building software with
    wxWidgets](http://www.wxwidgets.org/wiki/index.php/CMake)

<!-- end list -->

  - [Building eCos applications with
    CMake](http://www.linuxdevices.com/articles/AT6762290643.html)

<!-- end list -->

  - [Building Sms applications with CMake](http://www.smslana.eu)

<!-- end list -->

  - [Cross compiling from Windows to ARM
    Linux](http://blog.quickforge.co.uk/2011/10/exploration-of-cross-compiling-on-windows-for-arm-linux-distributions/)

<!-- end list -->

  - [Using CMake to build an FLTK application](doc/tutorials/ForFLTK "wikilink")

### Recipes

  - [How to process lots of input files with a processor built by
    CMake](doc/tutorials/How-To-Process-Lots-Of-Input-Files "wikilink")

<!-- end list -->

  - [Configuration Specific Settings for Visual Studio Generated Project
    Files](doc/cmake/recipe/VSConfigSpecificSettings "wikilink")

<!-- end list -->

  - [How to use the 'BundleUtilities' to deploy your OS X Application.
    Example uses Qt 4.](doc/cpack/BundleUtilities "wikilink")

<!-- end list -->

  - [How to write a simple CMakeLists.txt for Fortran
    code](doc/cmake/languages/fortran/ForFortranExample "wikilink")

<!-- end list -->

  - [How to emulate GNU Autotools 'make
    check'](doc/tutorials/EmulateMakeCheck "wikilink")

<!-- end list -->

  - [A toy model for add_custom_command and
    add_custom_target](doc/tutorials/CustomCommandCustomTargetInstall "wikilink")

<!-- end list -->

  - [Working with OS X Interface Builder
    Files](doc/tutorials/OSX-InterfaceBuilderFiles "wikilink")

<!-- end list -->

  - [Append the Version Number to the Install
    path](doc/cmake/recipe/AppendVersionNumberToInstallpath "wikilink")

<!-- end list -->

  - [Install to a local folder in the build dir for
    testing](doc/cmake/recipe/InstallToALocalFolderForTesting "wikilink")

<!-- end list -->

  - [Adding an uninstall target to your
    project](doc/cmake/recipe/AddUninstallTarget "wikilink")


## For CMake Development
  - [Assembler Support](doc/cmake/languages/Assembler "wikilink")
  - [Docs for Specific Project
    Generators](doc/cmake/Generator-Specific-Information "wikilink")
    (Eclipse, KDevelop3, CodeBlocks, Makefile)
  - [Contributed macros](Contrib "wikilink")
  - [Platform Dependent
    Information](doc/cmake/Platform-Dependent-Issues "wikilink")
  - [Documentation for previous
    releases](doc/cmake/version_compatibility_matrix/Released-Versions "wikilink")
  - [Matrix for checking backwards-compatibility of current
    features](doc/cmake/Version-Compatibility-Matrix "wikilink")
  - [CMake builtin documentation
    handling](doc/cmake/builtin-documentation-handling "wikilink")
  - [The architecture of Open Source Applications -
    CMake](http://www.aosabook.org/en/cmake.html)

## Converters from other buildsystems to CMake

All converters listed here are not "complete", i.e. the generated CMake
files are not 100% finished, in all cases some work is left for the
developer.

#### automake/autotools/autoconf

  - [am2cmake (requires
    Ruby)](https://projects.kde.org/projects/kde/kdesdk/kde-dev-scripts/repository/revisions/master/changes/cmake-utils/scripts/am2cmake)
    Converts automake/autotools/libtool based projects to CMake,
    specialized in converting from KDE 3 to KDE 4, should also work for
    others. This one has been used for converting the KDE buildsystem to
    CMake.

<!-- end list -->

  - [Alternative Automake2CMake (requires
    PHP)](http://emanuelgreisen.dk/stuff/kdevelop_am2cmake.php.tgz)
    Converts KDevelop projects that use automake to CMake.

<!-- end list -->

  - [Converting autoconf tests](doc/cmake/GccXmlAutoConfHints "wikilink")

#### qmake

  - [qmake2cmake (requires Python)](https://code.qt.io/cgit/qt/qmake2cmake.git/tree/src/qmake2cmake)
    Converts projects that use Qt's qmake. AKA `pro2cmake.py`
  - [qmake converter (requires Ruby)](contrib/scripts/ConvertFromQmake "wikilink")
    Converts projects that use Qt's qmake.

#### Visual Studio
  - [cmake-converter (requires Python) Github
    project](https://github.com/pavelliavonau/cmakeconverter) This project aims to 
    facilitate the conversion of Visual Studio solution to CMake 
    projects. The goal is to give to a Python script, a *.sln file,
    and output a set of CMakeLists.txt that may be used for generating 
    visual studio solution backward as perfect as possible. Supports (.vcxproj/.vfproj).
    May be installed from [pypi](https://pypi.org/project/cmake-converter/).
  - [vcproj2cmake.rb (requires Ruby) SourceForge
    project](http://vcproj2cmake.sf.net) Creates **and maintains**
    CMakeLists.txt files by extracting info from Visual Studio project
    files (.vcproj/.vcxproj). Elaborate script for development
    side-by-side the updated original static .vc\[x\]proj files,
    supports script hooks and powerful definition mappings. Patches and
    new project members very welcome. Older script versions below:
      - [Original vcproj2cmake.rb version (requires
        Ruby)](http://www.eskilson.se/vcproj2cmake.rb)
      - Slightly newer version here
        [vcproj2cmake.rb](http://dgwarp.hd.free.fr/vcproj2cmake.rb).
  - [vcproj2cmake.ps1(PowerShell
    version)](http://nberserk.blogspot.com/2010/11/converting-vc-projectsvcproj-to.html)
    Creates CMakeLists.txt. it supports vcproj configuration and detect
    'exclude from build' option
  - [folders4cmake (requires
    Java)](http://sourceforge.net/projects/folders4cmake/) Use Visual
    Studio project files to generate corresponding "source_group"
    information that you can use inside your own CMake scripts. Supports
    Visual Studio 9/10 project files (full round-trip possible).

#### Basic CMakeLists.txt from-scratch-generator

  - [gencmake (requires
    Ruby)](http://websvn.kde.org/trunk/KDE/kdesdk/cmake/scripts/)
    Creates basic CMakeLists.txt files from looking at the existing
    files.
  - [CMakeListGenerator
    (Win32)](http://www.vanvelzensoftware.com/postnuke/index.php?name=Downloads&req=viewdownload&cid=7)
    Creates complete CMakeLists.txt files as described in the
    [README](https://gucef.svn.sourceforge.net/svnroot/gucef/trunk/tools/CMakeListGenerator/docs/README.txt)
    using a combination of file and directory structure analysis.
    Supports resolving dependencies between multiple archives.

## Success Stories

  - What are some [projects using CMake](doc/cmake/Projects "wikilink")?
  - [Articles about CMake](doc/cmake/Articles "wikilink")
  - [Really Cool CMake Features](doc/cmake/Really-Cool-CMake-Features "wikilink")

## More Topics

  - [Fortran Issues](doc/cmake/languages/fortran/Fortran-Issues "wikilink")
  - [Generating dependency graphs with
    CMake](doc/cmake/Graphviz "wikilink")
  - [Experiments With Lua](doc/cmake/dev/Experiments-With-Lua "wikilink")
  - [Performance Tips](doc/cmake/Performance-Tips "wikilink")
  - [GNU style directory layout with
    CMake](doc/tutorials/GNU-style-example "wikilink")
  - [CMake TODO](doc/cmake/dev/OpenTasks "wikilink")
  - [Creating Qt Assistant Docs](contrib/scripts/CreateQtAssistantDocs "wikilink")
  - [Writing FindXXX.cmake modules that work with static
    libraries](doc/cmake/Static-libraries "wikilink")
  - [Writing FindXXX.cmake modules that work when multiple versions of
    packages are
    installed](doc/cmake/Multiple-versions "wikilink")
  - [CMake:Improving_Find\*_Modules](doc/cmake/dev/Improving-Find*-Modules "wikilink")
  - [CMake/C_Plugins_for_Loadable_Commands](doc/cmake/C-Plugins-for-Loadable-Commands "wikilink")
    For anyone who wonders what the `load_command` command is for.
  - [PC-Lint](contrib/macros/PC-Lint "wikilink") support for CMake

# CTest

### Tutorials

  - [Testing With CTest](doc/ctest/Testing-With-CTest "wikilink")
    Introduces to testing with CTest, submitting dashboards, and using
    CMake to add tests to the test system.

<!-- end list -->

  - [CTest Scripting](doc/ctest/Scripting-Of-CTest "wikilink")
    Describes the scripting with CTest which can significantly simplify
    and automate testing and submitting dashboards.

<!-- end list -->

  - [Generating Input Files For
    CTest](doc/ctest/Generating-Testing-Files "wikilink")
    Describe more in details the concepts behind testing with CTest and
    also explans how to use CTest without using CMake.

<!-- end list -->

  - [Buildmanagement With CTest](doc/ctest/Buildserver "wikilink")
    Describes how to setup a central configuration for all CTest
    scripts.

### More Information

  - [Configuring CTest Submission
    Methods](doc/ctest/Submission-Issues "wikilink")
  - [CTest Nightly, Experimental, Continuous,
    ...](doc/ctest/Nightly,-Experimental,-Continuous "wikilink")
  - [CTest/Coverage](doc/ctest/Coverage "wikilink")
  - [CTest\_Running\_Modes.pdf](/uploads/b60e91d1b1723f590e6541392fbaf472/CTest_Running_Modes.pdf)
  - [CTest Frequently asked questions](doc/ctest/FAQ "wikilink")

### More Topics

  - [CTest TODO](doc/ctest/OpenTasks "wikilink")
  - [Run tests on machines without building
    first](doc/ctest/TestWithoutBuild "wikilink")

# CDash

  - [CDash Wiki](http://public.kitware.com/Wiki/CDash).
  - [CDash FAQ](http://public.kitware.com/Wiki/CDash:FAQ).

# CPack

### Tutorials

  - [Packaging with CPack](doc/cpack/Packaging-With-CPack "wikilink")
    Introduction to CPack, installing and packaging of software.
  - [CMake tutorial](https://github.com/TheErk/CMake-tutorial) - Slides
    from a CMake tutorial (including LaTeX beamer source) including
    CPack.
  - [CPack Variables](doc/cpack/Configuration "wikilink")
  - [Supported package formats](doc/cpack/PackageGenerators "wikilink")
  - [CPack Win32 Newbie
    Checklist](doc/cpack/Win32NewbiesChecklist "wikilink")
  - [Component Install With
    CPack](doc/cpack/Component-Install-With-CPack "wikilink")
  - [List of CPack Generators](doc/cpack/PackageGenerators "wikilink")

### Recipes

  - [Add an application shortcut to the Start
    Menu](doc/cmake/recipe/AddShortcutToStartMenu "wikilink")

# Old (deprecated), kept for reference only

  - [CMake 2.6 Notes](doc/cmake/notes/2.6 "wikilink")
  - [Useful CMake Variables](doc/cmake/Useful-Variables "wikilink")

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake) in another wiki.
