You are now on the CMake community wiki.
Many pages are now **DEPRECATED** and sometimes even considered bad
practice. Those are only kept as reference for previous CMake versions.
The documentation for the latest version is available here:
[CMake Documentation](https://cmake.org/documentation/).

-----

**User Contributed Macros**

## Find Modules

Although CMake comes with a whole bunch of FindXXX packages, there is a
large number of packages there are not covered. The hope is that these
user-contributed macros will eventually make their way into the official
distribution. Before contributing, please have your FindXXX package
conform to the [CMake Module
Guidelines](http://www.cmake.org/cgi-bin/viewcvs.cgi/Modules/readme.txt?root=CMake&view=markup).

1.  [Find Autopack](contrib/modules/FindAUTOPACK "wikilink")
2.  [Find ClanLib](contrib/modules/FindClanLib "wikilink")
3.  [Find DirectShow](contrib/modules/FindDirectShow "wikilink")
4.  [Find MySQL](contrib/modules/FindMySQL "wikilink")
5.  [Find ParMetis](contrib/modules/FindPARMETIS "wikilink")
6.  [Find VLD - Visual Leak Debugger](contrib/modules/FindVLD "wikilink")
7.  [Find WIX - Windows Installer XML,v2](contrib/modules/FindWix "wikilink")
8.  [Find LibSVM - A Library for Support Vector
    Machines](contrib/modules/FindLibSVM "wikilink")
9.  [Find MuParser - High performance math expression parser
    library](contrib/modules/FindMuParser "wikilink")
10. [Find Octave - Interpreted language for numerical
    computations](contrib/modules/FindOctave "wikilink")
11. [Use Bison - Parser Generator](contrib/modules/UseBison "wikilink")
12. [Use Flex - Lexer Generator](contrib/modules/UseFlex "wikilink")
13. [Use LaTeX - Build LaTeX Documents](https://gitlab.kitware.com/kmorel/UseLATEX)
14. [Use RPM Tools - Build RPM (source or
    binary)](doc/cpack/UseRPMTools "wikilink")
15. [Find CTPP2](contrib/modules/FindCTPP2 "wikilink") - [C++ Template
    Engine](http://ctpp.havoc.ru/en/)
16. [Find Qwt](contrib/modules/FindQwt "wikilink") - [Qt Widgets for Technical
    Applications](http://qwt.sourceforge.net/)
17. [Find QScintilla](contrib/modules/FindQScintilla "wikilink") - [port to Qt
    of Neil Hodgson's Scintilla C++ editor
    control](http://www.riverbankcomputing.com/software/qscintilla/)

[Home](Home "wikilink")
[Back](Contrib "wikilink")

## Comprehensive Collection of CMake functions/macros

For CMake 2.8.7 and higher. More complex build scripts might need a
larger toolset than is provided by CMake. Here is a tested collection of
over 500 CMake functions which you can easily download and use by just
including a single file in your CMakeLists.txt.

Repository: <https://github.com/toeb/oo-cmake> MIT License

Some features

  - interactive cmake shell
  - eval
  - maps (objects without functions)
      - serialization to and from json
  - function functions
      - return values
      - dynamic call
  - objects
  - list tools
      - slice, splice, filter, map, fold
  - string tools
      - slice, splice, ...
  - semver
      - parse, compare, constrain semantic versions
  - shell tools
  - filesystem
      - many bash like functions which work on windows and unix
        systems(ls, touch, fwrite, fread, cd, pwd, pushd, popd, rm, ...)
      - read_line from console
  - debugging
      - breakpoint
      - print scope
  - ...

An up to date full feature list and installation guide is located
[here](https://github.com/toeb/oo-cmake#feature-overview)

## Make Equivalents

While attempting to convert a gnu makefile to Cmake, there was a need to
provide some macros to provide equivalent functionality.

1.  [FILTER_OUT (filter-out)](contrib/macros/FilterOut "wikilink")
2.  [CREATE_LIBTOOL_FILE (create a libtool archive
    file)](contrib/macros/LibtoolFile "wikilink")

[Home](Home "wikilink")
[Back](Contrib "wikilink")

## Some helpers along the way

1.  [COPY_IF_DIFFERENT](contrib/macros/CopyIfDifferent "wikilink")
2.  [MERGE](contrib/macros/Merge "wikilink") --- Merges two sorted lists into
    a single sorted list. Useful to keep source and header files next to
    each other.
3.  [CREATE_FINAL_FILE](contrib/macros/CreateFinalFile "wikilink") --
    create a KDE style final file, which includes all source files, so
    that the compilation will be noticable faster.
4.  [List Operations](contrib/macros/ListOperations "wikilink") -- A
    compilation of some helpful list operations.
5.  [PARSE_ARGUMENTS](contrib/macros/ParseArguments "wikilink") -- A macro
    to help parse arguments for other macros.
6.  [ADD_CXXTEST](contrib/macros/AddCxxTest "wikilink") -- A macro to add
    tests written the CxxTest testing framework.
7.  [Force Flags](contrib/macros/ForceAddFlags "wikilink") -- A macro to
    force certain arguments for specified flag (similar to a set union).
8.  [Test Inline](contrib/macros/TestInline "wikilink") -- A test for how your
    compiler defines inline.
9.  [COMPARE_VERSION_STRINGS](contrib/macros/CompareVersionStrings "wikilink")
    -- A macro to help compare arbitrary version strings for less than,
    equal to, and greater than.

[Home](Home "wikilink")
[Back](Contrib "wikilink")

## Visual Studio generator helpers

When generating Visual Studio projects, the priorities can be sometimes
different than for other generators. These are some VS-specific (and
sometimes not-so-specific) macros that could help you generate better VS
projects.

1.  [GatherProjectFiles](contrib/macros/GatherProjectFiles "wikilink") --
    create and cache a list of project files by recursively globbing a
    directory structure
2.  [GenerateProject](contrib/macros/GenerateProject "wikilink") -- creates a
    VS project with subgroups that mimic the directory structure

[Home](Home "wikilink")
[Back](Contrib "wikilink")

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake_User_Contributed_Macros) in another wiki.
