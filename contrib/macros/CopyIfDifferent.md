Generates a rule to copy each source file from source directory to
destination directory.

Typical use:

    SET(SRC_FILES head1.h head2.h head3.h)
    COPY_IF_DIFFERENT( /from_dir /to_dir ${SRC_FILES} IncludeTargets "Includes")
    ADD_TARGET(CopyIncludes ALL DEPENDS ${IncludeTargets})

will generate rules that look

    CopyIncludes: $(CopyIncludes_DEPEND_LIBS) \
      Include\head1.h \
      Include\head2.h \
      Include\head3.h

    Include\head1.h:
        @echo Building Copying head1.h /to_dir head1.h...
        cmake.exe -E copy_if_different /from_dir/head1.h  /to_dir/head1.h

The macro code is:

    MACRO(COPY_IF_DIFFERENT FROM_DIR TO_DIR FILES TARGETS TAGS)
    # Macro to implement copy_if_different for a list of files
    # Arguments -
    #   FROM_DIR - this is the source directory
    #   TO_DIR   - this is the destination directory
    #   FILES    - names of the files to copy
    #              TODO: add globing.
    #   TARGETS  - List of targets
    #   TAGS     - Since only the file name is used
    #              to generate rules, pre-pend a user
    #              supplied tag to prevent duplicate rule errors.
    SET(AddTargets "")
    FOREACH(SRC ${FILES})
        GET_FILENAME_COMPONENT(SRCFILE ${SRC} NAME)
        # Command to copy the files to ${STEP1}/src, if changed.
        SET(TARGET  "${TAGS}/${SRCFILE}")
        IF("${FROM_DIR}" STREQUAL "")
            SET(FROM ${SRC})
        ELSE("${FROM_DIR}" STREQUAL "")
            SET(FROM ${FROM_DIR}/${SRC})
        ENDIF("${FROM_DIR}" STREQUAL "")
        IF("${TO_DIR}" STREQUAL "")
            SET(TO ${SRCFILE})
        ELSE("${TO_DIR}" STREQUAL "")
            SET(TO   ${TO_DIR}/${SRCFILE})
        ENDIF("${TO_DIR}" STREQUAL "")
        ADD_CUSTOM_COMMAND(
            OUTPUT  ${TARGET}
            COMMAND ${CMAKE_COMMAND}
            ARGS    -E copy_if_different ${FROM} ${TO}
            COMMENT "Copying ${SRCFILE} ${TO_DIR}"
            )
        SET(AddTargets ${AddTargets} ${TARGET})
    ENDFOREACH(SRC ${FILES})
    SET(${TARGETS} ${AddTargets})
    ENDMACRO(COPY_IF_DIFFERENT FROM_DIR TO_DIR FILES TARGETS TAGS)


----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeCopyIfDifferent) in another wiki.
