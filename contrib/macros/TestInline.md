[Back](Contrib "wikilink")

This handy test is from Jack Kelly on the cmake email list.

Here is a minimal working example. It tests the inline keyword, then
__inline__ and then __inline. When it finds one that works, it
will ADD_DEFINITIONS(-Dinline=${KEYWORD}) and if none work, it will
ADD_DEFINITIONS(-Dinline=).

    # Inspired from /usr/share/autoconf/autoconf/c.m4
    FOREACH(KEYWORD "inline" "__inline__" "__inline")
       IF(NOT DEFINED C_INLINE)
         TRY_COMPILE(C_HAS_${KEYWORD} "${GPUIT_BINARY_DIR}"
           "${GPUIT_SOURCE_DIR}/test_inline.c"
           COMPILE_DEFINITIONS "-Dinline=${KEYWORD}")
         IF(C_HAS_${KEYWORD})
           SET(C_INLINE TRUE)
           ADD_DEFINITIONS("-Dinline=${KEYWORD}")
         ENDIF(C_HAS_${KEYWORD})
       ENDIF(NOT DEFINED C_INLINE)
    ENDFOREACH(KEYWORD)
    IF(NOT DEFINED C_INLINE)
       ADD_DEFINITIONS("-Dinline=")
    ENDIF(NOT DEFINED C_INLINE)

You also need this file in your source directory.

    /* Test source lifted from /usr/share/autoconf/autoconf/c.m4 */
    typedef int foo_t;
    static inline foo_t static_foo(){return 0;}
    foo_t foo(){return 0;}
    int main(int argc, char *argv[]){return 0;}

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeTestInline) in another wiki.
