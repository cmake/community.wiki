[Back](Contrib "wikilink")

This macro recursively collects the files from a project directory and
caches the list for future use. Tailor the extensions that are included
in the list according to your needs.

-----

    # add extra sources to the defined project
    MACRO ( GatherProjectFiles ProjectName ProjectDir ProjectSources )

      #look for the cache file
      SET ( FoundCacheFile "FoundCacheFile-NOTFOUND" )
      FIND_FILE ( FoundCacheFile ${ProjectName}Sources.cmake PATHS ${CMAKE_CURRENT_BINARY_DIR} NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH NO_CMAKE_PATH NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH)

      IF ( FoundCacheFile )
        # read cached file list
        MESSAGE ( STATUS "    Found cache file ${FoundCacheFile}" )
        INCLUDE ( ${CMAKE_CURRENT_BINARY_DIR}/${ProjectName}Sources.cmake )

      ELSE ( FoundCacheFile )
        # get the file lists
        SET ( Dir ${CMAKE_HOME_DIRECTORY}/${ProjectDir} )

        # add your other extensions here (this could be probably done nicer)
        FILE ( GLOB_RECURSE DirSources ${Dir}/*.c ${Dir}/*.cpp ${Dir}/*.h ${Dir}/*.inl ${Dir}/*.rc ${Dir}/*.ico )

        # write cached file list
        MESSAGE ( STATUS "    Generating ${ProjectName}Sources.cmake" )
        STRING ( CONFIGURE "@DirSources@" TempString @ONLY )
        STRING ( REPLACE ";" " " "DirSourcesAsString" "${TempString}" )
        CONFIGURE_FILE ( ${CMAKE_HOME_DIRECTORY}/CMake/SourceFilesTemplate.cmake.in ${CMAKE_CURRENT_BINARY_DIR}/${ProjectName}Sources.cmake @ONLY )

      ENDIF ( FoundCacheFile )
      SET ( "${ProjectSources}" ${DirSources} )
    ENDMACRO ( GatherProjectFiles )

-----

The template file (located in
*CMAKE_HOME_DIRECTORY\\CMake\\SourceFilesTemplate.cmake.in*) looks
like this:

    # Cache source files

    # All sources
    SET ( DirSources @DirSourcesAsString@ )

-----

A typical use for this macro would be:

    GatherProjectFiles ( MyLib src/MyLib MyLibSources )
    ADD_LIBRARY (MyLib SHARED ${MyLibSources} )

[Back](Contrib "wikilink")


----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeMacroGatherProjectFiles) in another wiki.
