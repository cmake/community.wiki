[Back](Contrib "wikilink")

This macro takes a list of files as input and generates source groups
that follow the relative path of the files from the ProjectDir folder.
This is strictly a cosmetic change, but it's nice if you're using the
CMake generated projects also for development

-----

    # create hierarchical source groups
    MACRO ( GenerateProject ProjectDir ProjectSources )

      SET ( DirSources "${ProjectSources}" )
      FOREACH ( Source ${DirSources} )
        STRING ( REGEX REPLACE "${ProjectDir}" "" RelativePath "${Source}" )
        STRING ( REGEX REPLACE "[\\\\/][^\\\\/]*$" "" RelativePath "${RelativePath}" )
        STRING ( REGEX REPLACE "^[\\\\/]" "" RelativePath "${RelativePath}" )
        STRING ( REGEX REPLACE "/" "\\\\\\\\" RelativePath "${RelativePath}" )
        SOURCE_GROUP ( "${RelativePath}" FILES ${Source} )
      ENDFOREACH ( Source )
    ENDMACRO ( GenerateProject)

-----

A typical use for this macro would be:

    GenerateProject ( src/MyLib ${MyLibSources} )
    ADD_LIBRARY (MyLib SHARED ${MyLibSources} )

[Back](Contrib "wikilink")

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeMacroGenerateProject) in another wiki.
