**CAUTION: The contents of this page may be obsolete**
![120px-Old_finnish_stop_sign.svg](/uploads/521180f925eeefd545a3a7a761185c2e/120px-Old_finnish_stop_sign.svg.png)

----

[Back](Contrib "wikilink")

**NOTE:** You can merge and sort lists using the SET() and LIST()
command and no longer need this macro.

    set(FIRST_LIST  a b c d i j k l)
    set(SECOND_LIST e f g h m n o p)
    set(COMPLETE_LIST ${FIRST_LIST} ${SECOND_LIST})
    list(SORT COMPLETE_LIST)
    # you can also remove duplicates
    list(REMOVE_DUPLICATES COMPLETE_LIST)

-----

    # This macro merges elements in sorted lists ALIST and BLIST and stored the result in OUTPUT
    MACRO(MERGE ALIST BLIST OUTPUT)
    SET(BTEMP ${BLIST})
    FOREACH(A ${ALIST})
        SET(SORTED)
        SET(UNINSERTED 1)
        FOREACH(B ${BTEMP})
            IF(${UNINSERTED})
                IF(${A} STRLESS ${B})
                    SET(SORTED ${SORTED} ${A})
                    SET(UNINSERTED 0)
                ENDIF(${A} STRLESS ${B})
            ENDIF(${UNINSERTED})
            SET(SORTED ${SORTED} ${B})
        ENDFOREACH(B ${BLIST})
        IF(${UNINSERTED})
            SET(SORTED ${SORTED} ${A})
        ENDIF(${UNINSERTED})
        SET(BTEMP ${SORTED})
    ENDFOREACH(A ${ALIST})
    SET(${OUTPUT} ${BTEMP})
    ENDMACRO(MERGE ALIST BLIST OUTPUT)

    # Here is an example that merges *.cpp files and *.h files into a single sorted list
    # This would be easier if FILE(GLOB...) properly matches "*.{cpp,h}"
    FILE(GLOB ALGEBRAIC_SRCS Implicit/Algebraic/*.cpp)
    FILE(GLOB ALGEBRAIC_H Implicit/Algebraic/*.h)
    MERGE("${ALGEBRAIC_H}" "${ALGEBRAIC_SRCS}" ALGEBRAIC_SRCS)

[Back](Contrib "wikilink")


----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeMacroMerge) in another wiki.
