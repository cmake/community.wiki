**CAUTION: The contents of this page may be obsolete**
![120px-Old_finnish_stop_sign.svg](/uploads/521180f925eeefd545a3a7a761185c2e/120px-Old_finnish_stop_sign.svg.png)

----

[Back](Contrib "wikilink")

**NOTE: This macro is *obsolete* by built-in support in CMake for
removing list elements with the LIST command:**

    list(REMOVE_ITEM <list> <value> [<value> ...])

    SET(MYLIST this that and the other)
    # remove "this" and "that"
    LIST(REMOVE_ITEM MYLIST this)
    LIST(REMOVE_ITEM MYLIST that)

-----

    MACRO(FILTER_OUT FILTERS INPUTS OUTPUT)
        # Mimicks Gnu Make's $(filter-out) which removes elements
        # from a list that match the pattern.
        # Arguments:
        #  FILTERS - list of patterns that need to be removed
        #  INPUTS  - list of inputs that will be worked on
        #  OUTPUT  - the filtered list to be returned
        #
        # Example:
        #  SET(MYLIST this that and the other)
        #  SET(FILTS this that)
        #
        #  FILTER_OUT("${FILTS}" "${MYLIST}" OUT)
        #  MESSAGE("OUTPUT = ${OUT}")
        #
        # The output -
        #   OUTPUT = and;the;other
        #
        SET(FOUT "")
        FOREACH(INP ${INPUTS})
            SET(FILTERED 0)
            FOREACH(FILT ${FILTERS})
                IF(${FILTERED} EQUAL 0)
                    IF("${FILT}" STREQUAL "${INP}")
                        SET(FILTERED 1)
                    ENDIF("${FILT}" STREQUAL "${INP}")
                ENDIF(${FILTERED} EQUAL 0)
            ENDFOREACH(FILT ${FILTERS})
            IF(${FILTERED} EQUAL 0)
                SET(FOUT ${FOUT} ${INP})
            ENDIF(${FILTERED} EQUAL 0)
        ENDFOREACH(INP ${INPUTS})
        SET(${OUTPUT} ${FOUT})
    ENDMACRO(FILTER_OUT FILTERS INPUTS OUTPUT)

-----

[Back](Contrib "wikilink")

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeMacroFilterOut) in another wiki.
