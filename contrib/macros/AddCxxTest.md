**CAUTION: The contents of this page may be obsolete**
![120px-Old_finnish_stop_sign.svg](/uploads/521180f925eeefd545a3a7a761185c2e/120px-Old_finnish_stop_sign.svg.png)

----

Please see
[FindCxxTest.cmake](https://cmake.org/cmake/help/latest/module/FindCxxTest.html),
included with CMake since version 2.8.0.

## About

This macro simplifies the inclusion of tests written using the [CxxTest
testing framework](http://cxxtest.sourceforge.net/).

Python is required to generate the test source files on the developer
machine. However, since the generated source files will be placed on the
source directory where the macro is called, there is no need for the end
user to have Python in order to build and run the tests.

## Definition

Some preamble code:

    # Make sure testing is enabled
    ENABLE_TESTING()

    # Use Python interpreter
    FIND_PACKAGE(PythonInterp)

You need to specify the path to the cxxtestgen.py script. Modify
accordingly:

    # Path to the cxxtestgen.py script
    SET(CXXTESTGEN ${CMAKE_SOURCE_DIR}/thirdparty/cxxtest/cxxtestgen.py)

The actual macro definition:

    MACRO(ADD_CXXTEST NAME)
      IF(PYTHONINTERP_FOUND)
        ADD_CUSTOM_COMMAND(
          OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.cpp
          COMMAND
            ${PYTHON_EXECUTABLE} ${CXXTESTGEN}
            --runner=ErrorPrinter
            -o ${NAME}.cpp ${ARGN}
          DEPENDS ${ARGN}
          WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        )
      ENDIF(PYTHONINTERP_FOUND)

      ADD_EXECUTABLE(${NAME} ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.cpp ${ARGN})

      ADD_TEST(${NAME} ${NAME})
    ENDMACRO(ADD_CXXTEST)

## Separate compilation

The above macro generates a single source file for all input test
headers. If by some reason you prefer separate compilation of each part,
you may use the variation:

    MACRO(ADD_CXXTEST_SEP NAME)
      IF(PYTHONINTERP_FOUND)
        # generate the parts
        FOREACH(_PART ${ARGN})
          GET_FILENAME_COMPONENT(_NAME ${_PART} NAME)
          GET_FILENAME_COMPONENT(_NAME_WE ${_PART} NAME_WE)
          ADD_CUSTOM_COMMAND(
            OUTPUT ${_NAME_WE}.cpp
            COMMAND
              ${PYTHON_EXECUTABLE} ${CXXTESTGEN}
              --part
              -o ${_NAME_WE}.cpp
              ${_NAME}
            DEPENDS ${_PART}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
          )
        ENDFOREACH(_PART)

        # generate the runner
        ADD_CUSTOM_COMMAND(
          OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}_runner.cpp
          COMMAND
            ${PYTHON_EXECUTABLE} ${CXXTESTGEN}
            --runner=ErrorPrinter --root
            -o ${NAME}_runner.cpp
          WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        )
      ENDIF(PYTHONINTERP_FOUND)

      # enumerate all generated files
      SET(PARTS ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}_runner.cpp)
      FOREACH(_PART ${ARGN})
        GET_FILENAME_COMPONENT(_PART_WE ${_PART} NAME_WE)
        SET(PARTS ${PARTS} ${_PART_WE}.cpp)
      ENDFOREACH(_PART)

      ADD_EXECUTABLE(${NAME} ${PARTS})

      ADD_TEST(${NAME} ${NAME})
    ENDMACRO(ADD_CXXTEST_SEP)


----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeMacroAddCxxTest) in another wiki.
