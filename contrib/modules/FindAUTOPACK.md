[Back](Contrib "wikilink")

-----

    # Find the AUTOPACK includes and library
    #
    # Autopack is a message-passing library which transparently packs small messages into
    # fewer larger ones for more efficient transport by MPI. It can be found at:
    #       http://www-unix.mcs.anl.gov/autopack/
    #
    # AUTOPACK_INCLUDE_DIR - where to find autopack.h
    # AUTOPACK_LIBRARIES   - List of fully qualified libraries to link against.
    # AUTOPACK_FOUND       - Do not attempt to use if "no" or undefined.

    FIND_PATH(AUTOPACK_INCLUDE_DIR autopack.h
      /usr/local/include
      /usr/include
    )

    FIND_LIBRARY(AUTOPACK_LIBRARY autopack
      /usr/local/lib
      /usr/lib
    )

    IF(AUTOPACK_INCLUDE_DIR)
      IF(AUTOPACK_LIBRARY)
        SET( AUTOPACK_LIBRARIES ${AUTOPACK_LIBRARY} )
        SET( AUTOPACK_FOUND "YES" )
      ENDIF(AUTOPACK_LIBRARY)
    ENDIF(AUTOPACK_INCLUDE_DIR)

-----

[Back](Contrib "wikilink")


----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeUserFindAUTOPACK) in another wiki.
