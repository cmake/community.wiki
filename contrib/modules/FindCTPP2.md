[Back](Contrib "wikilink")

-----

    # - Find CTPP2
    # Find the native CTPP2 headers, libraries, and compiler.
    #
    #  CTPP2_INCLUDE_DIRS      - where to find CTPP2.hpp, etc.
    #  CTPP2_LIBRARIES         - List of libraries when using ctpp2.
    #  CTPP2_FOUND             - True if ctpp2 found.
    #  CTPP2_CTPP2C_EXECUTABLE - template compiler

    # MACROS for optional use by CTPP2 users:
    #
    #    CTPP2_ADD_TEMPLATES(<installation_destination> <templates_to_compile...>)
    #       Compiles the text template (.ctpp2) into a binary form (.ct2)
    #       Parameters:
    #           _install_dest           The template installation destination
    #           templates_to_compile    The list of templates without the .ctpp2 extension
    #
    #       #==============
    #       Example Usage:
    #
    #       find_package(CTPP2 REQUIRED)
    #       include_directories(${CTPP2_INCLUDE_DIRS})
    #       set(_templates main.html main.json main.dbg)
    #       CTPP2_ADD_TEMPLATES(www/templates ${_templates})

    #=============================================================================
    # Copyright 2010 Kitware, Inc.
    # Copyright 2010 Mike Trinkala <trink@acm.org>
    #
    # Distributed under the OSI-approved BSD License (the "License");
    # see accompanying file Copyright.txt for details.
    #
    # This software is distributed WITHOUT ANY WARRANTY; without even the
    # implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    # See the License for more information.
    #=============================================================================
    # (To distributed this file outside of CMake, substitute the full
    #  License text for the above reference.)

    # Look for the header file.
    find_path(CTPP2_INCLUDE_DIR NAMES ctpp2/CTPP2.hpp)
    mark_as_advanced(CTPP2_INCLUDE_DIR)

    # Look for the library.
    find_library(CTPP2_LIBRARY NAMES ctpp2)
    mark_as_advanced(CTPP2_LIBRARY)

    # Look for the template compiler
    find_program(CTPP2_CTPP2C_EXECUTABLE NAMES ctpp2c)
    mark_as_advanced(CTPP2_CTPP2C_EXECUTABLE)

    # handle the QUIETLY and REQUIRED arguments and set CTPP2_FOUND to TRUE if
    # all listed variables are TRUE
    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(CTPP2 DEFAULT_MSG CTPP2_LIBRARY CTPP2_INCLUDE_DIR CTPP2_CTPP2C_EXECUTABLE)

    if(CTPP2_FOUND)
      set(CTPP2_LIBRARIES ${CTPP2_LIBRARY})
      set(CTPP2_INCLUDE_DIRS ${CTPP2_INCLUDE_DIR})
      #============================================================
      # CTPP2_ADD_TEMPLATES (public macro)
      #============================================================
      #
      macro(CTPP2_ADD_TEMPLATES _install_dest)
        foreach(_template ${ARGN})
        set(_input ${CMAKE_CURRENT_SOURCE_DIR}/${_template}.ctpp2)
        set(_output ${CMAKE_CURRENT_BINARY_DIR}/${_template}.ct2)
        add_custom_target(${_template}.ct2 ALL ${CTPP2_CTPP2C_EXECUTABLE} ${_input} ${_output})
        install(FILES ${_output} DESTINATION ${_install_dest})
        endforeach(_template)
      endmacro(CTPP2_ADD_TEMPLATES)
      #============================================================
    else(CTPP2_FOUND)
      set(CTPP2_LIBRARIES)
      set(CTPP2_INCLUDE_DIRS)
      set(CTPP2_CTPP2C_EXECUTABLE)
    endif(CTPP2_FOUND)

-----

[Back](Contrib "wikilink")


----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMakeUserFindCTPP2) in another wiki.
