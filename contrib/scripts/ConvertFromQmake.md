## Converting From Qmake

Just added to sourceforge:
<https://sourceforge.net/projects/qmake2cmake/> - this project does a
bit more than the ruby-script below.

Here is a Ruby script and a shell script which do the "grunt work" of
converting a large qmake-based project:

    #!/usr/bin/ruby -w

    # Get the file into a string
    file = IO.read(ARGV[0]);

    # Convert special qmake variables
    projectName = String.new;
    file.sub!(/TARGET = (.+)$/) {
        projectName = $1.dup;
        "PROJECT(#{projectName})"
    }
    templateType = String.new;  # We remove the project type and stick it at the end
    file.sub!(/TEMPLATE = (.+)$\n/) {
        templateType = $1.dup;
        ""
    }
    file.gsub!(/include\((.+)\)/,
               'INCLUDE(\1 OPTIONAL)');
    file.gsub!(/includeforce\((.+)\)/,
               'INCLUDE(\1)');
    file.gsub!(/INCLUDEPATH \*= (.+)((\n[ \t]+.+$)*)/,
               'SET(CMAKE_INCLUDE_PATH ${CMAKE_INCLUDE_PATH} \1\2)');
    file.gsub!(/SOURCES \*= (.+)((\n[ \t]+.+$)*)/,
               "SET(#{projectName}_sources $#{projectName}_sources" ' \1\2)');
    file.gsub!(/HEADERS \*= (.+)((\n[ \t]+.+$)*)/,
               "SET(#{projectName}_headers $#{projectName}_headers" ' \1\2)');
    file.gsub!(/DEFINES \*= (.+)((\n[ \t]+.+$)*)/,
               'SET(DEFINES ${DEFINES} \1\2)');

    # Now deal with other variables
    file.gsub!(/(.+)\s\*=\s(.+)/,
               'SET(\1 ${\1} \2)');
    file.gsub!(/(.+)\s=\s(.+)/,
               'SET(\1 \2)');
    file.gsub!(/\$\$\{(.+)\}/,
               '${\1}');
    file.gsub!(/\$\$\((.+)\)/,
               '$ENV{\1}');
    file.gsub!(/([A-Za-z_\-.]+)\.pri/,
               '\1.cmake');

    # Cleanup steps
    file.gsub!(/\\\)/, ')');

    # Put the project type back in
    file += "ADD_EXECUTABLE(#{projectName} #{projectName}_sources)" if templateType == "app";
    file += "ADD_LIBRARY(#{projectName} ${#{projectName}_sources})" if templateType == "lib";

    # Write the new file to CMakeLists.txt
    if ARGV.length > 1
        outname = ARGV[1];
    else
        if ARGV[0] =~ /.+\.pro$/
            outname = File.join(File.dirname(ARGV[0]), "CMakeLists.txt");
        elsif (ARGV[0] =~ /.+\.pri$/) || (ARGV[0] =~ /.+\.prf$/)
            outbase = File.basename(ARGV[0]);
            outbase.sub!(/\.pr./, ".cmake");
            outname = File.join(File.dirname(ARGV[0]), outbase)
        end
    end
    outfile = File.new(outname, "w");
    outfile.puts(file);
    outfile.close;

So you have a script to convert a single file. Now you need one to
recurse a directory tree:

    #!/bin/sh
    for file in `find . -name '*.pr?'`; do
       dos2unix $file
       filetype=`echo $file |sed -e 's/.*\.\(pr.\)/\1/'`
       path=`dirname $file`
       base=`basename $file`
       if [ -e "$path/CMakeLists.txt" -a "$filetype" = "pro" ]; then
          ./qmake2cmake.ruby $file $path/CMakeLists-$base.txt
       else
          ./qmake2cmake.ruby $file
       fi
    done

It's hardly complete, but it's a lot better than doing the whole thing
by hand\!

----
This page was initially populated by conversion from its [original location](https://public.kitware.com/Wiki/CMake:ConvertFromQmake) in another wiki.
